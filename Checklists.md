Checklists
==========

Onboarding process for new team member
--------------------------------------

- [ ] Vote on whether to extend an invitation in team meetings.
    - There are no formal criteria for the moment.
- [ ] Ask them to accept the [team agreement](https://gitlab.com/snowdrift/governance/-/blob/master/team-agreement.md)
    - Link to the [file at a specific commit](https://gitlab.com/snowdrift/governance/-/blob/79afa111903eda2674ec8ac40e920c3faff9c48a/team-agreement.md), so we can look back if we're ever not clear who agreed to which version.
- [ ] Have an onboarding meeting
    - [ ] Walk them through the private places they now have access to.
        - Private gitlab repos
        - Private forum categories
    - [ ] Answer any other questions they may have.
    - [ ] They explain Snowdrift.coop to us as if we'd never heard of it.
        - Why: It's extra confusing if a team member spreads misinformation about how the platform works. While we can't prevent misunderstandings, we can at least make sure that team members don't have any misconceptions they might inadvertently spread.
        - This is just an opportunity to correct any misunderstandings — the new member isn't being graded/assessed.
        - If there is more than one existing team member present at the onboarding, it works well to "tag team" — one existing team member role plays as if they've never heard of Snowdrift before, and the other team member and new member are explaining it to them — if the new member is having a hard time making a connection, they can "tag" the other person in.
    - [ ] Confirm the their email, GitLab username, community.snowdrift.coop handle, and (optionally) matrix account.
- [ ] Email OSUOSL support asking to create an `@snowdrift.coop` email alias
    - Add the new alias to our documentation. [TODO: Need link here]
- [ ] Give them developer permissions on snowdrift organization [here](https://gitlab.com/groups/snowdrift/-/group_members)
- [ ] Add them to the team group on the forum [here](https://community.snowdrift.coop/g/team)
    - [ ] Give them member moderator privileges on the forum, from their profile.
- [ ] Add them to the matrix community, +snowdrift:matrix.org (if they use matrix)
    - Right now only @iko can do this, since Matrix/element does not yet have the ability to add multiple admins or transfer ownership.
- [ ] Add them to `!folks` fpbot command if they're not already on it
    - fpbot command: `!set channels.#snowdrift.folks comma,separated,list,of,names,without,spaces`
    - Get the previous list with the equivalent: `!get channels.#snowdrift.folks`
- [ ] Ask them to [update their meeting availability](https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/1345/41) and [add their contact info](https://community.snowdrift.coop/t/team-member-contact-info/1083/5).
    - Ask them to subscribe to the shared calendar (linked from availability topic), if they use a digital calendar.
- [ ] If they would like, create a blog account for them. [link](https://blog.snowdrift.coop/ghost/#/team)
- [ ] If they would like, share the signup code so they can create a wiki account.
