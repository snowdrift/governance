Hi,

We're grateful for your continued support for Snowdrift.coop. You indicated your willingness to go ahead with the initial crowdmatching donation, but we got an error charging your credit card. Please re-register an up-to-date credit card at https://snowdrift.coop/payment-info and then reply to me to tell me when you've done it.

Thanks
