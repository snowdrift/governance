If you want to add or change a policy for a role you fill, go ahead (via git or
GitLab web UI); no discussion required.

Same goes for the Lead of a circle assigning people to roles in that circle.

Everything else (changing existing roles, domains, accountabilities, etc) should
go through a governance process where the appropriate role holders agree to
changes. That process may happen through asynchronous options (per our
[asynchronous governance policy]) like Discourse topic discussions, GitLab
issues, and/or merge-requests. As needed, we can also discuss in a real-time
governance meeting.

[asynchronous governance policy]: Main-Circle.md#asynchronous-governance
