Agreement for Snowdrift Team Members:

---

I agree to:

* Participate in weekly meetings as much as I can
    * When I know about absences in advance, I will notify others and relay pertinent information for the agenda
* Respond within 48 hours to snowdrift-related messages directed to me or pertaining to my role/s (as long as they come through whatever my preferred contact method is)
    * If other priorities come up that temporarily leave me less reliable, I will notify the team of the situation

I take full responsibility for all agreements I make. If I break an agreement, I will:

* Clean up disturbances caused
* Follow up as soon as possible with those affected
* Change the agreement instead of repeatedly breaking it