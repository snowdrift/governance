# Snowdrift.coop Website Circle

Developing the main site.

## Roles

### Architect

- Defining the Snowdrift technology stack
    - languages
    - libraries
    - frameworks
    - standards for use of technology etc
- Prioritizing coding maintenance chores in the backlog.

### Code QA

- Testing code
- Reviewing code
- Processing merge requests

### Developer

- Implementing frontend and backend features
- Maintaining lists of newbie-friendly development tasks

### Interaction Design

- Defining page existence, function, and content.
- Curating user stories.
- Validating designs through user research.
- Managing and publishing design plans and tickets.

### Licensing

- Ensuring our code isn't violating any license.
- Ensuring any use of our code by others follows the licensing.

### Visual Design

- Creating mockups
- Maintaining [visual style guidelines](https://gitlab.com/snowdrift/design/blob/master/docs/design-guide/readme.md)

## Policies

*See the [Policy Changelog](Website-Circle/Policy-Changelog.md) for the Website Circle.*

### Design process

1. Write User Stories
    * accountability: MSiep, interaction design
    * posted as issues in [design repo](https://gitlab.com/snowdrift/design/issues)
2. Produce mockups to fulfill US's
    * accountability: mray, interaction and visual design
    * posted to the design issues
3. Post approved mockups with accompanying US's as issues in
   [code repo](https://gitlab.com/snowdrift/snowdrift/issues)
    * accountability: MSiep, interaction design
    * tagged for implementation
4. Implementation work done by developers
    * accountability: developer(s)
    * assign/claim issues when starting
    * mark MR's as "WIP:" as needed
5. code MR's reviewed, accepted, deployed
    * accountabilities: MSiep, interaction design *and* chreekat, Code QA
    * update SPECS-STORIES.md with newly implemented US's.
