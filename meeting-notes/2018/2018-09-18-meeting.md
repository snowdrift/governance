 <!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->

# Meeting — September 18, 2018

Attendees: smichel17, wolftune, Salt

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (Bryan): Review/capture ops priorities [chreekat, in his own list]

## Better visibility for weekly meetings (esp. including inviting people who aren't core contributors/team)
- NEXT STEP (Stephen): post a discourse announcement about weekly meetings <https://git.snowdrift.coop/sd/outreach/issues/40>

## Governance overhaul
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (Bryan): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Volunteer recruitment
- NEXT STEP: Wait until ~end of September, to decide whether we want to open up option of moving off of haskell for our web framework.
- NEXT STEP (Aaron): Include this "call for frontend/backend split help" in discourse announce [added to draft: https://community.snowdrift.coop/t/please-join-us-on-the-snowdrift-community-forum/567]

## Project outreach, Kodi etc
- NEXT STEP (Michael): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## Diversity statement (and CoC etc), relation to ToS
- NEXT STEP (wolftune, in his list): Write a draft
```

## Metrics

Discourse (over past week):

- Signups: 1
- New Topics: 3
- Posts: 16

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## Homepage video
- Salt: I got a chance to look at the video, lgtm
- Salt: Anything needed, anyone talked to Robert?
- wolftune: Bryan/Robert/Michael talked, it's ready for coding https://git.snowdrift.coop/sd/snowdrift/issues/112
- Salt: This might be a good task to ask Iko about (private/personal email). I can do that.
- wolftune: That sounds fine, but it should be more "if you have time…" and not "hey, I have something for you to do". There are other people who can do this, too.
- Salt: Understood and I've been meaning to reach out to her anyway.
- NEXT STEP (wolftune): Send links to Salt (email, no gpg)
- NEXT STEP (Salt): Email Iko

## Discourse blockers?
- Salt: I've been out of the loop update?
- wolftune: We've been hashing through the ToS and everything. We're basically done, only oustanding question is what kind of entity projects will be
- wolftune: Also possible updates to CoC (are they blockers for wider announce?)
- Salt: I think it might be a good idea to focus on this consider the whole deal coming up around Linus
- wolftune: I think "pretty good" already defines our CoC right now, so not a hard blocker

## Kodi
- Salt: We're 1-2 months overdue. I want to contact them before it gets too cold. 
- wolftune: You should reach out. Michael bought a house and is moving, so that put a blip in his availability
- Salt: So, just nudge Michael?
- wolftune: That's part of it, but also don't wait for him
- Salt: Those are two very different answers…
- Salt: If we're not going to wait, we need someone else to be point on that, or we'll just have another "lunch chat" and it won't move anything forward
- wolftune: If you and he sit down together, you might be able to hash something out *enough* to make something happen
- Salt: ok.
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (wolftune): Email Salt reminding him ^

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->