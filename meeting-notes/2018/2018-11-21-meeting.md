<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2018/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — November 21, 2018

Attendees:Salt, wolftune, iko, msiep, mray, smichel

<!-- Check-in round -->

## Carry-overs from last meeting

```
## tracking ops tasks for just server maintenance
- NEXT STEP (chreekat): Review/capture ops priorities [chreekat, in his own list]

## Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP (chreekat): write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

## Tech volunteers and framework etc
- NEXT STEP (chreekat): Decide whether we're open to the option of moving off of haskell for our web framework

## Governance overhaul / Volunteer recruitment
- NEXT STEP (wolftune): posts on discourse about things he has personal notes on governance roles.
- NEXT STEP (wolftune): co-working to create a more complete list of roles/titles we want (capture wolftune's notes in better form)

## Project outreach, Kodi etc
- NEXT STEP (Salt): Set up an hour to co-work with Michael
- NEXT STEP (msiep): work on feedback form <https://git.snowdrift.coop/sd/outreach/issues/41>

## Blogging
- NEXT STEP (wolftune): Forum blog post

## Design work-flow (and issue grooming)
- NEXT STEP (msiep): Look through gitlab issues, do some grooming/organizing if you want.

## LFNW
- NEXT STEP (Salt, wolftune): Submit CFPs by January 31st https://git.snowdrift.coop/sd/outreach/issues/50
- NEXT STEP (smichel17): Start planning a roadmap, intermediate goals. Set up a meeting.

## Beta milestone prioritizing/scheduling
- NEXT STEP (smichel17): Clean up brainstorm and post to discourse to sollicit more
```

## Metrics

Discourse (over past week):

- Signups: 0
- New Topics: 5
- Posts: 89

Sociocracy:

- Page 108

<!-- New Agenda Down Here  -->

## General mray updates

- Salt: how's it going, mray?
- mray: busy with other things, hopefully it's going to change
- Salt: i think everyone moved some steps back, but I've seen a lot of movement lately, so I'm excited
- Salt: Any pressing snowdrift things you want to bring up? I wanted to give you some space, since it's been a while
- mray: I'd be interested in knowing what the current situation is, how things are developing in a technical way, so I have a better idea how to connect to whatever workflow is there
- smichel: Bryan has little availability for a while, it's just been people, e.g. h30, around to do stuff these days and we merge it
- a while back, Bryan visited me and we set up CI, so it's easier to deploy now. only Bryan and I currently have permissions on gitlab, but that's the main progress
- Salt: how far do you think that will be from that level of CI to dev.snowdrift.coop? not pushing for an answer, but would be really cool to get it
- smichel17: right now the CI runners are running on Bryan's servers. CI is the easy part, it's just the DNS/https to be set up
- smichel17: when we migrate to gitlab.com and use gitlab shared runners, we can't do that. but the plus side is, we won't have to use Bryan's servers
- smichel17: it'd help when Bryan has bandwidth, but not sure when he'll have time to set it up (he's moving to a Scandinavian country and found a job there, therefore low on time for some time)
- wolftune: any work that's not concrete can be managed on the forum. more governance on the forum. what I'd like to see more (as a governance thing) is maybe meetings separate from weekly meetings if needed
- smichel: I'd appreciate if you have little things you'd like to change, to put it all on a ticket and I can go through and check off each one. it'll be easier to work on the code rather than working on fpbot (for example) to learn haskell in order to work on code
- wolftune: we have some new people recently, e.g. the designer from France. we also have a feedback section on the forum for people to get direction from end users coming to the project, we should encourage that.

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->