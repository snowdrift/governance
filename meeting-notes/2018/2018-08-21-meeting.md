---
title: Meeting — August 21, 2018
...
<!-- Previous week's meeting notes: https://git.snowdrift.coop/sd/wiki/tree/master/resources/meetings/2018 -->

Attendees: msiep, smichel17, wolftune, chreekat, Salt

## Carry-overs from last meeting

### tracking ops tasks for just server maintenance
- NEXT STEP: Review/capture ops priorities [chreekat, in his own list]

### Better visibility for weekly meetings (esp. including inviting people who aren't core contributors/team)
- NEXT STEP: post a discourse announcement about weekly meetings <https://git.snowdrift.coop/sd/outreach/issues/40>

### Governance overhaul
- NEXT STEP: Wolftune posts on discourse about things he has personal notes on governance roles.

## Metrics

### Video
- wolftune: some changes I'd like to make, but what we have is good enough to put live (not a blocker). Just need to decide where to put it.

### Discourse
- wolftune: I'd like to look over the ToS, but otherwise ready to go

### Sociocracy

## New Agenda
<!-- Add agenda items below -->

### Bryan/Stephen meeting — CI/CD improvements
- NEXT STEP: write blog post about this process, document <https://git.snowdrift.coop/sd/snowdrift/issues/111>

### Trustroots (chreekat)
- chreekat likely to be working on Trustroots, potential 

### Site-map / roadmap / working process from design perspective
- wolftune: I figured it would be useful if there were a site map, where we could see the status of each page that's going to exist (done, needs update, going away, etc).
- result of discussion: We're really looking for a way to guide michael to useful things for him to work on.
- NEXT STEP: schedule some co-working time between michael & whoever is on top of the status of things

### Volunteer recruitment
- NEXT STEP: Include this "call for frontend/backend split help" in discourse announce [added to draft: https://community.snowdrift.coop/t/please-join-us-on-the-snowdrift-community-forum/567]
- NEXT STEP: Wait until ~end of September, to decide whether we want to switch off of haskell as our backend.

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->