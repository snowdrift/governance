<!-- Previous week's meeting notes: https://git.snowdrift.coop/sd/wiki/tree/master/resources/meetings/2018 -->

# Meeting — July 3, 2018

Attendees: msiep, wolftune, smichel17

<!-- Add agenda items below -->

## issue grooming
- discussed system for prioritizing issues
- NEXT STEP: Change priority labels from no/low/med/high to low/med/high/top [smichel17, DONE] https://git.snowdrift.coop/groups/sd/-/labels

## chreekat's priorities
- wolftune: chreekat has said he has some time, but seemed like he would like to be doing "actual" work rather than project management / issue grooming.
- wolftune: Anything you would like him to do / add?
- msiep/smichel: Not much except pointing him to issues that have gone through the design process.

## social.coop
- woltune: I went to a session at Open Source Bridge focusing on the nature of mastodon / issues around it.
- wolftune: Social.coop is a mastodon instance, relating to co-op things. There's a charge (pay what you can) to support the instance.
- wolftune: It's run democratically, they're finalizing a code of conduct; I've had some discussions around that.
- wolftune: It seems like the best fit for us. Should we make snowdrift@social.coop as our official mastodon account? Put in a couple $ a month?
- msiep: I'm on social.coop, haven't used it much but I've been planning to use it for a while. I like the idea of using them.
- smichel17: I think it makes sense, it's just a matter of priority vs finalizing discourse, etc.
- NEXT STEP: https://git.snowdrift.coop/sd/outreach/issues/31

----

## Carry-overs

<!-- Review status of carry-overs -->

<!-- Decide where to capture outstanding tasks (new & carryover) -->

```
## OSB

## Board priorities
- Tracked at: https://git.snowdrift.coop/sd/governance/issues/32

## looking forward: CLS, OSCON
- NEXT STEP: Make a list of things necessary for discourse to be ready for announce @oscon/cls [smichel17/wolftune, outreach repo] https://git.snowdrift.coop/groups/sd/-/milestones/discourse-announce?title=Discourse+Announce
- NEXT STEP: Make a discourse topic asking what wolftune's priorities at conferences should be? [wolftune] https://community.snowdrift.coop/t/what-should-i-prioritize-when-at-cls-and-oscon/871

## engaging on Discourse (outreach repo; Salt, but not today)
- NEXT STEP: write intros: https://git.snowdrift.coop/sd/outreach/issues/14
- NEXT STEP: post a discourse announcement about meetings status (including inviting people who aren't core contributors/team)… [wolftune/Salt]
- NEXT STEP: Salt opens Discourse topic on planning useful pinned topics (fpbot, etc)
- NEXT STEP: make a pinned post encouraging people to introduce themselves (some guidance about what sorts of things to mention)
- NEXT STEP: add pinned posts to welcome that are the guides to Discourse and community guideline links, FAQ…
- NEXT STEP: <https://community.snowdrift.coop/t/draft-please-join-us-on-the-snowdrift-community-forum/567>
- NEXT STEP: <https://community.snowdrift.coop/t/draft-welcome-to-the-snowdrift-coop-community/699>
```

<!-- Capture TODOs -->

<!-- Add meeting notes to wiki -->