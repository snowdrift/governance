# Meeting — June 12, 2018

Attendees: wolftune, smichel17, Salt, msiep

<!-- Add agenda items below -->

## GDPR and anything else relating to Discourse *full* announce

- wolftune: I felt personally there were things I didn't want to do after the fact; happy we've invested in setup even though I'm not a fan of waterfall design generally
- wolftune: privacy policy MR, happy people saw it; wished I'd recieved feedback
- wolftune: Final blockers to announcing discourse?
- msiep: is there a way to review the privacy policy updates not in html form?
- wolftune: could build the site, or I can make a pdf
- Salt: We haven't solicited feedback from individuals yet. Do that first?
- smichel17: I'd like to move the banner to the intro message. Not a blocker for anything, really, but would like to do it soon.
- wolftune: is updating ToS a blocker for full announcement (not inviting a few people)?
- consensus: yes
- NEXT STEP: Review terms of service https://git.snowdrift.coop/sd/legal/issues/9
- NEXT STEP: Invite regulars from irc, etc to come use Discourse, post about doing this in a Discourse topic https://git.snowdrift.coop/sd/outreach/issues/1
- NEXT STEP: wolftune makes a pdf [DONE]

## team communication

- wolftune: how to indicate when something is higher priority / urgent?
- msiep: email me
- smichel17: Be explicit that it's higher priority / give a deadline
- Salt: we could make a sticky topic in discourse staff category
- NEXT STEP: wolftune: Make a pinned thread in the discourse staff forum https://git.snowdrift.coop/sd/governance/issues/40

## chreekat, team communication, invite others to engage

- wolftune: I talked with chreekat, his inclination is to stick to async communication (email, gitlab, discourse) more than irc/meetings
- wolftune: There's a tension where I've been the main person corresponding with him (and with others);
- wolftune: it would be best if other people not me were engaging more; we want to avoid a case where every post etc. always gets a full response from me and less other perspectives
- NEXT STEP: others (not wolftune) reply to chreekat's Discourse post

----

## Carry-overs

<!-- Review status of carry-overs -->

<!-- Decide where to capture outstanding tasks (new & carryover) -->

```
## looking forward: CLS, OSCON
- NEXT STEP: Identify and focus on anything we can finish in a 6-week timespan and make a discourse post to communicate this time-frame to others

## Breaking things into tiny chunks, easy/short (governance repo, wolftune)
- NEXT STEP: make GitLab tags for: Quick, Easy [DONE]

## engaging on Discourse (outreach repo; Salt, but not today)
- NEXT STEP: write intros: https://git.snowdrift.coop/sd/outreach/issues/14
- NEXT STEP: post a discourse announcement about meetings status (including inviting people who aren't core contributors/team)… [wolftune/Salt]
- NEXT STEP: Salt opens Discourse topic on planning useful pinned topics (fpbot, etc)
- NEXT STEP: make a pinned post encouraging people to introduce themselves (some guidance about what sorts of things to mention)
- NEXT STEP: add pinned posts to welcome that are the guides to Discourse and community guideline links, FAQ…
- NEXT STEP: <https://community.snowdrift.coop/t/draft-please-join-us-on-the-snowdrift-community-forum/567>
- NEXT STEP: <https://community.snowdrift.coop/t/draft-welcome-to-the-snowdrift-coop-community/699>

## Project outreach steps (outreach repo)
- NEXT STEP: schedule meeting to work on kanban sorting for recruiting [Salt]

## governance roles overhaul (governance repo, wolftune)

- NEXT STEP: wolftune will work on list of roles and defined responsibilities for accepting *any* role [too broad to capture directly, but overall issues are captured in various ways]


```

