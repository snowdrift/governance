<!-- Previous meeting's notes: https://git.snowdrift.coop/sd/wiki/tree/master/resources/outreach-meetings -->

# Outreach Meeting —  July 17, 2018
 
Attendees: smichel17, wolftune, salt

<!-- Check-in round -->

<!-- Decide which carry-overs should be on the agenda / deleted / carried over again -->

## Carry-overs

```
## Using issues at outreach repo
- NEXT STEP: Steps to (really) deploy civi https://git.snowdrift.coop/sd/outreach/issues/27
- NEXT STEP: Have an outreach issues grooming meeting

## How to add items to a meeting agenda in advance? (gitlab issues w/ tag?)
- NEXT STEP: Write out updated process (reply or new thread from <https://community.snowdrift.coop/t/planning-meeting-agendas-in-advance/831>)
```

---

<!-- Add agenda items (below) -->

## Should we donate/pay for social.coop?
- wolftune: I feel like we *should*. $1 or $3
- smichel17: To some extent, base off our crowdmatching income. When we pass $1/mo, etc.
end decision: put off indefinitely, we don't use it a ton, @msiep and @wolftune are personally funding for their accounts already

## Add a list of "tags you might want to watch"
- to https://community.snowdrift.coop/t/welcome-to-the-snowdrift-coop-community-forum/8

## Decide on project onboarding process
- wolftune: my vision from the beginning is asking each project a bunch of questions we want to know from them:
    - What's your license?
    - Where are you located / what's your legal status?
    - etc
- wolftune: then we ask them, "What do you want to know about snowdrift to make you feel comfortable working with us? We hope we have these answers, if not we'll work to figure them out."
- Salt: Agree, there's an additional thing of, what do we want from them?
- Salt: we're giving them the opportunity to guide what all other projects are going to see. That's important.
- wolftune: I've been saying this all along, but we can get them to verify it: I think projects will want a /p/KODI, a pledge button, link to their website, what license, 
- Salt: This seems like a MSiep question.
- wolftune: we have ideas for how the project page should work… how do we get projects to tell us what they want (without overwhelming them) shown on /p/$project
- NEXT STEP: wolftune and msiep schedule a time to meet
- NEXT STEP: Request a mockup of project page from design
    - "We need this mockup as a general, 'this is what a page would be like' so we can take it to projects to get feedback; NOT a perfect design."
- Salt: We need a timeline. 2 months is an ambitious goal, wouldn't happen at current pace but I think it's do-able.
- NEXT STEP: Get feedback from projects, what information would they like on there? What of that do you need in place for launch vs what is ok to come later?
- overall issue: https://git.snowdrift.coop/sd/outreach/issues/41


<!-- Decide where to capture outstanding tasks -->

<!-- Closing round -->

<!-- Capture TODOs -->

<!-- Add meeting notes to wiki -->