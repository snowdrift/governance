*raw etherpad dump*

# Outreach Circle Meeting — February 23, 2018

Attendees: wolftune, smichel, salt, iko

Agenda: Discourse setup, mailing list migration plan

# Enabling "solved" solution for categories
- <https://community.snowdrift.coop/t/turning-on-the-solved-solution-option-for-most-all-categories/519/1>
- wolftune: I think we should have it on by default on 
- salt: no objection to this
- wolftune: is it currently enabled?
- Salt: not sure.
- iko: can we do it per-subcategory? some places doesn't make sense to do, e.g. categories with broad topics of discussion outside of snowdrift development (flo, economics)
- NEXT STEP: enable for parent categories [DONE]

# Auto-closure of topics
- wolftune: do we want to auto-close topics?
- salt: yeah, better for SEO to have multple threads on a topic, less moderation workload
- smichel: +1
- wolftune: what's the time period?
- salt: 1 month is probably fine
- NEXT STEP: change subcategories to auto-close after 1 month (instead of 1 week) [DONE]

# Top-level category posting
- <https://community.snowdrift.coop/t/allowing-top-level-category-posting/488/4>
- salt: Doesn't make sense for restricted
- Might make sense for meta, if we make forum -> meta
- In clearing the path? That's really what we're talking about
- NEXT STEP: enable top-level category posting for Clear The Path [DONE]

# Tags
- <https://community.snowdrift.coop/t/better-use-of-tags-also-assignments/507/3>
- salt: from my general experience, having subcategories is more effective, with the advantage of showing all the subcategories posts in the parent categories
- smichel: I would move up all the subcategories, but I buy it for being better for moderation
- wolftune: if it's not allowed to start top-level category topics, at least moderators should not be able to see the new topic button (to prevent adding a topic there that people can't reply to by mistake)
- there may also be topics that don't fit in any subcategory
- smichel: how about a general subcategory? (inbox style) if not sure where to post it, post it here, a moderator can move it
- (related: <https://community.snowdrift.coop/t/remove-uncategorized/516/1>)
- smichel: salt, did you create these manually or did they come with the install?
- salt: they are created by default
- smichel: I had moved them, but in light of earlier discussion, it's okay to keep
- I had some confusion when creating a new topic, unclear whether I was putting the topic in "Uncategorised" category or in the parent category (not in any category)
- NEXT STEP: create new description for Unsure category, explain it's not uncategorizable
- NEXT STEP: create discourse post to brainstorm a list of initial tags Assigned: wolftune [DONE]
    - NEXT STEP: Add agreed on initial tags (lowercase other than acronyms) Assigned: smichel17
    - NEXT STEP: get them in use some Assigned: Everyone

# Meta vs support?
- <https://community.snowdrift.coop/t/whats-the-difference-between-meta-and-support/479/3>
- wolftune: I could see meta as a temporary subcategory until forum is set up, but what is the difference with support? 
- salt: meta should be changed to e.g. "forum". support is not just forum support, it's support for other snowdrift infrastructure, the main website, etc.
- wolftune: the title shouldn't be called "forum", so we just need a new title for it
- NEXT STEP: change of the name of "meta" (now "forum") to another name (not "forum", something broader) that makes it clear we're talking about the forum:
    - IN PROGRESS: https://community.snowdrift.coop/t/change-name-of-this-meta-forum-category/528

# Moderation rules and procedures
- <https://community.snowdrift.coop/t/adding-a-reason-why-you-flagged-posts/497/14>
- wolftune: right now you can choose from a menu of options. ideally there can be checkboxes for various violations
- additionally there could be a freeform textarea to enter notes
- salt: everything you said sounds reasonable and nice to have, not sure if the features are available
- <https://community.snowdrift.coop/t/when-to-archive-close-delete/524>
- salt: <https://meta.discourse.org/t/what-is-the-difference-between-closed-unlisted-and-archived-topics/51238>
- basically, when a topic is closed, it's still possible to Like topic, etc. if it's archived, it locks everything down about it, e.g. flagging
- closed is a subset of archived, which just stops replying. unlist removes it from topic listing
- wolftune: which should we do in which case?
- smichel: seems like close happens automatically, but archive is manual?
- wolftune: I think of this as clutter-clearing for future users
- salt: people can read it if they want. crawlers may also index the content
- wolftune: do we want crawlers to index discussions about what plugins we use?
- salt: it's okay. you can mark it solved and click archived, which removes it from new/top listing
- smichel: I think archived is too much, closed and solved is plenty
- if I were a user who went away and came back after some time, I can't find new replies on discussions I had been following
- salt: I don't see much of a use case for archiving, but for wolftune's described scenario, it makes sense
- wolftune: ideally archived is more about unlisting. in gitlab you can reply to a closed topic
- another question that came up, use of whisper. unfortunately it still enables meta discussion that should happen out-of-band (if the people discussing were all moderators)
- NEXT STEP: list desired changes, features, find out which features are available and continue discussion in topic, make many tasks etc. to get as good as we can (maybe send some requests to Discourse too as needed)
- NEXT STEP: create FAQ summarising the differences among close, unlisted, archive, delete, solved:
    - Can we just link to existing discussions, like this? https://meta.discourse.org/t/what-is-the-difference-between-closed-unlisted-and-archived-topics/51238

# Place for setup discussions
- <https://community.snowdrift.coop/t/where-to-post-tasks-re-this-forum/489/2>
- wolftune: currently <https://community.snowdrift.coop/c/forum/meta>, but should we add tasks in gitlab instead?
- salt: I agree. discourse is for discussing things, sometimes a task needs further discussion, but better to post tasks in gitlab
- wolftune: add issue in ops repo?
- NEXT STEP: document that tasks are added as issues in ops repo:
    - Where? Here https://wiki.snowdrift.coop/resources ? The governance repo?

# Legal terms, FAQ and CoC
- <https://community.snowdrift.coop/t/consolidating-legal-terms/509/3>
- wolftune: do we need separate terms for discourse and main site? (terms, privacy policy)
- salt: I don't see any difference between terms, so no need for two separate sets
- current terms do need updating, it doesn't include people opening topics and commenting
- iko: is the task to update privacy policy due to discourse ip logging currently tracked?
- salt: not yet
- <https://community.snowdrift.coop/t/faq-vs-community-guidelines-coc/508/2>
- wolftune: some of the CoC will live in discourse
- salt: FAQ should be category, not a page, with CoC a pinned topic
- wolftune: the CoC is a bit too long, maybe we should split them up
- NEXT STEP: open ops tasks for cleaner link/redirects, removing *all* Discourse-specific terms, consolidating all terms (comb through Discourse terms and copy over all valuable stuff to our main terms etc): Assigned: smichel17
    - Where is "our main terms"; does that mean https://wiki.snowdrift.coop/about/terminology ?
- NEXT STEP: create issue about updating privacy policy to account for discourse ip logging: Assigned: smichel17
    - Or just mention that discourse is covered under its own privacy policy? There's already a thread: https://community.snowdrift.coop/t/privacy-policy/6/2
- NEXT STEP: split up "community guidelines" and "CoC" into separate topics pinned, in the right place, link them to one another, and make the relevant UI stuff in Discourse link to those and be labeled appropriately
    - Having them in the same thread makes them both show up on the faq page here: https://community.snowdrift.coop/faq 
    - I would prefer to have them on two tabs there, but I don't think that's possible and I think I'd rather all have it in the faq than only appear as a post, no?

# Plugin suggestions
- <https://community.snowdrift.coop/t/discourse-plugins-to-consider/511/1>
- salt: no issues with the list, some we already have
- wolftune: next step is to have discussions on how to use some of them? teach moderators to use them?
- salt: a few may already be integrated into discourse as core features
- I like the shared edit, but the plugin hasn't been in development for a few years
- <https://community.snowdrift.coop/t/we-want-to-keep-cakeday/505/2>
- wolftune: seems like a "what is that?!?" sort of feature
- salt: it's like an old-school community forum-building feature, some people like it/still use it
- wolftune: it reminds me of Facebook and social manipulation, but if you like it, we can keep it
- salt: yeah, I like it
- NEXT STEP: install plugins from the list that outreach members agreed to try Assigned: Salt
    - Where is the list of "agreed plugins" documented? <https://community.snowdrift.coop/t/discourse-plugins-to-consider/511/1> seems to indicate none except retort, which is already covered below.
    - Should make an ops issue for this. Assigned: smichel17
- NEXT STEP: salt clarifies position on cakeday (keep plugin) [DONE?]
- NEXT STEP: close cakeday topic [DONE]

# Like feature modification
- <https://community.snowdrift.coop/t/respect-and-agree-instead-of-or-addition-to-like/483/14>
- salt: I really like "agree" overall, over the other options mentioned
- wolftune: "agree" is more valuable if we can only have one of that feature
- we can try retort (or similar plugin, see topic for link) and see how customisable it is, if it allows specifying a subset of the emoji reactions
- salt: I'd want less than 5 reactions. does this replace or complement the existing Like feature?
- wolftune: I'd vote for complementing it
- salt: I'm okay with it
- NEXT STEP: open ops issue to install retort [DONE] https://git.snowdrift.coop/sd/ops/issues/8

# Badges
- <https://community.snowdrift.coop/t/snowdrift-badges/510/3>
- salt: I support this
- wolftune: we should figure out when in our plans we'll have this? before official launch?
- salt: yeah, I think it's very valuable
- wolftune: I have no idea what the technical issues are to mark a member as a patron in the snowdrift database
- salt: pretty sure it's doable as part of the discourse api
- wolfune: so we can open a coding task for adding this
- NEXT STEP: create a discourse topic in which to draft a list of badges [DONE] https://community.snowdrift.coop/t/decide-how-to-use-groups-badges-and-titles/539/
- NEXT STEP: create an ops issue in which to figure out implementation Assigned: smichel17

# Bugs?
- <https://community.snowdrift.coop/t/some-bugs-with-discourse/477/9>
- salt: this is about line breaks?
- wolftune: that was the main one, but on further thought, we should probably leave it as default
- there was another bug that appears to be fixed in the latest update 
- *(meeting note: discourse was updated at the start of the meeting)*
- NEXT STEP: n/a, issue can be closed [DONE]

# Other usage questions
- <https://community.snowdrift.coop/t/intentionally-threading-replies-versus-large-combined-replies/480/3>
- wolftune: there's a lot of value in posting smaller replies instead of a giant essay to better track discussion via embedded replies
- salt: mixed feelings on this. I'm a fan of msiep's approach (longer reply with different quotes in one reply). you can see it all in one post instead of scrolling through multiple posts
- wolftune: inclined to use either approach at user discretion
- salt: yeah, that seems like the right approach
- <https://community.snowdrift.coop/t/possible-to-get-a-parent-link/482/13>
- wolftune: I had a parent link question, smichel showed me a method and it's basically resolved. do we want to add a FAQ item about it?
- salt: unless people start asking about it, not really a FAQ item
- wolftune: *(while reviewing list of categories)* is there any way in the UI to sort the latest view by date created instead of activity?
- salt, smichel: there doesn't seem to be any way, though you can access an url for this view
- iko: example <https://community.snowdrift.coop/c/announcements?order=date>
- NEXT STEP: try different reply approaches to see what works better
- NEXT STEP: add FAQ describing how these two approaches are possible, have pros and cons, and posters should use best judgment (add also the options to reply as new topic and reply privately), generally the issue of keeping things wieldy and also open to widespread participation (long posts discourage new readers)
- NEXT STEP: Side issue: offer "solved" but put in the ... even for OP… investigate this possibility [DONE, opened as issue at Disourse's site]

# Moderation guide / welcome / plans
- wolftune: specifically use of canned-replies, welcome guide. did we want to discuss this at this meeting?
- salt: I'd like to, not sure if priority to discuss now. one thing we can discuss is what to rename the "blog" category
- my feeling is "blog" is a subcategory of "announcements"
- wolftune: blog posts are official announcements
- smichel: is it possible to toggle mod mode on/off? I'd like that because 1) I don't want to accidentally remove something and 2) I'd like to see what regular members see
- salt: not sure if it exists yet, if not it could be a feature to suggest to discourse development
- NEXT STEP: rename "blog" to "announcements" [DONE]
- NEXT STEP: Look up to see if "mod mode" toggle exists; if not, request on meta.discourse.org

# Review of existing categories and subcategories
- salt: let's review the list of categories/subcategories currently set up and confirm we're okay with the list (I'd rather not to change the existing ones later)
- wolftune: there's no "outreach" subcategory in "clearing the path"
- salt: we can add it
- wolftune: do we want to add a category for discussing partner projects?
- wolftune, smichel: I'm inclined to wait until we're ready to receive our first partner project
- salt: we can wait, but it also brings a question of moderating discussions
- i.e. discussion about the relationship between snowdrift platform and project, not a support forum for partner project's products
- wolftune: we can add it later
- do we need a subcategory for ops? e.g. for open-ended discussions on whether to use piwik or other analytics platform
- iko, salt: there's already gitlab issues for those discussions, it could be added later, currently not a lot of use for an ops discourse subcategory
- wolftune, smichel: do we have a subcategory for sensitive topics? e.g. discussing the discourse ip logging issue privately within the team
- salt: there's the "restricted" category. any other subcategories needed under "restricted"?
- wolftune: rename "partners" to "team"? (there was some confusion about "partners", whether it referred to snowdrift the org's community partners, partner projects, or core team members also called "partners")
- or we can keep it for discussing community partners, and "staff" would be team
- smichel: I created "board"
- salt: the "volunteers" category can be removed. I meant to review the restricted category when I created it and change things later. volunteers can use the "winter lodge"
- "forum" will be renamed ("meta") and the faq can become a site-wide knowledgebase
- wolftune: maybe a q&a? (at least the basic idea, I don't particularly like that name)
- what about a "welcome"/new member self-introduction category? we should have that
- NEXT STEP: create an outreach subcategory under "clearing the path" [DONE]
- NEXT STEP: research whether it is possible to create a "sensitive" category under a public category, e.g. "clearing the path", where anybody can post but people can only view their own threads (besides people with specific privileges)
    - In the description, mention use cases (eg: reporting harassment)

# ML migration plan

- iko: rough timeline for lists.snowdrift.coop landing page replacement?
- salt: next week. I'd like to get a few people from irc to test the forum for a few days first
- smichel: should we add a link to community.snowdrift.coop in the irc topic?
- iko: okay. I just need to open a MR with the files and someone can deploy/copy them into place? (nothing else needed from me?)
- salt: smichel, yeah. iko, yeah, just open MR
- NEXT STEP: Put a link to community.snowdrift.coop in the irc topic & ask irc people to check it out
- NEXT STEP: Open MR to list repo [DONE]

# Misc

- NEXT STEP: double-check that the blog automatic topic is working having moved blog category
- NEXT STEP: Add/update descriptions for each category
- NEXT STEP: Have team members introduce themselves.
