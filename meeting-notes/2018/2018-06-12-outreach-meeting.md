# Outreach Meeting —  June 6, 2018
 
Attendees: smichel17, wolftune, salt

<!-- Add agenda items below -->

## Using issues at outreach repo
https://git.snowdrift.coop/sd/outreach
- repo set to private (only visible to repo members)
- moving Taiga outreach stuff
    - https://tree.taiga.io/project/snowdrift-outreach/kanban
- https://tree.taiga.io/project/snowdrift-outreach/issues
- NEXT STEP: wolftune makes GitLab issue(s) for video promotion etc. https://git.snowdrift.coop/sd/outreach/issues/29
- NEXT STEP: Capture remaining taiga issues still marked as 'ready' in gitlab issues [DONE]
    
- consolidating outreach tasks
- relation to CiviCRM
- NEXT STEP: Create gitlab issue with steps to (really) deploy civi https://git.snowdrift.coop/sd/outreach/issues/27
- NEXT STEP: Have a gitlab issues grooming meeting

## Flagging status for now (separate from proposed ideal flagging)

https://git.snowdrift.coop/sd/outreach/issues/2


## Migrating from Taiga
- NEXT STEP: post about closing, give date until deletion [DONE] https://community.snowdrift.coop/t/deleting-our-taiga-projects/829/1


## Storing agenda items — gitlab issues w/ tag?
- create topic(s) on Discourse: https://community.snowdrift.coop/t/planning-meeting-agendas-in-advance/831



## Recommended Watched/Tracked/Watching First Post for team members/etc
- should be watching the team category
- should be watching (at least first post) for clearing the path categories relevant to your roles
- should be watching first post for announcements
- consider watching first post in the welcome category
- consider tracking general discussion category
- consider watching first post or tracking feedback category (or sub-categories)
- NEXT STEP: start sticky thread in team category on ,cdiscourse https://community.snowdrift.coop/t/expected-recommended-use-of-discourse-for-team-members/843


## Project sorting
- <https://git.snowdrift.coop/sd/projects/boards>
- NEXT STEP: meet on 6/14 @ 4pm PT


<!-- Capture TODOs  -->

## Carry-overs <!-- Does outreach want this section or just make sure everything is captured? -->