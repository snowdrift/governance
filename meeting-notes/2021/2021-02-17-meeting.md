<!-- See end of this pad for meeting-practices notes and reference -->

![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — February 17, 2020
=======================

1. Centering
--------------
### Personal Check-In Round
- Attendees: Salt, mray, smichel, alignwaivers, Adroit, iko, wolftune, MSiep (arriving late)
    - Leaving early:

- Facilitator: Salt
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):

### Previous week highlights
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-->

#### Metrics
- **Attendees last meeting:** Salt, MSiep, smichel, mray, iko, Adroit, wolftune, alignwaivers
- **Snowdrift patrons:** 137
- **Discourse:**
    - Signups: 1 -> 1
    - New Topics: 1 -> 3
    - Posts: 4 -> 8
    - DAU/MAU: 29% -> 25%
- **Missing:** Matrix+Gitlab stats

#### Meeting Feedback

##### Appreciations
- good meeting, one item but on topic
- grateful for everyone, appreciate efforts and everyone being here
- productive meeting

##### Reflections
- Co-facilitation is fine, but watch stepping on toes
- like hedgedoc except lack of authorship
- need to settle on meetingplaces (pad/audio) before the meeting
- dont like first minute of silence

##### Suggestions


#### Facilitator reads over last week next steps

###  Minute of silence
<!-- Look over the reminders & think about what to bring up today -->

Reminders
---------
<!-- Tensions, progress updates, blockers, questions
<!-- GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!-- Forum: https://community.snowdrift.coop/
<!-- Personal notes/tasks/emails
<!-- NEXT STEPs — mark each as [DONE or CAPTURED] <LINK>
<!-- > indicates carryover from previous week
<!-->

### OSI Meeting
> - NEXT STEP (smichel+wolftune): setup gitlab tracking, OSI graduation milestone [Some progress]

### Retreat Followup Post
> - NEXT STEP (wolftune, alignwaivers) summarize on forum post, connect to relevant gitlab issues [DONE]

### OSI Funds and graduation
- NEXT STEP (wolftune+smichel): Draft a contract to put in place anyway [TODO]
- NEXT STEP (smichel): Plan a meeting with phyllis to discuss (over weekend) + share a draft [TODO]
- NEXT STEP (alignwaivers+smichel): Forum post; decision deadline by next week, choose delay tactic if we don't decide by then [DONE] <https://community.snowdrift.coop/t/deciding-on-the-use-of-osi-funds/1712>

### Personal Scratch Pad

2. Open Discussion (8 minutes)
------------------------------
- salt: OSI elections are coming up for the board: snowdrift can nominate someone
- smichel: after Alanna leaves, no one on board with snowdrift
- wolftune: they all also want to get rid of the entire incubation program
- salt: I'm interested in running. 1 meeting a month plus other things
- smichel: shouldnt affect our graduation, but happy to nominate

- wolftune: working on not juggling too much, dont feel fully motivated on OSI and other Snowdrift priorities, but know it's needed
- smichel: suggest splitting into two groups (wolftune and I working on OSI $ contract stuff)

- wolftune: I have a sense where there's a shift/pivot point where everything starts happening, but I dont feel myself able to do much about it
- salt: I have a week left before my thesis goes in. I need a break, but also need to apply for jobs. Think I'm going to rely on savings for a quarter and do a lot of snowdrift work for that month. Felt good to get that retreat folllow up post.
- smichel: I have 6 weeks with more time, will try to align with salts free time
- wolftune: setting up for long term seems pertinent

3. Agenda
---------
<!--
### TOPIC <LEADER> (# minutes) Room ?
#### PLAN, GOAL
<!-->

### Pad Notes <alignwaivers> (3 minutes)
#### Goal: agree on how to use scratch pad explicitly and create next step for matrix+gitlab stats to incorporate
- Matrix+Gitlab stats
- time frame for scratch pad erasure
- sidenote: I also think using checkmarks is nice for next steps..
- [ ] alignwaivers: follow up on gitlab issue
- salt: think there's an issue about these metrics.

    - I personally use it as a scratchpad

- smichel: think it's each person's  responsibility
- alignwaivers: shouldn't be on the website
- salt: we could just remove it too
- smichel: proposal : leave it in the pad and delete before gitlab

### OSI Nomination <Salt> (2 minutes) Room ?
#### Submit Nomination
- [x] Submit Salt as OSI nomination (wolftune)
- wolftune: planning to do the holdover for funding
- salt: thought we  were going in the direction of contract for connor
- wolftune: we were gonna holdover if we didn't make a decision today
- smichel: maybe look over some of it and make decision by friday
- salt: select candidate nomimnation at https://opensource.org/contact (as affiliate seat nomination)

### Elm Migration <Adroit> (# minutes) Room ?
#### Select a page to migrate and work on
- adroit: did research on pages, selecting a page to iterate on 
- salt: did you get a chance to look at the prototype?
- adroit: yeah
- salt: if wanna look at this, changes that are not yet implemented https://community.snowdrift.coop/t/takeaways-from-the-first-snowdrift-coop-retreat/1711

    - adroit will implement this as a test page in the prototype format and work with mray on it

- mray: maybe we use the home page instead for a 1:1 iteration
- salt: doesn't matter so much, we want to test the workflow to see if you can make changes you want to 
- adroit: home page would make sense except its least interactive and going to change so
- mray: no preference for me
- salt: one of the main goals is streamlining the design/dev pipeline
- salt: it's going to be a change on an existing page
- mray: project page would be good for a new implementation
- adroit: but it's all dynamic content
- adroit: home page going to be significantly different, so many design things it would take a long time

    - part of the point of using different front end is to be able

- mray: the login page would be a good example for this, there are changes to be made

    - 3 pages: login signup and reset passphrase is a group of similar/different, and simple interaction elements

    - https://sdproto.gitlab.io/auth/login/

    - https://sdproto.gitlab.io/auth/create-account/

    - https://sdproto.gitlab.io/auth/reset-passphrase/

    - I also have mockups for those: 

        - LINK:

- salt: seems like a good candidate for adroit to test
- mray: asking myself how to integrate (penpot) into our workflow

    - use of penpot might get really interesting, could work on preparing pages that we work on so msiep could chime in without changing html/css, add commentary that would be visible to others.  is there any others?

- salt: failed login?
- mray: could use penpot for these pages
- mray: when redoing the login page, there's going to be some smaller glitches (in my experience, to be expected). I can use the prototype to communicate where issues are.
- salt: are you on penpot?
- adroit: didn't work for me, clicked on email invite
- mray: that was before iko created our own instance
- [ ] mray: send invite to adroit and msiep to snowdrift penpot instance

- [ ] submit a request to osuosl for adroit@snowdrift.coop (smichel)
- [ ] Add ^^email alias step to team onboarding gitlab issue (smichel)

### Roles Pad Cleanup <Salt> (# minutes) Room ?
#### Keep working on this so momentum isn't lost






### OSI Contract <smichel> (# minutes) Room https://meet.jit.si/snowdrift-a
#### wolftune+smichel coworking on drafting the contract

- wolftune shares oral history of snowdrift contracts & shares documents with smichel
- [ ] Read old contracts (smichel)
- [ ] Cowork on drafting a contract Fri 11 PT (smichel+wolftune)
- smichel: most things will be standard, real thing to decide is on hourly or deliverable scope


4. Wrap-Up (last 10 minutes)
----------------------------
### Report back: Summarize conversations, add if needed


### Meeting eval round: feedback, suggestions, appreciations

- alignwaivers: different fonts are happening?? little distracting. was a good meeting, thought we used breakouts well. thanks everyone

    - handraise could work for establishing who hasn't gone yet in rounds/popcorn(and then put hand down when your turn has past

- wolftune: glad we did what we did, glad I don't feel like I'm shouldering everything especially when i'm distracted, good feeling to be a part of the team, thanks salt for facilitating and smichel for being on top of stuff. I just shifted table of contacts to be on by default (since it was disappearing we switching pad/refresh). I like the font sizes actually being different in pad and table of contents; I like Jitsi, seeing each other
- adroit: good meeting, fit in time slot, got my action items taken care of. thanks everyone for being here
- smichel: we got through a lot of stuff today. I appreciate the minute of silence not being right after checkins. a little weird in the middle. I think 6-1-2-1- would be better. Headers have a code style. different fonts may have been result of copying that
- iko: good meeting, thanks everyone for being here
- mray: good meeting, thanks and looking forward to staying in touch and working on stuff
- msiep: good meeting, sorry for being late, like the bigger headings
- salt: good meeting. took awhile to get here, tech stuff is annoying but we are working on it. I didn't like the split of of time for silence, maybe smichels suggestion is better


#### Meeting Adjourned!


<!--
Meeting practices and tools
Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Move new attendee list and metrics to previous, leave new blank
    * Replace previous meeting eval notes with new, filtered down to essential points
    * Clear the "last meeting next-steps review" section
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Unbold the topic headers and add one # mark
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
