<!-- See end of this pad for meeting-practices notes and reference -->
![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — April 27, 2020
==============================================================================
- Meeting location: Mumble
- Server: mumble.sylphs.net port 8008 / murmur.sylphs.net port 8008

Centering
---------------------------------------------------------------------------------------------------------------------------
### Personal Check-In Round
- Attendees: smichel, Salt, mray, bnagle17, wolftune, Timmy, MSiep, alignwaivers
    - Leaving early: 

- Facilitator: Salt
- Note Taker: smichel
    - Note-saver/pad-updater (if different):


### Previous Meeting Feedback
- Etherpad headers are finnicky.
- Try not to cut people off

### Recent Pad Changes


### Metrics
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-- https://gitlab.com/groups/snowdrift/-/contribution_analytics
<!-->

- **Last meeting attendees:** 7 (smichel, MSiep, bnagle17, alignwaivers, iko, msiep, salt, adroit)
- **Snowdrift patrons:** 137 -> 138
- **Discourse:**
    - Signups: 0 -> 0
    - New Topics: 0 -> 0
    - Posts: 3 -> 5
    - DAU/MAU: 25% -> 28%
- **Matrix** (lines/words):  79 lines / 1061 words  -> 47 lines / 436 words
- **GitLab:**
    - Git (pushes/commits/people):  17/18/4 -> 8/11/4
    - Merge Requests (created/merged): 3/3 -> 1/2
    - Issues (opened/closed): 8/23 ->  0


Read Reminders + Minute of silence
------------------------------------------------------------------------------------------------------------------------
1. Facilitator reads the reminders (not the personal scratch pad)
    - Update any to mark if they are complete [x]
2. Minute of silence — think about what you want to discuss. For inspiration, consider:
    - Tensions, progress updates, blockers, questions
    - GitLab: https://gitlab.com/groups/snowdrift/-/issues
    - Forum: https://community.snowdrift.coop/
    - Personal notes/tasks/emails
    - Reminders (below). > indicates a carryover from previous week


### General
- [ ] smichel: Open gitlab issue for pad suggestions
- [1/2] (alignwaivers): do some extracting on the roles pad

> - [ ] ¿iko: Look into replacements or running fpbot on another server
>     - iko: (2021-04-27) waiting to hear back from fr33domlover for fpbot data transfer
> - [ ] smichel: open a new gitlab issue for finalizing the wiki/gitlab split (wiki as think tank publishing platform)

Open Discussion (8 minutes)
------------------------------------------------------------------------------------------------------------------------
- Timmy: Should we list our names in the attendees? I only see my name..
- Salt: Try refreshing the pad.
- [ Some meta discussion around mumble settings, half-duplex mode ]
- smichel: I don't have much to discuss, hoping to end early & get to next steps during coworking after
- smichel: Is monday nights not good for people? Not many have shown up.
- wolftune: If monday is the only time that works I can rearrange schedule, but don't want to for less important times. I have a conflict currently.
- alignwaivers: Seems like we could use a separate availability poll?
- wolftune: can look at my updated availability. If coworking schedule is during non-optimal times than I just probably wont show up
- salt: makes sense to have 2 inputs on the whenisgood calendar (aaron-meeting + aaron-coworking)


Finalize agenda + Minute of silence
------------------------------------------------------------------------------------------------------------------------
1. Finish adding agenda items, if needed
2. Each topic leader says what they want from their topic
3. Minute of silence — think about what you want to say

Agenda
------------------------------------------------------------------------------------------------------------------------
<!--
### TOPIC <LEADER> (# minutes)
<!-->

### Mechanism writeup <mray> (1min)
- mray: I just want to remind people to write up their proposal for future mechanisms in bullet-point lists (10 points or less) like the existing ones in the [repo](https://gitlab.com/snowdrift/mechanism)
- wolftune: Maybe msiep could make one for the original proposal?
- [ ] msiep: Make a bullet-point writeup for the or
- msiep: I did look into it after last meeting, some of it I was a little confused by
- wolftune: My impression is, like a computer program, you say, "It needs to do these things"
- msiep: confused by: it needed to be written as if — the original proposal's description didn't seem to acknowledge multiple projects; it was written as if the max was not across multiple projects.
- wolftune: In practice, there were no multiple projects existing for it to be distributed over; I think msiep just didn't take the time to explain it given that it didn't exist.
- msiep: Wouldn't the idea be to include that? E.g. for future proposals, it's not implemented yet but  it's still defined
- salt: have a next step to propose on that for later

### Salt's class timeline <Salt> (5 minutes)
- salt: would like a thumbs up or not on what im thinking
- salt: outline on line 141-143  (concrete deliverables etc)
- after discussing, I'm thinking,
    - Concrete deliverables would be organizational things:
        - org chart
        - business plan
        - bylaws
       Do these all seem reasonable direction? Although it's a compsci class, they seem ok that this is not strictly coding.
- wolftune: is it something to navigate or it's fine if you aren't actually doing computer sci
- Salt: The latter, but I need to turn it to get that feedback
- wolftune: i think you're describing what you're qualified/interested in working on, it's high value, no reason to shift away, but maybe prioritize within them.
- wolftune: if you want to frame this in broadest-impact way, you'd not only talk about suceeding/having these things for snowdrift, but also having those available for other orgs.
- Salt: that's outside of scope, I framed it as a project to help Snowdrift.

### Roles <Salt> (5 minutes)
- Salt: I went through the roles pad, Athan did a great job, looks complete to me, what needs to be done still for extracting?
- Alignwaiver: So much redundancy it's confusing. 95% is captured, the 5% is probably trivial. It's ready to move forward.
- salt: thanks for working on the final push. gonna mark that as done
- Salt: My next steps are:
    - Make it into a realistic org chart, using as much as I can of things we've agreed upon & want to incorporate. Don't want to remove the reasons we've spent a lot of time on sociocracy/holacracy, but 
    - org charts have very explicit purposes that we don't have the availability to do
    - don't like hierachies that much there's some benefits to it as well, clarity on who's in charge of what (domain)
- wolftune: +1 to all of that; we can evolve from whatever using sociocratic methods, but we need something that's complete first.
- Salt: One of the big ideas here is to have a single place where it's all in ??? (vs having to do this again).
- align: I'd like to help with this to some degree
- What's coming up for me is an enormous number of roles that we don't have enough people for. Maybe consolidate small roles into larger roles? Multiple hats on one person…
- Org structure where we can extract certain parts / rotate the number of staff to accomodate what needs to be done
- Salt: … Let's talk more later.
- [ ] Salt: Submit document/proposal for the class, including proposal for what the org chart will look like

### Linux App Summit <Salt> (5 minutes)
- Salt: it's in about half a month.
    - I haven't had a chance to do anything :(
    - I don't think LibrePlanet videos are live yet but they should be soon.
    - Would like to chat with folks who did see LP talk, so Linux App Summit talk can be better
    - Want to give enough time so maybe mray/iko can make slides, and the recorded version can be a better version / more widely share-able.
    - They're interested in hearing about snowdrift ("Haven't heard about it in a while") even though I framed it as "not about SD"
- sm: good thing to cowork on
- [ ] wolftune & Salt: Schedule a time to cowork on reviewing our last co-talk from LinuxFest NW
- [ ] salt: get feedback from people who attended: adroit, smichel, salt, timmy, alignwaivers
- salt: are you available for some slidepoking?
- mray: maybe we can discuss after the meeting


Wrap-Up (last 10 minutes)
------------------------------------------------------------------------------------------------------------------------
### Sign-off round: what's next, appreciations, suggestions
- Salt: glad there's so many folks here, even if you showed up late. Love the collaborative note-taking. Timer link at the top would be helpful
- smichel: a lil more meta than we needed to have but wasn't unbearable. appreciate everyone showing up. hope we can utilize the remaining time
- mray: second, too much meta (especially for the amount of other content). But I also appreciate everyone's persistence, & effort being put in where it's needed
- bnagle: second meta, but also other people seem good at stepping in, keeping it in line / manageable.
- msiep: thanks all for being here, next thing is to document the mechanism proposal in the new repo
- timmy: realized I was on the degrowth pad, was the source of confusion
- wolftune: should consider GTD principles (getting things done). need to get our minds off other things in order to focus, but doesn't necessarily have to happen out loud. Don't have to suppress thoughts about necessarily anything, but give space to address them: i.e. thoughts about meta stuff at the end of the pad, or notes for later. Then we can decide whether they need everyone's attention (they often don't). But we should err toward hearing from everyone & engaging. Salt did a good job facilitating. Felt wonderful that we didn't feel rushed to get everything into the meeting (which made it possible to end meeting early, knowing there's time for other stuff after)
- alignwaivers: sometimes discussing meta things (like if there's wishywashyness about the order of the topics) wastes more time and doesn't really streamline things (given the cost of  time to discuss): therefore if there's unless there's an alarming an obvious solution maybe skip discussion and just go forward if it 'might' be slightly be better. +1 getting to the end quickly is good, thanks smichel for pushing that (shorter meetings with less meta, meta after, makes sense). Let's not discuss ordering; if there's any wishy-washyness, just go with whatever's in the pad.

    - can we settles tabs vs spaces on indenting: 

    - wolftune: it only works with tabs if ALL indents are tabs. 

- adroit: great short meeting, sorry I missed some

### Meeting Adjourned!

### Open Discussion: plan next-steps & ensure they're captured




<!--
Personal Scratch Pad
Alignwaivers
- almost done with roledev link extractings

mray
- Blocked by https://gitlab.com/snowdrift/snowdrift/-/issues/190 
- For making progress on design work, I would like to know where we're heading (esp. mechanism)

Salt
- Computing for Social Good class
    - need to have a more full project plan by 4/24:
        1. Define the concrete deliverables you'll achieve for the end of quarter (with high probability), as we need something to evaluate, and know that sometimes releases get delayed when you're working in real world contexts.
        2. Define clearly how you're planning to evaluate the changes you've made to the system- this will also help define the scope of your research question (some finer/more narrow slice of your big problem), and will be important for the final project paper.
        3. Make it clear what your contribution is out of the team of 7.
- Linux App Summit talk accepted! May 13-15, same proposal as LP
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.sylphs.net/d/bd5f906b1cff4e55b76c/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.sylphs.net/d/07dfe7aefa944abc95e1/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- figure out roles pad progression?
    - https://pad.snowdrift.coop/p/role-development
    - still could use help with feature extraction before grooming/growing stage
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 8.9.13 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy
- confirm whether calendar bug with thursday meeting is resolved

smichel
- Want to cowork on GitLab (issues) cleanup https://gitlab.com/groups/snowdrift/-/epics/2
- Pad: Let's only track number of meeting participants in pad
- Meeting: If you didn't make Monday coworking, please arrive 5-10 minutes early to get meta out of the way?
- Meeting: Post-feedback response round?
- Meeting: Open discussion -> Just announcements & questions? (Not really *discussion* per se)

Meeting practices and tools
Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Move new attendee list and metrics to previous, leave new blank
    * Replace previous meeting eval notes with new, filtered down to essential points
    * Clear the "" "last meeting next-steps review" section
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
