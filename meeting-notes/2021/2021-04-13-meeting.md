<!-- See end of this pad for meeting-practices notes and reference -->
![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — April 13, 2020
==============================================================================
- Meeting location: Mumble
- Server: mumble.sylphs.net port 8008 / murmur.sylphs.net port 8008

Centering
---------------------------------------------------------------------------------------------------------------------------
### Personal Check-In Round
- Attendees: alignwaivers, Salt, smichel, iko, mray, MSiep, Timmy, adroit, wolftune
    - Leaving early: 

- Facilitator: Salt
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):


### Previous Meeting Feedback
- Better framing: when signing off, express clarity about looking forward in general
    - No need for details unless it's helpful.
- Appreciations are gratitude practice
    - Doesn't have to be meta about the meeting, it can be "I appreciate this person".
- Perhaps overuse of rounds
    - Sometimes we could just have open time for people to speak up, check in about others who are quiet
-  I would prefer a minute of silence before updating the reminders, like:
    - Facilitator reads reminders
    - Minute of silence
    - Facilitator asks status of any reminder not marked as complete
- Personal scratch pad is distracting (when full)
- Try for shorter meetings with coworking after
    - ¿Topics only in advance?
- Specific 'captured' symbol would be a better alternative to  - [x] etc
- Take notes on reminders / updates on previous todos?

### Recent Pad Changes
- Moved 2nd open discussion after signoff round

### Metrics
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-- https://gitlab.com/groups/snowdrift/-/contribution_analytics
<!-->

- **Last meeting attendees:**  Salt, mray, bnagle17, Adroit, alignwaivers, MSiep, smichel, wolftune, timmy, iko (10)
- **Snowdrift patrons:** 138
- **Discourse:**
    - Signups: 0 -> 0
    - New Topics: 0 ->  0
    - Posts: 4 -> 7
    - DAU/MAU: 34% -> 26%
- **Matrix** (lines/words): 38 lines / 659 words -> 123 lines / 1672 words
- **GitLab:**
    - Git (pushes/commits/people): 5/7/3 -> 15/17/5
    - Merge Requests (created/merged): 1 / 1 -> 1/1
    - Issues (opened/closed): 0 / 0 ->  12/23


Read Reminders + Minute of silence
------------------------------------------------------------------------------------------------------------------------
1. Facilitator reads the reminders (not the personal scratch pad)
    - Update any to mark if they are complete [x]
2. Minute of silence — think about what you want to discuss. For inspiration, consider:
    - Tensions, progress updates, blockers, questions
    - GitLab: https://gitlab.com/groups/snowdrift/-/issues
    - Forum: https://community.snowdrift.coop/
    - Personal notes/tasks/emails
    - Reminders (below). > indicates a carryover from previous week


### New Meeting Time <Salt> (5 minutes)
- [x] smichel: update the calendar and forum post with the new time

### OSI Funds
> - [x] <https://gitlab.com/snowdrift/snowdrift/-/issues/566>

### General
> - [ ] ¿iko: Look into replacements or running fpbot on another server

    - iko: (2021-04-13) waiting to hear back from fr33domlover for fpbot data transfer

> - [ ] smichel: open new gitlab issues for all todos related to wiki/gitlab split, and an epic to track them all

### bnagle primer project / wiki-book
- [x] bnagle: start working on update to illustrated intro / wiki-book (about page) Will reach out in irc or to individuals
- [x] ¿everyone: send bnagle examples of other sites that do this well

### Personal Scratch Pad

#### mray
- Blocked by https://gitlab.com/snowdrift/snowdrift/-/issues/190 

#### Salt
- Computing for Social Good class

    - need to write a short spec fict due tomorrow

    - need to have a more full project plan due.. soon

- Linux App Summit talk accepted! May 13-15, same proposal as LP
- need to update slide deck, would be nice to have a short/long version ready to go for anyone

    - 5-min version (newer): https://seafile.sylphs.net/d/bd5f906b1cff4e55b76c/?p=%2Fpromotional%2Fslide-deck&mode=list

    - longer version (old): https://seafile.sylphs.net/d/07dfe7aefa944abc95e1/?p=%2Fpromotional%2Fslide-deck-kit&mode=list

- figure out roles pad progression?

    - https://pad.snowdrift.coop/p/role-development

    - still could use help with feature extraction before grooming/growing stage

- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update

#### smichel
- Want to cowork on GitLab (issues) cleanup https://gitlab.com/groups/snowdrift/-/epics/2
- Request: Track the Elm prototye plan/progress on GitLab

Open Discussion (8 minutes)
------------------------------------------------------------------------------------------------------------------------
- wolftune: mray: 60 seconds -> 54 seconds for opening video on website, poster image
- smichel: wiki gitlab split stuff - not sure what should be open issues or not
- wolftune: didn't deal with open notifications, anything to be aware of specifically
- smichel: mostly was moving (not new issues), so nothing to worry about it
- wolftune: in general if there's a lot of noise, might miss something actually significant
- salt: yeah when noisy, would be a good point to ping in channel
- smichel: this is where changing notification settings might be helpful
- salt: could make that a topic
- smichel: would rather do some coworking on it
- salt: maybe add something about that to the readme (whether onborading or not)
    - [ ] Conversation / coworking on gitlab notification settings
- Salt: Linux App Summit talk accepted! May 13-15, same proposal as LP
    - leaving my libreplanet talk up, would like feedback etc
- wolftune: feel free to delegate (since you are owning it)
- salt: didn't submit as multispeaker but could be
- salt: i wanna work on roles dev pad: extracting before grooming blocker
- alignwaivers: I can help with that
- [ ] (alignwaivers): do some extracting on the roles pad 
- wolftune: meta thing, getting in your zone (tasks that don't make you feel drained but energized) - when you identify a nonzone task, make clear and share this, delegate if possible
- smichel: a lot of what seems to slow us down: have many things we are maintaining and when we are adding new stuff and review/summarize other stuff first


Finalize agenda + Minute of silence
------------------------------------------------------------------------------------------------------------------------
1. Finish adding agenda items, if needed
2. Each topic leader says what they want from their topic
3. Minute of silence — think about what you want to say

Agenda
------------------------------------------------------------------------------------------------------------------------
<!--
### TOPIC <LEADER> (# minutes)
<!-->

### Coworking times & meeting agenda/pad changes <smichel> (10 minutes)
- smichel:
    - I propose moving to two weekly coworking times — Monday 4-5pm PT, Thursday 1-2pm PT
    - Would like to simplify the meeting a bit (it has been an ongoing tension, meeting pad busy)
        - Move Open Discussion down a heading and put it under Agenda
            - Like: ### Open Discussion <Facilitator> (8 minutes)
        - Remove the topic template (new topics can be copied from Open Discussion)
        - Use formal "Agenda items" for resolving ongoing discussions
            - Only actionable topics; no open-ended discussion / brainstorming. "I want to come up with actionable tasks on $area_of_work" is ok.
            - Only for topics that people are already informed about. Better: also add the agenda in advance.
        - Leave other topics for coworking (hopefully after the meeting. Can include notes).
        - Move scratch pad to coworking / open discussion section at end. If it's not something to discuss in the meeting it doesn't need to be above the agenda.
            - "Coworking agenda"?
    - Only track number of attendees in metrics
- smichel: it's nice to have a coworking before AND after - but there's also not a time everyone can make
- wolftune: propose a standard for 'good/safe enough to try': if no objections or no one has an improved suggestion we could give it a try
- salt: I know a reason to have an open discussion was to bring up agenda items

    - thought it worked

- alignwaivers: I propose to postpone that as we haven't tried this iteration even 1x yet
- smichel: I noticed when it comes to open discussion it is more effective in coworking sessions ???

    - maybe can confirm coworking times after

- salt: I run things similarly to seagl, might be able to test things in those meetings as well

### Penpot and access etc <wolftune> (5 minutes)
- iko: wolftune, do you have an account yet?
- wolftune: yes, I made one for aaron@snowdrift.coop
- iko: okay, I'll look it up (5 minutes) and email you an invite to the team if you're not in it yet.

    - Or, are you in the team but cannot log in?

- wolftune: when I get invite emails, it sends me to a login even if I'm already logged in, I don't think I'm in the team officially
- iko: yeah, not in team yet, sending an invite shortly
- wolftune: have been having trouble accessing penpot, other people might not be aware of what's going on here: way of interacting / giving feedback on design things - excited to try it

    - want to be able to access

    - want to share with rest of team, figure out how we may be able to utilize, maybe give a summary?

- mray: no experience with Sigma, but it has been described as similar. Might be a goal of the application to become a replacement for this kind of app

    - online prototype tool to make fake websites / interfaces: add comments and make versions you can click and browse. anyone who wants to try can join (can give access)

- wolftune: know the expectations if it's working well, could be a really nice workflow: get prototypes in really nice state before implementation
- mray: still a young project, so there are +/- to it (be aware of that when using)
- msiep: quick question (just used it):

    - observation: when I tried to put  in comments they went to the wrong place (bug)

    - ?: is there a notification if someone responds or do you need to check comments

- mray: see a red speech bubble when there is a notification for you (at the bottom when you login)
- salt: would be really cool to have a blogpost demonstrating how it was used. Would be nice for us & penpot
- [x] wolftune + iko: get wolftune access

### Computing for Social Good class proposal <Salt> (5 minutes)
- Project proposal due 4/25, speculative (dis|u)topian fiction due 4/14

    The general format is a 2-4 page paper in academic format (2 column) detailing any updates on your initial proposal:

    1) Who is on your team?

    2) What is the problem you are trying to solve?

    3) What are some existing solutions (related work)?

    4) What is your design/solution?

    5) What technologies are you leveraging to build your design/solution?

    6) How do you plan (or hope) to evaluate your design/solution?

    7) A general timeline for tasks over the next two months to the deadline of June 5th.

- salt: did the blurb already. copy pasted what is being looked for, need to write this within relatively soon. want us to think we might we able to realistically get done. could be good opportunity
- wolftune: 
- alignwaivers: seems like another iteration of the Mozilla proposal
- smichel: 2,3,& 4 could be answered by wiki pages. 5 easy enough to get answers, 6 would be interesting to discuss
- salt: 6 will relate back to 5

    - its a computer science class but what I actually do is evaluated. not that we cant do coding stuff but it's not required: giving the talk is part of this

- smichel: how are you going to evaluate success of work on snowdrift?
- salt: have some ideas but that's part of why i'm bringing up. could be 'I produced an org chart'
- smichel: if thats the direction, could be we have roles to work on, get someone to work on
- wolftune: I read this as 'can crowdmatching be successful' ???
- salt: could be building an organization to be in a better place to succeed
- wolftune: could have to do with volunteer recruitment too
- salt: as smichel said, it's what I could imagine it to be

    - having links to open positions that are missing might work




Wrap-Up (last 10 minutes)
------------------------------------------------------------------------------------------------------------------------
### Sign-off round: what's next, appreciations, suggestions
- iko: good meeting, appreciate alignwaiver's thorough notes and Salt back on facilitation

    - next: made a bit of progress on the CiviCRM Docker container (with Salt's help), will continue to iron out errors

- timmy: + 1 to ^ appreciations
- Salt:
    - next step, working on speculative fiction tonight, & owe lance an email, poking at civi more (hoping for positive email…)
    - Appreciate everyone being here, lots of energy from people showing up
- wolftune: I have OSI grad blogpost, also gonna look over penpot

    - appreciate clarity on ownership and being able to track tasks and just know what one thing to focus on

    - would like us to figure out how we can help each other prioritize as we have a lot of stuff

- alignwaivers: appreciate salt facilitation and note-taking backups. Liked how Salt mentioned specific people who were left in opening round (e.g. if we're halfway through, there's often "who's left?"). Going to work on roles, maybe gitlab.

    - PROPOSAL: have a volunteer (not facilitator/notetaker)  track rounds to expedite: aka copy the attendees and strikethrough each person as they go i.e.: 

    -  Salt, smichel, iko, mray, MSiep, Timmy, adroit, wolftune

- smichel: most of it would involve co-working, which would be nice

    - appreciate the ways the meetings go, willingness to try different things. 

    - dont wanna dump suggestions here but will cowork

- adroit: gonna predict I won't be able to do much until next week. did some work on monday. decided on the original agreements

    - need to sign OSI contract and send it back

- mray: too much time on meeting meta (like always :)). Also like always, appreciate everyone showing up & putting in time.

    - congrats & appreciation to Timmy, Aaron and Athan for the kite guitar concert

    - next: look into css for page(s), but more in a waiting position in terms of issues

- msiep: going to track things in penpot


### Meeting Adjourned!

### Open Discussion: plan next-steps & ensure they're captured
- [ ] (adroit) sign OSI contract and send back
- smichel: now's a good time to check schedules to confirm the proposed changes might work
- wolftune: the monday time might allow for me to check in but not reliably attend

    - to be clear about my availability: if we were gonna have the meeting, certain times could've worked (but lower priority

    - if you see lowest level of availability for me

- smichel: overview doesn't make this clear in whenisgood

    - have this as a task / project to do over some weekend

- salt: might be a good weekend hackathon / grant etc
- wolftune: the thursday time might not work either, but can switch
- alignwaivers: would like to make sure it's explicitly not expected for team members to make both
- wolftune: my vote is the times are pretty good

<!--
Meeting practices and tools
Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Move new attendee list and metrics to previous, leave new blank
    * Replace previous meeting eval notes with new, filtered down to essential points
    * Clear the "" "last meeting next-steps review" section
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
