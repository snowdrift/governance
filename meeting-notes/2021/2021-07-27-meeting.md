<!-- See end of this pad for meeting-practices notes and reference -->
Meeting info: https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010

Meeting — July 27, 2021
=====================
### Personal Check-In Round
- Attendees: Salt, iko, bnagle17, alignwaivers, smichel, MSiep, wolftune, Adroit
    - Leaving early: 
- Facilitator: smichel
    - Facilitation links (timers and metrics): <https://smichel.me/snowdrift/timer/>
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different): 

### Previous Meeting Appreciations
- updates/progress is nice
- consensus that short meetings are good
    - Salt: I like if we don't have stuff to talk about, there's tasks to be done.
- alignwaivers: Good to see davidak

### Meeting/Pad Updates & Reminders
- In checkins, consider "how are you feeling right now" instead of a personal report ("how are things?")

### Minute of silence
https://www.online-timers.com/timer-1-minute

### Metrics
- Meeting attendees: 7 -> 7
- Snowdrift patrons: 139 -> 139
- Discourse:
    - Signups: 0 -> 0
    - New Topics: 0 -> 1
    - Posts: 4 -> 1
    - DAU/MAU: 17% ->  25%
- Matrix (lines | words): 126 | 1574 -> 127 | 1431
- GitLab:
    - Git (commits | people): 36 | 5 -> 28 | 4
    - Merge Requests (created | merged): 4 | 2 -> 4 | 2
    - Issues (opened | closed): 8 | 34 -> 3 | 5

NEXT STEPS Review
--------------------
1. Plan how to deal with each incomplete step. [ ] is incomplete; [x] is complete.
    - Lines starting with '>' are a carried over from an earlier week.
2. Ask who would like time for post-meeting discussions.
3. Confirm Agenda length & order (including open discussion).

### General
- [x] smichel: walk through gitlab issue settings at thursday's coworking session

> ### Stuff smichel would rather someone else do (but will be doing after reconsidering / discussion ;)
> - [-] smichel: try putting in a half-hour writing a post without the team member list
> - [x] smichel: reach out to david thomas about contract payment


Agenda
---------

### Open Discussion (Facilitator) - 5 minutes
- wolftune: talked with Deb, have an update: she's been busy / not ready to do stuff but still wants to help
- smichel: hoping to get bylaws done by September when our filing is due in Michigan
- wolftune: we should probably share that with the board as a point worth considering
- smichel: would be good to talk about new co-working times
- Salt: agreed, it's bad for me right now
- alignwaivers: I can check and ensure everyone's availabilities are updated within the next week (or today actually)
- [ ] smichel: send a quick message to the Board with filing date as an ideal deadline

- [ ] Aaron: schedule next board meeting
- [ ] Athan: Make sure everyone updates their availability
- [ ] smichel: Pick a new time for coworking

### CommunityRule and Org Chart (Salt) - 10 minutes
- Provide team overview of current state of CommunityRule, how it connects to the Org Chart, and upcoming changes to each
  - have been working on this project, which relates to our org chart and how decisions are made
  - Have been referring to it as organizational communication chart as opposed to a power chart
  - Currently in Penpot but also working on a GraphViz version to make it easier to update
  - roles don't have anything to do with authority, more about communication channels
  - with current push, smichel has the project manager role for July (the "BDFL" role)
  - The goal of the project: I want to remove the questions of if one 'can' work on things and help clarify generally / address questions that might come up when someone says "I want to work on something"
    - 'snowdrift organizational/gov documentation for contributing'
    - communication channel to connect people who might want to work on something together
- Community rules are processes and procedures in order to make changes
    - there are 4 groups: board, supporters, team (core with proven track record of contributions), and volunteers (supporters who are more involved, have autonomy)
      - focusing on the team for now, there is consensus on current agreements
      - but we don't know how decisions are made for work (we have a decision tree for consensus with polling stage etc)
      - if someone wants to make a change or add a change, we don't have a system
      - Steps I think would be good for making a change
          - 1. Polling - Ask the highest (root) person in the chain of the org chart: keep working back until you find that person
            - which hat you are asking from
          - 2. Lazy consensus - risk of getting change reverted, but if you think there is a low chance of objections, you can proceed in the meantime
         - 3. based on holacracy ???
         - 4. if there's objection, 
             - work it out together or 
             - ask for mediation (from highest root domain)
             - revert to team consensus
         - I haven't worked out yet how to select the "BDFJ"/project manager for July
- Goal is to take temperature of team and gather any suggestions

### Round for anyone who hasn't spoken yet (Facilitator)



Wrap-up (5 minutes)
------------------------
### Check that all NEXT-STEPS have someone assigned (Facilitator)

### Reflection Round — short, concrete, & specific
Clear on what's next for you? Meeting appreciations & suggestions?:

- Salt: great to see everyone here, thanks for allowing me to take the floor for a while, took a little longer than expected
- iko: good meeting, thanks for the community rule update
- bnagle17: nice meeting
- alignwaivers: thanks everyone for showing up. given Adroit and smichel17 are getting paid, I'm glad ya'll are demonstrating the value of the work being done (money well spent)
- smichel: briefing in meeting without talking with facilitator, was partially distracted and wouldve been nice to have a facilitator decide in advance if I knew what was going to be said (on the topic)
- MSiep: good to be here and see others here, appreciate hearing things are happening, thanks
- wolftune: short meeting is good and have a checkin, thanks to smichel for facilitating
- Adroit: thanks everyone for being here


Meeting Adjourned!

Post Meeting Discussion
----------------------------








<!--
Personal Scratch Pad
Salt
- computing for social good class project
    - https://pad.snowdrift.coop/p/role-development
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.sylphs.net/d/bd5f906b1cff4e55b76c/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.sylphs.net/d/07dfe7aefa944abc95e1/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 8.9.13 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy

smichel
- Meeting meta ideas:
    - WIP: Update sections below
    - Chat in element?

Meeting practices and tools
Pre-Meeting Tasks
- Update metrics, from these links:
     - https://snowdrift.coop/p/snowdrift
    - https://community.snowdrift.coop/admin?period=weekly
    - https://gitlab.com/groups/snowdrift/-/contribution_analytics
- Add items to the agenda. Include a timebox & GOAL(s). Decide on order.

Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Replace previous meeting eval notes with new, filtered down to essential points
        - Leave re-writing for meeting-prep, but do remove routine "thanks all" comments & next-ups
    * Clear previous-week metrics; update this week's attendee count if anyone arrived late
    * Clear the "NEXT STEPs Review" section, unless they have next-steps which are not done/captured.
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)

Timeboxing
- Prefer 5 minute increments. Leave time for Wrap-Up.
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
- Informal "thumb polls" inform the decision for extra timebox
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
