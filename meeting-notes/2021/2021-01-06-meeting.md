<!-- See end of this pad for location, meeting-practices notes and reference -->

Meeting — January 6, 2020
=======================

1. Personal Check-In Round
--------------------------------
- Attendees: wolftune, alignwaivers, MSiep, Salt, smichel
    - Leaving early: 

- Facilitator: smichel
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):

2. Silent Centering
----------------------
<!-- Timebox: https://www.online-timers.com/timer-2-minutes
<!-- Remind yourself of anything you want to bring up today. Check:
<!--— GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!--— Forum: https://community.snowdrift.coop/
<!--— NEXT STEPs — mark as [DONE or CAPTURED] <LINK>
<!--— Personal notes/tasks/emails
<!-->

### Metrics

**Attendees (last week):** wolftune, Salt, davidak, smichel, alignwaivers

<!-- from https://snowdrift.coop/p/snowdrift -->
**Snowdrift patrons:** 132

<!-- from https://community.snowdrift.coop/admin?period=weekly -->
#### Discourse (over past week)
- Signups: 0 -> 1
- New Topics: 1 -> 1
- Posts: 2 -> 3
- DAU/MAU:  23% -> 26%

### Previous Meeting Wishlist
- Updated pad sections. Mostly the same. This one: starts with facilitator summary.
- More focus on discussion / less meta talk
- Less formal feel / less time spent formalizing breakout rooms
- Err toward fewer topics
- Breakout rooms -> fewer lines in the pad [DONE]
- Save meeting feedback until the end
- Create breakout rooms during snow-shoveling discussion.
    - Note-taker does this based on discussion.
- Decide on room order faster.

### Bylaws/org coordination
- NEXT STEP (salt, smichel, wolftune): have meeting to discuss this in next week [DONE]

### Upcoming Events
- NEXT STEPS (salt, wolftune, mray): discuss New_ Public Festival

### TODOs under Capturing NEXT STEPS in gitlab issues
- NEXT STEP (alignwaivers): come up with tag / label / 3rd state - this next step being 'nudged' [DONE]
    - Decision: Use quote blocks (`> ` prefix) for each week delayed

### Mechanism
- NEXT STEP (salt): schedule a collective workshop/s [CAPTURED]  https://whenisgood.net/3dmmkp7/
- NEXT STEP (salt, alignwaivers): prep structure/outline for workshops [CAPTURED] https://gitlab.com/snowdrift/governance/-/issues/84


3. Snow-Shoveling Discussion
-----------------------------------
<!-- Timebox: https://www.online-timers.com/timer-8-minutes
<!-- Share progress updates, tensions, blockers; ask questions… Add breakout rooms if needed.
<!-->

### Open Discussion
- Salt:
    - Want to finalize HPP today, should we post tomorrow AM or this afternoon? still needs links
    - Gitlab dual repo for issues progress update would be nice
    - Schedule time to work on bylaws so that we can goto the board with something?
        - Get proper introduction to board group chat to avoid awkwardness?
    - Aaron's spreadsheet hesitation - figure out what we're doing about that
        - wolftune: I don't refuse to use it; it's just that other things are higher priority.
- smichel: I have a tension about discussing vs doing — I somewhat want to continue coworking today.
- wolftune: should really have more coworking and use meetings to make decisions
- Salt: we use the term meeting loosely - we have limited time, we use weekly time to *meet*
- wolftune: don't want to use meeting time to discuss unless has been prepped properly
- Salt: like the idea of coworking and coming back together at the end to discuss
- Wolftune: have OSI report to do, HPP, the new public thing, forum stuff, stuff on wiki, gitcoin, and whenisgood. Want insight on prioritizing
- smichel: does 20 mins sound good for HPP to get into ghost?


### Facilitated Discussion

#### Round (anyone who hasn't spoken yet)


#### Last-Meeting Next-Steps Review (anything not done, captured, or already discussed)



4. Breakout Rooms
-----------------------
<!--
### TOPIC (LEADER) <# minutes> Room 1A
#### PLAN, GOAL
<!-->

### HPP Coworking (Salt, alignwaivers) <\~20 minutes> Room A
#### Get HPP post polished enough that it's ready to put into ghost.
- https://snowdrift.sylphs.net/pad/p/FSF_HPP
- salt: went through and looked at questionable spots, looks mostly pretty complete
- Salt: mostly been finding places for links / stephen adding; athan going over language / awkward sentences.
  - I'm in ghost and mostly ready to get up there
  -  struggling with wanting to cite stuff (footnotes) vs links
  -  most of us want to continue?
  - This afternoon or tomorrow morning?
- smichel: i think tomorrow morning better since lots of news today
- wolftune: not a big deal to post and promote it later
- salt: disagree, I have experience with SEO research

### OSI Status report (wolftune) <20 minutes> Room B
#### Submit OSI status report
- wolftune: hard time finding report from 2019, not filed in a clear place
  - thinking to put both the 2019 and 2020 reports???
  - (we don't have a clear place, should just put in git legal repo even though it's odt
  - adapted to 2020, 3/4 of the way through writing / summary.

5. Wrap-Up
-------------
<!-- Timebox: https://www.online-timers.com/timer-10-minutes -->

### Report back: Summarize conversations, add notes if needed


### Meeting eval round: feedback, suggestions, appreciations
- msiep: I was mostly listening in; HPP draft looks good & it would be great if we got on the list. Also looked @ new public festival.
- salt: pretty pumped about what we are working on in snowdrift. Thanks for reading over, meeting went well. Dunno how it would go if we weren't working on the most pertinent things
- align: I really want to try Codi. Really like this format. When we have specific things to work on, it works well. @wolftune's "meetings should be for decision-making", I agree *in an ideal world*…
- wolftune: thanks everyone - was good of @smichel to ask to facilitate, good to rotate facilitation and see how things work or not. Felt, like other types of coworking results in a lot of stuff, helps motivate through internal tensions. Don't want things to be forced as decision topics if it isn't there yet. Good to turn meeting into coworking if there's nothing we need to decide on.
- smichel: pretty happy with it overall. Person who made the changes (me) facilitating meeting and everyone being more comfortable with the format helped it go smoother. +1 to what others side. stuff on line 60 wasn't cleared before meeting


#### Meeting Adjourned!

- NEXT STEP (msiep): look into gitcoin thing

<!--
Meeting practices and tools
======================

Post-Meeting Tasks

    Clean up meeting notes

    Add to wiki: https://wiki.snowdrift.coop/resources/meetings/2021/

    Prepare this pad for next meeting:

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers and add one # mark

    Open discussion notes

    Clear authorship colors

    Update next meetings date


Location

    Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)

    and/or for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift


Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last


NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Timekeeper
- Use https://online-timers.com, start for each item, paste each url in the chat

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->