<!-- See end of this pad for meeting-practices notes and reference -->
![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — March 03, 2020
=======================

Meeting location: mumble.sylphs.net port 8008 (must use a mumble client to join - mumla is good for android)

1. Centering
--------------
### Personal Check-In Round
- Attendees: Salt, mray, smichel, iko, MSiep, wolftune, Adroit, bnagle
    - Leaving early:

- Facilitator: Salt
- Note Taker: smichel
    - Note-saver/pad-updater (if different):

### Previous week highlights
<!-- https://snowdrift.coop/p/snowdrift
<!-- https://community.snowdrift.coop/admin?period=weekly
<!-->

#### Metrics
- **Attendees last meeting:** Salt, alignwaivers, wolftune, smichel, iko, mray, MSiep, adroit, bnagle
- **Snowdrift patrons:** 137
- **Discourse:**
    - Signups: 0 -> 0
    - New Topics: 2 -> 0
    - Posts: 10 -> 0
    - DAU/MAU: 25% -> 20%
- **Matrix (subjective):** Some discussion
- **Missing:** Gitlab stats

#### Meeting Feedback
##### Appreciations, Reflections, Feedback
- Specific is good!
- On sign-out, no need to capture next things already captured, just check that you're clear on steps or briefly mention upcoming things not otherwise brought up
-  The new [] next step format is nice to parse
-  A little more silent space between people, and maybe have a secondary open discussion towards the end of the meeting around the wrap-up/report
- rather than read each next step word by word, simply note the line number
-  I like the occasional rotation of facilitator role to give the dedicated facilitator a break
- facilitator could take a step back in conversation and also be explicit when facilitator hat is off

#### Facilitator reads over last week next steps

###  Minute of silence
<!-- Look over the reminders & think about what to bring up today -->

Reminders
---------
<!-- Tensions, progress updates, blockers, questions
<!-- GitLab: https://gitlab.com/groups/snowdrift/-/issues
<!-- Forum: https://community.snowdrift.coop/
<!-- Personal notes/tasks/emails
<!-- NEXT STEPs — mark each as [DONE or CAPTURED] <LINK>
<!-- > indicates carryover from previous week
<!-->

### OSI Funds and graduation
> - [/] (wolftune+smichel): Draft a contract to put in place anyway

### Elm Migration
#### Select a page to migrate and work on
- [/] send invite to adroit and msiep to snowdrift penpot instance (mray)
- [x] submit a request to osuosl for adroit@snowdrift.coop (smichel)

### OSI Funds
- Next steps (smichel, wolftune, adroit):

    - [x] Draft scope of work

    - [x] Send draft contract & scope of work to connor — get initial approval

    - [x] Get OSI feedback

    - [x] Clarify bureacratic

    - [x] Finish 2nd (potential final) draft

    - [ ] Get OSI approval

    - [x] Get approval from Connor

    - [ ] Sign!


### Newcomer bnagle
- [ ] schedule a video chat (and invite others), potentially next monday (smichel & bnagle)


2. Open Discussion (8 minutes)
------------------------------

- smichel: What's the concrete next step at the OSI?
- wolftune: contact deb, we have contract drafted, all set but have specific questions, discuss by email or time to chat?
    - [ ] Send email to Deb@OSI: "all set but have specific questions, discuss by email or time to chat?" (smichel drafts, wolftune sends)
- Salt: have BoF for libreplanet (20-21st of this month). Drafted skeleton talk on Monday, should do some for Birds of a Feather. We'll get to select a time for the talk.
- Salt: New social network iko found, hummingbard. Very new (launched 5 days ago), based on Matrix. Made a profile.

- Salt: Any updates on front-end, Adroit?
- adroit: Would like to put the demo repo on our gitlab. Do I have permission to do that? Technical permission.
- [ ] Check if adroit has permission / figure out a place to host the demo (smichel)
- mray: I'm up to do anything front-end related
- bnagle: We had some trouble scheduling the onboarding meeting
- smichel: Maybe do that now, as agenda?

3. Agenda
---------
<!--
### TOPIC <LEADER> (# minutes) Room ?
#### PLAN, GOAL
<!-->


4. Wrap-Up (last 10 minutes)
----------------------------
### Report back: Summarize conversations, add if needed
- Make sure next steps are captured
- Have a little open discussion / coordinating next-steps

### Sign-off round: what's next, appreciations, suggestions
- iko: appreciate smichel's quick note-taking, Salt's great facilitation
- msiep: I appreciate everyone being here
- Salt: not sure what's next. Appreciate how many people are here, especially given late notice (matrix ping)
- bnagle: my next step is figuring out onboarding
- mray: thanks for participation
- wolftune: apologize for being distracted, many things I know would make a difference if I could get to them, frustrated to have my attention divided. Appreciate that things happened anyway.
- smichel: I appreciate that we had so many people here, and ending the meeting earlier. Not sure how I feel about name assigned in front for tasks, it's easier to scan. My next step is onboard adroit for creating gitlab repos.
- Adroit: had some audio breaking up, good, short meeting. It's good to check in even if we don't have much to discuss
- alignwaivers: hi! (busy in two other meetings but listening in)

#### Meeting Adjourned!

<!--
Meeting practices and tools
Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Move new attendee list and metrics to previous, leave new blank
    * Replace previous meeting eval notes with new, filtered down to essential points
    * Clear the "last meeting next-steps review" section
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Unbold the topic headers and add one # mark
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
