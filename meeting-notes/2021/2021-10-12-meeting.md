<!-- See end of this pad for meeting-practices notes and reference -->
Meeting — October 13, 2021
=========================
:::info
<https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
:::

### Personal Check-In Round
- Attendees: wolftune, smichel, Salt, MSiep, alignwaivers, Adroit
    - Leaving early: 
- Facilitator: salt
    :::info
    Facilitation links (timers and metrics): <https://smichel.me/snowdrift/timer/>
    :::
- Note Taker: smichel, wolftune helps
    - Note-saver/pad-updater (if different):

### Meeting Feedback & Updates
- wolftune: Could have brought up more things but I didn't. Thanks to salt for facilitating and smichel for notetaking
- MSiep: Appreciate everyone being here, short meeting.
- alignwaivers: Good to see stuff going on, thanks for showing up.

### Minute of silence
:::info
https://www.online-timers.com/timer-1-minute
:::

Previous Week Review
--------------------
### Metrics

|                                        | Last week | This Week |
|----------------------------------------|-----------|-----------|
| **Meeting attendees**                  | 5         | 6         |
||
| **Snowdrift patrons**                  | 140       | 140       |
||
| **Matrix**                             |           |
|             #snowdrift (lines / words) | 20 / 179  | __ / ___  |
||
| **Discourse**                          |           |           |
|                                Signups | 0         | 0         |
|                             New Topics | 0         | 1         |
|                                  Posts | 4         | 3         |
|                                DAU/MAU | 22%       | 23%       |
||
| **Gitlab**                             |           |           |
|                   Git (commits/people) | 5 / 3     | 1 / 1     |
| Merge Requests (Created/Merged/Closed) | 0 / 0 / 0 | 0 / 0 / 0 |
|               Issues (Opened / Closed) | 0 / 0     | 5 / 1     |

### NEXT STEPs
:::success
Plan how to deal with each incomplete step. 
:::
:::info
**Legend:**
- [ ] incomplete
- [x] complete
> - [ ] Carried over from an earlier week.
:::

### New meeting day, Tuesdays (wolftune) - 5min
- [x] smichel: Update shared calendar and discourse post with new time
    - Seems like there are some issues with the calendar / Google calendar
- [x] smichel: look over availability and propose new coworking times

> ### Dropping some metrics (smichel) - 5min
> - [ ] Salt: Open an issue related to automating stats gathering entirely with a bot

### Netnl funding requests (alignwaivers) - 5 mins
- [x] alignwaivers: open an issue to make it a task due

Agenda
------
:::success
- Ask who would like time for post-meeting discussions.
- Confirm Agenda length & order (including open discussion).
:::


### Updated Coffee Shop hours (smichel) - 5? minutes
- smichel: there are now updated coffee shop hours
- clustered Tue/Th
- aimed less for just quantity of people and more for coincidence of particular people available together
- sessions have a rough topic focus, design, outreach, development and so on
- Salt: checks on updating the forum indication, Stephen says yes
- Stephen: rewrote the public post to describe it not as all just meetings (we don't want excessive formal meetings)
    - https://community.snowdrift.coop/t/weekly-team-meeting-and-coffee-shop/1010

### Open Discussion (Facilitator) - 5 minutes
- wolftune: seaGL: who/what/when task of scheduling coworking on our presentation? We should tell people it's a thing so they can put it on their calendars.
- Salt: We're all juggling a lot, things are weird. Having a deadline for scheduling would help me.
- w: Can we do it within the next 7 days?
- Salt: I have a lot of weekend time..
- align: I should have time this week
- sm: might be time to do it after this meeting
- wolftune: I like the idea of informal pre-meeting stuff and it's possible to have time after
- wolftune: I have a bunch of thoughts / things to bring up
    - smichel: good idea!
- wolftune: on the verge of finally really incorporating. Many small tasks. Technical details, but also confirming that we won't do  501c4
- wolftune: lawyer moving out of michigan, we need to confirm who will become our registered agent (who gets mail for us).
- wolftune: The reason for MI incorporation is that they have a law for making a consumer coop on non-profit terms
- wolftune: I have friends, in-laws, and previous connections.
- wolftune: It would be good to talk (mostly me brain dumping) about this, maybe with smichel
- wolftune: I think with some work, our legal situation is getting figured out
- wolftune: Big picture, if we get development settled, I think we have all the pieces in place to be functioning.
- wolftune: I have enough on my plate without development, maybe someone wants to speak to that?
- Salt: …but we're out of time :P

### Round for anyone who hasn't spoken yet (Facilitator)


Wrap-up - 5 minutes
-------------------
:::success
Check that all NEXT-STEPS have someone assigned
:::

### Sign-out round
:::info
*Short, concrete, & specific:*

- How much will you be around this week?
- What will you focus on? (and do you need anything captured/scheduled?)
- What can others do to help you succeed?
- What are your meeting appreciations and/or suggestions?
:::

- smichel: hopeful the new meeting time will be more productive, felt like I haven't accomplished as many big tasks as small things lately. Need David to make a decision on the bitcoin, and I can focus on technical things. There was more we could've talked about on the agenda with the 30 minutes before
- Salt: Unsure how much I'll be around, probably minimal. I want to get back to governance, but civi is now ready to be worked on & needs a few things. Thanks for being here, trying out new meeting format — that's how we move forward.
- MSiep: I'm generally around, somewhat busy with work. I'd forgotten that the meeting would be in the second half of the hour.
- wolftune: I have a mixed week, planning to participate in a humane tech podcast relevant to Snowdrift, although it will be cut short by a music lesson. I'll be busy this week, but open Sunday and next Tuesday— planning to work with Brendan and somewhere I'll need to move forward on legal issues. If things outside the meeting were moving more, we'd have more to discuss. Thanks for facilitating, note taking.
- alignwaivers: good meeting, glad there's no meta, sorry I need to leave early, thanks everyone
- Adroit: Sorry for missing last week, changes to it and coffee shop hours look promising. Appreciate meeting format, so many people being here, and facilitation

### Meeting Adjourned!
:::success
Move to the "coffee shop"
:::

Post Meeting Discussion
-----------------------

- smichel: weirdness around sign-out not being timeboxed, it kind of becomes a second open discussion
- Salt: Maybe we could even have boxes for people to fill in 
- wolftune: ??? (something about people more actively writing in the pad themselves) ???
- smichel: What about a minute of silence right before check-out? Facilitator reads questions and people think about them.
- wolftune: Encourage people to write it down, either in the pad or on paper.
- Salt: Some people might just say, "Read my thing", others might want to say it out loud
