<!-- See end of this pad for meeting-practices notes and reference -->
Meeting — August 18, 2021
=========================
:::info
<https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
:::

### Personal Check-In Round
- Attendees: smichel¸ alignwaivers, wolftune, bnagle17, Salt, iko, MSiep
    - Leaving early: 
- Facilitator: smichel
    - Facilitation links (timers and metrics): <https://smichel.me/snowdrift/timer/>
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different): 

### Previous Meeting Appreciations
- The usual

### Meeting/Pad Updates & Reminders
- Please avoid typing on the line being typed in as well as the line following
- Salt: Felt a bit cut-off by wolftune and that facilitation style was a bit strong-handed.
- Move to coffee shop for breakout rooms

### Minute of silence
:::info
https://www.online-timers.com/timer-1-minute
:::

Previous Week Review
--------------------
:::success
1. Plan how to deal with each incomplete step. 
    :::info
    - [ ] incomplete
    - [x] complete
    > - [ ] Carried over from an earlier week.
    :::
2. Ask who would like time for post-meeting discussions.
3. Confirm Agenda length & order (including open discussion).
:::

### Metrics
- Meeting attendees: 7 -> 4
- Snowdrift patrons: 139 -> 139
- Discourse : 0 | 0 | 0 | 20% -> 0 | 1 | 6 | 33%
     *(Signups | New Topics | Posts | DAU/MAU)*
- Matrix (lines | words): 38 | 487 -> 132 | 2566
- GitLab:
    - Git (commits | people): 5 | 3 -> 5 | 3
    - Merge Requests (created | merged | closed): 2 | 1 | 0 -> 2 | 0 | 1
    - Issues (opened | closed): 7 | 3 -> 4 | 2

### NEXT STEPs

- [ ] smichel: prep some agenda for next week's meeting to discuss bylaws for the whole hour, bring up topics team may be interested in.

- [ ] Athan: try tables for metrics in pad

> - [x] smichel: Pick a new time for coworking



Agenda
------


### Open Discussion (Facilitator) - 5 minutes
- smichel: wish I could make more progress on backend but also working on bylaws
- [ ] smichel: ping David Thomas on Signal
- wolftune: recently heard about a coop structure that has a return proportional to work put in on the coop
  - fundamental issue is paying people who are doing the work (and ironically we aren't really being paid for our work)
- salt: I want to stop trying things out because it's taking so long to get launched
  - I have tensions about doing incorporating multistakeholder structure into the coop
- wolftune: the challenge is in the coop world (paying those who are doing the work). ??? some ownership models don't work with public good



- iko (a comment from a previous meeting): re: pad line typing comment below, I wonder if switching to mariadb would make a performance difference, currently sqlite default?
- smichel: I don't think it was a performance issue
- iko: yeah, probably not, if it works fine for people now maybe leave it alone


### Round for anyone who hasn't spoken yet (Facilitator)


Wrap-up - 5 minutes
-------------------
:::success
Check that all NEXT-STEPS have someone assigned (Facilitator)
:::

### Reflection Round — short, concrete, & specific
:::info
Clear on what's next for you? Meeting appreciations & suggestions?
:::

- smichel: appropriately quick meeting
    - Experiment with moving minute of silence?
- alignwaivers: thanks for showing up at the new time
- wolftune: I see a big gap in myself between how meetings would go if we all had the time/energy we'd like to have and what we do. Thanks everyone for putting in best-effort. E.g. Beginning would go better if facilitator could prep in advance and know what matters to mention or not.
- bnagle17: thanks to everyone for showing up
- Salt: popping in when it's 15 minutes late and it just getting started feels weird. It's important to announce meeting right at meeting time. Miss having mray around
- iko: appreciate the short meeting. I think mray is waiting for design/website workflow to settle, but is aware/around.
- msiep: thanks everybody for being here, glad I could make it


### Meeting Adjourned!
:::success
Move to the "coffee shop"
:::

Post Meeting Discussion
-----------------------





<!--
Personal Scratch Pad
Salt
- computing for social good class project
    - https://pad.snowdrift.coop/p/role-development
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.test.snowdrift.coop/d/6861aa006f4e441da72a/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.test.snowdrift.coop/d/130a3bf5e9ab455bbc2e/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 9.2.1 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy

smichel
- Meeting meta ideas:
    - WIP: Update sections below
    - Chat in element?

Meeting practices and tools
Pre-Meeting Tasks
- Update metrics, from these links:
     - https://snowdrift.coop/p/snowdrift
    - https://community.snowdrift.coop/admin?period=weekly
    - https://gitlab.com/groups/snowdrift/-/contribution_analytics
- Add items to the agenda. Include a timebox & GOAL(s). Decide on order.

Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Replace previous meeting eval notes with new, filtered down to essential points
        - Leave re-writing for meeting-prep, but do remove routine "thanks all" comments & next-ups
    * Clear previous-week metrics; update this week's attendee count if anyone arrived late
    * Clear the "NEXT STEPs Review" section, unless they have next-steps which are not done/captured.
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.test.snowdrift.coop (leave port blank, or 64738) (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

Personal Check-in
- This is a place for everyone to get in sync with where everyone else is at mentally, not for Snowdrift updates.

Previous week review
- Facilitator reads/summarizes each section

Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)

Timeboxing
- Prefer 5 minute increments. Leave time for Wrap-Up.
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
- Informal "thumb polls" inform the decision for extra timebox
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
