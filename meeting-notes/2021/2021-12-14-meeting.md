<!-- See end of this pad for meeting-practices notes and reference -->
Meeting — November 23, 2021
=========================
::: info
- **More information**
 <https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
- **Past weeks' notes**
   <https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021>
- **Facilitation links** (timers and metrics)
   <https://smichel.me/snowdrift/timer/>
:::

### Check-in - 14 minutes
:::	success
- Informal time for socializing (updates on your week..)
- Pad prep (update metrics)
- Ask who would like time for post-meeting discussions.
- Confirm Agenda length & order (including open discussion).
:::

- Attendees: Salt, wolftune, MSiep, smichel, Adroit
    - Leaving early: 
- Facilitator: Salt
- Note Taker: smichel-ish
    - Note-saver/pad-updater (if different):
    
### Minute of silence
<https://www.online-timers.com/timer-1-minute>
::: success
Get ready to be present in the meeting.
:::

### Centering Round
::: success
**What should others know about interacting with you today?**
- Are you present?
- What's distracting you?

::: warning
*Not for a full status update of everything that happened to you this week (unless it's affecting you today)*
:::

Previous Week Review
--------------------
::: success
Facilitator reads/summarizes
:::
### Meeting Feedback & Updates
- **Moved agenda confirmation to pre-meeting**
- Renamed early rounds to reflect their purpose
- Moved this section into previous week review
- Remove headers from next steps (they should stand on their own)

### Metrics

|                                        | Last week | This Week |
|----------------------------------------|-----------|-----------|
| **Meeting attendees**                  | 4         | 5         |
|| 
| **Snowdrift patrons**                  | 142       | 142       |
|| 
| **Matrix:** #snowdrift (lines / words) | .. / ...  | .. / ...  |
|| 
| **Discourse**                          |           |           |
|                                Signups | 0         | 0         |
|                             New Topics | 0         | 2         |
|                                  Posts | 0         | 3         |
|                                DAU/MAU | 21%       | 26%       |
|| 
| **Gitlab**                             |           |           |
|                   Git (commits/people) | 3 / 2     | 7 / 4     |
| Merge Requests (Created/Merged/Closed) | 0 / 0 / 0 | 2 / 1 / 0 |
|               Issues (Opened / Closed) | 0 / 0     | 0 / 0     |


### NEXT STEPs
::: success
Plan how to deal with each incomplete step. 
:::

#### Last Week

n/a

#### Earlier (Carried over)
- [ ] ALL: read Aaron's [forum post](https://community.snowdrift.coop/t/thoughts-on-open-collective/977/9) on OpenCollective/FundOSS
- [ ] compile list of where we're actually different from OpenCollective/FundOSS
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/241>
- [ ] ??? : reach out to OpenCollective — after more internal discussions (with Board?)
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/687>
    
- [ ] ALL: What would it take for you to put in ~full time on Snowdrift for 6 months, dropping all other commitments which would interfere with that?

- [ ] Try out process for moving past stalemate without discussing forever
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/686>
    
- [ ] ???: brainstorm uses of money, then make a list with categorizations of budget categories

- [ ] ALL: please consider how much we all care about being part of the decision-making process (for funds). E.g. would you want to vote on it, delegate the decisions, or something else?

- [ ] ???: nail down what roles we're recruiting for
- [ ] wolftune, Salt, smichel: Cowork on description for a tech lead / advisor role
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/688>

Agenda
------

### Hcoop potential collaboration (smichel) - 10 minutes

- [ ] smichel: Get more info from hcoop about what would be helpful for them

### Open Discussion (Facilitator) - 5 minutes

- Salt: low availability, grading
- wolftune: I have relatively more time, question of how much time to spend. Can cowork. Sorry to miss some of the meeting (phone call). Thanks for showing up.
- MSiep: ???
- smichel: Around more this week & in December, then not much in January
- Adroit: ???

### Round for anyone who hasn't spoken yet (Facilitator)



Wrap-up - 5 minutes
-------------------
### Minute of Silence
https://www.online-timers.com/timer-1-minute
::: success
**Think about (and perhaps write out) your sign-out.** *Short, concrete, & specific:*
- How much will you be around this week?
- What will you focus on? (and does anything need capturing/scheduling?)
- What can others do to help you succeed?
- What meeting appreciations and/or suggestions do you have?
:::

### Sign-out round



### Meeting Adjourned!
::: success
- Check that all NEXT-STEPS have someone assigned
- Move to the "coffee shop"
:::
