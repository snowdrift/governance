<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — September 30, 2019

Attendees: msiep, alignwaivers, wolftune, salt

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 1 -> 0
- New Topics: 2 -> 2
- Posts: 23 -> 28

Sociocracy 3.0, wolftune @ page: 19 of 37

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune + alignwaivers): schedule grooming session
- NEXT STEP (msiep): groom design repo issues

## Code development progress
- NEXT STEP (wolftune): if chreekat hasn't run the crowdmatches yet, reach out to him to find out how to do it DONE via https://gitlab.com/snowdrift/snowdrift/issues/169

## Team communication commitments etc
- NEXT STEPS (wolftune + alignwaivers): keep studying Sociocracy ideas around this, find current stuff we have, organize our statements

## Marking prominently that we're under-construction
- NEXT STEP (msiep): respond to mray and other forum suggestion etc.

```

<!-- New Agenda Down Here  -->

## Nathan Schneider as Board recruit?
- Michael went on System 76 tour with him, welcome to use that reference as a prompt
- NEXT STEP (wolftune): reach out to Nathan

## General interest, CiviCRM
- Salt met more people, still getting enthusiastic response, Snowdrift still really matters
- Wolftune agrees, focus needs to be on converting that interest into team recruitment etc
- CiviCRM is possible to start discussing with OSU, just prioritizing our GitLab CI/CD runners first
- NEXT STEP (wolftune): prompt OSUOSL discussion/progress toward CiviCRM too
- NEXT STEP (wolftune): keep working on team recruitment plans

<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->