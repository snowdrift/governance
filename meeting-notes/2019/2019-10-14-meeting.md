<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — October 14, 2019

Attendees: alignwaivers, msiep, salt, wolftune

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 0
- New Topics: 3 -> 1
- Posts: 20 -> 10

Sociocracy 3.0, wolftune @ page: 19 of 51

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune + alignwaivers): schedule grooming session
- NEXT STEP (msiep): groom design repo issues


## Team communication commitments etc
- NEXT STEPS (wolftune + alignwaivers): keep studying Sociocracy ideas around this, find current stuff we have, organize our statements https://gitlab.com/snowdrift/governance/issues/57

## Marking prominently that we're under-construction
- NEXT STEP (mray): respond to proposal https://gitlab.com/snowdrift/design/issues/111

## Nathan Schneider as Board recruit?
- Michael went on System 76 tour with him, welcome to use that reference as a prompt
- NEXT STEP (wolftune): reach out to Nathan

```

<!-- New Agenda Down Here  -->

## update from smichel
- he's going through personal questions, personal and location (whether moving etc)

## CiviCRM
- OSUOSL is going forward with clean install of Drupal and CiviCRM
- after install, we'll have to do configuring
- then import of data
- after CiviCRM is working, various design tasks for survey forms / volunteer intake forms…

## SeaGL
- NEXT STEP (athan + wolftune): get accommodations planned, see if anyone wants to come up with us
- no updates, but *likely* that we'll have a table
- NEXT STEP (wolftune): ping OSUOSL about getting Civi up by SeaGL with time for us to configure a sign-up / survey etc. through it
- ask them whether realistic enough for us to prioritize our plans for that
- social media, blog outreach
- NEXT STEP (wolftune): add blog post about SeaGL along with https://community.snowdrift.coop/t/seagl-seattle-wa-november-15-16-2019/1372

## Code development progress
- NEXT STEP (chreekat): run the backlog of crowdmatches https://gitlab.com/snowdrift/snowdrift/issues/169
- NEXT STEP (wolftune): ping chreekat about getting this by SeaGL


<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->