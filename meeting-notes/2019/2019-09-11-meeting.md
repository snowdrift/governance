<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — September 11, 2019

Attendees: wolftune, MSiep, alignwaivers, Salt

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 1 -> 0
- New Topics: 1 -> 1
- Posts: 12 -> 16

Sociocracy 3.0, wolftune @ page: 11 of 37

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## New Project Forms
- NEXT STEP: (salt) discussing LibreOffice draft with Kodi and/or other closer project contacts

## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## design progress, pace / how's h30
- NEXT STEP (wolftune): bug h30 (Henri) about documenting process of implementing mockups <https://gitlab.com/snowdrift/snowdrift/issues/158>
- NEXT STEP (all): recruiting front-end (haskell?) developers

## IRC / forum activity
- need more activity, prompting etc. etc.
- mray: [longer, clear, well-said discussion about the need to just move forward in desperation toward whatever we can do, can't be perfect or always wait for someone else to do the right things etc]
- NEXT STEP (mray): post this and other questions on the forum

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune): schedule grooming session

## Better meeting tech (Jitsi failed us today)
- NEXT STEP (alignwaivers): post discussion of this to forum
- NEXT Step (all): test mumble BEFORE next meeting [freedombox.mray.de]

## Comparison page
- NEXT STEP (all): respond to forum post
- NEXT STEP (Salt): show kodi mock-up in person

```

<!-- New Agenda Down Here  -->

## Marking prominently that we're under-construction
- NEXT STEP (msiep): move forward the idea of marking some state-of-the-platform expressed more clearly (issue or forum post etc)
- [DONE] https://community.snowdrift.coop/t/how-to-make-it-clear-snowdrift-coop-is-not-and-we-realize-its-not-ready-for-prime-time/1361

## Funding
- we discussed the difficulty and importance of clarifying what level of funding we actually need to sustain the platform
- how should we make that clear to projects and patrons, and how to communicate our hopes to do this without an imposed fee


<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->