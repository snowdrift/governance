<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — September 25, 2019

Attendees: msiep, alignwaivers, wolftune

<!-- Check-in round -->

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 1 -> 1
- New Topics: 2 -> 2
- Posts: 16 -> 23

Sociocracy 3.0, wolftune @ page: 11 of 37

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
    - forum discussion of our use: https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards
- Use the boards to pick your tasks, dragging or marking To Do and Doing appropriately
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Carry-overs

```
## Salt met author of unfinished book on patronage funding at Communications conference
- NEXT STEP (salt): follow up with conference contacts

## design progress, pace / how's h30
- NEXT STEP (wolftune): bug h30 (Henri) about documenting process of implementing mockups <https://gitlab.com/snowdrift/snowdrift/issues/158>
- NEXT STEP (all): recruiting front-end (haskell?) developers

## Issue grooming / dumping etc
- <https://gitlab.com/groups/snowdrift/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smchel17): Delete or close the LFNW 2019 milestone
- NEXT STEP (smichel + wolftune + alignwaivers): schedule grooming session

## New meeting time
- Salt: https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/1345
- Salt: I have one more meeting tomorrow or Friday and then I'll know my availability until Dec
- NEXT STEP (alignwaivers): bug everyone to update their availability on whenisgood

## SeaGL
- NEXT STEP (alignwaivers): send email to sponsors@seagl.org saying we're okay being waitlisted but happy to have a space

## Code development progress
- NEXT STEP (wolftune): if chreekat hasn't run the crowdmatches yet, reach out to him to find out how to do it DONE via https://gitlab.com/snowdrift/snowdrift/issues/169
- NEXT STEP (alignwaivers): follow up to find a special meeting time with chreekat 

```

<!-- New Agenda Down Here  -->

## Team communication commitments etc
- wanting a minimum of sending regrets for meetings vs just no-show
- we need clearer team member agreement document
- alignwaivers: need to find the right balance, not push people away too much
- NEXT STEPS (wolftune + alignwaivers): keep studying Sociocracy ideas around this, find current stuff we have, organize our statements

## Marking prominently that we're under-construction
- we have good driver statement now
- https://community.snowdrift.coop/t/how-to-make-it-clear-snowdrift-coop-is-not-and-we-realize-its-not-ready-for-prime-time/1361
- NEXT STEP (msiep): create an issue in GitLab, note it on forum [DONE], ping mray

## Issue grooming
- need some better clarity about the steps, sociocracy has relevant stuff
- msiep: lack of activity in general makes this feel less motivating
- wolftune: it is a blocker somewhat or at least tension for recruiting
- the more clear and tidy our process and issues are, the more comfortable it is to do outreach, inviting newcomers
- NEXT STEP (msiep): groom design repo issues


<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->