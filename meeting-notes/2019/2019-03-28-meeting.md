<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2019/ -->
<!-- Meeting location: https://meet.jit.si/snowdrift -->

# Meeting — March 28, 2019

Attendees: Salt, alignwaivers, wolftune, mray

<!-- Check-in round -->

## Carry-overs

```
## Tracking ops tasks for just server maintenance
- NEXT STEP (chreekat): Review/capture ops priorities [chreekat, in his own list]

## design progress, pace
- NEXT STEP (h30): document process of implementing mockups <https://git.snowdrift.coop/sd/snowdrift/issues/158>
- NEXT STEP (wolftune): check in with h30 about progress^

## Ops meetings, progress
- NEXT STEP (Aryeah): sets up fresh server and makes sure comfortable with monitoring etc. all key management points, *then* can get into decisions on Docker, Drupal, Civi etc. <https://git.snowdrift.coop/sd/ops/issues/46>
- NEXT STEP (Salt): discuss some regular meeting time (if possible with chreekat), also Aryeah & Salt meetup

## New Project Forms
- NEXT STEP: (salt) implement forms 1 and 2 in civicrm (and 3 when ready) https://git.snowdrift.coop/sd/outreach/issues/41 (will be updated with the implementation step)
- NEXT STEP: (wolftune) gather more complete list of all the items we need to have from project for legal, transparency, Stripe, etc. etc. https://git.snowdrift.coop/sd/legal/issues/12
- NEXT STEP: (msiep) split up website-focused vs behind-the-scenes details from projects, have project page design to go along with the forms
-updates: wireframe of project page done, that in progress, form needs to reference that
- NEXT STEP: update form to make sure it gets all the info for the project page

## Athan joining the team
- NEXT STEPS: Add Athan to team group on forum, other access…

```

## Reminders on building new best-practice habits

### Sociocracy 3.0 stuff
- Drivers, describing driver statements: https://patterns.sociocracy30.org/describe-organizational-drivers.html
    - Current situation + effect + what's needed + impact of doing it
- NEXT STEP: wolftune posts that specific link to forum https://community.snowdrift.coop/t/sociocracy-3-0-drivers/1309

### Use of GitLab kanban boards (+ track civi todos there instead of this pad?)
- NEXT STEP (everyone): Start using the boards to pick your own assignments, mark To Do and Doing appropriately, capture any missing issues
- https://community.snowdrift.coop/t/using-the-kanban-boards-in-gitlab-plus-wolftune-check-in/1263

## Metrics

Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->

- Signups: 0 -> 0
- New Topics: 5 -> 1
- Posts: 18 -> 13

Sociocracy 3.0, wolftune @ page: 11 of 37

<!-- New Agenda Down Here  -->

## LFNW 

## LFNW physical stuff, art, printouts etc.
- NEXT STEP (Athan): order tablecloth <https://git.snowdrift.coop/sd/outreach/issues/53>
- NEXT STEP (mray): provide feedback on overhauled tall banner <https://git.snowdrift.coop/sd/design/issues/104>
- NEXT STEP (Salt): start Discourse topic on possibility of handbills or other hand-outs that aren't biz-cards

## LFNW + beta milestone prioritizing/scheduling
-  <https://git.snowdrift.coop/groups/sd/-/issues?milestone_title=LFNW+2019>
- NEXT STEP (smichel): further grooming, figure out assignments for most of the LFNW issues (with good communication with the assignees), also due-dates

## LFNW blog post/announce
- NEXT STEP (wolftune): publish blog post after LFNW publishes their schedule <https://blog.snowdrift.coop/lfnw-2019/>

## New Meeting Time
update your listings (everyone; athan poke people): https://community.snowdrift.coop/t/please-update-your-availability-for-meeting-times-etc/1082

## OSUOSL ops stuff
- chreekat prompted this, and wolftune followed up, seems totally promising
- waiting to hear back on emails about possibility that OSU could manage up to *all* our servers and sysadmin, just need to work out the details, we donate $ as we can…



<!-- Decide where to capture outstanding tasks -->
<!-- Closing round -->
<!-- Capture TODOs -->
<!-- Add meeting notes to wiki -->