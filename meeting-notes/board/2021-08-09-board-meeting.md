Snowdrift.coop Board Meeting 2021-08-07
===================================
### Personal Check-In Round
- Attendees: Stephen, Aaron, Salt, Eli
    - Leaving early: 
- Facilitator: Aaron
    - Facilitation links: <https://smichel.me/snowdrift/timer/>
- Note Taker: Aaron + Erik with help
    - Note-saver/pad-updater (if different): Stephen

### Previous meeting appreciation
- We all made it and on time
- Multiple short breaks were great

### Previous meeting feedback & suggestions
- Work to keep clarifying on what the asks or goals for each topic are
- Consider trying BBB again?
- Consider more explicit clarity about the required items for each quarterly meeting

- Consider taking private notes-to-self as we go to help get to the point when speaking
- Consider signing out with 3 things: "my take-aways / next steps", specific appreciation/gratitude, specific suggestion(s)


### Minute of silence
https://www.online-timers.com/timer-1-minute

Updates
-----------
### Metrics
(none yet)

### Treasurer's report

- Not technically our finances, but waiting for OSI money to be transferred to 
- https://wiki.snowdrift.coop/operations/accounting 
    - BTC listed under 2014
    - David was wallet-holder, will make sure those funds are available for snowdrift (whether or not he has access to the wallet)
    - Stephen payment details to be worked out, will probably go directly from David to Stephen (not through Snowdrift accounts).
    - Erik: Donation was recorded as gift to Snowdrift, though
    - Aaron: I don't really know the legal issues around transferring the money
    - Salt: When it's sold, it'll be sold as capital gains, we need to be aware of that; not sure how it intersects with nonprofit stuff but it could be important.
        - Aaron: money was pre-OSI
    - A: In practice, David might just be buying the BTC from Snowdrift
    - Salt: I don't know how we deal with "Selling a bitcoin". And David needs to know what price he's buying the BTC from us for, for his taxes (capital gains).
    - Eli: For him, if he's literally buying it from us, there's no capital gains issue.
    - Aaron: Details about how
    - Erik: I think there are still capital gains— we got it when it was some small amount, and it's now worth thousands of dollars. Ideally that'd be recorded every year, but it should still be recorded somehow.
    - Aaron: We're still under the $50,000 level (or whatever) that the IRS cares about.
    - Aaron: When we first files our Articles of Incorporation, with the we were aiming toward 501(c)(4), self-determined.
    990-N, not fully incorporated but working on it, negligible income/expenses and we've spent all our money each year.
    Some tension around "if OSI is our fiscal sponsor then maybe we shouldn't be filing", but we kept filing because we had been already.
    BTC never got reported
    - E: Any agreement for when David will get us the money?
    - Aaron: When we talked about the contract, BTC had peaked earlier and then fallen, the conversation was around that level (~35-40k). It could be, we agreed to sell at that price, on that day.
    - E: in writing?
    - A: Signal conversation only.
    - E: A formal purchase agreement would be a good idea.
    - A: Could send the lump sum to our bank, or he could hold the money for us
    - E: If you want to have a single balance with one person, that's ok, but the BTC money 
    - A: If David had found the BTC paper at the time, he'd just have handed it over and been done with it.
    - A: ……… Whether we sell or sit on BTC………
    - E: My assumption has been that we want to sell the BTC and spend it on contracts as needed; if board members have concerns about that, now would be a good time to raise those
    - [] Some purchase agreement with David
    - [ ] David sends funds into our bank account
    - [ ] Update accounting/reporting
        - Perhaps Phyllis (from OSI) could give us pro-bono advice (independent of OSI)
- A: At this point, in our accounts, we have $254.21 (+0.34826 BTC assets)
    - $331.90 misc donations
    - $156.50 legal consultation, just past our retainer, paid
    - No other expenses, but upcoming expense of renewing domain name

### Previous meeting NEXT STEPs
- [x] Approve previous meeting notes: <https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/board/2021-05-01-board-meeting.md>
    - Approved: Erik, Stephen, Eli, Aaron, (Micky not present)
- [x] Aaron: pad for Micky to refer to for May 5 event https://pad.snowdrift.coop/p/Civic_Tech_Sweden
- [ ] Salt: Coordinate roles / areas-of-responsibility comittee meeting with Eli and who else…
- [ ] smichel: Consider instead of meetings, doing pair work on things

Agenda
----------

### Updates on progress & plans made since last time we met (smichel, Salt) - 30 minutes

#### Stephen's July progress

(Stephen) Turned out I did very little coding during my full-time month; first thing on my agenda was to look through GitLab and get priorities organized, talk to team members. To make progress, I decided I needed to be a bit more of a benevolent dictator in software organization governance terms. 

With a volunteer org you can't tell people what to do because they can just say no. So I understood my role to say what not to do. We've done a lot of bouncing around, "a little of this, a little of that", instead of focusing on one thing until it's completed. What I decided in my role is that there was a leadership gap and that this was as important as the technical problems.

(Aaron) Can you switch to an "I did .." narrative

(Stephen) Yep. [screen-sharing] I set up epics on GitLab, where the list of epics is a list of the general things that need done and where people can help. "Here's the background, here's the rough plan", issues attached are the actionable tasks we need to do.  Set up some milestones, what makes sense to do in what order. 

The general steps I'm looking at: what we already did, cleaning up GitLab and making it clearer how you can contribute; etc. Have we talked about mechanism 0.3 with the board?

(Aaron) Not sure if we used that term but we've talked about it in principle.

"Mechanism 0.3" The Snowdrift project as a first project will not continue crowdmatching past 6,000 donors at $6/month per patron.

This solves the core issue of, "am I on the hook for infinite money?" and replaces the original approach of a per-patron budget setting and dropping out of the crowd beyond that.

(Stephen) These are the core issues to resolve. After that we'll focus on making charges & everything else we need to solve before a publicity push (e.g., password UX cleanup).

I did spend a little bit of time learning Haskell--enough to take ownership of the repo. Not enough to do major refactors yet. 

Wrote a blog post about all that.  https://blog.snowdrift.coop/what-snowdrift-needs-to-launch/

That's all on my end. A bunch of misc. things, but nothing that really stands out.

(Aaron) Anything you can share that will help the Board think of useful questions?

(Erik) Next steps technically?

(Stephen) Conor(sp?) has been working on the new frontend. Next concrete step I'll be involved in code-wise is to hook that up to the backend. Right now it's all rendered server-side, but have to expose information via an API. 

We're keeping tech-stack as-is & adding an API.

(Aaron) Stephen also did some good work with Salt identifying core needs for recruitment. Someone who'd help with DevOps & API & continuous deployment of ELM software stack.

(Stephen) High level tasks we could use help with: getting frontend deployed (ops help); another person to help w/ bylaws.

(Aaron) Eli, thoughts?

(Eli) No Qs right now, all pretty clear.

(Stephen) Did a couple of updates: updated the crowdmatch tool to be a bit more defensive in what inputs it accepts; ran the crowdmatches so the months are up-to-date. Also now shows tenths of a percentage.

(Salt) Was concerned it might run charges, but you ran it so it doesn't do that.

(Stephen) That's correct

(Aaron) We might do charges for just the team members on a one-off basis.

(Stephen) About $500 in total pending donations. May give people an opt-out since for many folks it's been years since they pledged.

(Erik) Probably large % of expired cards

(Eli) When we send out email, remind people to update card & let them know how much money would be charged. People will probably be fine with tiny donations.

(Erik) Do we have mechanism on the site for updating expired card?

(Stephen/Aaron) Yes.

(Stephen) Only thing we may want to change before we send out announcement -- right now you'd remove your card and add a new card, and it would unpledge you.

(Aaron) Let's capture that task to address that.

(Stephen) [ACTION] Yep, doing that right now. <https://gitlab.com/snowdrift/snowdrift/-/issues/663>

(Aaron) How about recording meeting?

(Stephen) We can start recorded section after the next break.


#### Salt's July

(Stephen) As I said, when I talked to people it became apparent that there was this leadership gap, not just in the sense of one person to decide, but the structure who decides on what not being particularly clear; how we make decisions. Only way before was consensus - very time-consuming, as a result a lot of decisions not made.

Salt has been doing work to clarify decision-making process— both *how* we make deicisions, and *who* makes them about what. 

(Salt) Been taking public policy classes; had in mind that we'd be able to make a system that works for us as volunteers & works around hangups we've been dealing with. I'm a comms major. One of the points of this is an org chart. Trying to be explicit from the get go that it's about comms flows, not power.

Also brought in concepts from CommunityRule.info which does deal with power and decision-making. That provides a structure for governance

1. ???
2. Good handle on one person controlling one thing

https://docs.snowdrift.coop/communityrule — some boilerplate remains, italicized;  some proposals otherwise

I don't think full notes are super important here, don't duplicate what's in the pad


- Hope is that we can translate some of this work to bylaws & articles
- The groups of people are what we have now, more than what we want
- Main thing I focused on was the team, specifically the decision process for work
- Team members are still volunteers, but they've made a commitment to communicate.

Feedback from Erik: the design/presentation/concepts should be worked out so that someone can grasp it comfortably within 5 minutes maybe 1-2 pages at most, this seems complex and intimidating. Maybe talk to design and UX people about how to simplify and present -- think about layers that are not actively used now and will not be for some time, and could be collapsed/removed from _current_ model.


### Break - 5 minutes


### Bylaws (smichel) - 45 minutes

- Decide on membership (single vs multi-stakeholder)
- Review articles & bylaws // identify decision points & divide them, for next meeting
- Priority is articles of incorporation

Open Q: What happens when we incorporate re: initial membership?

(Aaron) I think we could file Articles of Incorporation that are intended to be amended, or that are clear about the structure for full operations

(Eli) By defining the basics of what Snowdrift is, once we open up governance and decision-making to everybody, they're not likely to go, "Oh, we just want it to be Patreon".

(Aaron) When I didn't have two-phase concern explicit in my head, I realized that I've focused on creating ultimate bylaws that lock in things we don't want to change.
We probably need something more fluid— the current team knows wha'ts going on, they'll be in control for a while.

(Stephen) 

E: Key to resolve from my perspective: 1) Make sure bylaws/articles are consistent with our decision-making processes, e.g., asynchronous voting, 2) Are we a coop where patrons purchase services? Or what kind of coop are we?

Salt: We were talking about a sunset clause: by the time there's a certain number of patrons/projects, we need a way to incorporate them in the decision process.

Aaron: so we could say that the team are the first members and define a point at which we lower the bar membership opens to wider crowd (including general patrons)


Stephen) It's hard to change away from being a patron-coop, vs if it's team-only it's easier to change to whatever structure we eventually want.

Aaron) We could have membership be a requirement still, and also being a team member

Salt) I do think that eventually multi-stakeholder has lots of value, but happy to punt on it for now

Aaron) Whatever we decide now (is it a "temporary" vs eventual), it'll affect our conversation with Deb

Erik) I've said before, I think multi-stakeholder adds unecessary complexity, and we have discussed this already, we probably *should* lock-in the single-stakeholder patron co-op approach

Stephen) I think the sunset clause should be something like, "When we hit 6,000 patrons, then co-op membership must be open to all patrons"

(Eli) If everyone who is a patron of Snowdrift.coop is the membership class, and we've set it up once it's the maximum donation, no matter how many people join, it's still that maximum donation, then we have that membership class that can make decisions. The issue having an appropriate amount of power to the projects, there are certain things that are decided day-to-day by people in comms with the projects, and then there are decisions decided with the larger membership.

For day-to-day matters, the projects will have an opportunity to give input, that's not part of the **big** decision-making process. "We have this task, there's variables within it that need to be decided by the team, and they'll discuss those decisions with the projects". In effect there's "multi-stakeholder" where different people have input in different places, just not necessarily via the voting process.


### Meeting Tooling (smichel) - 5 minutes
- BBB next time?
- Hedgedoc next time?

### Open discussion (Facilitator) - 5 minutes

Wrap-up
----------
### Check that all NEXT-STEPS have someone assigned (Facilitator)

### Reflection Round — short, concrete, & specific

(Aaron as facilitator) Running up against meeting time - Stephen, did you get out of the meeting what you needed, if not, what do you want to get out of the next meeting? What went well this meeting, how can we do better?

(Stephen) Not super concrete on my next steps. I think I will after reviewing though. I like the facilitator hat. Was off my expectations with late start and Micky absent. I still want to get something we can send as a draft to Deb in couple weeks, but nervous.

(Eli) I want to get the links for Articles and Bylaws and everything that I need to look over in ONE place here in notes. I don't have Signal on my computer. Sorry for being late, meeting went well. No suggestions
    - https://community.snowdrift.coop/t/bylaws-drafting/1772
- Stephen: Not f

(Erik) I really think we need to keep focusing on simplicity, simplicity, simplest possible. Timeboxing would be better, decision-making stuff ate up too much of meeting. We need to still vote on solid structural decisions, sunset clause, I do support general idea of sunset clause / opening up membership / simple patron-only co-op. Happy to join working group or another board meeting just to get feedback on bylaws/articles revs ready.

(Salt) I feel clear on some next steps, but I did wish to have more delegation of some tasks that relate to bylaws, structure could be better timing and clear boundaries during meeting, even if it means topics get cut off.

(Aaron) I think there are some principles we could apply more consciously. E.g. Satisficing instead of maximizing— "good enough" instead of crossing every t and dotting every i. This applies both to how we discuss things; we should call each other out, maybe just talk about it as a tension. Thanks for feedback, we could have had more clarify about who would do timeboxing 

[NEXT STEP] August 21 meeting focused on bylaws/articles; Stephen will import docs from forum thread into GDoc for collaborative review & annotation.

Meeting Adjourned!
