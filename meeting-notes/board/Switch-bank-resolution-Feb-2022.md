# Record of Board resolution taken by asynchronous vote

February 5, 2022

Proposed and voted via private forum poll at:  
https://community.snowdrift.coop/t/move-banking-to-a-local-cu-and-away-from-ncb/1813/5

## Proposal to switch banking services

WHEREAS the Board of Directors for Snowdrift.coop, Inc. has identified an ongoing problem with the NCB.coop web access for the current checking account and recognizes the importance of efficient banking to support the organization’s daily operations,

RESOLVED, that the Board of Directors approves and authorizes the Treasurer (Aaron Wolf) to open a bank account on behalf of Snowdrift.coop, Inc. with Advantis Credit Union in Oregon (where Aaron lives).

RESOLVED, that the Board of Directors approves and authorizes the Treasurer to transfer all funds from NCB to the new Advantis Credit Union bank account and to subsequently close the account with NCB.

RESOLVED, that the Board of Directors approves and authorizes the Treasurer to access the new account’s balance, to write checks on behalf of Snowdrift.coop, Inc., and to transfer funds, in the normal course of the organization’s business.

## Vote

Move Snowdrift.coop banking to Advantis CU as proposed above?

Approved unanimously with all Directors participating
