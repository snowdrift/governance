<!--
Previous week's meeting notes:https://wiki.snowdrift.coop/resources/board-meetings/
Meeting location:https://meet.jit.si/snowdrift
backup: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
See end of this pad for meeting-practices notes and reference
-->

Snowdrift.coop Board Meeting — May 1, 2021
================================================

1. Check-in
-------------
<!--

    Personal Check-In Round— Snowdrift-related thoughts are ok but not required.

    Where is your head at?— Air distracting thoughts & understand others state of mind.

    Add your name to attendees.

    Does anyone need to leave early?

    Assign facilitator

    Assign time keeper— usehttps://online-timers.com, start for each item, paste each url in the chat

    Assign note taker

    Assign someone to clean and save notes and update pad for next time

-->

- Attendees: Salt, Eli, Stephen, Aaron, Erik, Micky, Athan

2. Review previous meeting
---------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> -->

### Approve previous meeting notes
https://gitlab.com/snowdrift/governance/-/blob/master/meeting-notes/board/2021-01-23-board-meeting.md

- Approved: all

### Feedback review

- Salt: I didn't like not being attentive at the start. Liked 2-part split with open discussion at the beginning & then brain dump for clear agenda of 2nd part.
- E: Good discussion, reminds me of FOTPF board operation sometimes; open-ended conversation, then followed by specifics. Thanks for "train-of-thought-ing" verb.
- L: Went pretty well, towards the end (last 10-15) felt a little long, but otherwise smooth.
- sm: split worked nicely, toward the end seemed we extended conversation, hard to say things and take notes
- W: realized it would have been ideal if Athan (or someone else) were here to take notes. Like the practice of brain-dumping before break, helps both with picking up where left off & being able to let it go during break. Appreciate Erik & Lisha's experience, helping us learn from your mistakes & not have to make them ourselves

3. Updates
-------------

### Metrics

(none yet)

### Treasurer's report

- Aaron:
    - We had one nontrivial donation, someone decided to give us $1 per current patron ($138)
    - Our lawyer, Deb, sent us a bill for around $150 that we went over our retainer, which we were able to cover.
    - We now have $60.31 in our checking account, and the only ongoing expense is domain registration which is $78/year
    - $933.68 in our name with the OSI — all of which will go to an independent contract with one of our volunteers to work on Snowdrift, being finalized (waiting to be signed by OSI). 
    - With crowdmatching, some people on the site are approaching or past threshold to charge without excessive proportional fee, but we haven't run actual charges yet

### Open discussion<!-- Timebox: 5 minutes -->

- smichel: when we were talking about bylaws and structure: one plausible framing: 
    - 1 org that does more networking and markets the platform
    - 1 org that builds the platform
    - We won't be pursuing this legally, but have been thinking in that framing.
    - Given our resources and other orgs starting to work in the space, I wonder how much it would make sense for us to focus less on launching our own platform and more on networking and advising those who are launching platforms / have more resources.



4. Agenda
------------

### Upcoming events <!-- Timebox: 10 minutes -->
- Micky:
    - http://actical.org/ - building a calendar for cooperative events with volunteers - could take a while...
    - differences from https://joinmobilizon.org/en/ and https://gettogether.community/ ?
    - Swedish civic tech group. no location yet (probably zoom thing); financial modeling as it pertains to cooperatives. Talk is about how Agaric has managed finances but I would like to bring up snowdrift, not sure for how long. it's somewhat thrown together ad-hoc.
    - It's a panel of 4 people
- wolftune: the way I would bring up snowdrift is by mentioning that different ownership structures are possible including both the non-equity non-profit approach and patron co-ops instead of worker, snowdrift is an example going in these other directions

- [ ] Aaron: Make a pad for Micky to refer to
    - https://pad.snowdrift.coop/p/Civic_Tech_Sweden
- micky: would like to add that snowdrift should be part of a lot more conferences
    - get a template to send to conferences
- smichel: we've done more confs in the past (around free software, less related to coops), but less lately
- salt: we've taken a step back consciously but we've been working on decks (for different lengths of talks: 5 minutes, 30 minutes, 45 minutes)
- micky: Plan a party on communitybridge to introduce people to snowdrift concepts? Start small & build, every few months we could have a get-together/open house.
- salt: we were doing open houses for awhile, could get back to that
- wolftune: tension: if I know I have a list of roles to recruit for (and having civicrm setup) that would be the ideal situation before pushing forward on more events
    - I have a pile of stuff from past conferences that I haven't followed up on because we don't have a system to manage it
- Eli: there's still a lot of online conferences which will reduce the $$$ needed to attend/present, so we may want to take advantage of that.
- Salt: we've gotten many volunteers from all over the world from SeaGL, etc
- Salt: presenting at LAS, "Making dollars and sense of free software funding's future"
    - 5/14

    - https://conf.linuxappsummit.org/event/3/contributions/44/

    - I gave this talk at LibrePlanet earlier, it was a little rough and needs to be revised

- Salt: presenting at GeekBeaconFest,  (they reached out to us directly)

    - 10/16

    - https://www.gbfest.org/



### Org chart and roles <!-- Timebox:  20+10 minutes -->

- salt: I'm taking a class and need to give a snowdrift proposal (and don't want to do anything technical), especially these things that are foundational for recruiting people etc. So, bylaws and business plan also apply, and roles fits in too

    - We've consolidated areas where we have discussed roles (many years of discussion, holacracy, sociocracy, and more).

    - although I'm not a huge fan of hiearchy, having domains is helpful aka knowing who is responsible for what area)

- wolftune: we have a pad where we are working on developing roles

    - Salt: A little too much detail here, but for reference: <https://pad.snowdrift.coop/p/role-development>

    - Salt: more reference links: <https://gitlab.com/snowdrift/governance> and <https://gitlab.com/snowdrift/legal>

- salt: other documents of this nature could help get us moving (scaffolding)
- wolftune: we want to get clear on outcome: I'd like to get perspective from the board on how to get where we want to go (we know these roles during recrutiing etc) and also knowing who to go to get info for those roles. 

    - what's the challenges/issues preventing us from moving forward.

- eric: happy to look at the pad and give some insights
- Athan: I think we're trying to optimize, which is holding us back from getting *something* functional
- salt: not sure what the best use of time is but at least want to update where the board is, and get insights on any issues other's have faced. What about one person doing this thing vs. it being a collaborative effort
- wolftune: a round on what perspectives people here have to maybe share insight one

    - knowing which of you we may be able to go to as we continue to work

ROUND:
- wolftune: I'm self-employed, looking over the fence at big orgs and the way they operate, maybe valuable perspective as an outsider, mostly only otherwise worked in volunteer groups
- Erik: these discussions of roles has been a part of every org I've been involved in.
    - Freedom of the Press Foundation is relatively small, 20 people, bigger than snowdrift, but still no full time accountant, communications person
    - What are the specific tasks that need doing, hwo are they distributed across the people doing them? There are standardized tools, like MOCHA and RACI,
    - e.g. build a matrix of what needs to be done, who needs to do it, who needs to know about it? 
        - https://en.wikipedia.org/wiki/Responsibility_assignment_matrix
        - That may not map well to here, people doing multiple roles
        - have budgeting, hiring/firing responsibilities, often have to put on many hats
        - cautious about assuming roles map to one person, probably won't be there until 100 people
        - " With the people we have, what are the most critical tasks to do that *don't* have people doing the work?
- Micky: I don't have experience in a hierarchical-driven entity.
    - people bubble up to their position, defer to people who are well versed in talking about hierarchies
    - Good to know who to ask questions about certain topics (who is responsible for what)
- Eli: There are different times when it's good to have "This is a role we need, they do these tasks & responsibilities" vs "We need these things done, let's find people to get these things done."
    - when talking about volunteers with only a small amount of time. Almost always better to have volunteers do one thing.
    - If they happen to have more time to do more things ,that's awesome, but ??? you do need someone in the org to do volunteer management, who would make sure people/volunteers are not overcommiting as well, keeping an eye out stuff like that - in the vein of HR but in a supportive way to ensure positive impact on the org (that *could* be a volunteer position as well; if they're paid, they'll also be doing other things).
    - when it comes to the issue of defining roles: which of these things do we actually need to fill with someone who's doing 20-30 hours a week - that's something that requires crafting a job description for them… which is not written in stone (in any business, even the most hierarchical); it will change, either accidentally or on purpose.
    - In a hierarchical org you'd just be told "this is what you do now", outside you'd discuss it.
    - I think some of the challenges occuring is a part of 'perfection is the enemy of good': if you know you have a thing to get done you can start recruiting a volunteer to get something done. when you have resources to pull more people involved that's where you can create roles with a whole sphere of influence. Of existing internal folks, can make 'internal resumes' to note what roles have they had and what roles do they have now to get an idea of status moving forward
    - happy to talk about these things anytime after next week
- smichel: don't have much experience in formal roles in orgs, but have insights on how other volunteer operations work
- salt: a lot of great information and resources here. I have experience doing this for startups <10 and in academic fields.  Took a class on managing nonprofits and there's a chart I really like: a skills inventory chart with different people. 
    - We could use something like this to see "holes" where we're missing skills. Not the same as defining a role and a hat. We want to be able to recruit, until we know what those roles are, hard to do that.
        - Sm: not that much to add, I don't have much experience in larger orgs, although I do have more experience with how Open Source software development happens…

<5min break>

- Aaron: Think there are benefits to making sure volunteers feel like they've been honored and delegated to (with roles) with meaningful titles
- smichel: …at SD, we operate differently from most other OSS projects I've seen. Somewhat victims of our own success — not many projects have 8 team members meeting for a full hour weekly. But we haven't had to do project management well as a result.
- salt: framing this as roles is not as much the case as tasks. A lot of overlap between these topics
- wolftune: I don't have as much experience with organizational structure in the tech world (website vs dev vs security etc). I think in terms of GTD, things that never get checked off, "areas of responsibility", I want us to have those listed completely enough. For example, I'd like there to be some person or people who check in with others, ask how they are doing, what they are working on, if they need help, if they are burning out, etc.
- eli: that's called a volunteer coordinator
- wolftune: ok, great example of the ideal, I mention the need, Eli gave a name to it, we go to conference and recruit for it, this doesn't have to be so long and delayed
- Salt: I have those terms in the pad, volunteer coordinator (line 230)
- wolftune: what are our next steps to recruit one or give someone the role
- Eli: after the next 7 days, have a subcommitte meeting to go through the list, maybe some will be put aside but we'll figure out the quick ones
- [ ] Salt: Coordinate this meeting
- wolftune: business plan still seems different to me. Stuff that's taken us years… I think the right people could sit down in three hours, good enough for now and move forward with it.
- salt: the blocker is often too many people working on the same thing, instead of one
- wolftune: what about two people?
- salt: it'll take longer 
- wolftune: I might take longer by myself, hesitating and struggling to be decisive
- eli: if you've got the right person to write the initial document it'll go really smoothly. Two people on the same time on the same document (like in coding) is a good way to to work: the downside is coordinating the time. So either choosing one person or two people do it depending on the context.
- micky: welcome to use a room at community bridge has breakout rooms (still connected to chat with main room)

- [ ] smichel: Consider instead of meetings, doing pair work on things

### Other organizations in the space <!-- Timebox: the rest (7 min) -->
- Aaron: Things happening in the FLO funding and matching-funding space, I have mixed feelings: other people recognizing things we've known for along time, moving into the space we're in.
    - FundOSS, collaboration betwen OS collective & gitcoin folks, haven't researched it yet but despite crypto focus, they have a quadratic funding model that does some sort of matching, with a shared pool, you get matched higher when you donate less.
    - How do we fit in? What's the distinction between us

    - https://fundoss.org/ has crowdmatching but opposite of our egalitarian focus

    - https://www.comradery.co/

- wolftune: I'm seeing more movement in this space, people are aware of the tension. I see almost everyone focusing on some mix of 'motivating people to donate' and also models of how to do that. Open source collective made progress etc. I see the issue of no one focusing on the end vision of funding things as public goods. No one seems to considerate how citizens end up with these things. Not sure how to take this into account there is *some* overlap but we are focused on different things. 

  - https://givingmultiplier.org/ is another that doesn't compete with our focus but is emphasizing matching, and matching across the regular contributors. It's for 501c3's only, pushing people to donate to "more effective" charities — something about splitting their donation, donate more in order to join a matching fund
- eli: I think whatever else happens our key thing is that we are funding public goods. Even if crowdmatching is a bust, the public goods focus is a big deal. My concern is that worrying about any of this is meaningless if we don't have a ready-for-launch product. The reason these things are coming up on our radar is cause they are already ready-to-launch


5. Meeting evaluation / feedback / suggestions / appreciations round<!-- Timebox: final 5 minutes -->
--------------------------------------------------------------------------------------
- eli: thought the meeting went pretty well. sorry I couldnt see chat during meeting, thanks for keeping it to the time
- micky: just a note bbb works for the phone with chat/simultaneous. Thought the meeting was really good, things being revealed and have a better idea of what needs to be done. Can reach out to others to potentially fill roles and just donated.
- smichel: thought it went pretty well overall, frequent breaks are really nice. Helps to think/sort out thoughts. there were a couple of points with silence/not sure how to push forward, but that happens sometimes
- wolftune: encouraging people to share what's next for them as well appreciations and suggestions: I really appreciate we all made it today and on time. Want to make sure we are on track, this is the Q2 meeting need to make sure we get the relevant tasks done and know that we've checked off everything a quarterly meeting should cover
- erik: I appreciate not being the one to light the fire (since I am a project manager at my work). the middle section was a bit muddled, being clear on what the asks are would be helpful. Don't think we made too much progress on the bylaws. thanks for facilitating salt and athan for notetaking.
- salt: next steps, gotta get this proposal together (the organizational work for the class project) and task role development. Appreciate everyone being here. The idea of 'being here' (mentally). +1 to optimizing aaron's getting to the point (taking notes-to-self as a suggestion). 
also liked multiple shorter break, thinking about things while getting some water etc.
- note for wolftune: make bullet points for while talking

<!--

    Meeting adjourned!

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Clear meetings date, attendee list

    Clear report / metrics (keeping the topic titles)

    Replace previous meeting eval notes with new and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas

    Change the last-meeting-notes wiki link to this past one

    Clear authorship colors

-->

<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox viahttps://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
- "^" approve, extend the timebox
- "v" disagree, move onto the next topic
- "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
- "o/" or "/" means you have something to say and puts you in the queue
- "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
- ">" means you understand someone's point, please move on
- "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->

 
