# Meeting: Informal Technical Meeting

Time: 03/14/2016 08:30 PM - 10:00 PM (GMT+00:00) Casablanca

Location: meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Athan Spathas; Brian Smith; Bryan Richter; Charles Wyble; curtis gagliardi; Iko ~; Jason Harrer; kenneth wyrick; Michael Siepmann; Peter Harpending; Robert Martinez; Stephen Michel; William Hale

Attendees: Bryan Richter; Iko ~; Robert Martinez; Stephen Michel; William Hale

---

# Minutes

## (re: implementation aspects of design)

mray:

- we should address css framework - no one should have to create new typography rules
- css tags for pages with parts of pages nested
- we should clarify what the landing/how-it-works page should look like, I see changes in the functions the page has to fulfill
- I want to know what role Snowdrift.coop plays for the user - this seems to have changed from just clicking the big button and the current design will need to change to address the functions
- RWD - I put effort to make the pages responsive, but we may have a simpler layout

## (re: scheduling an Interaction meeting for MVP)

chreekat:

- those are valid questions, but there are also some things that are outside the development scope - e.g. what the site is supposed to do
- from the previous governance meeting, it emerged there is a whole role that exists, Communications - what do the pages try to say to the user, to do. I hope clarifying the role be a concrete thing will help focus conversations

mray: not satisfied, I don't feel like we are making things right from the start

chreekat: I think you should schedule a conversation with MSiep. That's the communications part you want to talk about, and MSiep can help clarify the top-level processes

mray: smichel17, what would be the best way in the Holacracy process to contact them?

smichel17: I guess it would be a tactical meeting, If you know who you're waiting on, then you can approach the individual directly. I say we should just said an email to MSiep, mray, ikomi and wolftune and arrange a time to talk it through, then send the results to the email list.

mray: so we should defer the discussion for later?

chreekat: yeah, this is something to ask wolftune. However, I think we know the purpose and it should be written down somewhere.

mray: the goal of the Snowdrift.coop project and the purpose of the site/servers we are offering. MVP defines the boundaries, but we need a clear purpose of the website, as opposed to the project as a whole

chreekat: launching MVP is a mission/task that will someday be done, task checked off and more features added

mray: next steps after MVP? 

chreekat: I think you should draw a hard line at the MVP, and future development is another story. 

mray: e.g. if purpose is recurring donations, do users need to re-visit the pledge page often, and the design can change depending on how it should work when the visitor is on the page

chreekat: yes, simply put, the goal to transfer funds from one person, but also the website explains why people can do this and how
the example is too far in the future

mray: website seems to have a dual purpose: as a propaganda machine for what we do in general as well as accepting pledges

smichel17: the propaganda is important to distinguish the website from other funding websites

chreekat: when you say design, do you mean visuals? color scheme?

mray: more like layout, order of things, make clear 1) why this project is important and 2) describe tool

chreekat:

- how about making a proposal to MSiep and wolftune?
- we need these pages for this part of the goal, another set for another goal, different style depending on which goal is being achieved

smichel17: wolftune will make a decision and we'll run with it

mray: we talked about establishing a reusable list of visuals (e.g. road, snow), any thoughts about that, ikomi?

ikomi: I really like the idea but uncertain of the site purpose, hence can't comment whether that would ultimately be the best approach

mray:

- distinction between the text and visual components (content), then interaction (presentation), but there's interaction in both
- we should have a consistent way to educate people - this is something in the scope of the Design team, if we have an interaction role

smichel17: this was something we tried to define and capture in the roles during the governance meeting. I think the question of which pages should exist should not be determined by communications (wolftune), but in design/interaction (mray, MSiep)

chreekat: the role of determining the purpose of the site is completely independent of other roles - purpose goal is determined by Communications

## (re: implementation process)

1. Communications defines the purpose and goals
2. Interaction takes the stated goals, comes up with the themes for the pages, categories and visuals
3. Development takes the output from Interaction, build the pages to fulfill these goals
User Research brings in feedback informing Interaction
QA role starts at the very top since it is involved in testing quality of the site overall 

## (re: MVP)

chreekat: maybe the MVP should be broken down according to the two natures - get the information pages done first, then the funding pages

mray: we need the mission and clear pupose of the product

chreekat: wolftune can continue to define the principles, whereas who is the Interaction Designer and the person (filling that role) should come up with the pages

mray: whoever fulfilling the role would need not only the skills, but also a deep understanding of the politics, possible sources of friction

chreekat: what a page is and what is this page for? the Interaction Designer will be the one to define that.

smichel17:

- purpose of the site: 1) to educate users about Snowdrift/our model and 2) to provide a way for users to pledge to support Snowdrift.coop. 
- how about MSiep and mray filling the interaction roles for now?

chreekat: the purpose of the meeting would be how to fulfill the given purpose with a set of pages

mray: where is the purpose written down?

smichel17: not yet, it will most likely be written as a milestone on OP. The meeting will be an "informal meeting".

## (re: Honor Pledge for MVP)

Salt: I think the Honor Pledge is important, so that people commit to working together

mray: how is it important?

chreekat:

- we want people pledging to be honest and respectful in their membership in the community
- "community" - anyone who is pledging, has an account

mray: do we really want to take on a position to judge people's behaviour in other avenues e.g. Reddit?

chreekat: let's back up a bit, we don't have to decide now, the Interaction meeting will determine what pages we'll have

smichel17: I agree with mray, it's a slippery slope and how do we do that?

chreekat: we're not trying to stop them in that we're not disabling their Reddit account

smichel17:

- however, there are some aspects that still applies to Snowdrift.coop, maybe a stripped down version that applies to MVP
- if we have a forum later, then you can ask users to sign it

mray: that I can imagine, but we don't have the features yet

Salt: I think that user experience, but for MVP I'm fine with it not being available yet

chreekat: that's up to the Community Manager whether that page should exist

Salt: maybe have volunteers sign it

mray: regarding chreekat's question on typography, we should establish a stable groundwork

chreekat: I need the css files to fulfill requirements, questions about css to make things look good, that's my expertise

## (re: passphrase-related pages)

iko: plan for change-passphrase?

chreekat:

- yeah, it's not enough to just move the files, we should meet again tomorrow to discuss next step
- square away the visual design and css framework

## (re: static storage)

chreekat:

- that ties in with the stuff we have to move off the wiki (I feel like it's not Snowdrift's problem though it is)
- it's a governance question, a project manager thing
iko: did we say knowledge management is one of the project manager's domains?
chreekat:
- yeah, maybe we need a content management role
- wolftune has an idea to include articles on FLO resources, etc. and it's really a separate project
- blog content and videos - e.g. blog files are in the db and we want to move them elsewhere
- we could work on the pages and decide later what to do with wiki articles, though wolftune may want to include it in the MVP

smichel17:

- wolftune has given me the authorization to determine the MVP scope
- a middle ground: let's take the wiki pages and put it on sub-site, wiki.snowdrift.coop

## (re: code style guide/documentation)

iko: plans for code documentation from the wiki?

chreekat: coding documentation will go in the repo. I believe the development role is care and maintainance of the repo, and the repo would be the place to put that information.
