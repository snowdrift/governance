# Meeting: Weekly Meeting

Time: 02/29/2016 08:00 PM - 09:00 PM (GMT+00:00) Casablanca

Location: meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Athan Spathas; Brian Smith; Bryan Richter; Charles Wyble; Iko ~; Jason Harrer; kenneth wyrick; Michael Siepmann; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Athan Spathas; Bryan Richter; Iko ~; Jason Harrer; Robert Martinez; Stephen Michel

---

# Checkins

## Salt (not in person)

Sorry, I will be MIA again this week, it looks like an interesting meeting!
I have been hitting the CiviCRM book hard and have lots of notes on organizational improvements and creating custom pages, such as the new volunteer form.
However, I am quite pre-occupied with the conference and travel, so it may be slow in implementation, hopefully started by next meeting.
In the meanwhile, I have created a custom data entry form. It should be pretty self-explanatory as to how you can take the volunteer forms and input them.
https://mx1.snowdrift.coop/civicrm/profile/create?gid=15&reset=1

## smichel17:

* I plan to give an overview of Holacracy and go over any issues
* implementation: adopt Constitution, things don't need to change immediately

## wolftune:

* still juggling things, happy we're making progress 

## chreekat:

* working on diagram of software components
* organize dev meeting
* assess current status of tech
** cleaning is needed

## JazzyEagle:

* been distracted, confused about next steps
    
## mray:

* waiting for dust to settle, clarity about role and scope of design
* things seem unstable
* want clear next steps

# Action Items

## wolftune:

* schedule governance meeting to define groups
** Ideally before monday
* see to making adoption process happen
** by signing https://github.com/holacracyone/Holacracy-Constitution/blob/master/Adoption%20Declaration%20Sample.pdf
** put himself as lead link of the anchor circle

## All:

* watch your emails, be on IRC for meeting announcements

# Decisions

## organise {groups = circles} structure in OP?

No. Just document it first.

## Where to document our structure?

OpenProject wiki. 
* we can open a wiki page for the primary circle and build from there

# Discussion

## Holacracy Explanation

_[Note: This is no longer a transcript, it was edited to be a better educational resource]_

* Holacracy works by making the written structure identical to the actual structure (eg, not like an always-out-of-date org chart), and then incrementally modifying the structure to be more like the ideal structure (course correction). It's Agile to traditional management hierarchy's Waterfall.
* Holacracy is organisational structure based on a heirarchy of _*Roles*_ and _*Circles*_.
* Roles are like a job description, except a person can fill multiple roles
** They have a *_Purpose_*, _*Accountabilities*_, and possibly *_Domain(s)_*
*** Domains are things they own. ie, the code base might be chreekat's domain. The others are what you think they are, with some restrictions on what they are allowed to be.
** Each role responsible for defining/documenting next-actions and projects in a visible place. in the absence of competing priorities, these are:
*** next-actions - both possible and useful to do now
*** projects - larger, multi-step things that are useful to work towards
** There's a special type of role called a _*link.*_ A link has the purpose of representing one circle to another
* A circle is a collection of roles (or circles).
** It's a good way to divide up a role, like how a startup might have a marketing person while a mature company has a marketing department.
** All circles have at least 4 specific roles, which are created when a circle is formed: Lead link, Rep link, Facilitator, Secretary.
*** *_lead link_*: inherits purpose, accountabilities, and domains of the role the circle replaces. The lead link represents the super-circle in which the circle (in this context, a sub-circle) resides.
*** _*rep link*_: represents the sub-circle to the super-circle (not the same person as lead, if enough people)
*** _*facilitator*_: keeps everyone to the Holacracy rules and keeps meeting on task
*** _*secretary*_: schedule meetings, keep minutes, interpret the Constitution 
*** Full role descriptions here. That's also a good example of what official role descriptions look like
https://github.com/holacracyone/Holacracy-Constitution/blob/master/Appendix-A.md
** Circles can also contain other roles (or sub-circles), which are created in _*Governance Meetings*_
* Governance meetings are how circles modify their own structure
** this *process* essentially replaces managers
** Invited to governance meetings: anyone with a role in the circle, and the rep link of any sub-circle
*** See how sub-circles look, to the super-circle, a _lot_ like a role?
** only structural proposals, not tactical decisions
** Works by processing _*tensions*_ into proposals to fix them.
*** "tension" -> a gap someone senses between how things are and how they could be
**** This could be "X sucks right now and if we did Y it would suck less"
**** It could also be "X is fine right now but if we did Y it would be awesome"
** Process for tension processing:
*** whoever has tension/problem proposes a solution -- only they talk
**** They may describe the tension/problem here but it is not required.
*** everyone can clarify and ask questions -- no opinions or reactions
*** everyone can react/respond to it -- no responses to reactions
**** This is the proper place for things like "I don't think this will solve your problem."
*** proposer can amend their proposal -- only proposer speaks
*** objection round -- only "objection" or "no objection"
*** only certain objections are valid.
**** Generally, "does this cause a NEW tension for someone else?"
**** "I don't think this will solve your problem" is NOT a valid objection
*** modify proposal given any valid objections -- this is the only step for free-form discussion
*** Snowdrift.coop should create a policy to put proposals to mailing list and give 3 days for others to raise objections (or something like that)

Note: All links that are not lead or rep, are _*cross links*_, which go between any two circles with no parent/child relationship.

* "Tactical meetings" are team planning / work meetings
** They have their own process which we'll talk about when we get there.

## Holacracy: Getting started

* Create an "anchor circle" whose purpose, accountabilities, domains are assigned by the highest authority (the board).
** Purpose is whatever the company's purpose is
* Have an initial governance meeting of the anchor circle, create roles and sub-circles
* Have initial meetings of sub-circles, create roles and subcircles (if needed), etc
* Initial structure should simply reflect current structure, then can be modified through governance

## Board vs Holacracy

wolftune:
* the Board is responsible for deciding the overall direction, org mission, legal requirements, etc. (stipulated in bylaws)
* bylaws also state criteria for becoming a org member, standing rules (org policies, such as pay equity)
* if we were ever to change policy, it would be within the bylaws structure
* the Board is elected, they hire people to run/oversee daily operations of the site, define job description for general manager leading the anchor circle 
** That "job description" becomes the description of the anchor circle
* the Board could theoretically break down the process and decide not to adopt Holacracy, because they are outside of the Holacracy structure, 
** but this is done within the scope of bylaws and if people are happy with the decision

## Context on OP wiki as structure documentation

smichel17:
* in terms of managing circle documents:
** folder for each circle and sub-circle with roles
** everyone has read access, roles in the circle have write access to the folder
** concern that there could be too much maintenance overhead
*** best solution is probably to give everyone write access and trust people won't change what they shouldn't

chreekat:
* I'd suggest not use technology, use a wiki (e.g. OP wiki) instead and have policies for people to follow

wolftune:
* we can attach documents to OP wiki pages

## Misc.

_[At the time, there was confusion over lead, rep, and cross links, so there was talk of changing the names. No more.]_

wolftune:
* smichel17, you're going to LibrePlanet
* we should get you a title
* talk to mray about getting business cards

mray:
* let me know what image you want on your card

wolftune:
* we also have T-shirts, let me know which size/color you'd like
