# Meeting: weekly checkin 2016-01-25

Time: 01/25/2016 08:00 PM - 09:00 PM (GMT+00:00) Casablanca

Location: https://meet.jit.si/snowdrift

Invitees: Aaron Wolf

Attendees: Aaron Wolf; Bryan Richter; Michael Siepmann; Robert Martinez; William Hale

---

# Minutes

SCaLE

* shirts++, quality great, heather grey is lovely, inverted eunice needs update, not to go back to the excessive-ink version, but inverted look could be cleaned to be more acceptable
* banner, good conversation starter, co-op logo drew some people, 2-road poster super useful
* lots of praise regarding new design
* feedback sheet had too much text, was "like a flyer" aka read later, more useful if it were less content so it could be more quickly responded to
* illustrations about what we do: not self explanatory
* new poster immediately explains dilemma

connection: Charles had good ideas on turning struggles into concrete tasks

* co-founder of free network foundation
* user stories as organizing tool to determine coding tasks
* use redmine (organizing) + etherpad (collaborating on forming ideas) + wekan (prioritizing, sorting)

lack of clarity issue arose due to website stubs, new site not as clear on some points as even a year ago:

* campaign video was good intro
* old intro page mroe thorough, new one too vague and incomplete
* temporary regression while trying to make things better
* we need better process about when to actually roll out new things

project contacts:

* main dev of inkscape interested in being an example, will get board decision after Aaron contacts

* From Robinson Tryon: Document Foundation board member Simon Phipps (former OSI board, has met Aaron, strongly supportive) could be good advocate for us in getting LibreOffice as early project, send email to Robinson to get process going


had time to tighten up the pitch

current site trying to fill three functions: actual product (in progress), billboard for volunteers to help now, managing work

Potentially should segregate, distinguish the focus/purpose

focus

* there is too much to deal with unless certain things are in place
* focus point should now be of getting funding mechanism working


Upcoming: Salt presenting at FOSDEM, will discuss presentation Aaron tomorrow


## Tasks

aaron

* share contact info so mray and charles arrange meeting time
* share lawyer contact info with bryan's brother (Aaron Richter)
* scan feedback forms
* send new contacts to salt for input to civicrm

salt

* finish FOSDEM presentation
* look into redmine install

bryan

* create architectural document
* send new contacts to salt for input to civicrm

michael

* setup meeting for communication channels regarding user feedback
* come up with list of technical requirements for better getting user feedback

robert

* setup meeting for design team focus
* tailor homepage for top priority, working prototype


