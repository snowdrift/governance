# Meeting: Weekly Meeting

Time: 02/15/2016 08:00 PM - 09:00 PM (GMT+00:00) Casablanca

Location: https://meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Brian Smith; Bryan Richter; Charles Wyble; Iko ~; Jason Harrer; Michael Siepmann; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Bryan Richter; Iko ~; Jason Harrer; Robert Martinez; Stephen Michel

---

# Agenda for Feb 15th, 2016

## Salt's Topics:

* Sorry everyone, been moving and won't have Internet until Wednesday.
* Lots of CiviCRM updates local that will be pushed and finished once I am online.
* Still planning on Portland meeting at 5pm, please email any information I will need.
* More apologies about being MIA for an extended period, Snowdrift.coop is my top priority over the next few months!

h1. Attendees (not listed above since not a member of OpenProject):

* Kenneth

---

# Minutes

## Checkins

### Bryan

* Working on test framework for persistent based models
* suggestion: brainstorming session? But then, tickets already exist,
so we already can move to the next step, organizing them.

### Robert

* Working on file structure for design storage

## Action Items

* (Aaron) Start creating an Etherpad page with a bunch of User Stories.
  http://flurry.snowdrift.coop:2040/shared/PSWc6nMFW8Pyw1-0rCG_0jdT2UMtdawdKlO3j8P369t
* (mray) Make sure Seafile is adequate.

## Decisions

* Remove Testing phase from OpenProject (testing is part of development)
* Use Etherpad to track User Stories for now:
  http://flurry.snowdrift.coop:2040/shared/PSWc6nMFW8Pyw1-0rCG_0jdT2UMtdawdKlO3j8P369t
* [made by chreekat after the meeting:] We will go through and prioritize
user stories as a team in a week's time
* We have enough personas for now
* Use (fictional) personas for all user stories, even if they're inspired
by real users.
* Some tasks are related to using CiviCRM, but CiviCRM should not be a
category of tasks unto itself


## Discussion notes

### On separate components

* wolftune: Should we have separate libraries for features? However, some mechanisms are interwined
* Bryan replies: it's somewhat unrelated from simply having the ability to
test components in isolation, which is the key thing to develop.

### On OpenProject organization

* JazzyEagle: [speaking on the topic of having a Testing phase] OP currently set up
with overkill expected, the idea is to let people give feedback on what's
necessary and what works, then adjust accordingly

### On User Stories

* wolftune: user stories is a nice concrete way to communicate requirements
** feedback received: actual user feedback is better but not if details are unclear
* mray responds: better to keep personas more neutral

* Some discussion on where to put the user stories
** chreekat suggests etherpad, since OpenProject's rigidness would just get
in the way at first.
** JazzyEagle: bullet list in etherpad?  I thought we were removing
Sandstorm due to memory issue.
** chreekat: this is a transient document, can transfer later to OP. Could
even use flurry.snowdrift.coop for now.

* wolftune mentions his hope for continued input from MSiep

* mray: what form do the user stories take? I have rough idea of
terminology and nesting e.g. what is an epic?
** JazzyEagle mentions the wiki page that discusses this

* maybe just stick to user stories due to potential confusion between user stories and epics.

* chreekat confirms that "categories" are the OpenProject word for "tags",
and can be used for filtering work packages.

### Discussion of user story format

Example of format for user stories:
Mara comes to the site, doesn't know much about the site, but sees the domain is .coop and sees a button. She can click it to see what snowdrift.coop is and what it means to us.

JazzyEagie: personas are already generally written, so that they can be referred to in user stories without re-iterating her identity.

wolftune: however, sometimes personas are written with reference to persona motivation, e.g. "As a newcomer to snowdrift.coop, Mara ..."

### On organizing user stories

chreekat: the website is only one component of this project, we'll also need to manage other aspects of snowdrift.coop 
wolftune: e.g. send T-shirts to donors is not a user story

wolftune: should I try to keep website and non-website stuff (e.g. legal stuff) separate? Or should we focus on prioritisation?
chreekat: keep them separate. Could be a sub-project eventually.

chreekat: CiviCRM is a tool for doing certain tasks regardless of priority
wolftune: maybe there some website-related things to CiviCRM
chreekat: that's like project outreach. What are the roles? Development is one item, reaching out to projects is a role. One of the priorities is defining these roles.

### On roadmaps

JazzyEagle: do we want to aim for an alpha?
chreekat: figure out the tasks, then the priorities, then we reach milestones

### On bylaws

wolftune: welcome Kenneth. Had our lawyer look over our bylaws documents - some aspects were unclear, would you like to help us finalize the documents for confirmation with our lawyer?
Kenneth: I am here and will help with bylaws
