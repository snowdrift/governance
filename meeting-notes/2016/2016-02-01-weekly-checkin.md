# Meeting: weekly checkin 2016-02-01

Time: 02/01/2016 08:00 PM - 09:00 PM (GMT+00:00) Casablanca

Location: https://meet.jit.si/snowdrift

Invitees: Aaron Wolf; Brian Smith; Bryan Richter; Charles Wyble; Iko ~; Jason Harrer; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Bryan Richter; Iko ~; Jason Harrer; Michael Siepmann; Robert Martinez; Stephen Michel

---

# Minutes

## Decisions made
* Use jitsi etherpad to take meeting notes, for real-time collaboration
** Edit notes into minutes and add to OpenProject
* Recruit meeting facilitators and recorders
* Jason will take ownership of OpenProject, becoming the expert and setting it up as he sees best
** Will be done in collaboration with Brian Smith and Charles
* Docs on Snowdrift.coop wiki that are focused towards curious site visitors will stay and continue to be used
* Attempt to use OpenProject backlog plugin instead of WeKan

## Next actions
* Read OpenProject docs (Jason)
  > https://www.openproject.org/help/user*guides/
* Move old meeting minutes from the wiki into OpenProject
** Move them (who?)
** Delete the pages (chreekat)
* Document personas for user stories (Aaron)
** Will record them in OpenProject wiki
* Update Haskell dependencies (chreekat)
* Move any outstanding sandstorm etherpads -> openproject wiki pages (Jason):
** Need links (Aaron)
** Project page specs
** FLOSS project outreach
** Meeting notes from the 25th?
* mray's going to test the seafile server set up by Iko
* Move Snowdrift project management content out of the Snowdrift.coop wiki, into the OpenProject wiki
** Leads for various circles will decide where bits relevant to their circle go
* Begin preparing Architecture tasks to be added to OpenProject when Jason has a good system (chreekat)
