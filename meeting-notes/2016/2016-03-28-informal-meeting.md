# Informal Meeting — March 28. 2016

Attendees: chreekat, iko, mray, MSiep, Salt, smichel17, wolftune

---

## Agenda

* Backlog prioritisation


## Holacracy ideas

```
smichel17: some Holacracy ideas …
* 5-min. stand-up, but probably no need to do it, we already have tactical meetings and are a distributed team
* sprint planning meeting at the beginning of each sprint to decide what goes into the backlog - we aren't doing sprints but we should still have some kind of planning meeting
* retrospective - how to work together to get things done and when, a bit like a governance meeting

chreekat: 
* I don't want to talk about sprints yet but take things one step at a time
* retrospective - different from governance meeting. What can we do better? It's like a brainstorming session and important. I think we can postpone it for two weeks.

wolftune: the function of governance meetings is to change accountabilities

mray: who's going to decide what we'll be doing next?
chreekat: we'll do it together, with the product owners (e.g. mray, wolftune). As scrum master I can help facilitate the process.
```


## Taiga

```
mray: what's Taiga's business model?
smichel17: anyone can create public projects, with charge for private projects 
mray: do we trust them in terms of uptime? Can we backup our work?
smichel17: there's an export function, we can periodically make backups manually
wolftune: can we assign an accountability for someone to take backups periodically?
smichel17: noted
smichel17: I marked some US as epic (see example \#97 "Holacracy constitution and more specified for us" under the grey bar)
chreekat: I think it's premature optimisation, that's like grooming
```


## Prioritising backlog

Process: teams look at [kanban of User Stories](https://tree.taiga.io/project/snowdrift/kanban) and move US in "New" to ordered list under "Prioritized"

Currently, chreekat (in scrum master role) helps product owner (wolftune) with prioritsing items on backlog

```
chreekat: #98 "Aaron Richter onboarding" - needs re-write, it's not very meaningful as a US. wolftune, what are you trying to capture here?
wolftune: there's work to do to onboard a volunteer to help with some legal aspects
smichel17: this could be something stored in CiviCRM
Salt: CiviCRM still needs testing, the tests should be added to Taiga like sysadmin tests. I haven't dived into Taiga yet.
wolftune: can we tag it with CiviCRM? Does it make sense?

smichel17: one thing we don't have recorded is how much time a task takes
chreekat: I'd like to prioritise now, this is grooming
smichel17: we don't have to address it now, but the reason I bring it up is using tags 

mray: when we prioritise, how do we split the list into top/bottom half?
chreekat: for now, things in the top half are more important, the other is less important

smichel17: if a US has a design and legal task, how do we determine which priority is higher?
chreekat: I'm not sure but would like to feel it out
smichel17: we don't have a product owner role, but potentially the role has authority to decide
mray: if there's a design US and there are other US from other teams being sorted, how do I determine relative priority of things of interest to me compared to other teams' US?
chreekat: anyone should add US to the backlog (even if it's crazy)
smichel17: there are some tasks that don't belong in Taiga e.g.  #76 "Improve design library storage redundancy by mirroring seafile to gitlab" > "research feasibility". It's something you do for yourself
chreekat: I don't entirely agree, research takes time and if we want to track time and effort …
wolftune: I think it's perfectly fine, but it's a choice of whether people want to include it

chreekat: if a US is not in top 10, then I'm not too worried about exact order
```

### re: wiki-related US and their differences:

* 28 - how content is organised
* 32 - where content is served 
* 130 - whether content is up-to-date


### re: mvp US

```
chreekat: creating a new column "Website" for mvp US, for now we can forget mvp as a concept and just organise by relative priority
mray: how do we determine which tasks from different teams are higher-priority?
chreekat: we can worry about that later, they're tasks done by different people
chreekat: it's also possible to have mutlple project feature lists and merge them later (based on what teams think are priority/valuable features)

smichel17: before we adopted scrum, we used a two-tier prioritization system. Everything was either (1) MVP and worth focusing on, or (2) Not MVP and not worth focusing on. We're switching to a much more fine-grained prioritization system
chreekat: for what it's worth, it was not sufficient to say "these are mvp, these are not". It was/would have been necessary to also do fine-grain prioritizing WITHIN the mvp category
smichel17: which is why we're doing scrum now -- it fills that need
wolftune: example #57, we want that in place because it's meaningful to some of our audience to be using LibreJS. How do we mark that? chreekat's suggestion was to tag it mvp (which is a list of relative priorities for mvp), but it's important to be able to state requirements

wolftune: there's a point at which patron's pledge/money successfully goes towards project, then we're at a milestone when we check it off as complete
smichel17: in terms of what's tagged as mvp
mray: we have tools that help us determine what to do next, and we have the kanban cards.One of those columns decides what we do next.  I don't see a clear decision on this?
smichel17: let's try to finish prioritising the backlog today, however far we can get, then have a conversation later on the topic for alignment
```


### re: \#115 great guidelines for volunteer intake

```
smichel17: it's policy-related
* In Holacracy, a policy is a rule stating how work is done. Policies are governance, but not determined at governance meetings
* Glassfrog as example, they have a location that lists all the policies that people can look up.
chreekat: if it's placed with governance, does that mean we're using markdir? What's the point of having a wiki?
smichel17: markdir can process .md and .page to be displayed in gitit. The idea is that thee's one location where people can navigate to find the policies
chreekat: then it's solved with a link to the wiki in the governance mdir
```

## Next steps

* backlog grooming - arrange a meeting tomorrow, Wednesday or sometime before Monday (most likely Wednesday since mray is unavailable most Tuesdays)

