# Meeting — September 12, 2016

Attendees: Salt, mray, iko, wolftune

---

## Checkin
- Salt: got my laptop back, but configuration takes time. beyond that, busy week with relatives coming in. I will be here on and off.
- mray: got caught up in design work, ended up doing some research on different types of grids. I also explained to iko last time there were some design changes and moving forward there. otherwise busy with other things and didn't get to do as much as I would've liked.
- iko: nothing to report, made a small css MR last week. watching mray's progress, see how to help with design tasks.
- wolftune: today is my first unscheduled day at new home. Settling in, figuring out my priorities. annoyed with a recent laptop hinge issue.

## Agenda
- state of docker/mx1 (Salt)
- transition from master to production (Salt)
- looking for direction from chreekat in terms of priorities (mray)
- be updated on status of certain things, e.g. CiviCRM (wolftune)

### State of Docker
- Salt: we were hosting a lot of things, so it made sense to gather the processes together in a virtualised environment
- that was completed months ago, but when we tried to set up Discourse a few weeks ago, we ran out of space
- chreekat was away for 2 weeks, and on his return and we worked on it a few times
- basically, we need to add a separate drive for Docker, but we're working on it
- there are still a few things to move to Docker: gitit, mailing list (but it can be round-robin, not an issue for now)
- on CiviCRM, I haven't had a chance to speak with Athan, I have been looking at the spreadsheets. does it capture all the signups from previous conferences?
- wolftune: I don't think Athan has them either. other things like business cards, I have
- Salt: sorry for taking so long
- wolftune: apology accepted, thanks for your help to date

### Transition from master to production
- Salt: previous ML message from chreekat about how to deal with existing user accounts
- what did people think?
- wolftune: I think the decision was to do the thorough version, but not make it a complete blocker
- push to production and send an email to people in the edge cases
- Salt: one of my major focuses is to get that done, and slightly related to CiviCRM
- wolftune: one thing I noticed in Taiga, the how-it-works needs to be redone in general, with new terminology, etc.
- but in the interim, I'd rather we keep the current one with the "more about ..." links. they were removed for some reason
- mray: as I recall, it wasn't part of mvp so no effort was made to implement it
- wolftune: maybe chreekat thought we didn't need them down the road and removed them
- mray: is it better to confuse people or give them more info?
- wolftune: I don't think it adds confusion, it just wasn't explained as well as we wanted
- what I don't like is a dead-end page, it lists a few things but doesn't say much
- if we added back links to the wiki page in context, then it wouldn't be a dead end
- even if it's just a link at the bottom to the wiki that says "you can read more on the wiki", so that people who have questions can continue
- maybe to <https://wiki.snowdrift.coop/about> ?
- mray: I'd rather not link, as people will look around, see incomplete pages and there's the overhead of taking care of that
- wolftune: do you suggest an "under construction" notification?
- I want people to be able to read the stuff even if it's not perfect
- Salt: I understand both views, but we're up against the deadline, and people should be able to see the info
- wolftune: I'd rather people see the info, mess and all. would anyone here object if I made a draft today and send a MR?
- mray: I've been working on the how-it-works page with MSiep's survey feedback, but it's not a solution to your immediate problem
- wolftune: okay, I'll send a MR later. keep working on it, and it can be refined later

### Direction in terms of design priorities
- mray: trying to balance between doing current work (research, grids) and not wanting to slow down others' progress
- what's the status of the codebase for the pledging?
- wolftune: the backend is there but the html isn't there yet
- mray: when is chreekat returning?
- Salt: chreekat is back and on in your morning hours, I've spoken with him

### General status update
- wolftune: is there anything people want me to look at/focus first?
- I just need to start working on things again
- iko: what's the status of US-308, is it waiting on something? it still says "Dev In Progress"
- wolftune: as far as I'm concerned, it's done *(wolftune marks it Done)*
- if there isn't anything else, then we can close the meeting

