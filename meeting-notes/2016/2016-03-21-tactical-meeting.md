# Tactical Meeting — March 21, 2016

Attendees: smichel17, mray, chreekat, msiep, wolftune, iko, salt

##Checklist \& Metrics:

None yet

## Project Updates:

Not recorded.

## Triage Issues:

CiviCRM entries

- Salt to finish CIVI setup

new tool set

- Tagi set up -- smichel17
- Taiga uptraining -- smichel17
- Wiki setup 
    - Move gitit wiki to wiki.snowdrift.coop
        - chreekat creates wiki.snowdrift.coop
        - ask fr33domlover if he's willing to sysadmin
        - chreekat talks to fr33domlover about wiki.snowdrift.coop management
    - have an informal meeting with engineering team ASAP
        - decide what do with snow wiki
- Meeting clarity -- smichel17 send email RE: gov't meeting time

githost (smichel17)

- wolftune email githost people with green light and ask what next steps are

## Chat log:

smichel17

- http://www.holacracy.org/wp-content/uploads/2015/08/Tactical-Meetings-card-bw.pdf

chreekat

- "Any circle member can request checklist items to be added the list, as long as they are for actions that have already been accepted by the role-filler as an action their role would take. In other words, checklist items may not convey new expectations on a role. If a circle member needs to add new expectations to a role, they need to propose adding an accountability via governance."

smichel17

- taiga.io

smichel17

- https://tree.taiga.io/project/smichel17-snowdrift/

iko

- technical issue, not getting any audio, please ignore the entries/exits

smichel17

- https://tree.taiga.io/project/snowdrift/

smichel17

- https://gitlab.com/iko/snowiki

smichel17

- http://snowiki.rel4tion.org/

smichel17

- https://github.com/smichel17/Governance.mdir/

smichel17

- https://github.com/smichel17/markdir.mdir

chreekat

- awesome mray that did it 
