# Informal Meeting — July 25, 2016

Attendees: chreekat, iko, mray, MSiep, wolftune

---

## Blog
- iko: what's the status of the blog setup? did anyone reach out to Ghost?
- wolftune: no, we were discussing other options (Known, etc.). 
- wolftune: maybe a subsection on the snowdrift.coop website with past posts in markdown pages

## Taiga and MVP subproject
- chreekat: smichel17 and I looked at Taiga and extricating MVP items
but it's not worth the effort manually or automatically importing them
- instead, we're looking at what we need for MVP

## MVP notes
- Front page does one thing, how-it-works does another
- Front page video would not go into details of the mechanism
- aaron: best video, think that pique interest: anita's thing with the roads side-by-side, a video form of the booth visuals
- mray: break it down to a single message for a video

- iko: is /honor page (#70) part of mvp?
- wolftune: no, though it's something we can add if there's time later
- iko, wolftune: add link to wiki partners page on website footer (#62) for mvp, not milestone

## Milestone
- chreekat: milestones (about 21 items) involve breaking down todos to make tasks more manageable
- currently there are some US on the kanban that aren't mvp and probably belong in Issues instead
- in the meantime, we can have a master to production sprint and tackle some tasks for 2 weeks, then repeat again for the next set of tasks

---

## MVP Items

- how-it-works section engages newcomers and explains what we do 
  - Pages are implemented 
    - Time is spent drafting and revising the content
      - Outline of messaging/content is written
        - NOTES:
          - Michael has good feedback
          - Remove some parts
          - Restructure it
          - Needs visual language (Robert: "quite some changes")
          - Breaking down how-it-works to something that is easily adjustible not an easy task
          - Producing a video or animation? Cons: extra work Pros: more effective way to approach the thing
            - Could be something mainly typographic, shapes, etc
            - Visualize an audio file?
          - There's a feedback loop between working on dashboard/project page -- simpler = minimizes the work the how-it-works section needs to do
          - First message is the two roads, unmaintained roads or toll roads. But we don't need to be sorlid on that message for the 1st milestone.
    - Look and feel is developed 
      - (CSS framework is expanded as necessary)

---

## Master to Production milestone

- Add activity feed to Snowdrift project page, which includes showing links to new blog posts. (Content will be manually updated)
- User auth is migrated; pre and post checks are done
  - Old users are told what to do. Also explain that their user profile will be gone for now (but it's backed up)
    - Document the steps needed to migrate user accounts, including the migration procedure and timing (tbd right as the site switches over?
  - Team people need to move over stuff on their personal user profiles to the wiki
  - Develop Pre and post QA checks for user migration
- Move blog posts to the wiki (/blog) (update #61)
- On wiki, set up /blog branch page for blog listing
- Permanent-Redirect legacy blog urls directly to their new location
- On wiki, rearrange front-facing pages to set up better permalinks (#308)
- Fix broken wiki links on /about (#304)
- Add new wiki url links in dev docs (#407)
- Check if any other wiki links need updating or are broken (#407)
- Make a list of wiki pages that need redirects, their legacy url, and their new url (@wolftune, blocked by #308)
- Permanent-redirect legacy wiki urls to the new location
- Create /trademarks page and check info is up-to-date (#54)
- Create /privacy page and check info is up-to-date (#182)
- Create /terms page and check info is up-to-date (#51)
- Remove 'Projects' from navbar
- Remove 'Search' from navbar
- Auth pages need nicer labels on fields (html/css)
- Check css on auth pages for bugs
## MAYBE
- Design error pages (404, 503)  (#262)
- Create error pages and add css (#262) 
- Give auth form failures good feedback
- Give feedback for weird email addresses. "Did you make a typo?"
