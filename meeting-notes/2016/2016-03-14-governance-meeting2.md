# Meeting: Governance Meeting #2

Time: 03/14/2016 05:00 PM - 06:45 PM (GMT+00:00) Casablanca

Location: meet.jit.si/snowdrift

Invitees: Aaron Wolf; Anita H.; Athan Spathas; Brian Smith; Bryan Richter; Charles Wyble; curtis gagliardi; Iko ~; Jason Harrer; kenneth wyrick; Michael Siepmann; Peter Harpending; Robert Martinez; Stephen Michel; William Hale

Attendees: Aaron Wolf; Stephen Michel; Iko ~

---

# Agenda

Finish building out initial Holacracy structure.
See WIP at http://shovel.snowdrift.coop/projects/snowdrift/wiki/Governance

- Project Manager

Process tensions:

- ML Heirarchy, owner
