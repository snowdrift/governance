<!-- See end of this pad for meeting-practices notes and reference -->

![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — May 6, 2022
==========================
::: info
- **More information**
 <https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
- **Past weeks' notes**
   <https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021>
- **Facilitation links** (timers and metrics)
   <https://smichel.me/snowdrift/timer/>
:::

### Check-in - 14 minutes
:::	success
- Informal time for socializing (updates on your week..)
- Pad prep (update metrics)
- Ask who would like time for post-meeting discussions.
- Confirm Agenda length & order (including open discussion).
:::

- Attendees: Salt, smichel, Adroit
    - Leaving early: 
- Facilitator: Salt
- Note Taker: smichel
    - Note-saver/pad-updater (if different):
    
### Minute of silence
<https://www.online-timers.com/timer-1-minute>
::: success
Get ready to be present in the meeting.
:::

### Centering Round
::: success
**What should others know about interacting with you today?**
- Are you present?
- What's distracting you?

::: warning
*Not for a full status update of everything that happened to you this week (unless it's affecting you today)*
:::

Previous Week Review
--------------------
::: success
Facilitator reads/summarizes
:::
### Meeting Feedback & Updates
- Many: Good to see everyone
- Salt: I'm not good at taking notes

### Metrics

|                                        | Last week | This Week |
|----------------------------------------|-----------|-----------|
| **Meeting attendees**                  | 5         |          |
|                                                                |
| **Snowdrift patrons**                  | 142       | ...       |
|                                                                |
| **Matrix:** #snowdrift (lines / words) | .. / ...  | .. / ...  |
|                                                                |
| **Discourse**                          |           |           |
|                                Signups | 0         | .         |
|                             New Topics | 0         | .         |
|                                  Posts | 3         | .         |
|                                DAU/MAU | 19%       | ..%       |
|                                                                |
| **Gitlab**                             |           |           |
|                   Git (commits/people) | 2 / 1     | . / .     |
| Merge Requests (Created/Merged/Closed) | 0 / 0 / 0 | . / . / . |
|               Issues (Opened / Closed) | 0 / 0     | . / .     |


### NEXT STEPs
::: success
Plan how to deal with each incomplete step.
:::

#### Last Week
- [ ] smichel/wolftune/??: Identify missing bits of clarity in goals/mission
    - [ ] Schedule coworking time to draft answers
        - We'll need to wait on this due to wolftune personal issues

#### Earlier (Carried over)
- [<] Salt: nail down what roles we're recruiting for

Agenda
------

### Open Discussion (Facilitator) - 5 minutes

- smichel: status update: made small progress on website— looked into discourse SSO and pinged osuosl about possible oauth provider. No concrete outcomes yet.

### Round for anyone who hasn't spoken yet (Facilitator)


Wrap-up - 5 minutes
-------------------
### Minute of Silence
https://www.online-timers.com/timer-1-minute
::: success
**Think about (and perhaps write out) your sign-out.** *Short, concrete, & specific:*
- How much will you be around this week?
- What will you focus on? (and does anything need capturing/scheduling?)
- What can others do to help you succeed?
- What meeting appreciations and/or suggestions do you have?
:::

### Sign-out round
- smichel: short & sweet, could have been shorter + sweeter.
- Salt: Agree it could have been shorter/sweeter. Would like to try and drop into the coffee shop more. I should have some time on weekdays this next week, checking-in with me appreciated.
- Adroit: Thought length was fine, sometimes they're short, especially with fewer people. Should be around starting next Thursday.
- msiep: I'll try to join coffee shops more, I like the idea of joining in the "busy but available" more just to be in the same space.

### Meeting Adjourned!
::: success
- Check that all NEXT-STEPS have someone assigned
- Move to the "coffee shop"
:::
