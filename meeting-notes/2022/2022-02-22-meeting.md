<!-- See end of this pad for meeting-practices notes and reference -->

![](https://pad.degrowth.net/uploads/upload_77445cab95fcc615d82149e0622dd4ab.svg)

Meeting — February 22, 2022
==========================
::: info
- **More information**
 <https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
- **Past weeks' notes**
   <https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021>
- **Facilitation links** (timers and metrics)
   <https://smichel.me/snowdrift/timer/>
:::

### Check-in - 14 minutes
:::	success
- Informal time for socializing (updates on your week..)
- Pad prep (update metrics)
- Ask who would like time for post-meeting discussions.
- Confirm Agenda length & order (including open discussion).
:::

- Attendees: Salt, alignwaivers, smichel, Adroit, MSiep
    - Leaving early: smichel, ¿MSiep?
- Facilitator: Salt
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):
    
### Minute of silence
<https://www.online-timers.com/timer-1-minute>
::: success
Get ready to be present in the meeting.
:::

### Centering Round
::: success
**What should others know about interacting with you today?**
- Are you present?
- What's distracting you?

::: warning
*Not for a full status update of everything that happened to you this week (unless it's affecting you today)*
:::

Previous Week Review
--------------------
::: success
Facilitator reads/summarizes
:::
### Meeting Feedback & Updates
-  Good balance of voices in open discussion. Aaron did a good job of leaving space.
- Salt: Didn't post the timers publicly, not sure if people use that. We did go over time several times
- wolftune: I appreciated non-strict timeboxes (human judgement). Glad I brought up topics by phrasing a question, I think that's a good pattern. 


### Metrics

|                                        | Last week | This Week |
|----------------------------------------|-----------|-----------|
| **Meeting attendees**                  | 4         | 5         |
|                                                                |
| **Snowdrift patrons**                  | 142       | 142       |
|                                                                |
| **Matrix:** #snowdrift (lines / words) | .. / ...  | .. / ...  |
|                                                                |
| **Discourse**                          |           |           |
|                                Signups | 0         | 0         |
|                             New Topics | 0         | 0         |
|                                  Posts | 5         | 3         |
|                                DAU/MAU | 18%       | 19%       |
|                                                                |
| **Gitlab**                             |           |           |
|                   Git (commits/people) | 9 / 3     | 2 / 1     |
| Merge Requests (Created/Merged/Closed) | 1 / 0 / 0 | 0 / 0 / 0 |
|               Issues (Opened / Closed) | 0 / 0     | 0 / 0     |


### NEXT STEPs
::: success
Plan how to deal with each incomplete step.
:::

#### Last Week


#### Earlier (Carried over)
- [x] wolftune: put back Nicky Case link in wiki article prominantly with more enthusiasm and a screenshot
- [ ] smichel (support appreciated): make sure team availability is updated and pick a new time

- [ ] Salt: nail down what roles we're recruiting for
- [ ] wolftune, Salt, smichel: Cowork on description for a tech lead / advisor role
    - <https://gitlab.com/snowdrift/snowdrift/-/issues/688>

Agenda
------

### New meeting time (smichel17) - 1 minute
- Most weekdays have times when *almost* everyone can make it. I'd like to chat 1:1 with everyone to get a more human sense of your schedules, then I will pick a time.

- [ ] Smichel announce new meeting time (which will begin next week)

### Momentum (smichel17) - 10m
- smichel: thinking about time and availability to put into things right now. I feel like we have more room to make actual progress and perhaps meetings are not the most productive 
    - we might be more effective to cowork weekly and then schedules meetings as necesary
- salt: I know it feels tedious but it would be great to automate metrics
    - I see value in checking with people but thats more like 5 - 15 minutes, might be nice to quickly move to coworking
- alignwaivers: seems very reasonable
- salt: the question would be if the formal meetings are helpful
- msiep: the checkins seem nice
- adroit: I wouldn't want to have a meeting less than once a month. One hour of work vs. meeting might not be that efficient in terms of the deep work at least what I do
- salt: part of what works with this coworking block, if I'm available for a big chunk of time I'd rather get to coworking
- smichel: It's nice to have people around when I'm working so I can ask a quick question
    - proposal: have a middle chunk of a 3 hr block for meeting part/ people around for questions 
- msiep: would it work to be a coffee shop available (you don't hear anthing unless someone asks a question)
- adroit: rephrasing 'guaranteed overlap of people being around'
- smichel: I'd propose to have coworking / checkin everyweek but not have a meeting unless there's something specific to discuss
- msiep: what if we try this for a month
- salt: like the idea of trying this for a certain trial period
- smichel: unless there are objections, I will pick a time to implement this next week and share


### Open Discussion (Facilitator) - 5 minutes



### Round for anyone who hasn't spoken yet (Facilitator)



Wrap-up - 5 minutes
-------------------
### Minute of Silence
https://www.online-timers.com/timer-1-minute
::: success
**Think about (and perhaps write out) your sign-out.** *Short, concrete, & specific:*
- How much will you be around this week?
- What will you focus on? (and does anything need capturing/scheduling?)
- What can others do to help you succeed?
- What meeting appreciations and/or suggestions do you have?
:::

### Sign-out round

- Salt: unsure about availability, but pingable, probably around thursday/friday. primary focus on "wrapping up" civi then maybe working on snowbot metrics collection... meeting time was used well, voices seemed balanced.
- alignwaivers: good meeting, not much to add
- smichel: (had to leave early)
- Adroit: updated availability, good meeting
- MSiep: good meeting, good to be here






### Meeting Adjourned!
::: success
- Check that all NEXT-STEPS have someone assigned
- Move to the "coffee shop"
:::

Post Meeting Discussion
-----------------------



### Next-steps capturing

<!--
Personal Scratch Pad
Salt
- computing for social good class project
    - https://pad.snowdrift.coop/p/role-development
- need to update slide deck, would be nice to have a short/long version ready to go for anyone
    - 5-min version (newer): https://seafile.test.snowdrift.coop/d/6861aa006f4e441da72a/?p=%2Fpromotional%2Fslide-deck&mode=list
    - longer version (old): https://seafile.test.snowdrift.coop/d/130a3bf5e9ab455bbc2e/?p=%2Fpromotional%2Fslide-deck-kit&mode=list
- work with athan on multiple versions of funding request but modified for researchers?
- civi... OSUOSL has been asking us for an update
    - CiviCRM 9.2.1 installation process test completed: https://pad.snowdrift.coop/p/civicrm-deploy

Meeting practices and tools
Pre-Meeting Tasks
- Update metrics, from these links:
    - https://snowdrift.coop/p/snowdrift
    - https://community.snowdrift.coop/admin?period=weekly
    - https://gitlab.com/groups/snowdrift/-/contribution_analytics
- Add items to the agenda. Include a timebox & GOAL(s). Decide on order.

Post-Meeting Tasks
- Clean up meeting notes
- Save them: https://gitlab.com/snowdrift/governance/-/tree/master/meeting-notes/2021
- Prepare this pad for next meeting:
    * Replace previous meeting eval notes with new, filtered down to essential points
        - Leave re-writing for meeting-prep, but do remove routine "thanks all" comments & next-ups
    * Clear previous-week metrics; update this week's attendee count if anyone arrived late
    * Clear the "NEXT STEPs Review" section, unless they have next-steps which are not done/captured.
    * Move new NEXT STEPs to that review section and then clear out other notes areas:
        - The snowshoveling check-in notes
        - Agenda items
            * Keep the topic title (but not topic leader/time) with the NEXT STEPs to review
            * Open discussion notes
    * Clear authorship colors
    * Update next meetings date

Location
- Normal: Mumble server: mumble.test.snowdrift.coop (leave port blank, or 64738) (mobile: half-duplex mode avoids echo)
- If we do video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift



Open Discussion
- Facilitator: make sure everyone speaks & that all outstanding NEXT STEPs are mentioned

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)

Timeboxing
- Prefer 5 minute increments. Leave time for Wrap-Up.
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
- Informal "thumb polls" inform the decision for extra timebox
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral
- Use https://online-timers.com, start for each item, paste each url in the chat
- Quick links for here: https://smichel.me/snowdrift/timer/

NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
