# Meeting — March 6, 2017

Attendees: iko, mray, Salt, wolftune

## Checkin

- iko: nothing to report
- mray: met Johannes yesterday and made some progress with video
- salt: good and super loaded with a lot of things, but snowdrift is a priority
  for LibrePlanet
- wolftune: will try to get back on track and drop some things
- been involved in some discussions on Portland political reform, slightly
  related to organising for goals leading up to snowdrift

---

## Agenda
- Alternating meeting times (Salt)
- Animation progress (mray)
- CiviCRM progress (Salt)
- Coworking more (wolftune)


## Alternating meeting times (Salt)

- salt: availability tool
  <https://oasis.sandstorm.io/grain/5dZHew9EgNfzTvm5BuXtNk>
- wolftune: this time works for me, but I'm flexible. I like the idea of
  alternating to get more people in
- salt: do we want to consider days besides Mondays?
- mray: I can do some days and I agree with enabling more people to attend
- a group I'm in does rotating days every 15 days, maybe we can do something
  like that every 6 days?
- salt: I like the idea but can't do it with my current calendar. what would
- how about 2 h earlier meeting every other week?
- mray: 1 h earlier could work, 2 h is nearly impossible
- salt: how about later? I'll have to ask smichel17 if the schedule is
  up-to-date
- the next slot is 3 h later, which may be late for you, mray
- mray: it can work, I just might not be able to stay much longer
- salt: smichel17's reply is friday or weekends is better
- I'll ask him about +3 … smichel17 says it's okay. do we want to give it a try
  next week, 3-13 at  23:00 UTC?
- mray: for me that would be midnight
- salt: I'm not sure how that's going to work with DST. the week after I'll be
  in Boston, but I can make time
- wolftune: DST is on the 12th for the US (this coming weekend) and 26th for
  Germany
- **Next step: wolftune makes announcement for trial alternating meeting time**

## Animation progress (mray)

- mray: <https://snowdrift.sylphs.net/seafhttp/files/6effbf9d-f77a-4997-82b0-50bf81765512/Animatic_v02.mp4>
- animation I made prior to Johannes' joining <https://snowdrift.sylphs.net/seafhttp/files/c83bf2d5-71f7-4d8e-811c-24aef7ce7ff3/x02_mechanism.mp4>
- I had some feedback for Johannes, which may keep him busy for a little while
- if possible, we should find a way to avoid numbers, but on the other hand we
  see the benefits of it
- we need some way to expand the quadratic growth without numbers, and depict
  the limit
- then as a 2nd part of the video, we add an emotional trigger with specific
  example with numbers
- salt: that looks good
- mray: tell that to Johannes. though the clip above is already outdated, I've
  sent him some feedback
- wolftune: I like the idea of figuring out, even the script, the part prior to
  the numbers before going to the numbers
- get the mechanism part right first. I wished there was a way to convey,
  related to the limit, the sense of where we are in the big picture
- where the free software is getting any decent sort of funding at all
- we're talking about people spending their own money and time, struggling and
  some money would make a difference
- what funds a project needs and we're not even close yet. we're about getting
  from nothing to something okay.
- mray: I'm not sure how that relates to the video?
- wolftune: people sometimes say, it's not always about the money, what happens
  when we get the money, just throwing money at the problem
- mray: I think you're overthinking this. not that I disagree, but …
- I think this will broaden the scope for a 1-min video. knowing that this
  project has potential at this stage is good enough
- wolftune: okay, I was just expressing my opinion, we can talk more about this
  later
- mray: what we need would be some audio. I'm not sure which way would be more
  productive: either wolftune could record audio touching on the 3 items
  discussed
- the other solution would be Johannes and I get together to come up with some
  text
- wolftune: we could do some live chat too, which relates to my agenda item
- mray: would you like to set up a time that works for you? Johannes is mainly
  available via email, he doesn't stay in chat much
- wolftune: I'll go ahead and do it on the Design ML in case anyone else is
  interested in joining
- **Next step: wolftune sends meeting announcement to Design list**


## CiviCRM progress (Salt)

- salt: I'm looking at cases at the moment, so if you feel there's a case that
  should be represented, I'd like to hear about them
- current cases: a donation was received, volunteer form submitted, user
  support
- iko:how about advisors?
- wolftune: oh yeah, we have advisors who we connect with. salt, what do you
  think about that as a topic?
- salt: I'm trying to think about how they start and end
- wolftune: it could be advisor onboarding. there's the matter of clarifying
  what the expectations are, and who to contact when a volunteer needs help
- salt: do you have any expectations for civicrm?
- as a reminder, civi is the thing that will track all our contacts (like an
  address book)
- anyone who interacts with other people on some level, e.g. look up people's
  contact info
- wolftune: as an example, mray, you may know someone who has certain skills,
  and you may add it to the db to let us know who they are
- mray: I'm hesitant to add people to a db who are not part of the project yet.
  I don't know how much consent there is when adding people
- salt: that's a good point. mainly to have contacts
- some things are passive consent, e.g. business cards given at a conference.
  not to sign them up for something, but to have that info


## Coworking more (wolftune)

- wolftune: I like coworking, any ideas how to do more of it?
- salt: I agree, sometimes it helps just to talk about it
- wolftune: imagine I'm sitting there distracted, if someone wanted to reach
  out and work on something together, that would help
- mray: I've been busy with mockups and using the right font, and changing
  other things in the process
- I just wanted to say if you're curious, everyone has access to the seafile
  and have a look if you're interested
