# Meeting — March 20, 2017

Attendees: iko, Salt, smichel17, wolftune, mray

---

## Agenda

- intro video script gap (mray)
- LibrePlanet BoF (Salt)
- CiviCRM etc.(wolftune)
- Taiga (wolftune)
- Coworking (wolftune)
- Elections (Salt)


## intro video script gap

- mray: it's about filling the gap in the video in terms of what we say, maybe
  working with wolftune
- wolftune: I'm happy to and have time today. we can do it as soon as the
  meeting ends, if that works for you
- **Next step: mray and wolftune work on the script with others interested
  after the meeting**

## LibrePlanet BoF

- Salt: I've been looking into Birds of a Feather, one is Sat lunch and Sun
- I was talking with freescholar, who mentioned lunch on Sat would be the
  better time, so I'm leaning towards that. any thoughts about that before
  I proceed?
- wolftune: it may be too late, but lightning talks are a thing to look at. Sat
  sounds fine
- maybe reach out to other snowdrift Mass. connections
- draft an announcement email with reference to other conferences coming up,
  maybe they'll just show up?
- Salt: I like the idea. wolftune, can you help me with drafting the
  announcement? today or tomorrow will work best for me
- wolftune: sure, after working with mray. how about sending out a casual
  message at LibrePlanet that says, "this is my first time at LP and
  representing snowdrift.coop, where do we go for lunch, etc."?
- smichel17: there might still be room for Lightning Talks
  <https://libreplanet.org/wiki/LibrePlanet:Conference/2017/Lightning_Talks>
- in the past they have an editable wiki page for the talks where people
  interested can add their talk to the list, but there isn't one this year, so
  I'm not sure what's happening with that
- **Next step: Salt posts to LibrePlanet list about BoF; start drafting an
  announcement to the snowdrift list; smichel17 possibly give a lightning
  talk**

## CiviCRM etc.

- wolftune: it's one of those things I'd like to keep mentioning until we get
  through it. any status updates?
- Salt: not yet, I'm still working through the technical list. I'm not sure
  what else needs to be done to bring it to an usable state
- wolftune: we can sit down and try adding 1-2 entries, to see what needs to be
  configured
- Salt: I can make an hour for that either today, tomorrow or Wednesday morning
- I'd like to play around with a few cases too, with conference cards and such
- it's now living at contacts.snowdrift.coop **Next step: Salt and wolftune
  meet for a short session to work on CiviCRM setup**

## Taiga

- wolftune: I should just go to the page and start looking at things
- I wanted to bring it up so we stay connected to the tool and use it more
- figure out what is/isn't working well for us
- smichel17: what would make you use Taiga more, for yourself?
- wolftune: if I went to just the main Taiga … it's hard to explain. I see a
  lot of stuff at once, many options to go, and have a cognitive tension
- the landing process at Taiga is "all of that" versus "I can focus on this"
- smichel17: I'd suggest using the kanban as the landing page and not worry
  about the other things for now
- different people may need different views/columns open
- wolftune: I'd probably also need to see what's assigned to me
- smichel17: that would be useful
- the main snowdrift project is there so we can have the roadmaps section, so
  you can see the milestones
- for tracking progress across Circles, that's the overview
- if you want to do something for one of them, then you go to the kanbans
- wolftune: instead of linking to Taiga, we can link to a wiki page (the wiki
  page could live on Taiga) with a list of links to the different projects and
  how they're set up
- smichel17: sounds reasonable
- wolftune: can you update it?
- smichel17: sure. the only reason I haven't done it sooner is the images from
  the taiga guide have to be re-uploaded again whenever the guide is moved,
  which is cumbersome
- Salt: the reason I don't use Taiga as much is unrelated — I already know what
  I'm doing and don't need to spend a lot time on it
- iko: different from <https://wiki.snowdrift.coop/resources>?
- wolftune: something like that. it would be nice to duplicate this on the
  Taiga wiki page
- **Next step: smichel17 updates a wiki page on Taiga main project, adding to
  it links that are most valuable for using Taiga and link to it from Resources
  page on snowdrift wiki**

## Coworking

- wolftune: I brought it up as something we can do more often
- we've actually covered it in other agenda items, no need for further
  discussion at the moment

## Elections

- Salt: should we try to do an election process now and see if people want to
  drop their current roles, take on new ones, etc.?
- wolftune: no particular opinion either way
- smichel17: if I'm in class, you can ping me on IRC and I can share my opinion
  for 5 minutes
- I can archive those notes and make sure they get to the right places later
- smichel17: if the secretary/facilitator cannot attend to a meeting, the
  person can do it for when they can attend and someone can step up and do it
- Salt: sounds weird to me, people shouldn't step up and do something because
  another person isn't there, but people can of course arrange for someone else
  to do it in their absence
- smichel17: yeah, I wouldn't suggest doing that in the long run
- Salt: iko, how do you feel about the current status and taking notes?
- iko: no problem, I'm not formally listed for the role and just do it
  on a "I'm here when I'm here" basis. hopefully someone will do it when I'm
  not around :)
- if whoever took notes left it on the etherpad afterwards, I can file it later
- smichel17: individual initiative
  <https://github.com/holacracyone/Holacracy-Constitution/blob/master/Holacracy-Constitution.md#43-individual-action>
- iko: thanks Salt for facilitating too and keeping the meetings on track
- Salt, wolftune: in that case, I propose we don't do elections today and leave
  it for another time

