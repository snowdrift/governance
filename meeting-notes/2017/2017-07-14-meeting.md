<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2017/ -->

# Meeting — July 14, 2017

Attendees: mray, MSiep, Salt, smichel, wolftune

**No new agenda items**

<!-- Agenda -->

## Carried over items / next steps
From https://wiki.snowdrift.coop/resources/meetings/2017/2017-06-16-meeting
and also https://wiki.snowdrift.coop/resources/meetings/2017/2017-07-07-meeting

### Holacracy thoughts

- smichel: make report about what we’ve got so far, what seems to be working, and what we would need to do to go further

### prioritizing iterative work process / dev

- smichel will document this process better: find tasks: Kanban → US → internal tasks for the US

### alternate co-op direction

- wolftune: create report / pros-cons etc. for everyone to read and then discuss by email, organize discussion

### code policy clarity

- fr33 will talk to chreekat, get him to document his criteria/policy for code merging and onboard other maintainers

### Specs for code modules

- fr33 will talk to chreekat, about the mechanism spec

### Update design US reference links

- mray updates reference link in https://tree.taiga.io/project/snowdrift-dev/us/454 to the mockup (?)

### DB variables from backend needed for dashboard history

- fr33: ping chreekat / make issue to provide these variables

### Ghost blog launch

 - Salt: put up Ghost docker

### Discourse launch

- Salt: back up existing Discourse to allow safe testing prior to full launch/use
- Salt: check with MitchellSalad about SSO



