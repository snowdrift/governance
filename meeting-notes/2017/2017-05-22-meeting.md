# Meeting — May 22, 2017

*Note: the meeting on May 15, 2017 was brief with no agenda or carried-over items. No meeting notes were recorded. Attendees were iko, MSiep, Salt, wolftune.*

Attendees: alyssa, iko, mray, Salt, wolftune

---

## Agenda

### Blog update \\o/

- iko: blog demo instance now has a different look <https://snowdrift.sylphs.net/blog/>
- translated mray's front page mockup to html/css. it's based on the default theme (Casper) for page templates, stripped unused features and remote asset requests from those and added new stylesheet
- have some questions to review with mray, hopefully not many changes. otherwise it should be usable in current state
- made some minor fixes to imported posts, e.g. footnote numbering, converting pandoc-style footnotes and Ghost not automatically inserting `<em>` tags in footnotes
- side effect: wiki is now using the updated base css file, with decision to move back to Nunito
- also made a little script to simplify updates on demo instance, in case people want the demo instance around a little longer (have updated twice already, at least it's not a complete waste)
- wolftune: are there any blockers?
- iko: no blockers. I'll make some adjustments to the theme after mray's had a look at it, then wrap up the Docker script with the data
- Salt: I'll try to make some time this week to install the blog
- **Next step: Salt will look into installing Ghost Docker on mx1 on Wed/Thu**

### Public listing of tasks/roles, delagating, outreach

- wolftune: we're all busy, it would be nice if we can post updates to the ML regularly to let people know we're around and keep them engaged
- if people see someone is working on a task and they're busy, they can ask questions and offer help to see the task through
- alyssa: do you already have a issue tracker for this?
- iko: we do. my impression is the current project management tool, taiga, enforces a particular way of doing things and has a learning curve
- people are busy and don't really have time to learn the whole system, practise using it
- wolftune: that's one part. another thing is, when talking about research, community and other project management-type stuff, dumping all of that into an issue tracker isn't as usable
- there's also a human element of engaging people on the ML
- alyssa: that's okay, but e.g. in libreboot, people focus on the dev issues and less on the management
- wolftune: in some ways that is a simpler situation, people already familiar with the development environment
- we want to get people externally engaged enough to become a contributor
- Salt: I would like to see a weekly/bi-weekly blog post
- an issue tracker is useful, and taiga may be useful again with the text moved off
- we're stuck in the mud working with tools that no one wants to work with. we should focus on getting the work done
- wolftune: I would like to suggest that if you accept a task or next step, then you see to it that something is happening to it, e.g. find someone else to delegate to if you are unable to work on something
- alyssa: would it help to send out meeting notes to the ML?
- wolftune: possibly … there might be better or worse ways to do it, but more broadly. it helps keep the ball rolling
- alyssa: by the way, in a week I'll be done with finals, so I'll be available then
- wolftune: putting out your thoughts publicly so it's more transparent and people are more aware of what's going on
- Salt: I'll try, but time used for that will be less time to do the work
- mray: it sounds I would have to spend even more time writing this down, it will set an expectation for myself to be clear, and it will be quite a burden
- I think it will be better to keep it simple as alyssa mentioned, having an easy-to-use issue tracker, e.g. I'm working on issue x
- wolftune: I'm not trying to make it too strict, if you don't have time to explain the situation, then you can just say "I'm working on this, it's still in progress"
- then people might ask how that works and what you need help on
- mray: but if I did that, then I'd have to expect some interested. like I'm trying to do this to motivate people
- that requires me to be there to be a responsive partner and initiate things
- wolftune: some tasks will be easy to say, some tasks are more complicated
- alyssa: it's unclear what the primary method of communication is, whether it's irc, ML, forum, etc.
- having all of it is off-putting to newcomers. I've been following the project for a month now and I still haven't got the full picture of the status
- I don't think adding a new method of communication is going to solve anything
- wolftune: my thought is, the problem is not having multiple communication tools, it's that we're not communicating at all
- alyssa: I didn't realise there was a ML until someone mentioned it 2 meetings ago
- Salt: can I address the communication tools topic?
- we had this launch momentum at one point, but didn't get to it. the big reason we went "dark" was to not announce things until things were ready
- mray:  it's not only about showing activity, it's about being relevant. as soon as people realise it's like a keepalive signal, people will eventually tune out
- Salt: I agree with mray, and I think it should be a well-crafted, full announcement email. not a dribble, or people will tune us out
- wolftune: we can still start discussion on the ML, I would not see it as an announcement to the public
- mray: I wouldn't make such a distinction between ML subscribers and the public, the people on the ML are part of the public
- Salt: yeah, but that's not how they're used. right now it's dead air, so to post things without actual content is not the right direction
- alyssa: I guess what's bothering me is (and similar to mray and Salt's concern) right now, there isn't stuff there. like the project is dead, and yelling that it's not is not going to attract people
- wolftune: our ML announcements can still get people asking questions and discussing things
- mray: there are too few things happening to discuss about. the people who would like to discuss (project features/direction) are so far in the future that we are not ready to talk about it yet
- wolftune: this is more a case about the perfect being the enemy of the good type of issue. it's better to engage with people while in progress than wait and wait to reach out to people
- it's not announcements, but it can just be discussion
- mray: I don't see how we can draw people
- wolftune: smichel17 isn't here at the meeting and hasn't been very active the past few weeks. an announcement can bring in people who may have project management-related or other skills to help with tasks
- mray: if there is a task and you want to trigger some help, there needs to be something to build upon. what people will attract people is reading news about progress and things that are happening
- wolftune: I agree but, there are potentially people who could make a difference in the project.
the other side is the pressure, keeping and maintaining momentum. it's like an accountability that we don't do as well we could
- alyssa: are we not getting this on IRC?
- wolftune: some, but there are people who aren't on IRC and would be interested in engaging in a non-RTC medium
- I would like people to keep this context in mind even if we're not making a decision. look for opportunities where it's a positive thing to post to the list. I encourage people to consider it
- Salt: would you like this to come up as a next action?
- wolftune: no, I've said what I needed to say for now


### BeWelcome guest feedback

- Salt: I met an EU developer who was very interested in snowdrift because he's had thoughts on similar things. he sent me a list of questions a few days after
- e.g. whether or not people will actually think about game theory as an important question when funding things?
- wolfune: as a quick answer, no, we don't prioritise that people will think about game theory, only a subset of people will care about that. the fundamental thing is public goods vs. proprietary things
- *(Salt provides an excerpt with questions)*

<blockquote>
Some thoughts in my mind now, most of which we shortly touched upon:

  * An important question is whether a game-theoretically advantageous
    solution will also convince people to contribute. People are not
    economically rational beings; knowing others will match my gift
    might not do much to me, unless I learn to value this principle. We
    should take psychology into account as well as game theory. Having
    to pay more when others pay too could actually also demotivate.. I'm
    thinking that maybe attaching a sort of visible membership status
    for patrons of a project might motivate people.
  * In software projects, each depends on others, and having users pay
    just the ones they know about may skew rewards strongly towards end
    applications. I believe we should much more strongly incentivise
    people to build/publish/document/maintain reusable software modules
    rather than monoliths. Considering the end application's developers
    as the 'end users' of such a module, I wonder if we can concoct a
    similar payment scheme between software projects and their
    dependencies. So we get a donation structure that applies
    recursively, just like markets (the same money drives B2B as well as
    B2C trade).
  * When letting people pay for things they enjoy, and the payment just
    depends on how many people enjoy it, we fail to take into account
    the question how much funding this project actually requires. Some
    things provide a lot of joy, but cost very little to make, and will
    receive more money than needed, and in a bad scenario everybody will
    try start more such projects. Everything being free, we lack a price
    mechanism. I wonder how resources can be allocated effectively
    without the usual price mechanism powered by scarcity and
    competition. Perhaps the quadratic system does create a similar
    effect to some extent: for those things which make easy money, many
    may try compete with them, and assuming people then spread among the
    different projects, each project will get less money. Because of the
    non-linearity, the whole bunch of competitors as a whole will then
    get less money allocated.
  * Perhaps the assessment which people should support which projects
    could be made by third parties (other people). This could occur
    between users and their software, but especially between projects
    and their dependencies. No idea how this would work though.
</blockquote>

- **Next steps: add contact to CiviCRM, respond to the email**


### OSB

- wolftune: OSB is coming up. Salt, will you be coming?
- Salt: I'll be there, but haven't got lodging and bus ticket settled yet. I haven't heard about volunteering arrangements yet
- on Fri I'll probably be attending the IndieWeb leadership summit. it's for people leading IndieWebCamps
- alyssa: I'll be there. we have a reservation for the hotel, I haven't heard back from the volunteering coordinator and don't know about the tickets yet
- wolftune: we could check on chreekat and others interest and arrange a snowdrift meetup around OSB
- Salt: we can both balance that and discuss it later
