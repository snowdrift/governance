# Meeting — June 9, 2017

Attendees: alyssa, chreekat, iko, Salt, wolftune

## Agenda

### Taiga tours?

- wolftune: I think a few people (smichel17, iko, chreekat, etc.) understood
  the workflow in Taiga better than I do
- since becoming a parent I've had some time but a feeling of uncertainty when
  to do what
- it made a difference iko pinged me this week, I used to be better at it
  myself
- it may be representative of some other volunteers where approaching them to
  schedule things to do might be useful
- can we schedule a time next week to invite people to a jitsi meet, a live
  thing with screenshare and walkthrough would be really nice. any thoughts on
  that?
- Salt: sounds like a good idea
- alyssa: I thought we decided that Jitsi Meet doesn't scale well
- wolftune: the newer version has some useful features
- chreekat: +1 walkthrough usefulness
- I tried jitsi with iko last week and it bombed spectacularly
- alyssa: might as well just do that via ffmpeg or whatever.... i'm a nerd haha
- Salt: xforwarding over ssh yo!
- chreekat: also, bandwidth issue, i think it would be most benificial use of
  time to *work on things* as a means of understanding the systems
- like we don't need complete information, we need to practice our own work
- as i do code and talk about code, other people who know taiga are doing a
  better job than i could keeping it up to date
- i learn by proxy. do more stuff :) and talk about it
- somewhat speaking for myself, of course, but "individuals and interactions
  over processes and tools"
- wolftune: I agree, but have been forced to use the thing and ran into an
  obstacle, and stopped
- thoughts on going about making this idea happen? does it make sense to start
  a new thread about it?
- there's the idea of a volunteer orientation session. maybe we can arrange one
  sometime?
- iko: not really any newbie focused tasks at the moment, so it's not helpful.
  maybe later once there are more new tasks, we can leverage new volunteers
  with their skills. but now? probably not useful for this arc
- Salt: chreekat you sound like someone who attends IndieWebCamps :P
- alyssa: +1 to what iko is saying
- chreekat: i'll second that notion: "once we have a new epic"... i.e. there
  may be some moments in time that are better than others for doing
  orientations
- chreekat: "defer till next epic kicks off" ?
- **Next step: defer orientation session until next epic starts**


### Task wrangling / nagging

- wolftune: related to the previous item, feel free to nag me if there's
  something you think I should look at/do
- I guess Salt is also feeling like that?
- Salt: same except I'm busy until the start of July and nagging won't help
  then
- wolftune: I don't want to rely on people nagging me to do stuff, I'm
  self-motivated enough, but while we're transitioning to a better workflow
  with the tools nagging is the fallback
- iko: the Taiga kanban is available to find 2-3 higher priority tasks to work
  on. if you're unsure how something works, feel free to ping me or smichel17
- wolftune: how's the taiga guide?
- smichel17: It needs some updates.. it's a reasonable tutorial of the features
  of taiga but not great for workflow
- wolftune: the tactical meetings we had can be helpful
- smichel17: I think the holacracy tactical meeting process may have been too
  formal and led to a bit of lost productivity
- wolftune: it would still be helpful to have a tactical meeting where people
  talk about updates and where things are
- smichel17: that's ideally the purpose of tactical meetings, too, I think
- *(Salt disconnected due to network issue and reconnected again)*
- Salt: I missed part of the log. are there Next Steps for task wrangling?
- smichel17: I said, essentially, "Tactical meetings have a bunch of check in
  rounds at the beginning. I think they're too formal to be useful (people
  aren't comfortable with them), but they're essentially the same purpose as
  what you (wolftune) said earlier"
- iko: wolftune, didn't you and smichel17 had a session sometime ago to review
  your backlog? there's a taiga wiki page with a list, or is there a new list
  now?
- smichel17:
  <https://tree.taiga.io/project/snowdrift/wiki/wolftunes-old-backlog>
- iko: smichel17 and I had a look through the list, and it seems some of the
  items are someday feature ideas (with no decision whether they will be
  implemented) mixed in with actionable tasks
- chreekat: my guess is that what's needed is to just go through that list
  again, add/remove/update
- chreekat: definitely think that if anyone has the opportunity to sit with
  wolftune and talk through them, being free with opinions on "do this now" and
  "don't do this, or do it way later", that will help bring clarity
- iko: wolftune, can you clarify your status/frame of mind on tasks? e.g. do
  you find yourself with time and looking for tasks to do?
- wolftune: my current status is I have some tasks in my own personal organiser
  with a tag for snowdrift
- I should put them in taiga so things are clear, but it may bring up issues
  about future features we're not ready to talk about yet
- I have a bunch of snowdrift emails to follow up
- there's the idea of going to taiga and doing the stuff there. essentially I
  have some awareness of the types of things in each
- a lot of the tasks aren't fully captured in taiga, some aren't specific
  enough for me to do, like a topic
- epics that are too broad and multi-faceted
- chreekat: re: brain dump, I would suggest that not too much time is spent
  getting wolftune up to speed on using taiga. let iko and smichel own that
  tool and use it/configure it as they wish. I think that looks like it will
  work a lot better than getting all of us using the tool. They use the tool
  for themselves to build data that's useful to all of us, but really, the fact
      that it's Taiga and that "you add a user story here" is implementation
      details for how they generate their value for the project in that role -
      not necessary for others to be deeply familiar with
- chreekat: i recognize that both of you have other roles in which you do
  valuable things for the project :)
- wolftune: chreekat: seems sorta like git in that it's possible to work with a
  contributor who doesn't know it by organizing things and accepting their
  submissions otherwise, but show them the process and eventually they may
  become comfortable themselves
- smichel17: I have two disagreements:
    - 1) The tool should be simple and easy enough that anybody can contribute
      without any training to get up to speed, like github/gitlab issues. We
      just need to minimize process as much as possible.
    - 2) I agree that capturing and setting up new stuff is something iko and I
      could own, but others need to be updating those items as they work on
      them; it is not feasible for us to keep on top of that.
- smichel17: The primary value of taiga is that it enables async work
- chreekat, iko: agreed
- chreekat: When I literally have 2 hours a week to devote to snowdrift,
  however, I am going to spend those hours programming, and I will feel no
  remorse at being confused by taiga
- smichel17: for example, the past few months I've been somewhat out of sync
  with things. If Taiga is up to date, you can link me to a relevant US/issue
  and I can quickly get up to date and be productive on it this afternoon :)
- smichel17: wolftune: I think it is perfectly acceptable to put a task of
  "send an email" into taiga.
- wolftune: your <https://tree.taiga.io/> should be useful as a todo list (when
  logged in)
- iko: but there's no difference (in terms of knowing which tasks to work on)
  between me checking the Taiga kanban then reminding you about a task, and you
  going to the kanban and seeing the same task listed?
- wolftune: it may be the human interaction, at times it's helpful to have
  someone remind me I have a task to do instead of reading articles
- smichel17: that sounds like a personal problem, not a tooling problem :P
- chreekat: I think stream-of-consciousness talking into irc about work status
  is a step up
- smichel17: chreekat: that's great for real-time but not for general project
  stuff because it is not feasible to read the whole backlog all the time
- iko: yeah, chreekat. I asked to try to understand what the blocker(s) are for
  others e.g. wolftune, between finding tasks and doing them
- smichel17: but agreed on the previous thing of hopefully improving over time
- wolftune: I kind need a day to clear this up and try to build systems that
  don't let it build up
- I probably should've gone to the library and find the work space
- smichel17: After this meeting, I am walking back home, then working on
  snowdrift stuff until I get sleepy
- smichel17: Prob half hour to an hour after the meeting for me to get home and
  set, if you want to commute to the library this afternoon
- chreekat: ^ wolftune are you free this afternoon?
- wolftune: yes
- chreekat: doo ittt
- **Next step: wolftune goes through backlog with help from smichel17 and
  others interested**


### PSA: design documentation moved

- iko: design docs have moved to
  <https://git.snowdrift.coop/sd/design/blob/master/docs/readme.md> in case
  people want to link to them
- smichel17: Where were they before, on the wiki?
- iko: yeah, partially. the branch page was deleted and the links in it were
  relocated to `/resources`. a few of the links were helpful to know but most
  were specific to design and not of general interest
- I spoke with mray and he's interested in moving things to GitLab and trying
  out GitLab Issues. seeing also dev docs are hosted with the code repo
- hopefully it'll be easier for mray and other design people to update and
  maintain the docs, from Seafile
- smichel17: I was thinking about the wiki and essentially it has 3 types of
  stuff on it:
     - 1) ABOUT: "This is the context around FLO funding and the mechanism"
     - 2) WORKING: "This is how snowdrift.coop does/will work"
     - 3) "This is the current status of things / how you can get involved"
- Salt: next step is to update the wiki with the new link?
- wolftune: iko, haven't they already been updated?
- iko: yeah, I'll double-check but pages should all be updated


### Carried-over items

- Salt: <https://wiki.snowdrift.coop/resources/meetings/2017/2017-06-02-meeting>

#### Merge request for alpha-ui branch

- Salt: has this happened yet?
- chreekat: yes, completed. MR is live, WIP
  <https://git.snowdrift.coop/sd/snowdrift/merge_requests/40>

#### Task list reviewing, check-ins, public list of assigned tasks

- chreekat: let's call the next step "talk about it this meeting" and call it
  done
- i think it was much the same as what we were talking about today

#### Haskell volunteer onboarding

- Salt: this was about tagging newcomer-friendly haskell tasks
- iko: I threw this up
  <https://tree.taiga.io/project/snowdrift-dev/wiki/volunteer-info>
- it includes Taiga issues filter links to sort development (haskell) tasks by
  priority, one for "development and newcomer"
- it's on taiga wiki at the moment, I haven't had a chance to discuss with
  smichel17 where to incorporate or put it
- wolftune: looks fine overall, one thing that should be mentioned is building
  the codebase to get a starting foundation
- we should add this information to the how-to-help wiki page and the code
  documentation in the repo
- chreekat: on that note, the README of the website is much too verbose, needs
  to look much more like the design repo's readme!
- **Next steps:**
    - smichel17 reviews volunteer-info page, decides where it should go
    - add mention of initial code building as a start before then finding tasks
    - iko adds links to the final version of that page at appropriate code docs
      and wiki pages


### Items to move forward

- Video status - mray was not present at the meeting
- Salt to insert Ghost Docker Container
- Salt to check with MitchellSalad about SSO
