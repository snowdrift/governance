# Meeting 12

*February 15 2014, 9AM Pacific*  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)*

## Background readings / updates

Exploring the updated site, including the discussion comments, tickets

### Assigned tasks from before

* Aaron: write Knight fellowship app, continue work on site, recruiting, contact Deb, contact Task Coach devs, contact Nina
* Mike: Come up with list of potential projects
* Kim: Standing Rules, come up with at least one potential project, review Knight application
* Joe: Continue site development.
* David: Keep whittling away at tickets, be available in IRC and get new developers up to speed.
* James missed the meeting, but proposing projects is focus along with various other things

## Agenda

* Check in with assigned tasks from previous meeting, agenda review
* [New committee applicant](https://snowdrift.coop/p/snowdrift/application/18)
* Technical update
* Starting to move ahead on fundraising and fund-drive
    * Signed up for Dwolla, Coinvoice, starting others, accepting funds ASAP
* Site moderation status / discussion
* Project sign-up status / discussion
* Recruiting representatives from potential Snowdrift projects
* Fake money testing discussion
* Legal status discussion
* Priorities for Aaron: code-study vs site design vs writing vs recruiting vs presentation/video etc.
* Should Aaron go to LibrePlanet?
* Between meetings clarification about best way and how much to engage the committee

## Next steps assigned

Aaron: meet.jit.si testing, write welcome message for new users, work on presentation / video etc. toward fund-drive, work with David implementing established-user process, etc., contact Nina and other projects (whoever is most aligned) and ask for specifically feedback about proposed requirements / honor-code; schedule flight and other details for LibrePlanet.

David: BalancedPayments set up, established user stuff, mod stuff, project application forms live for project intake, if time: more ticketing features and other priority things

Kim: Finish Standing Rules revision

Mike: Move project ideas to wiki, keep working on project ideas and issues

Joe (who was absent from meeting): code improvements, updates on site, etc.

James: Work out that issue with possible web-design intern

Chad: Get caught up on anything else around the site, share any thoughts on things, comments on the site, esp. regarding project requirement pages.

David? Someone else?: Test Dwolla to verify whether it can operate with LibreJS or not (and if trouble, see if can manually or something allow specific JS and determine whether any of the required JS is proprietary or not)

ALL: be available by e-mail and/or IRC and/or on site discussions for particular issues. We will discuss project requirements / honor code as we start reaching out to projects for feedback.

## Summary / minutes

### Check in

Aaron:  
Knight fellowship app went in, got rejected, no reason given
Lots of work on site, many minor details
Posted to some co-op connections online,
Got recommendations from Deb re: Accountant — nothing good just some references she couldn't speak for.
Have not contacted project folks yet, but worked on the listing on the wiki for planning to reach out

Mike:  
Here's some project ideas: Long-term community collaborations with incorporation but not large benefactor, eg http://www.appropedia.org/Welcome_to_Appropedia
Individual artists/programmers contributing to FLO whole career, but without large benefactor like Peter St. Andre (does translations of texts in other languages in addition to programming)

Kim:  
Standing Rules moved, revision at least started.

Open Goldberg Variations by Kimiko Ishizaka: has done successful Kickstarters to create public-domain sheet music and performances of classical music, could benefit from steady income and more frequent releases through the Snowdrift system.

(Aaron says: isn't Open Goldberg basically complete? That project on its own is done, isn't it? Or no? MusOpen is the larger idea of more of the same I thought.)

David:  
Got some things fixed earlier in the month. Both computer and backup computer died, so no progress in the last bit. Fixed computer is purportedly "out for delivery".

James:  
Talking about potential design internship for a friend since we're now working on web design and mobile compatibility and Joe's internship end may be coming up soon.  James and Aaron will review her resume and meet with her to discuss this.

Chad: Introduced self

Agenda review: approved agenda. Two items removed as mostly resolved during other discussion.

Starting to move ahead on fundraising and fund-drive

Signed up for Dwolla, Coinvoice, starting Balanced, accepting funds ASAP

Aaron wants to focus on making payment possible, creating promotional videos, and emphasizing that we take donations, instead of setting a timeline for a formal fund drive right now

Fluctuations in mobile-to-mobile market right now mean we might want to avoid until things settle

Probably do a self-hosted drive like MediaGoblin, some but not much coding involved

Working with Coinvoice to get something better than an "email us for an invoice" system.

### Site moderation status / discussion

David can manually delete users and individual comments, moving comments is almost done.

Established users: should have made several valid moderated posts, moderator sends them notification requiring acceptance of honor pledge, then they can post unmoderated.

No wiki editing until a user is established through commenting (for now anyway)

### Project sign-up status / discussion

Kim's sign-up forms have not yet gone live.

Need to produce a way to view for approval, probably via wiki page.

Discussion system is secondary concern

Recruiting representatives from potential Snowdrift projects

Are we ready to start talking to projects?

Should probably start with "here's what we're proposing re: setup and project requirements, how would this work for you / what feedback do you have for us on site design to serve you better?

Fake money testing discussion

We do want projects involved in this stage to show them how it scales before we start with real money

### Should Aaron go to LibrePlanet?

Yes, Aaron will go.

Between meetings clarification about best way and how much to engage the committee
