# Meeting 16

*Wednesday, August 6, 2014, 7PM Pacific*

via <https://meet.jit.si/snowdriftmeeting> — requires Chromium, works with either audio-video or audio-only and has text chat too

back-up-option:
phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
backup alternate number: *951-262-7373*  
live-recording of minutes on [etherpad](https://snowdrift.etherpad.mozilla.org/1)

## Background readings / updates

### Assigned tasks from before

* Kim: investigate Loomio
* Aaron: start process of Open Hatch signing up, draft letter, clarify things; site work as feasible, follow-up with more connections etc.
* David & Mitchell: coding etc.

## Agenda

* Check in with assigned tasks from previous meeting, agenda review
* Technical update
* Aaron's report from CLS / OSCON

* **Legal direction discussion**
    * Questioning assumptions, revisit:
        * Why do we need 501?
        * Consider a mission-driven status otherwise like B-Corp? As long as co-op structure works…?
    * updates about 501(c)(4) options:
        * We can legally operate under self-determination of (c)(4), although asking for formal IRS determination has benefits
            * Only serious issues with self-determination are in case of audit, and we otherwise just do normal 990 filing
            * Preferable to still get IRS determination, of course
        * Could/should still get legal advice
        * Are we sure that we can say we provide no special benefit to members?
* **Project-sign-up discussion**
    * Probably get actual project intake forms up first, right?
* **Planning the next couple months**
    * Fund drive soon?
    * Making the most of the end chunk of Mitchell's summer contract
    * General discussion about launch time-frame
    
* Any other issues / important business

## Next steps assigned

Aaron will refocus our legal presentation toward the idea of forgoing 501 tax-exemption. Work will start ASAP to run a fund-drive for legal costs.

Mitchell and David (and Aaron) will keep working on progress focused on making the site more engaging, providing notifications etc.

## Summary / minutes

So, meet.jit.si failed! It basically didn't work at all. We ended up just doing IRC chat, here's the log:

    [7:17:32 PM] <wolftune> So, CLS / OSCON went well, but for the first time after a lot of conferences, I haven't bothered following up on every contact
    [7:18:03 PM] <wolftune> I have stacks of biz cards from WriteTheDocs, Open Source Bridge, and CLS/OSCON, and a couple weeks from now is DebConf
    [7:18:22 PM] <wolftune> We have a number of new interested volunteers and connections
    [7:18:32 PM] <wolftune> some Haskellers and other folks here in IRC etc.
    [7:18:49 PM] <jsheldon> Maybe you could get a business card scanner and we could store them in the wiki?
    [7:18:53 PM] <wolftune> been nice that enyst and lpsmith and a few others have been somewhat active
    [7:18:58 PM] <wolftune> jsheldon: interesting idea…
    [7:19:30 PM] <wolftune> I haven't talked about this before, but what I've done is sent a follow-up e-mail so that I can later go through a backlog of e-mails to follow up when we want to reach out more in the future
    [7:19:37 PM] <wolftune> and some people get back to me sooner of course
    [7:19:42 PM] <apotheon> It might be a good idea to ask people whether they mind their business cards posted online.
    [7:19:54 PM] <wolftune> I wouldn't do that without asking yeah
    [7:19:57 PM] <jsheldon> Yeah.  I meant like steering cmte only.
    [7:20:14 PM] <apotheon> ah
    [7:20:25 PM] <wolftune> I think private collections of contacts is something, but the ideal is to get to where the site is more able to coordinate things
    [7:20:33 PM] <wolftune> so MitchellSalad has been working on e-mail integration for the site
    [7:21:04 PM] <wolftune> I kinda figure when I can more readily say "make an account, comment, etc" and the site will have e-mail integration so it keeps people connected, then it's more worth it to promote
    [7:21:19 PM] <wolftune> anyway, I participated in several meetings with FLOSS Foundations folks
    [7:21:36 PM] <wolftune> and that was surrounding lots of discussion about 501 tax-exemption issues
    [7:22:26 PM] <wolftune> Let's take a moment to get MitchellSalad and davidthomas to just add anything you want to make sure or get quick feedback on regarding technical progress / priorities and then we'll move on to this discussion about legal things and plans
    [7:24:08 PM] <MitchellSalad>    alright
    [7:24:53 PM] <wolftune> maybe summarize where things are and what the plans are for the next few weeks or time-frame on getting the immediate work done
    [7:25:21 PM] <MitchellSalad>    i'm working on implementing an event system so that we can react to things that happen on the site (such as a comment being posted). this will eventually power the email integration, but before that, it's going to power an in-house message delivery system (the snowdrift inbox)
    [7:25:40 PM] <MitchellSalad>    i'm also in the process of revamping the project discussion page, so it's a little easier to read
    [7:26:04 PM] <MitchellSalad>    and sort of a combination of the two, building out a project feed page, which combines all project events (new comments, wiki pages, pledgers, etc.)
    [7:26:27 PM] <MitchellSalad>     /newcomments and /newedits are dead
    [7:26:37 PM] <MitchellSalad>    let's see, what else... i think that's about it
    [7:26:42 PM] <MitchellSalad>    bug fixes and refactoring
    [7:27:13 PM] <MitchellSalad>    and, as i say every time, we desperately need more developers
    [7:27:45 PM] <wolftune> any questions from anyone?
    [7:27:54 PM] <mlinksva> not from me
    [7:28:02 PM] <wolftune> Maybe we can discuss a little if anyone has ideas about getting more developers…
    [7:28:21 PM] <wolftune> I note that several people, such as notthemessiah, have started building the site and looking into it
    [7:28:30 PM] <wolftune> so there's some interest from volunteers
    [7:28:34 PM] <apotheon> I mentioned a bug I encountered, and davidthomas said something aobut looking into it.  That's about what I have to offer on the dev front.
    [7:28:35 PM] <Kimberley>    Do we _specifically and exclusively_ need Haskell people?
    [7:28:37 PM] <MitchellSalad>    have we considered taking out a loan or something so we can actually hire people instead of relying on volunteers?
    [7:28:51 PM] <MitchellSalad>    Kimberly: i'd say yes
    [7:29:07 PM] <wolftune> Kimberley: not really, there's other stuff to do, but it does take a little time to get around what's going on
    [7:29:17 PM] <apotheon> MitchellSalad: Good developers who might pick up Haskell aren't good enough, then . . . ?
    [7:29:18 PM] <wolftune> anyway, the most vital work is definitely Haskell
    [7:29:40 PM] <wolftune> I'd certainly say we'd welcome everyone interested in doing whatever they can, of course
    [7:29:42 PM] <MitchellSalad>    apotheon: it's a tough sell to someone not familiar with haskell
    [7:29:58 PM] <MitchellSalad>    but yes of course they are good enough
    [7:29:58 PM] <apotheon> Oh -- difficulty convincing them to pitch in?
    [7:30:01 PM] <MitchellSalad>    exactly
    [7:30:06 PM] <apotheon> heh
    [7:30:08 PM] <MitchellSalad>    convincing them to push through and learn the language
    [7:30:19 PM] <wolftune> I'm not sure that's the block
    [7:30:26 PM] <apotheon> Some would probably welcome the opportunity to learn Haskell, but I see your point.
    [7:30:52 PM] <wolftune> JazzyEagle: meeting going on btw
    [7:31:27 PM] <wolftune> joeyh: if you're around, feel free to follow along and weigh in
    [7:31:29 PM] <MitchellSalad>    what about taking out a bank loan or something similar to hire developers?
    [7:31:41 PM] <MitchellSalad>    is that a bad idea?
    [7:31:56 PM] <wolftune> MitchellSalad: loans are not the issue, in that *cash flow* is less of a problem than the issue of total risk of investment
    [7:32:15 PM] <apotheon> Also, I'd be jealous of someone being paid to write Haskell.
    [7:32:21 PM] <wolftune> as in, I'd loan some savings…
    [7:32:34 PM] <MitchellSalad>    wolftune: what do you mean?
    [7:32:38 PM] <wolftune> apotheon: MitchellSalad is getting paid (modestly, less than he's worth in some sense)
    [7:32:54 PM] <apotheon> MitchellSalad: I'm jealous.
    [7:32:59 PM] <MitchellSalad>    lol, alright
    [7:33:26 PM] <wolftune> we actually had a specific e-mail from a Haskeller in Russia, dedicated FLO guy, looking for paid work and hoping we could hire him
    [7:33:51 PM] <wolftune> sounds like a great guy…
    [7:34:03 PM] <apotheon> wolftune: If Snowdrift was incorporated somehow (e.g., as a nonprofit) the risk could be kept non-personal, I think.
    [7:34:06 PM] <mlinksva> some time ago there was discussion of traditional crowdfunding to cover legal startup costs. might be more compelling if it included dev costs. or that'd be a tier thing once the legal costs raised.
    [7:34:14 PM] <wolftune> apotheon: we ARE incorporated
    [7:34:24 PM] <apotheon> Wait -- really?
    [7:34:27 PM]     * apotheon missed the memo.
    [7:34:40 PM] <wolftune> apotheon: yes, we incorporated as a non-profit co-op in Michigan over a year ago
    [7:34:41 PM] <apotheon> . . . or I just didn't ask.  Oops.
    [7:34:46 PM] <apotheon> ahh
    [7:34:50 PM] <wolftune> but we don't have 501 status
    [7:34:58 PM] <apotheon> gotcha
    [7:35:32 PM] <wolftune> it's still the case that if I loan Snowdrift.coop funds, it's a personal risk beyond the fact that I've already invested $thousands and dedicated the bulk of my time at no pay
    [7:35:41 PM] <apotheon> mlinksva: Good point, re: dev costs getting a better reaction from crowdfunders.
    [7:35:49 PM] <wolftune> I think running a fund drive is a sensible thing to pursue
    [7:36:16 PM] <apotheon> wolftune: I meant the incorporated entity taking out a bank loan.
    [7:36:22 PM] <wolftune> davidthomas: are you around? You haven't weighed in…
    [7:36:26 PM] <wolftune> apotheon: oh yes, that's true
    [7:36:31 PM] <MitchellSalad>    from the kickstarters i've occasionally seen, i don't anticipate more than a few thousand coming our way, but it is for sure worth a shot
    [7:36:44 PM] <MitchellSalad>    this project is sort of hard to explain, and not a shiny toy
    [7:36:51 PM] <wolftune> MitchellSalad: indeed
    [7:37:14 PM] <wolftune> so the thing is, if we get further along FIRST, then a fund-drive will likely go better because we'll be shinier and more clear
    [7:37:49 PM] Quit   jsheldon (~jsheldon@184.169.16.168) has left this server (Ping timeout: 260 seconds).
    [7:38:25 PM] <MitchellSalad>    i think we have a timing problem on our hands - my contract ends in about 5 weeks and i can't keep living on this salary, i'm unfortunately going to have to find other work. getting further along first would be nice, but it's not going to happen as quick as we'd like
    [7:38:32 PM] <wolftune> so hmm… we could actually explore the idea of a bank loan, but it sounds complex and like a lot of work and maybe hard to achieve anyway… a fund-drive doesn't have to be repaid…
    [7:38:52 PM] <wolftune> MitchellSalad: right
    [7:39:14 PM] <davidthomas>  wolftune: I'm around, just haven't had much to say that others didn't cover
    [7:39:19 PM] <wolftune> k
    [7:39:29 PM] <apotheon> wolftune: The nonprofit status might be helpful in getting nonprofits whose mission involves giving out grants to give Snowdrift money.
    [7:39:46 PM] <wolftune> true
    [7:39:51 PM] <MitchellSalad>    hehe... nonprofits giving grant money to up and coming nonprofits
    [7:39:58 PM] <apotheon> It happens all the time.
    [7:40:00 PM] <davidthomas>  we applied for a couple of those
    [7:40:09 PM] <wolftune> but grant orgs usually only support 501(c)(3) which is a crazy hassle, probably would take years to then get denied anyway
    [7:40:13 PM] <MitchellSalad>    i'm sure it does, it's just a funny thought
    [7:40:20 PM] <wolftune> other fellowships are worth continuing to pursue
    [7:40:29 PM] <apotheon> A local hackerspace got huge heaps of money from an established nonprofit because of the hackerspace's plan to get 501 status.
    [7:40:50 PM] <wolftune> apotheon: I expect c3 is still the goal in that case
    [7:40:59 PM] <wolftune> but indeed, there are options that are not so limited
    [7:41:13 PM] <wolftune> we could indeed reapply to Shuttleworth and others and ones we haven't tried
    [7:41:59 PM] <apotheon> The hackerspace looks pretty certain to get its status, pretty much entirely on the strength of the fact it offers a lot of free, open-to-the-public classes.
    [7:42:10 PM] <wolftune> apotheon: sure, that's an easy fit for c3
    [7:42:12 PM] <apotheon> 501(c)(3), that is.
    [7:42:17 PM] <apotheon> yeah
    [7:42:31 PM] <wolftune> we're not an easy fit
    [7:42:36 PM] <wolftune> let me go into that…
    [7:42:43 PM] <apotheon> I was just thinking that, actually.
    [7:42:53 PM] <wolftune> so…
    [7:43:09 PM] <wolftune> the concern initially involved c3 and there were various doubts
    [7:43:25 PM] <wolftune> we looked at c4 and still would need a lawyer to determine with real certainty
    [7:43:29 PM] <wolftune> but there's some concerns
    [7:43:39 PM] <wolftune> first thing to know is: we can legally self-determine
    [7:43:54 PM] <wolftune> we can just say today that we think we qualify for c4 and go ahead and say we're c4
    [7:44:19 PM] <wolftune> but that doesn't go well if we later get the IRS to look at our operations and decide we don't qualify
    [7:44:32 PM] <wolftune> so it's common to ask for a determination although it's optional
    [7:44:43 PM] <wolftune> c4 is social welfare org
    [7:45:07 PM] <wolftune> there's concerns about whether fundraising qualifies
    [7:45:09 PM] <mlinksva> what is the worst case if we eventually get rejected by irs?
    [7:45:18 PM] <wolftune> mlinksva: you mean after we self-determine?
    [7:45:23 PM] <mlinksva> yes
    [7:45:44 PM] <wolftune> I think the worst case is penalties and tax burdens
    [7:46:10 PM] <wolftune> not like anything absolutely destructive
    [7:46:27 PM] <Kimberley>    Correction.
    [7:46:34 PM] <wolftune> I don't actually know
    [7:47:01 PM] <wolftune> Kimberley: is that an answer to mlinksva or you have something to say to clarify what I said?
    [7:47:11 PM] <Kimberley>    If they investigate and find that we had reason to believe we would NOT qualify for status, and that we did not seek a decision because we were afraid it would be "no", that could be perjury.
    [7:47:20 PM] <wolftune> indeed
    [7:47:31 PM] <wolftune> we have to honestly self-determine if we do that
    [7:47:49 PM] <wolftune> it has to be completely clear that we went through a valid self-determination process
    [7:48:03 PM] <wolftune> we can't just say it because we wish it so
    [7:48:23 PM] <wolftune> so… the concerns about c4 include:
    [7:48:43 PM] <wolftune> there's this interpretation concern about means vs ends
    [7:49:03 PM] <wolftune> as in, does fundraising for public goods count as serving a mission of providing public goods?
    [7:49:23 PM] <wolftune> One IRS agent I spoke with said that fundraising is not an exempt activity
    [7:49:52 PM] <wolftune> But we can say that we're directly working with the projects to create the public goods depending on how we frame our relationship
    [7:50:08 PM] <Kimberley>    Are we "fundraising" or are we "providing an accounting service"?
    [7:50:30 PM] <wolftune> well, the scope of the co-op is potentially inclusive of the project activities
    [7:50:47 PM] <wolftune> in that the projects could be considered fundamental part of the org as members of the co-op, right?
    [7:51:00 PM] <Kimberley>    ...what?
    [7:51:24 PM] <wolftune> well… I dunno
    [7:51:28 PM] <apotheon> wolftune: seems like a little bit of a stretch
    [7:51:31 PM] <Kimberley>    The question is our activity, not the project activities.
    [7:51:40 PM] <wolftune> the point is: I think this is too much of a stretch to subsume the project activities
    [7:51:41 PM] <apotheon> I'm generally pretty legally risk-averse, though.
    [7:51:49 PM] <Kimberley>    And our activity is not, say, throwing benefit drives for projects.
    [7:51:52 PM] <wolftune> other than saying that we produce our own FLO
    [7:52:15 PM] <wolftune> Kimberley: how would you characterize the activities we do to further our mission?
    [7:52:30 PM] <Kimberley>    Our activity is providing accounting services to organizations that produce certain public goods.
    [7:52:42 PM] <apotheon> I kinda feel like to be "safe" it would probably need to involve spending money on the project's behalf, rather than just funneling money to a project.
    [7:52:48 PM] <wolftune> clearinghouse is a good term, yes? And that's part of our service?
    [7:53:06 PM] <wolftune> I agree that accounting is part of it
    [7:53:07 PM] <apotheon> . . . or make it accounting services, sure.
    [7:53:32 PM] <wolftune> so doing accounting isn't itself an exempt activity
    [7:53:35 PM] Join   fuzzyhorns (~Adium@73.38.57.191) has joined this channel.
    [7:54:00 PM] <apotheon> wolftune: It's providing a service to projects that produce things for the public.  If that's exempt . . .
    [7:54:05 PM] <mlinksva> probability of coming to a conclusion on whether we're ok with self-declared 501c4 within next couple months?
    [7:54:34 PM] <apotheon> I don't know enough about 501(c)(4) to have any clue whether a support organization for other projects qualifies.
    [7:54:49 PM] <wolftune> mlinksva: or, in another direction, the question would be: is it better for our time and trade-offs to simply forgo tax exemption?
    [7:54:58 PM] <MitchellSalad>    guys, IMO these meetings are 99% legal issues and 1% development issues, when in fact it should be the other way around. maybe that's just the programmer in me? i don't know if y'all are aware how many dev man-hours this project needs to succeed
    [7:55:23 PM] <mlinksva> you're right MitchellSalad
    [7:55:39 PM] <mlinksva> my guess at my own question is well under 50%
    [7:56:02 PM] <wolftune> MitchellSalad: yes, that's true, but really we should have independent project-direction / legal / planning meetings vs dev meetings
    [7:56:25 PM] <mlinksva> thus i'm fine with just forgoing the tax exemption.
    [7:56:38 PM] <wolftune> so… I'm leaning toward forgoing tax exemption
    [7:57:01 PM] <apotheon> mlinksva: could be
    [7:57:19 PM] <wolftune> I think the concern is: if we serve orgs that do the tax-exempt activities, it could limit us to projects that we can show as tax-exempt
    [7:57:19 PM] <mlinksva> let's just say we do that. what does that enable us to do immediately that otherwise is being held up by question?
    [7:57:23 PM] <apotheon> (re: under 50%)
    [7:57:34 PM] <wolftune> I'm worried about a bar that would stifle the scope of projects
    [7:57:56 PM] <mlinksva> (do that = move on, forget about tax exemption)
    [7:58:10 PM] <apotheon> mlinksva: probably just get more grants
    [7:58:18 PM] <wolftune> So, I think if we forgo tax exemption, we move on to figuring out what range of our activities are taxable and make sure there's no surprising burden in our mechanism
    [7:58:28 PM] <apotheon> mlinksva: Oh, wait, you probably meant skip exempt status by "that".
    [7:58:43 PM] <mlinksva> apotheon: yes, 2nd interp
    [7:58:44 PM] <wolftune> we might not get grants whether we like it or not
    [7:58:59 PM] Quit   fuzzyhorns (~Adium@73.38.57.191) has left this server (Quit: Leaving.).
    [7:59:21 PM] <apotheon> I think the big benefit of skipping exempt status is that we aren't talking about it any longer.
    [7:59:23 PM] <wolftune> so, there is this concern that if we hold funds, we need to say that it isn't income we should be taxed on, it's just a formality of funcitoning as a clearinghouse
    [7:59:41 PM] <wolftune> I think the time and energy and cost of tax-exemption is likely not worth it…
    [7:59:52 PM] <wolftune> our legal fees are reduced by forgoing it
    [8:00:04 PM] <wolftune> and our flexibility is largely enhanced I think
    [8:00:31 PM] <wolftune> We're still non-profit, still mission-driven, just have a tax burden
    [8:00:33 PM] Join   fuzzyhorns (~Adium@73.38.57.191) has joined this channel.
    [8:00:52 PM] <wolftune> but only if we have net income that's taxable of course
    [8:01:06 PM] <Kimberley>    I am fine with forgoing non-profit status, as long as we get a lawyer/accountant opinion on how to handle clearinghouse status.
    [8:01:14 PM] <wolftune> right
    [8:01:18 PM] <MitchellSalad>    wolftune: i didn't mean we need to talk about internal dev stuff, i meant that literally this monthly project-direction meeting should be primarily concerned with when/how we are going to find more programmers, because that's our biggest problem
    [8:01:55 PM] <wolftune> MitchellSalad: I agree, so in part the issue here is about getting past some distracting other concerns
    [8:02:07 PM] <wolftune> so that we can focus on development and recruiting developers
    [8:02:17 PM] <MitchellSalad>    starbucks is closing, be back in a bit
    [8:02:19 PM] <wolftune> the idea of funds and paying developers is tied up with some of these legal things
    [8:02:20 PM] Quit   MitchellSalad (c600dd2b@gateway/web/freenode/ip.198.0.221.43) has left this server (Quit: Page closed).
    [8:02:25 PM] <apotheon> I should get going within about twenty minutes, I'm afraid.
    [8:02:29 PM] <wolftune> ok
    [8:03:16 PM] <wolftune> so, let's push forward with this… does anyone want to say anything about discouraging us / hesitating to move forward as though we're forgoing tax exemption?
    [8:04:04 PM] <apotheon> nothing in particular from me
    [8:04:11 PM] <Kimberley>    Get a lawyer before we take outside money.
    [8:04:40 PM] Quit   fuzzyhorns (~Adium@73.38.57.191) has left this server (Ping timeout: 244 seconds).
    [8:05:05 PM] <wolftune> Kimberley: ok, makes sense, what about the issue of how to fund a lawyer in the first place? chicken/egg issue with fundraising to pay a lawyer…
    [8:05:37 PM] <wolftune> we could keep looking for pro bono legal help
    [8:05:48 PM] <Kimberley>    Get a lawyer within the same tax year we take outside money.
    [8:05:56 PM] <wolftune> ah ok
    [8:06:50 PM] <wolftune> Kimberley: you're considering that, e.g. David's funding of Mitchell via Snowdrift.coop is already outside money, right?
    [8:07:15 PM] <Kimberley>    Yes.
    [8:07:34 PM] <wolftune> well, it's simple to say (as I would have suggested anyway), that we need a lawyer this year
    [8:08:45 PM] <wolftune> Although it's a big personal risk at a time when I have no income stability, I could, if it came down to it, just pay Deb Olsen…
    [8:09:50 PM] <apotheon> I just had a crazy thought.  Is Snowdrift, in some way, a labor organization?
    [8:09:53 PM] <wolftune> I otherwise do have some other legal contacts
    [8:10:12 PM] <wolftune> apotheon: yes it is, one of the co-op classes is the workers who do Snowdrift.coop (and the project workers)
    [8:10:33 PM] <wolftune> our vision is multistakeholder including workers (laborers)
    [8:10:40 PM] <apotheon> 501(c)(5) style labor . . . ?
    [8:11:06 PM] <wolftune> probably not
    [8:11:40 PM] <Kimberley>    That is specifically labor unions and you have to meet several very specific requirements.
    [8:11:56 PM] <apotheon> Okay.
    [8:12:01 PM] <apotheon> I did say it was a crazy idea.
    [8:12:06 PM] <wolftune> sure
    [8:12:32 PM] <wolftune> so… I could focus attention on legal contacts to try to recruit pro bono assistance…
    [8:13:01 PM] <wolftune> or we could focus on deciding how to go ahead and pay Deb (who still seems potentially the best option)
    [8:13:15 PM] <apotheon> So . . . I guess the upshot is "We're not pursing 501(c) status at this time.  We need a lawyer by the end of this tax year.  Given the lawyer need, we can take in outside money this year."
    [8:13:18 PM] <wolftune> we could focus on running a fund-drive for the legal costs as we've said before
    [8:13:33 PM] <wolftune> apotheon: right, so we could go ahead and run a fund-drive
    [8:13:36 PM] <wolftune> it's not crazy
    [8:13:59 PM] <apotheon> The crazy part was 501(c)(5), looking for a way to fit "labor union".
    [8:14:01 PM] <wolftune> all sorts of orgs run fund drives to start up before they have all the legal details perfect
    [8:14:48 PM] <mlinksva> i'm +1 on going ahead with fund drive. wolftune make a great video.
    [8:15:00 PM] <wolftune> mlinksva: thanks
    [8:15:01 PM] <apotheon> I say look for opportunities to get some pro bono assistance while trying to hammer out a plan to pay a lawyer, and see which works out best/fastest.  That seems to be a summation of the whole legal thing thus far.
    [8:15:31 PM] <wolftune> I'd like everyone else to weigh in at this point
    [8:16:12 PM] <apotheon> I guess that's Kimberley and davidthomas then¸ plus possibly anyone who has dropped offline.
    [8:16:22 PM] <apotheon> . . . though Kimberley already hinted at an answer.
    [8:16:36 PM] <wolftune> yeah, looks like jsheldon is off now
    [8:16:46 PM] <wolftune> davidthomas: thoughts?
    [8:16:50 PM] <apotheon> (offline depending on definition of "everyone")
    [8:16:57 PM] <davidthomas>  I've no objection to a fund drive
    [8:17:12 PM] <Kimberley>    +1 to funddrive now for legal costs hopefully before the end of the year.
    [8:17:34 PM] <wolftune> ok, I'll go ahead and focus on fund-drive as priority now while continuing to pursue pro bono legal help
    [8:18:13 PM] <mlinksva> great
    [8:18:21 PM] <apotheon> Holy cow.  Is that the whole agenda apart from loomia and talking to Mitchell about development?
    [8:18:34 PM] <wolftune> I should probably see if joeyh could help us set that up, as he ran successful self-hosted drive for git-annex on his own Yesod site
    [8:18:37 PM] Join   fuzzyhorns (~Adium@73.38.57.191) has joined this channel.
    [8:18:41 PM] <Kimberley>    Re: Loomia.
    [8:19:18 PM] <wolftune> Loomio was just a past record of stuff, but not a big part of agenda
    [8:19:25 PM] <wolftune> so, the bulk of the agenda is covered
    [8:19:25 PM] <Kimberley>    My verdict is not going to help us much in meetings but useful for deciding things out of meetings, as it makes it possible to track changing opinions.
    [8:19:47 PM] <wolftune> ok
    [8:19:48 PM] <apotheon> err, loomiO, sorry
    [8:20:22 PM] <apotheon> Okay, folks . . . good timing.  It's about two minutes before I should do some other stuff.
    [8:20:42 PM] <wolftune> ok
    [8:20:50 PM] <apotheon> I'm glad I got to see the agenda addressed.
    [8:21:07 PM] <apotheon> (apart from talking to Mitchell about dev some more)
    [8:21:31 PM] <apotheon> Is there anything else before I go?
    [8:21:48 PM] <wolftune> so the later follow-up things are: given a general idea of fund-drive, what are the priorities for Mitchell (too bad he's off now) in the next few weeks to best help things be ready for fund-drive?
    [8:22:50 PM] <wolftune> I think the stuff we've focused on that helps integrate e-mail and clear updates, encourage involvement with the site, I think that's important
    [8:23:07 PM] <wolftune> and davidthomas is just finishing up a blog option so we can have more time-focused blog updates
    [8:23:36 PM] Join   MitchellSalad (18065c30@gateway/web/freenode/ip.24.6.92.48) has joined this channel.
    [8:23:41 PM] <MitchellSalad>    back
    [8:23:53 PM] <apotheon> MitchellSalad: Welcome back.  I've gotta run, though.
    [8:24:07 PM] <apotheon> Later.
    [8:24:09 PM] <MitchellSalad>    thanks, alright. catch you later
    [8:25:19 PM] <wolftune> hey say something anyone, checking it I'm still connected
    [8:25:48 PM] <wolftune> 10 9 8 7 6 5 4 3 2 1 0… nope
    [8:26:01 PM] Nick   Nickname already in use. Trying awolf.
    [8:26:03 PM] Notice -NickServ- This nickname is registered. Please choose a different nickname, or identify via /msg NickServ identify <password>.
    [8:26:10 PM] Join   You (~aaron@c-24-20-66-208.hsd1.or.comcast.net) have joined the channel #snowdrift.
    [8:26:10 PM] Topic  The channel topic is "https://snowdrift.coop | clearing the path to a Free/Libre/Open world".
    [8:26:10 PM] Topic  The topic was set by awolf!~aaron@c-24-20-66-208.hsd1.or.comcast.net on 2014-01-21 1:32 PM.
    [8:26:20 PM] <awolf>    sorry got disconnected
    [8:26:23 PM] Nick   Nickname already in use, try a different one.
    [8:26:30 PM] Nick   Nickname already in use, try a different one.
    [8:26:51 PM] <awolf>    ok, maybe we should discuss other things later
    [8:27:03 PM] Mode   Channel modes: no colors allowed, no messages from outside, topic protection
    [8:27:03 PM] Created    This channel was created on 2013-02-02 12:23 PM.
    [8:27:19 PM] <awolf>    the issues involve how to focus development and everything else on what will lead to a successful fund-drive
    [8:28:44 PM] <awolf>    anyone have anything else to discuss right now? (I have tons from development priorities to thoughts about alternative mechanisms to avoid holding money as an option, but we can't go over everything all at once)
    [8:29:08 PM] Quit   wolftune (~aaron@2601:7:1e00:8af:c40e:7db7:2bc0:f541) has left this server (Ping timeout: 240 seconds).
    [8:29:12 PM] Nick   You are now known as wolftune.
    [8:29:13 PM] Notice -NickServ- This nickname is registered. Please choose a different nickname, or identify via /msg NickServ identify <password>.
    [8:29:37 PM] <MitchellSalad>    let's hear the development priorities
    [8:29:45 PM] <MitchellSalad>    that affects me so i'm interested :P
    [8:30:03 PM] <wolftune> MitchellSalad: from what you missed:
    [8:30:11 PM] <wolftune> I think the stuff we've focused on that helps integrate e-mail and clear updates, encourage involvement with the site, I think that's important
    [8:30:15 PM] <MitchellSalad>    yeah, a recap would be nice if i missed anything big
    [8:30:17 PM] <wolftune> and davidthomas is just finishing up a blog option so we can have more time-focused blog updates
    [8:30:26 PM] <wolftune> that's it really
    [8:30:37 PM] <wolftune> but I'd like to get any thoughts from others…
    [8:30:59 PM] <wolftune> what priorities for site development will help lead to the most successful fund-drive?
    [8:32:24 PM] <wolftune> Kimberley: davidthomas: mlinksva: (or anyone else)… maybe just share your opinions on this
    [8:32:31 PM] <wolftune> then we can wrap things up for now
    [8:32:40 PM] Quit   fuzzyhorns (~Adium@73.38.57.191) has left this server (Quit: Leaving.).
    [8:33:19 PM] <wolftune> MitchellSalad: you could also go ahead and discuss your thoughts about what development goals will most serve having a site that will serve to support a successful fund-drive
    [8:33:22 PM] <Kimberley>    I really want to see the site updates done, so we're apparently in agreement.
    [8:33:54 PM] <wolftune> Kimberley: by "updates" you mean things that make it clear to people about the updates of things happening on the site / activity-feed and blog?
    [8:34:06 PM] <Kimberley>    Yes.
    [8:34:09 PM] <wolftune> k cool
    [8:34:59 PM] <MitchellSalad>    i think 1. feed stuff
    [8:35:04 PM] <MitchellSalad>    and 2. much better discussion syste
    [8:35:28 PM] <MitchellSalad>    both are about half done :P
    [8:35:29 PM] <wolftune> MitchellSalad: you mean more forum-like, have discussion section that is general and not wiki-tied or you have other thoughts?
    [8:36:07 PM] <MitchellSalad>    no, well yes, but short term - making the /p/snowdrift/d page be an index of wiki pages, each with their own unviewed comments count
    [8:36:15 PM] <MitchellSalad>    err /p/snowdrift/w
    [8:36:16 PM] <wolftune> right
    [8:36:19 PM] <wolftune> got it
    [8:36:28 PM] <wolftune> yes, that will be major
    [8:36:45 PM] <MitchellSalad>    also a forum would be great, but that work hasn't even been started
    [8:37:14 PM] <MitchellSalad>    wolftune: https://github.com/mitchellwrosen/snowdrift/blob/master/View/SnowdriftEvent.hs
    [8:37:17 PM] <wolftune> I think a forum can be basically a more flexible build out of the discussion pages, but yeah, get what we have working more flexibly first
    [8:37:32 PM] <MitchellSalad>    that entire file has stubby HTML for you to prettify
    [8:37:39 PM] <wolftune> k
    [8:38:01 PM] <MitchellSalad>    each function renders one feed item in /p/snowdrift/feed
    [8:38:11 PM] <wolftune> cool
    [8:38:42 PM] <MitchellSalad>    so, whenever you get that done, and then i iron out any bugs, the feed page will be finished
    [8:38:51 PM] <wolftune> mlinksva: davidthomas: ? thoughts on this topic?
    [8:40:14 PM] <mlinksva> i haven't given much thought, or even any, but my first thought is that a forum that people like is hard, and not on critical path
    [8:41:11 PM] <wolftune> mlinksva: sure, um, not so much what you think of that, but of the topic of "what priorities should we see for the site for MitchellSalad to work on that will best serve the goal of successful fund-drive?"
    [8:41:11 PM] <mlinksva> i don't know that there are any development blockers for doing traditional fundraiser.
    [8:41:27 PM] <wolftune> sure, but MitchellSalad will be continuing to work, so what will help most?
    [8:41:40 PM] <mlinksva> i don't have an opinion
    [8:41:42 PM] <wolftune> ok
    [8:41:44 PM] <wolftune> davidthomas: ?
    [8:41:53 PM] <mlinksva> i wouldn't try to tie development priority to fundraiser
    [8:41:53 PM] <wolftune> thoughts about all these things?
    [8:41:58 PM] <mlinksva> i don't think they're connected
    [8:42:00 PM] <wolftune> mlinksva: ok
    [8:42:08 PM] <wolftune> thanks
    [8:43:13 PM] <wolftune> I'm not sure why davidthomas isn't replying, maybe he's distracted by something
    [8:43:42 PM] <wolftune> before we finalize, MitchellSalad: any questions / concerns you'd like the group to address that we didn't cover?
    [8:43:58 PM] <davidthomas>  I'm not sure what the question is
    [8:44:05 PM] <MitchellSalad>    nope, don't think so
    [8:44:12 PM] <wolftune> davidthomas: hi ok…
    [8:44:44 PM] <wolftune> um, let's just call it a meeting. It was good, I have a sense of some things and appreciate again everyone's encouragement. Helps to get back into feeling focused each meeting…
    [8:44:58 PM] <wolftune> please everyone else feel free to add some parting thoughts
    [8:45:22 PM] <wolftune> we'll all be in touch and we'll be posting updates and questions as things go
    [8:46:07 PM] <MitchellSalad>    wait, i wanted to hear davidthomas's thoughts, the question is of priority that i should be working on?
    [8:46:22 PM] <MitchellSalad>    whoops lost a word or two
    [8:46:29 PM] <MitchellSalad>    the question is: what is of priority that i should be working on?
    [8:49:46 PM] Join   jsheldon (~jsheldon@67.142.235.252) has joined this channel.
    [8:50:04 PM]     * jsheldon forgot how they switch off the wifi for takeoff and landing :(
    [8:50:28 PM] <wolftune> jsheldon: hey, you're back
    [8:50:48 PM] <jsheldon> yeah, i have two separate flights
    [8:50:49 PM] <jsheldon> tucson to LA
    [8:50:52 PM] <jsheldon> and LA to Oakland
    [8:50:59 PM] <jsheldon> I'm on flight number two now.
    [8:51:29 PM] <jsheldon> From what I saw of the IRC meeting
    [8:51:33 PM] <wolftune> jsheldon: so we kinda settled on (A) pursuing a fund-drive ASAP with the goal of hiring a lawyer so we can clarify money handling and co-op issues / bylaws but (B) forgo pursuing 501 tax examption (unless a lawyer convinces us otherwise)
    [8:52:07 PM] <wolftune> thus be a non-profit co-op without tax exemption
    [8:52:22 PM] <jsheldon> I think it makes sense to wait to pursue federal tax exemption until we have enough money coming in that it would be a significant cost savings
    [8:52:55 PM] <wolftune> which could be never if we always just use the funds for covering costs and development anyway
    [8:53:07 PM] <jsheldon> yeah.
    [8:53:20 PM] <jsheldon> like, maybe the money we disburse to projects can be not considered income?
    [8:53:57 PM] <wolftune> right, it shouldn't be. That's the stuff we need legal help with
    [8:54:03 PM] <davidthomas>  MitchellSalad: I think the event stuff makes good sense
    [8:54:05 PM] <wolftune> primarily
    [8:54:14 PM] <jsheldon> And an accountant that understands nonprofit coops.
    [8:54:23 PM] <wolftune> yes, accountant…
    [8:54:24 PM] <wolftune> hmm
    [8:54:27 PM] <wolftune> indeed
    [8:54:30 PM] <davidthomas>  with the site-notifications => email-notifications trajectory
    [8:54:34 PM] <davidthomas>  not sure, past that
