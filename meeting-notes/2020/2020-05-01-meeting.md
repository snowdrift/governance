<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; alternate options for video and screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift -->

<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/  -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below (select the whole line and drag to your bookmarks bar). You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
 
# Meeting — May 1, 2020
 
- Attendees from last week: alignwaivers, smichel, iko, Adroit, MSiep, wolftune
- Attendees (current): Salt,  alignwaivers, iko, Adroit, wolftune, smichel17

<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 8 -> 9
- Posts: 84 -> 105
- DAU/MAU:  58% -> 49%

Snowdrift patrons: 119 <!-- from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!
- "NEXT STEPS" should be clear and actionable for assignee (who should double-check this for themselves)
- Use chat in etherpad (and add your name)

### Discussion mechanisms
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means that you have something to say and want to be put in the queue
    - "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    -  ">" as an indicator of understanding someone and the point can be concluded, please move on

### Facilitation by topic
- There is a leader for each topic (generally the person who raised the topic), and the facilitator will assist the topic leader in the discussion via the etherpad chat

### Notetaking
- "???" in notes measn something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback
- adroit: wasn't active on the agenda items here but have been participating in discourse, getting lots of notifications there
- wolftune: glad things worked out with me showing up late, well-focused on topics which was nice¸ (awkward that salt was here in text only)
- iko: good meeting
- smichel: it was good and we can end 5 minutes early
- msiep: good meeting
- alignwaivers: glad we got through items and it's nice to be done early


## Last meeting next-steps review

### LFNW video recording (alignwaivers)
- NEXT STEPS (alignwaivers): will post a draft sometime next week

### Team Agreements Follow up  (alignwaivers)
- NEXT STEPS (alignwaivers): follow up with others who haven't responded yet

### Team member titles vs roles and presentation (wolftune)
- NEXT STEP (msiep): driver statement for showing team and directors on the main site about page [done: https://gitlab.com/snowdrift/design/-/issues/117 ]

### Optional video chat (alignwaivers)
- NEXT STEP (alignwaivers): mention video chat in the etherpad as an option
- NEXT STEP (smichel): add link to shared calendar to discourse post about meetings [DONE]



<!-- One minute silence, check with ourselves mentally and personal notes/tasks/emails to surface any tensions, add agenda if appropriate -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->


## Current Agenda
<!-- In etherpad, put topic titles in bold so they stand out -->

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->

### Top-level prioritised wishlist (chreekat/iko)
- pre-meeting summary (iko): chreekat's suggestion. he asked if there is a top-level, prioritised wishlist of things to be accomplished, e.g. ops fully migrated to osu osl, add necessary features, onboard a partner project. his opinion is we need it to have momentum.
- I think it would be a good idea for a slightly different reason. to take stock of current position, what gaps exist, what resources are immediately needed.
- the closest thing currently is <https://wiki.snowdrift.coop/planning> although we also have <https://wiki.snowdrift.coop/community/how-to-help> but is it prioritised and up-to-date?
- note from wolftune: a stronger roadmap is a key item requested by OSI and that the Snowdrift Board is also working towards
- smichel: I think the wiki page is still up-to-date, but there's no prioritised. as wolftune mentioned, a roadmap was requested of us by OSI
- smichel: we were planning to meet on the 9th but may meet tomorrow (or within the week) to have more prioritized
- wolftune: not strictly a board task, but maybe they will weigh in, figure out what the issues are (e.g. if team is having trouble with it)
- salt: a bit confused about the separation of responsibilities between board and team? 
- smichel: this is on the board's agenda … [audio cut out]
- iko: is this process defined by the board? how will this process work, who will actually partcipiate in the process, add items and prioritise them?
- wolftune: I'd say no, the board, the team members agree: here are the core requirements, here's who it's mapped out and what the plan is
- the board's role is to hold the team accountable/help - team could draft, and the board could agree that's good, but also give feedback - it's not clear enough, can we 
help?
- smichel: fits into the sociocracy stuff, it should fit in to team members roles and how they see themselves fit in - roadmap should be guide (order can be individually decided)
- wolftune: I think it should be about accountability: chreekat asking for the team to have clarity on what to do, clear for newcomers as well
- board says, how are you doing, are we on track (basically same with OSI): core point - this needs to happen, no one 100% clear and we need to figure out what to do in order to make this happen. 
- I'd be happy if we had an example from other teams etc, would also be good to have a nitty-gritty working session to hash this out
- I suggest going through the sociocracy process, start with a driver statement, etc.
- smichel: wasn't there something related on the forum that photm drafted?
- wolftune: possibly. the next step is to discuss the meta process then arrive at a concrete task to come up with the actual prioritised list
- smichel: I can broadcast the meeting date if any team member wants to attend
- NEXT STEP (smichel): groom via forum or have a working session, identify tensions, driver, proposal through sociocracy process
- NEXT STEP (smichel, alignwaivers, wolftune and anyone else from team): meet on May 9 or within the week to prioritise items?
- NEXT STEP (smichel): Announce board meeting when it is decided

### Backend dev for cross-functional ux team (iko) (:07-08)
- pre-meeting summary (iko): based on previous attempts in ui/ux design implementation, there needs to be a cross-functional ux team to work from design/vision building to the coding.
- we have a team, but it seems to be missing one role on the dev side.
- I'm looking for an experienced dev, not necessarily with haskell but system backend. some familiarity with the codebase would be a plus. basically the dev can provide input, help with sanity check at the requirements level, before any wireframes/prototypes are finalised.
- e.g. chreekat would be great, I asked him. he's currently busy with ops migration and also has funding mechanism development lined up. this could be on his list eventually, the possibility is not discounted. there is a feature design and enhancement I'd like to see before go through for implementation before onboarding another project (design\#116, with driver statement, mockups and a bit of prototyping so far, which chreekat did an initial check and said was okay). however, I'm not sure what the timing would be like.
- is there anyone who can do this and maybe confer with chreekat in the process? some other arrangement?
- note from wolftune: this should be among our main recruitment efforts, define the scope of the role so we can describe it clearly to potential volunteers
- note from Adroit: I suggest we recruit via the homepage - List of who we're looking for - also serves to indicate status
- wolftune: before a lot of work goes into design, should have someone to look at the interaction of design and implementation.
- could try to go through the sociocracy process to fill the role. In my experience, good to identify this role and recruit specifically
- smichel: it's possible to check with people who have done code review in the past
- wolftune: there's quite a backlog of people we could search through to see if we can recruit from
- iko: sounds good, thanks. not urgent right now but eventually, not to put too many tasks on chreekat
- wolftune: urgency relative but I'm hesitant to recruit when we DON'T have this specific role
- NEXT STEPS (wolftune): define new role, then recruit for it

### Update on Board progress (wolftune) (:10)
- wolftune: board had another meeting, usually quarterly. Had some concerns around not having as much discussion between last meeting
- using signal collectively more which is good. 
- wolftune: they posted intros on the forum  so please welcome. Board willing to help with the roadmap stuff for OSI (accountability, etc) <https://community.snowdrift.coop/g/board>
- also getting bylaws drafted so we can have those bylaws
- NEXT STEP (team): engage with board members via intro posts in welcome category on discourse https://community.snowdrift.coop/t/about-the-welcome-category/527

<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->

### Posting team meeting times (wolftune)
- wolftune: outdated: <https://community.snowdrift.coop/t/remote-coworking-weekly-routine/1281>
- wolftune: not public: <https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/1345>
- wolftune: what can we make more public?
- smichel: Public: <https://community.snowdrift.coop/t/weekly-all-hands-on-deck-check-in-meetings/1010>
- wolftune: the team posts that list when we have the time: we don't want the links for people to update availability public, just the meeting times themselves
- adroit: this one thread [linked by smichel] seems pretty great
- wolftune: smichel, can you make sure there's one place we can link all of the things?
- smichel: yeah, I can consolidate them
- salt: I think another question here is, we're still all okay with these meetings being open?
- wolftune: yeah, and we'll see if anyone shows up
- alignwaivers: it would be ideal for newcomers to have a place to see the meeting times without searching everywhere
- wolftune: it should be pinned somewhere on the forum
- adroit: Or linked from the homepage, as above
- wolftune: is it possible to get an box that get's automaticall updated
- smichel: The place for newcomers is <https://wiki.snowdrift.coop/resources>
- smichel: For anyone who wants to be involved in the team
- smichel: (Also for existing team members wanting to know where everything is if you lose bookmarks or something)
- NEXT STEP (smichel): consolidate all the meeting times posting locations

### Sociocracy processes via the forum (wolftune)
- wolftune: general discussion, getting everyone clear. see also <https://community.snowdrift.coop/t/reviewing-the-process-for-consent-decisions/1497/29>
- wolftune: just want to make sure people are aware of the discussion and how we are using discourse for sociocracy process, etc.
- think it's going well but wanted to be clear and remind people of the link
- alignwaivers: speaking of minor edits and the tech we're using ...
- wolftune: Want there to be consistent, and not overly bureaucratic. Don't have time now but want to have conversations to assess how people are feeling etc
- iko: small feedback about forum proposals (e.g. based on team agreements proposal process) — whether it would work to have a feedback period for a draft then one poll at the end, instead of multiple successive polls which led to some confusion and need to check/vote multiple times on the same document. not sure if the same will apply to other sociocracy processes
- iko: I see the poll as the official voting motion, and any minor or major adjustments should happen before the poll
- wolftune: I see what you mean. we'll work out the initial process for team agreements where there was proposal, addressing how to draft, etc
- alignwaivers: link to this discussion already happening  <https://community.snowdrift.coop/t/consent-decision-allow-minor-edits-during-consent-decisions/1523>
- wolftune: proposal: post a proposal on the forum. A proposal to have the proposal, a time for posting feedback before the official voting poll

### Team Agreements Follow up  (alignwaivers) (:27)
- alignwaivers: consented: iko, adroit, msiep, mray, salt, wolftune, smichel 
- wolftune: we'll have to weed people off that haven't responded (given reasonable efforts to contact them)
- wolftune: do we have a summary of the decision posted? we don't necessarily need a formal process, but will ask people as we onboard them
- alignwaivers: I can move ahead and post the formal agreement 
- wolftune: just post it we don't have to officially agree
- wolftune: also do we have it documented somewhere
- Adroit: TL3 is given dynamically based on current involvement level, could be used (once adjusted down) to decide who to require a response from
- NEXT STEP (alignwaivers): update team agreement document (as accepted proposal) on the forum and on git as an official documeent


<!-- Open discussion? ~5min. if time -->



---
 
## meeting evaluation / feedback / suggestions / appreciations round

- adroit: sounded like a decently productive meeting - I didn't care for the timeboxing, can't account for how much other people talk. The baton-passing fills an important need of saying when you're done talking, but maybe something like "over" when we're done talking could satisfy that though
- smichel: terrible, had connection issues the whole time. first half was fine. I think baton passing just takes getting used to and we should stick with it for while. [timeboxing] I'd be more concerned with the reverse — that it might provide pressure to use the whole timebox even when it's not needed
- alignwaivers: I had a few dropouts for a few seconds, it would be helpful for people to note in chat if they previously dropped out
 - wolftune: I find the pass the baton is kinda too hard to follow sometime, have to consistently remind people
- would suggest going back to a faciltator and varying the order. the timeboxing was helpful to stay focused. thanks everyone for doing whatever they can. felt a bit rushed
- iko: good meeting - glad we cleared up some governance process questions. thanks to notetaker/facilitator, very good as usual
- Salt: the shared facilitation will hopefully improve as everyone gets better at facilitation, also anyone can ask for the main facilitator to facilitate. Time boxing is a good idea but was a bit wishful, needs to be more realistic. We need to get better at starting right on time. Having video was nice. I need to figure out a way to eat or get up, but this was my only hole in the schedule for a very long block. I really like the talkingstick passing for rounds.



<!-- Meeting adjourned! -->

<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->
