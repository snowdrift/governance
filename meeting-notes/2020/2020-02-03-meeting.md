<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
 
# Meeting — February 3, 2020
 
Attendees: alignwaivers, wolftune, msiep, smichel
 
<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 1 -> 0
- New Topics: 8 -> 4
- Posts: 54 -> 28

Snowdrift patrons: 117
 
## Reminders on best-practice meeting habits
 
### Use chat in etherpad (and add your name)

### Conversation queuing

- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on
- three discussion mechanisms: hand symbols (above), call for a round (and vary the order), open discussion
 
### Notetaking

- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback

- mray: question: can we remove the matrix meeting channel? [salt says 'not yet'] my impression about using text before speaking keeps me from participating a bit
- smichel: liking mumble more than jitsi, chat in etherpad worked well, miss having the noise. Would still be interested in exploring matrix (esp. with reactions)
- msiep: feel similarly - so far, raising hand works but getting accustomed to it
- wolftune: good meeting, dont know how to get better at it but preventing topics to specific scope and avoiding tangents
- alignwaivers: agree to keep matrix open, suggest trying it next week, as in mray's point, maybe a bit less-strict about hand-raise text etc
- salt: really liked number of people here, agreed there was some tangents that were avoidable, sympathize with the problems of handraising from mray, opposed to using anything untested

## Last meeting next-steps review

<!-- Verify that each item is captured or done. For any items that need to be addressed, copy (do not delete!) the old agenda into the new agenda item for this meeting and groom during discussion-->

### Alternatives to chatting on mumble (alignwaivers)

- NEXT STEP (alignwavers): Test before next meeting
- NEXT STEPS (alignwaivers): ping iko about enabling audio notifications (with link to plugin)

### LFNW (Salt) (continued from previous meeting) https://gitlab.com/snowdrift/outreach/-/milestones/5

- NEXT STEP (alignwavers/smichel): Populate new milestone with tasks from previous milestone that are still relevant

### Discourse Announcement Milestone https://gitlab.com/snowdrift/outreach/-/milestones/4 (alignwaivers)

- NEXT STEPS (wolftune) [DONE]: I closed it

### LibrePlanet (salt)

- NEXT STEP: Create discourse thread with tags, etc
- NEXT STEP (salt): ping Micky to let her know you'll be attending
- NEXT STEP (smichel): Sign up for libreplanet

### Team Agreements (alignwaivers) https://community.snowdrift.coop/t/individual-collective-accountability-agreements-for-team-members/1383/26 [DONE]

- NEXT STEPS (alignwavers): incorporate feedback from this meeting, make a formal proposal of the commitment text (add: "…explicitly directed to me")
ROUND
- NEXT STEPS(alignwaivers): use the proposal mechanism to implement
- NEXT STEP (wolftune): Start a conversation about the process for inviting someone to the team [DONE https://community.snowdrift.coop/t/when-and-how-to-invite-new-team-members/1457 ]

---
 
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->

## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->

### Team Agreements (alignwaivers) https://community.snowdrift.coop/t/individual-collective-accountability-agreements-for-team-members/1383/26
- alignwaivers: updated wording: "messages directed to me or pertaining to my roles"
- process will be a post of the proposed wording and then a poll


<!-- Late New Agenda Down Here ( Added within 48 hours of the meeting)-->

### Snowdrift-dev Channel, move GL announcements there (Salt)

- not able to make the meeting, but this came up on Matrix and I would +1
- NEXT STEPS (smichel17): setup snowdrift-dev channel 
smichel: good for notification management - can have separate channels/notifications based on which channel 

### Metrics (wolftune)

- wolftune: we could use gitlab metrics as well. Should we? Also how many patrons snowdrift has.
- smichel17: what metrics does gitlab provide?
- wolftune: various things such as groups & activity
- smichel17: don't think it's worth counting patrons untill we start promotions
- wolftune: if no objections, would like to include and keep that in mind
- smichel17: no objection

 <!-- Open discussion? ~5min. if time -->

- wolftune: one talk accepted one rejected to LFNW
- Msiep: what's the latest on the charge?
- wolftune: live site is up, deployment working, chreekat was able to to crowdmatching to date - can talk about an actual test run, don't think (quite sure) this has been done yet - maybe it's just a manual override, 
- Msiep: current charge 3.75??
- msiep: reaching the point where we actually do a test run for everybody would be great
- wolftune: extra busy with contract related to OS projects and documentation, ends on the 19th.
- wolftune: started using draw.io - contrasted with uml stuff (plantuml), using diagrams etc. could be useful for describing aspects of snowdrift
- mray: limitation is it's only about creating charts, not sure about pricing
- wolftune: it's free & open source
- mray: would need some storage space which seemed a little problematic
- alignwaivers: it's apache v2
- wolftune: could do flowcharts, etc: seeing the potential value in visual explanations for 'funding for open source projects, etc'. 
- smichel17: etherpad seems to have broken, refreshed but still can't see anything

---
 
## meeting evaluation / feedback / suggestions round

- wolftune: happy we have an effective meeting, low stress, etc
- msiep: did seem quite efficient
-- smiche17: thought we even could have been a little shorter, given little to talk about

<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, leave new blank-->


