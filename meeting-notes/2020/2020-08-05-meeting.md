<!--

    Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
# Meeting — August 5, 2020

- Attendees from last week: msiep, Salt, smichel, wolftune , Adroit
- Attendees (current): wolftune, Salt, smichel, MSiep, Adroit, Alignwaivers

<!--

    Check-in round

    Assign note taker

    Assign time keeper

-->

## 1. Metrics
 
Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 1 -> 1
- Posts: 20 -> 7
- DAU/MAU:  31% -> 39%

Snowdrift patrons: 124 <!-- from https://snowdrift.coop/p/snowdrift -->

## 2. Review previous meeting feedback
- Adroit: appreciations of finishing early, also uncomfortable with wondering if we got to everything
- smichel: Happy it was fast
- Salt: Happy everyone's still here, ??? robert, would like to have a way to move through open discussion
- wolftune: Missing a few people, first time at the new time but we should figure out why alignwaivers & mray aren't here


## 3. Last meeting next-steps review

<!-- Between meetings when done, mark each NEXT STEP as [DONE] or [CAPTURED AT <LINK>] -->

### (carry forward) Reaching out to potential funding sources
- NEXT STEP (wolftune): Follow up with alignwaivers re: draft email [DONE]

### Crowdmatching mechanism update
- NEXT STEP (wolftune): lead asynch work on clear criteria list for the model [CAPTURED https://gitlab.com/snowdrift/wiki/-/issues/26 ]


## 4. Current Agenda

<!--

    One minute silence, check thoughts and notes/tasks/emails to surface tensions, add agenda if appropriate

    Confirm agenda order and timeboxes (in 5 minute increments), adjust for anyone who must leave early

### TOPIC TEMPLATE (LEADER) <!-- Timebox: XX minutes (until hh:mm) -->
<!-- -->

### Roadmap, planning 1:1s, and ending early (smichel17) <!-- Timebox: 1 minute (until hh:11) -->
- smichel: As we near the end of the half-quarter, it's time to plan our next phase on the roadmap.
- smichel: The next thing to do is for me to have 1:1s with everyone again (hopefully faster than last time)
- smichel: Rather than scheduling those meetings individually, I would like to try and fit them into this hour if we can end early (and other meeting/coworking sessions this week and next)
- smichel: So, I would appreciate it if we can try to end early today.
- smichel: Also a reminder to check the roadmap if you claimed something for this phase.
- NEXT STEP (wolftune, smichel17, Salt, adroit, alignwaivers): check Q3.1 <https://wiki.snowdrift.coop/planning>

### OSI graduation update (wolftune) <!-- Timebox: 5 minutes (until hh:23) -->
- wolftune: I want to make sure everyone is comfortable with their understanding.
- wolftune: definition of the OSI goal (having bylaws in place, etc): to be able to have a press release, would like to have sooner (than the years end). New configuration after we are not an incubator project (still potential to be affiiated etc).  Task for us to make a more clear timeline of steps to get this done
- Salt: I'm unclear about the timeline (year end is broad). They want us functionally stood-up, but not function*ing*/prosperous, yes?
- wolftune: We *redefined* graduation as, legally incorporated (eg, bylaws in place).
- adroit: what do they do differently when we graduate
- wolftune: the core is that they make a press release and say we graduated. I get the impression that we'd have to make a new agreement if we want them to continue accepting 501c3 donations for us
- Salt: clarification: this is not related to crowdmatching donations
- wolftune: it's just a matter of any organization thats a 501c3 saying snowdrift is aligned with their mission and they can accept donations
- wolftune: only matters if it's not crowdmatching donations coming anywhere that's not a 501c3
- adroit: if we are't a 501c3 we need this sort of sponsorship for it to be tax-deductbble
- wolftune: yes
- salt: we need to figure out a business plan / structure
- wolftune: the biz plan we have is not 501c3 based (which is usually mostly based on grants, which is not our goal)
- adroit: we are gonna do a social purpose corporation?
- wolftune: our plan is to continue the business as a consumer coop and be a state-based nonprofit (out of Michigan) (as opposed to an llc or some other structure)
- salt: is there a reason we are holding onto michigan
- wolftune: yes, many states require co-ops to be for-profit.

### Accountability / coworking / mutual support (wolftune) <!-- Timebox: 5 minutes (until hh:29) -->
- wolftune: want to bring up the potential for more people to support each other succeed
- wolftune: smichel and I have been doing daily coworking/stand-up hour, which has been helpful
- wolftune: How can we do a better job, as a team, of connecting with & supporting each other? 1:1s?
- ROUND:
    - Salt: knowing/seeing that people are meeting is helpful. Limited time/bandwidth to *schedule* meetings. Reminding people of regular coworking times is helpful.
    - adroit: agree: one to one seems ideal option, posting it in irc, showing that the activity is happening [is beneificial]
    - smichel: Ive been doing more of this lately, no strong thoughts 
    - msiep: agree that having times that are prompts to work on snowdrift stuff is helpful (such as coworking slots in the calendar)
    - alignwaivers: mutual accountability is a good idea, need to iterate appropriately for each person. Even just checkins / status updates are helpful.
- wolftune: just want people to say how they could use help in succeeding (with snowdrift stuff)

### Mozilla Update (Salt) <!-- Timebox: 5 minutes (until hh:35) -->
https://openlab.mozillabuilders.com/ (for links to content - the town hall we missed was springi
- Salt: wanted a more concrete update, I think I saw something for calendar that went by…?
- wolftune: we did miss an event last week (the town hall)
- smichel: There is one this week, I put it on the shared calendar, but no notification, to avoid bothering people subscribed for other reasons
- Salt: have we been getting reports in?
- wolftune/align: yes
- alignwaivers: there's also office hours (every other week) to discuss with a mozilla builder person
- NEXT STEP (alignwaivers): follow up with team about this (what we want to get out of it) and schedule

---

## 5. Open discussion <!-- Timebox: 5 minutes (until hh:39), cut off 5 minutes before meeting end -->



## 6. meeting evaluation / feedback / suggestions / appreciations round

- smichel: I'd apprecate a not-captured post-meeting discussion section (for reminding of stuff I wanted to do after meeting). Uncomfortable with overriding time-box. Tension with expanding to fill time-box. Tension around meandering discussion when I specifically requested to end early. People distracted with other things?
- Salt: +1 timebox tensions, want to make sure we do check-ins of late arrivals, also nice to still be ending early!
- msiep: I agree with moving meetings efficiently. Appreciate everyone
- adroit: I was okay with the meeting, agree it's weird if there's one more thing to say to wrap up that takes as long as it does to vote. Timeboxes are always hard.
- wolftune: I think it would be helpful with timeboxing in rounds (people aren't sensitive to the amount of time that a round takes). The timeboxing vote can just be for if we need another 5 minutes or not and if not a thought can still be wrapped up quickly
- alignwaivers: agree with default of ending early / pushing non-essential discussion till after formal ending. Don't vote necessarily unless someone proposes extension, no need to be so strict about requiring a vote and blocking a quick finish out of a topic.


## 7. Post-meeting agenda <!-- No need for notes in this section -->





<!--

    Meeting adjourned!

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Replace previous meeting eval notes with new and filter it down to essential points

    Move agenda notes up to "last meeting next-steps review" section

    remove everything except the topic title (without the topic leader) and the NEXT STEPs

    unbold the topic headers

    Update next meetings date, clear attendee list

    Update old metrics, update date, leave new blank

    Clear authorship colors

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/


Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

### Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

### Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
