<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — September 9, 2020
===========================

- Attendees from last week: Salt, smichel, wolftune, davidak, alignwaviers, MSiep
- Attendees (current): Salt, mray, smichel, MSiep, davidak, alignwaivers, wolftune

<!--

    Personal check-in round

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
--------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 9 -> 5
- Posts: 52 -> 43
- DAU/MAU:  43% -> 28%

Snowdrift patrons: 126 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
-------------------------------------------

- smichel: Please pre-fill eval round and breakout room topics during meeting. I notice people wanting to give feedback during meeting, but make notes separately (privately or here at the eval or in breakout room section)

3 Last meeting next-steps review
----------------------------------------

<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

### Nominated topics?
- DECISION: Experiment w/ typing in chat before/during minute of silence

### Use === and ---- markdown more?
- NEXT STEP (wolftune/smichel): change pad to use underlines instead of hashes [DONE]

### Prototype Fund
- NEXT STEP (alignwaivers+wolftune): connect alignwaivers to potential networks / people [CAPTURED] https://gitlab.com/snowdrift/fundraising/-/issues/6

### Etherpad plugins?
- NEXT STEP (wolftune): test out CodiMD via https://pad.nixnet.services/ [DONE]
    - [report] it's nice, sometimes may be preferable, but not for all cases
    - it has no built-in chat, no separate commenting on specific text
    - forces users to have extra-raw-code-style (monospace font etc) or *uneditable* rendering with no indication of authorship colors or anything else
    - it's a tool to add to our list not to replace Etherpad
- NEXT STEP (wolftune): if we don't love then ask iko about installing plugins [DONE]

### Civi deadline
- NEXT STEP (Salt): Dump as much civi data as possible but don't worry about formatting it for import [DONE]
- NEXT STEP (Salt): Focus getting new civi usable. If new civi is not usable by 2020-09-09 (next meeting), we'll just start using a spreadsheet in the meantime, until it is [CAPTURED enough]


4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    Quick answers:

    what is your current focus?

    are you blocked on anything?

    what help could you use?

    Add to agenda or breakout room topics as needed

-->

- align: Not much progress, grant pitch + networking focus; 
- msiep: following up on new crowdmatching mechanism options, specific proposals /feature sets
- davidak: No time :)
- mray: thinking about new mechanism proposal
- wolftune: sent email to lawyer about bylaws and got quick response, she's ready; focusing on seagl talks proposal and capturing key political statements in those talks
- salt: working on seagl, civi; would like to discuss seaGL, civi blocked by docker/php/drupal bug; can use help on civi from anyone w/ expertise
- smichel: focused on getting bylaws to lawyer. not blocked & no need for help

5 New Agenda
------------------

<!--

    Confirm agenda order and timeboxes (in 5 minute increments), adjust for anyone who must leave early

### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->


### Civi status and transition to spreadsheet (Salt) <!-- Timebox: 5 minutes -->
- Salt: civi is a shared contacts system, we've had it for several years, & revisions
- osuosl suppose to take over full hosting. supposed to be setup entirely, but triple bug.
- Salt: was suggested to use a spreadsheet to track contacts, since a week has past probably good to get started on that
- Salt: the old civi is ready to be imported to the new once the bug is fixed
- wolftune: charles, former board member is interested in / using civi, may or may not have civi expertise but is an experienced pro sysadmin
- Salt: potentially good to consult
- NEXT STEP (wolftune): ping charles, ask if he's interested in helping
- Salt: we want data to be as importable to civi as possible
- Salt: was thinking nextcloud might be best place
- wolftune: thinking simpler since this is not the long term goal. one or two people
- davidak: is also ethercalc, but main instance is flaky
- wolftune: also more public than we want
- smichel: propose libreoffice spreadsheet, synced w/ nextcloud
- mray: since we have a seafile, could use that too
- NEXT STEP (Salt): setup shared spreadsheet, put into nextcloud

### State of the Source (wolftune) <!-- Timebox: 5 minutes -->
- https://drive.google.com/file/d/1Sk0fy2vvSQ8IjJbttWXstqFHMVAzHm8s/view
- wolftune: strange (free) registration process. There's a hallway track, more similar to outreach.
- wolftune: In general if there a talk can always watch video later unless want to discuss directly. Hallway track is probably better
- Salt: is this spreadsheet from you directly? can I share it?
- wolftune: anyone w/ time & interest, please consider going; ask if you have questions about how to prioritize to get the most out of it
- Salt: not sure how much bandwidth I have. Hallway sessions look cool
- NEXT STEP (salt): put a link in the snowdrift channel [DONE]

### SeaGL (wolftune) <!-- Timebox: 5 minutes -->
- wolftune: wanted to have a plan for what we're going to do about this: salt and I collaborated on a end-user driven software freedom talk.
- wolftune: Plan make Snowdrift.coop a patron coop focused on crowdmatching / driven end user
- wolftune: application is due today
- Salt: got some feedback on this from seagl office hours:
    - cut half the words and make it more straightforward, bulletpoints might be better
    - comes across as something philosophical/sophisticated, but takes [too long] to get to what it's about (snowdrift talk?)
- wolftune: was considering not mentioning snowdrift at all. want people to come because they are interested in end-user driven software freedom
- wolftune: was hard to discuss without mentioning snowdrift, is a tension
- NEXT STEP (wolftune & salt): submit talk proposal today

### Present thoughts about mechanism 2.0 (mray) <!-- Timebox: 5 minutes -->
- mray: wanted to convey thoughts and get feedback on new mechanism.
- mray: Focus on patrons (as opposed to their money) was important, growing the crowd (we have that now, don't want to lose it)
- mray: came with a 3 part system: 
    1. Project sets a goal every month, # of patrons they want to attract
    2. Patrons that join get to join at different levels, shouldn't be too much (hard cap), small set of options
        - e.g. $1/$2/$5/$10 as your willingness given set crowd size goal
    3. When projects meet goals for ¿amount of feedback? and reach point of 'overfunded'. Crowdmatching inverses and lessens other patrons contributions

ROUND:
    - msiep: agree w/ patron focus in our presentation. Problem with project's goal being #patrons; it should mean something that they will be able to do something. #patrons is ok secondary goal (want broad support, not 1 wealthy donor). *Generally* skeptical of limiting levels *too* much. Overfunding is ok, been thinking similar things.
    - davidak: also said earlier that overfunding isn't a thing bc most projects can use more money
    - wolftune: appreciate new proposal. Don't fully agree about $ amount, can be flexible — in our *current* model, we still want projects to express goals & intentions. "We estimate that we could accomplish X with $Y" (including multiple levels) is compatible w/ crowd-size goal
    - smichel: not sure yet. given a minimum donation level, crowdsize goal is related to # of patrons at given amount
    - Salt: like the "#patrons" framing a lot, less sold on practicality of it. Maybe you can choose either or both. In regards to different levels, rubs me the wrong way.  the overfunding is a neat concept, addresses feedback gotten, adds another complexity getting away from simplistic
    - alignwaivers: #patrons can correspond at least approximately to $ amounts, really like the overfunding and think its worth the complexity, not sure on differing levels

- wolftune: the shortest smallest version of this would still allow the budget to be per-project budget. at least can remove the 'kicking out' at site-wide budget
- salt: really like removal of kicking out
- msiep: one other concern on #patrons, might disincentivize pledging at higher amounts
- mray: makes a difference because if you are close to reaching budget: it translates to 'you are able to amplify funding more' and superficially making up for people donating smaller amounts
- wolftune: to reiterate what robert said: if i put it in at a higher level, i'm signaling to others that i'm 'extramatching you', you should really join
- davidak: another point about the overmatching is no incentive to join if budget is already attained

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
---------------------



7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------

### Breakout rooms vs in-meeting discussion (smichel — wolftune, Salt, align if interested)

- NEXT STEP (smichel): bring up in next meeting, non-binding votes w/ facilitator decision; erring towards no extension
- NEXT STEP (smichel): bring up in next meeting, different name for "Breakout rooms", it was only changed since I didn't like implications of "Post-meeting discussion" (that most people will leave)

### bylaws email (smichel — wolftune)

### SeaGL (Salt — wolftune, anyone else)

### Mechanism (mray - Salt, davidak)
- mray: 2 main issues I wrote down

    the goal should be meaningful, not just an arbitrary # of people. at least a minimum level of $ with # of patrons

- davidak: I think in the end this is about money. Need a specific amount of money. To have a meaning, the goal can have text: want to have 3 developers
- mray: is money really the goal? lots of weirdness with power distributions in VC's etc
- davidak: agree the communication should be about # of patrons but there is a baseline budget amount
- mray: putting up the $ amounts is a bit weird because people can switch amounts day before
- wolftune: feels a bit awkward to look at a pile of money rather than the # of people donating
- mray: a takeaway is that pennies may be a drop in the bucket in reality but by participating in the crowd, this is meaningful (and has impact at the highest level)
- wolftune: what's the key message was a question?
- salt: if  we are focusing on the # of people vs dollars. Deciding bw minimum level of money vs minimum level  of patrons. helps project doesn't help understanding
- alignwaivers: projects could have the option for an overfunding limit or not (if they decide they can't use overfunds etc)
- mray: if no overfunding, project can game by setting a low budget
- wolftune: an incentive brought up for joining after overfunded could be sustainable 
- wolftune: determining a compatible inital state would be ideal: one that allows us to keep potential future changes with 



8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------
- Salt: appreciate note-taking, timekeeping (side chat worked well). Thanks aaron/athan for showing up despite fires.
- davidak: excited we are making progress, feels good!
- mray: grateful for everyone showing up, effective meeting, thanks for notetakers since I dont do that much
- smichel: would like to encourage people to use open discussion & breakout more. We should weight v's more heavily in timebox vote; the people who voted ^ can continue in breakout room. Interesting that we tooks so long to decide/vote. want to propose tiebeak in the favor of people, or a single vote to move on automatically postpones to open discussion / breakout room
- wolftune: notices that rounds/checkins take longer when more people show up but that's ok, good tradeoff. Noticed a tension with timeboxing: everyone wants to be efficient with time; if it's hard to extend timeboxes people will lean toward claiming larger initial timebox. Better to make it safe to be short and know that extending when sensible won't be too strictly rejected. It's a balance.
- alignwaivers: think we are still getting used to accurate timeboxes, makes sense there's some pendulum swinging with over/underestimating. Want to expedite voting, thanks for showing up
- msiep: appreciate everyone being here + note-taking.


<!--

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling checkin notes

    Agenda items

    keeping the topic title (but not topic leader) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
