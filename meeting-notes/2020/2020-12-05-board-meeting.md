<!--
Previous week's meeting notes: https://wiki.snowdrift.coop/resources/board-meetings/
Meeting location: https://meet.jit.si/snowdrift
backup: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)
See end of this pad for meeting-practices notes and reference
-->

Snowdrift.coop Board Meeting — December 5, 2020
================================================

1. Check-in
-------------
<!--

    Personal Check-In Round — Snowdrift-related thoughts are ok but not required.

    Where is your head at? — Air distracting thoughts & understand others state of mind.

    Add your name to attendees.

    Does anyone need to leave early?

    Assign facilitator

    Assign time keeper — use https://online-timers.com, start for each item, paste each url in the chat

    Assign note taker

    Assign someone to clean and save notes and update pad for next time

-->

- Attendees: Aaron, Stephen, Lisha, Micky, Salt, Erik

2. Review previous meeting
---------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> -->

### Approve previous meeting notes
https://wiki.snowdrift.coop/resources/board-meetings/2020-11-21-meeting

- Approved: Stephen, Lisha, Micky, Aaron, Erik

### Feedback
- Erik: better timeboxing: take more time to consciously decide and the outcomes desired
- Lisha: Whenever we get to a point where our opinions haven't changed and not moving forward, that's when the topic probably needs to be tabled and conversations move to a different place. For me right now, live conversations are easier than long threads.
- Aaron: let's make sure 'what you want out of it' is a clear actionable outcome, and not go ahead with a topic until we have that. It's worth the up-front extra time.
- Salt: Identifying if it's gonna be an hour or 1.5 at the start would be good. I had some tension around being here as a facilitator, but also being a team member, hard to keep separate. At 45 minutes there should be a bio break. Thanks everyone for taking time

### Bylaws legal review update
- NEXT STEP (Stephen): share with Board the final bullet points sent to Deb [DONE]

### 501(c) considerations
- NEXT STEP (Stephen): Schedule board mtg to discuss coop vs nonprofit situation, decide on goals/mission [DEFERRED]
- NEXT STEP (Aaron): Make sure we are clear on questions for Salt to pass along to his professor.
- NEXT STEP (Aaron, Stephen, Athan): update mission statement and vision statement 
    - What is this about / what is the update intended to accomplish?
    - There is https://gitlab.com/snowdrift/wiki/-/issues/20, which I don't know if it is a duplicate.
    - NEXT STEP (): Board discusses/updates/approves mission/vision statement
    - (Salt): Make checklist for this
    - (Stephen); schedule meeeting to discuss

### Officers/roles/scheduling/attendance
- NEXT STEP (Lisha): connect with Mickey to update / check in about attendance [DONE]
- NEXT STEP (Stephen): Schedule a meeting, send out reminders in advance [DONE]
- NEXT STEP (Stephen): take scheduling role until end of the year [DONE]

### MVP crowdmatching decision strategy
- NEXT STEP (Lisha): reach out to Mooks in Berlin about CSS help for Snowdrift (he may have free time till january) [Update: I called Moox. He's not able to help with this right now. - Lisha]
- NEXT STEP (Stephen): Plan for bringing this up at next Board meeting [DEFERRED / maybe not needed]

### Draft timeline for OSI graduation (carry-over)
- Stephen: one date to put on it which is December 15th (when deb can begin working with us)
- NEXT STEP (Aaron): begin a thread on discourse / prompt discussion [DONE-ish] <https://community.snowdrift.coop/t/osi-graduation-timeline/1688>


3. Updates
-------------

### Metrics

(none yet)

### Treasurer's report

- Aaron: we were charged an extra 30 cents, final charge from AWS, no change

### Bylaws work with Deb / Aaron Williamson consultation

- AW is lawyer, in free softwar e space, sflc, in 501 osi working group w/ AWolf re: "open source" & IRS
- He's able to understand what we're doing, not so much co-op side
- He was counsel w/ gratipay, navigated them through a new payment provider
- 1 hr consult yesterday

Key takewaway:
    - He doesn't have much other experience w/ ToS/Contracts for a platform like us, but
    - He coudl clearly say that the thing that will be most important is having solid ToS (e.g. what does hitting the "pledge" button mean specifically)
    - Separate from the bylaws focus we have with Deb
    - Could hire AW, *possibly* discounted rate but still several thousands of $
    - He was very skeptical of 501(c)(3) fit, 
        - Idea of patrons entering in contract, where they pay projects for a result — released publicly to everyone, but still work — & part of a co-op that is not 501
        - IRS does not consider "works being non rivalrous" alone as sufficient for 501(c)(3)
        - Only way we could be 501(c)(3) would be if we were specifically for subset of FLO that is otherwise a (c)(3) qualified area of work
        - Pledge is contract between patrons & projects
        - He suggested that we'll need funds for ourselves, to handle liability (e.g. legal costs)

- The one part we might work with him for co-op/bylaws work is to help Deb understand us.

- May be worth reaching out to other lawyers

### Open discussion <!-- Timebox: 5 minutes -->

Remaining legal budget/retainer for work w/ Deb?

    - (Aaron) Not sure, haven't received an update on time she has remaining. She and I have discussed music lessons, maybe we can come to barter arrangement :)

- NEXT STEP (Stephen): Clarify^ w/ Deb what remains

4. Who governs what?
---------------------------

Use full "Board of Representatives" probably okay, can still ask.
- Erik: on wikimedia, we had two, Board [of Trustees] & "Advisory board", the latter got qualification, no confusion

Number of people on Board
- Aaron: I'm inclined to ask Deb's recommendation
- Salt: I don't like the idea of a fixed number. Allows for growth & flexibility
- Erik: Don't think we need a cap, but expanding board should always require bylaws update

Decision: always require Bylaws update to expand the Board

5. Making policy changes
------------------------------

- Erik: wikimedia, went through stages. First board, working board, day-to-day decisions. As we went forward,just strategic. SD has an existing volunteer community, board should be voting on principles, not projects (maybe special case for first project). Membership driven. Finances more likely to be a board decision. To the extent that we have staff, some policy decided by them, e.g. HR policies. Board should not approve patrons or projects except in special cases.
Board decides on *parameters* for project acceptance and maybe approves first beta projects.
- Micky: w/ May First, we have 25 board members, which works b/c not every member makes it to every meeting, people assigned to bring others up to date on mtgs they missed. We form working committees to deal with issues/ day-to-day; it's a member-driven co-op. I think that works much better than involving board w/ every level of decision. Important that board be elected by membership.
- Stephen: Seems many cases could involve asking Deb for her recommendations, she sent as a different consumer-co-op Bylaws to review…
    - Micky: always good practice to look at other org's bylaws.
- Aaron: Apprciate emphasis on committees. Decision now seems to be: how much goes in bylaws? How much power w/ board vs others. Bylaws essentially give Board the leeway to put decisions in Standing Rules however they like, delegate or not, committees or not, etc, unless we specify in Bylaws right? If we take committee approach, then that puts something in the Board's purview. Are there things we *want* to designate to staff and not to Board as compared to just allowing the Board to choose whether to delegate or not?
    - Micky: Committees work well because they do the bulk of talking, bring to Board *concisely*, less frustration for full board. 3-4 people per committee is useful; some things need more research than initally though.
    - Erik: Wikimedia has had Board committes; most nonprofit Boards have some (e.g., Finance Committee). In early years, also programmatic committees, e.g.,  special projects idenfied projects to partner with. Pros and cons, keeps board involved in day-to-day that I think isn't good long-term goal. Committees w/ some board, some volunteer could still be a good thing in early stage of org.
- Aaron: Separate from what goes in bylaws, answers to deb must communicate our vision. "We don't envision Board approving projects". Bylaws themselves having high level requirements for projects (e.g. public goods). Staff approving specific projects…

- Stephen: related topic is the decision process (levels of approval, notice timeframe etc)
- Aaron: I imagined:
    - standing rules written by board. They could say, "staff must make decisions about these policies" (delegate) or do something else, no additional barriers. The questions are what items we want to lock-in in the Bylaws as compared to allow the Board to set later, and also whether standing rules, once set, use any different process for amending compared to any other Board decisions.

- Erik: notice period definitely belongs in bylaws themselves.
- Aaron: I think we already put some notice-period points in the summary bullets at least
- Stephen: We said 21-90 days notice for member + board meetings, but did not decide on requirements for other types of decisions.

- Erik: bylaws changes should have pending period after approval where people can still object, e.g. 30 days


- Stephen: Maybe I bring a specific suggestion to next meeting?
- Erik: maybe homework assignment for everyone: look at another org's bylaws, to get additional input on some of the open questions
- Micky: A "bylaws jigsaw" meeting
    - NEXT STEP (): Schedule 1-hour meeting for bylaws


- Aaron: [meta] ask/communicate w/ deb, have roadmap: "How much should we worry about deciding for 1st version of bylaws?" Maybe make amending easier in 1st version, then make more difficult once we've gotten through other checklists. Ask about how many parts could be put off for planned future amendments in order to have an active Bylaws sooner within which we're working now.

- Aaron: "What is the decision? How do we decide if it's an emergency?"
- Salt: In orgs I've been on in the past, this is chair's job; someone else needs to convince chair to call the mtg.
- Micky: This may tie in w/ committees. If member raises emergency Q or change, committee could bring it to the board. Important that committee chair can bring to the board.
- Aaron: I don't think we've clarified how President/Chair is decided.
- Salt: …and that should be in the Bylaws.

- Erik: agree w/ idea of chair or vice chair calling emergency mtgs, not sure how that works w/ bylaws change, that should still require a notice period
- Aaron: emergency Bylaws change sounds crazy


- Aaron: Should mention we are deferential to sociocracy ideas. And I assume within Sociocracy, once a role has been delegated a domain, higher level roles can't just unilaterally micromanage. Similarly, once standing-rules decisions have been set, perhaps there's a different process for changing that policy.
- NEXT STEP: bring up Sociocracy to Deb

<!-- figure it's worth pasting this here anyhow, Salt's amendment process
# Club Document Amendments

## Constitutional Amendments
Procedures for amending the Constitution are described in the Constitution.

## Amending the By-Laws and Policies
The By-Laws and Policies may be adopted, amended, and revised by a majority vote at a General Meeting called according to the procedures outlined in Article VII - Section C of the By-Laws where a quorum is present.

## Procedure

### By-Laws
A proposed By-Laws amendment must be announced at a General Meeting where discussion and revision of the amendment will take place. The revised amendment will then be posted in an appropriate manner to inform the Members at least seven (7) calendar days prior to the General Membership meeting for which the vote is scheduled. Details for this posting are described in the Policies.

### Club Policies
A proposed Policies amendment must be posted in an appropriate manner to inform the Members at least seven (7) calendar days prior to the General Membership meeting for which the vote is scheduled. The amendment may be revised at the same meeting as the final vote. Details for this posting are described in the Policies.

## Announcement
1. After the Constitution, By-Laws, or Policies have been amended, adopted, or revised the Program Director shall incorporate the changes within two weeks (14 days) and post the amended version to the “Club Documents and Forms” page of the Club website.
2. If any document is not updated within two weeks of the changes, those changes shall not be binding until the document has been updated and appropriately distributed to Members.

# Sunset Provision
If an action approved at either a General Meeting or an Executive Council Meeting has not been started within 10 weeks of the vote or the time set for the commencement of that action, that action shall be considered null and void.
-->

6. Meeting Frequency
--------------------------

- Aaron: Quarterly is good?
- All: yes!

Month for mtg?
- Aaron: we had Feb originally, not sure where it came from
- Salt: Generally you want to align these times w/ most important constituencies
- If you're grant funded, align it with when grants start closing their books for the year
- For our members, December is the big "giving push" (but also crowded), April is tax season
- Aaron: Feb came from, "month that doesn't tend to be full w/ other stuff"
- Micky: May? Is when people have their taxes done.
- Erik: July-June 30 is also a common fiscal year
- Stephen: Propose May?
- All: sounds good
- Aaron: We could set a goal to have our first official member mtg in May 2021.

7. Suspension/termination of Membership Agreements
-------------------------------------------------------------------

- Erik: I suggest, within purview of board to vote on both ToS & membership terms, but that's separate from the bylaws. But ToS is a separate, complex legal topic. Enforcement not left to Board.
- Salt: I think this has to do with understanding of Board responsibilities?
- Aaron: membership, too. "How do we make it clear / due process when someone has violated the terms?"
- Stephen: skeptical of full membership vote to kick someone out; we may have 1000's of members.
- Aaron: could do jury
- Aaron: aside — read an opinion that alternatives to voting might make sense, e.g. lottery https://community.snowdrift.coop/t/election-voting-systems-star-proportional-etc-bylaws/1554/2?u=wolftune

- Erik: administration of membership must be specified in full detail in he bylaws? As in the day to day issues of enforcement of terms
- Salt: that could be in policy
- Erik: I think we can err on the side of not over-specifying in Bylaws

- Stephen: Decide to leave that to policies?
- Aaron: I suppose, Bylaws would say that member agreement includes following platform ToS.

8. Confirm: We do not want to allow member loans
---------------------------------------------------------------

- Aaron: Have not done this work… Uncomfortable with this decision… considered putting personal $ in to Snowdrift — ability to zero-interest loan to snowdrift instead of gift might change that decision. Not sure if that's pre-bylaws or will change later.
- Lisha: Could think of it as giving business interest-free loan. Could also think of, when people pay out of pocket & want to be reimbursed later (but org doesn't have the $ yet). Reasonable to be able to turn in expense report & have it paid later.
- Aaron: So it's like, the board authorizing expenses that they don't yet have the funds to pay. E.g. hiring Aaron Williamson, board decides 

- Stephen: So, we *are* intersted in that type of "loan"?
- Aaron: at least for now, yes.
- Aaron: tension about liability
- Lisha: once the org is functioning & has money, it will still come up. E.g. someone needs to buy a plane ticket today but getting $ from org account takes time.
- Erik: not against language that gives us the flexibility to do these things, but skeptical that we should handle *anything* this way financially.
- Erik: If we need to have those legal costs, we should try to fundraise instead
- Stephen: at hcoop, organizational credit card, normally 

- Salt: If we get into a situation with fundraising goals, being able to do that, it's good to spell out


9. Dissolution
----------------

- although MI has defaults, preferable to set a defined process in Bylaws anyway
- being super specific could mean just stating in advance what should happen


- Lisha: I think we should not decide in advance which org to go with, not sure that it will exist. Board at the time should decide who's similar/relevant/etc.
    - Could even have a successor org that is effectively Snowdrift v2, and we don't want to have blocked that as the process
- Erik: seems unlikely that we have lots of assets that need to be distributed in the event of dissolution, but to prepare anyway, I'd prefer more membership involvement rather than Board decision
- Aaron: idea of something in the bylaws, "an org that serves the mission of snowdrift.coop". I don't imagine we'll have lots of assets. More concerned w/ the brand value as what matters the most. Does some other entity get to claim that they're our ideological successor etc.
- Micky: +1, membership should have a say in what orgs we value. It's an ongoing conversation, can be mentioned in annual membership meeting.
- Aaron: I've talked w/ someone who made up a story that Snowdrift.coop would end up getting acqui-hired.
- Salt: Hadn't thought of that, is a good point (look at Red Hat).
- Salt: you can either frame it as, "If a decision cannot be made, it will be X" or "This is how it will be, unless X".
- Salt: It's a little [I don't like using this word] irresponsible not to have a plan. If we're at a point of disolution, there are going to be unhappy people.
- Salt: I like membership driven up front, but should *have* a fallback plan; I've seen some messy dissolutions.
- Aaron: some idea of orgs we have a partnership with, could say dissolution will go to an org we've endorsed in the past.
- Salt: If we're not going to be c3, opens up what can be done w/ assets, don't *have* to go to another c3.
- Salt: Whether or not we have "real" assets, the IP will be a thing.
- Erik: Bylaws should IMO not specify orgs, but I do believe that a membership-driven process should be stated clearly


10. Meeting evaluation / feedback / suggestions / appreciations round <!-- Timebox: final 5 minutes -->
--------------------------------------------------------------------------------------
- Aaron: thanks Stephen for leading/organizing/note-taking. Happy we're all engaged & making progress. My ideal would be that Deb was here & that would be a different process, but I don't want to take too much of her time. Process could be less wishy-washy/speculative, but I don't have specific suggestions for improvements even though I'm sure many ways to improve
- Lisha: Things seem to be moving forward, does feel slow to me, although I get we have a lot to run through. Would be awesome to have deb @ meeting, boom-boom-boom answer questions & we're done. But at least we're getting through this.
- Erik: Thanks for getting mtg more streamlined this time around. +1 lisha. My opinion differs from Aaron's tendency to show deference to everyone, sometimes we need people to be assertive and opinionated. Would be great to have someone on each area with a specific opinion/recommentation/proposal; could help move things along, & will make things less contentious move, more contentious would become obvious immediately.
- Micky: got a lot accomplished, long process & nobody is a pro at it, reachable goals; appreciate team being here
- Stephen: happy to just barely get through everything, really hard to facilitate and take notes all at once. This meeting was a bit more general and vague than I'd hoped, partly people didn't have enough solid recommendations/opinions. With more preparation, I think we can do better. Looking forward to future.
- Salt: being able to sit in without being facilitator was much nicer, tensions around being non-Board-member, but it was good that I felt I could speak up at times. Timeboxes were missing. 2 hour clarity up-front and planned break worked well. Thanks to all.

asking permission to add my voice vs de-facto being in the meeting because I am facilitating.


<!--

    Meeting adjourned!

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Clear meetings date, attendee list

    Clear report / metrics (keeping the topic titles)

    Replace previous meeting eval notes with new and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas

    Change the last-meeting-notes wiki link to this past one

    Clear authorship colors

-->

<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, rounded to nearest 5min., settled during agenda confirmation
- format is: "Timebox: 10 minutes (until hh:mm)" (in an html comment so it doesn't appear in note archives)
- at topic beginning, convert the :mm to expected end time
- at timebox end, "thumb polls" may add 5 minutes at a time
- hand symbols
    - "^" approve, extend the timebox
    - "v" disagree, move onto the next topic
    - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
