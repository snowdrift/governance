<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below (select the whole line and drag to your bookmarks bar). You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
<!-- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/  -->
 
# Meeting — April 6, 2020
 
Attendees from last week: Adroit, mray, msiep, smichel, wolftune
Attendees (current): wolftune, smichel, mray, msiep, Adroit, alignwaivers, Salt

<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 1 -> 0
- New Topics: 2 -> 5
- Posts: 10 -> 25
- DAU/MAU:  38% -> 39%

Snowdrift patrons: 118 <!-- from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!

### Use chat in etherpad (and add your name)

### Conversation queuing
- three discussion mechanisms: hand symbols (below), call for a round (and vary the order), open discussion
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on

### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback

- Adroit: effective meeting, relative to others
- mray: good meeting, sorry it took so long :)
- msiep: really glad, but going over was problematic tension, we need reliable process to acknowledge end of hour. Didn't feel like I really had the time but didn't want to drop.
- Salt: really happy about the whole meeting, don't think we spent too much time on mray's topic, agree about time mindfulness, thanks wolftune for facilitation, cue yourself is good etc, could spend more REAL time carefully reviewing order
- smichel: we should have prioritized better, put the important topic higher maybe
- wolftune: It was hard for me to take notes, facilitate, and participate. Minute of silence does seem really useful. Whatever tensions you're not sure about bringing up, seem to usually be worth bringing up more often than not.

## Last meeting next-steps review

### Adding IRC/Matrix stats to weekly metrics, both channels (wolftune)
- NEXT STEP (Adroit): Investigate if this is possible/practical <https://matrix.org/docs/projects/bot/matrix-stats>

### Clearing notifications in etherpad / clearing the chat / ping without desktop notifications? (wolftune)
- NEXT STEP (wolftune): Test this to try and find the cause

### Responding to threads on Discourse (Salt)

### IRC bot feature request process (Salt)
- NEXT STEP (Salt): Ask fr33domlover [about what?]

### Status of implementation workflow (mray)
- NEXT STEP (wolftune): add this to next week's agenda for further discussion (earlier in meeting)
- NEXT STEP (smichel): Inquire with bryan re: incrementally transitioning to a different frontend framework (than yesod) [[ I am going to drop this pending documentation of the current status / whether we find that yesod is actually a blocker on development workflow ]]


<!-- One minute silence, check with ourselves mentally and personal notes/tasks/emails to surface any tensions, add agenda if appropriate -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->


## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->

### Resolving design implementation workflow (smichel)
- smichel proposal: iron out workflow via specific issue (about page?), implement it and document the process, iterate
- adroit : likes idea, but doesn't think the "about page" is the best example - it's more automatable compared to other things, not a good representation of other pages
- smichel: agree and that's why I picked it - things have taken longer than ideal: starting with something trivial will be better 
- adroit: I volunteered to do the about page for hamlet
- mray: I just send over the designs, not concerned about the implementation process. Sometimes need multiple iterations, but where the initial mockups are concerned, makes sense to go forward
- adroit: do html structure, have css been in dynamic structure (not tied to yesod), so you can refresh a page and have css updated
- mray: The workflow iko and I are using right now, a static site generator, uses a sass precompiler for css. 
- adroit: whatever we use, sass (instead of css)
- wolftune: we're already using sass in the main site
- adroit: I use a precompiler called Stylus
- wolftune: I think there's a question about the types of pages — we don't want to have to go through the static process when we just need to tweak an existing page. We should make it as easy as possible to tweak existing content.
- wolftune: the key is that we should document the process.
- adroit: I was imagining that we could get to a point where hamlet is precompiled, designers can just save the css and *only it* gets regenerated (hot reloading)
- wolftune: Problem: in order to make a minor html tweak, designers need to download the entire yesod stack and compile the site first..
- adroit: would be better if we just had html files
- mray: youre proposing to have a copy of the entire snowdrift site locally? running by yesod
- adroit: but in a way that would be no more difficult than a static site generator. Can see the results after hitting save in the browser immediately
- smichel: so what youre saying its nice to have hot reloading immediately
- adroit: may look unfamiliar, but can recognize it's just a ptag, etc.
- mray: would be nice to make change and not have to update the entire yesod stack
- wolftune: ideally people who don't know haskell can still work on things
- wolftune: if you want to make a new page, need to make or edit? a new hamlet file. Can have an assist from someone who can set that up
- wolftune don't want to take it on but I'm happy to help
- guest: side note, hotloading works in the sense that yesod will automatically re-compile hamlet, and sass hotloading should also be working (according to h30, he probably patched the libsass library to make that happen). however, for hamlet it is slow, especially when adding new routes. there was a brief experiment to do the same using hakyll via calling yesod libraries (with thanks to fr33domlover's help), and it suffers the same problem. there may be a way to change this behaviour, maybe some effort to re-code with the yesod libraries.
- NEXT STEP (adroit): Document current state of hot reloading. What works; what doesn't; how long does it take?
- NEXT STEP (adroit): Implement new about page, document process


### Comparing to other platforms: Filling in the checkboxes (msiep)
- see https://community.snowdrift.coop/t/page-comparing-snowdrift-coop-to-other-platforms/1360/6
- msiep: realized we needed to get the matrix content with the checkmarks up. Footnotes aren't being included - question is whether to do them and how in mockups. Mainly wanted to point it out to people to let them look at it
- wolftune: question of how to distinguish coops and non-coops
- NEXT STEP (msiep): Figure out site flow (how does one get to this page, /compare)
- wolftune: for example, mutual assurance which kickstarter does, but no one is doing "crowdmatching"
- msiep: I think it's better to be more generous towards acknowledging the good aspects of what others are doing -- not exaggerating
- smichel: Is OpenCollective a nonprofit?
- wolftune: no, C corp, it's funded)
- wolftune: I was also thinking, possibly a different icon instead of check or X
- NEXT STEP (all): Discuss this on the related gitlab issue

<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->

### Payment Services research (adroit)
- adroit: there's a thread posted about payment systems, wiki page is out of date though. Spent time evaluating these options. 
- Braintree (paypal based) has the features for recieving and distribution of funds all in one PI
- wolftune: Can you charge many to many?
- adroit: in the case of all that I researched, you have to call and know what youre talking about. Have an option for doing the escrow method which is a workaround
- salt: would be up for making some of these calls - but concerned about 'going live' when governance not fully set up, might not "seem real" to payment processors & get approved.
- adroit: will take a while before this goes live so might not be as much an issue
- wolftune: Want to make a decision about business partners with board and all in place. But still useful to get information about the different providers, to present to the board, etc.
- adroit: I did skip paypal. payza? no longer exists
- wolftune: these details can be updated on wiki, things that need to be discussed can addressed here
- salt: one similarly aligned org im curious if you researched is the mifos initiative, deal a lot with finances
- adroit: my conclusion was dwolla (uses bankdirect) is probably best long-term, if we're ok having users use their bank accounts, means we don't need to worry about credit card fees, better for recurring transactions. GNU Taler is very promising, not designed for recurrring
- smichel: how are you documenting?
- adroit: it's in my head - what part of these details do we want on the documentation
- NEXT STEP (adroit, wolftune, smichel17 can replace/augment wolftune): schedule time to get together and update the wiki <https://wiki.snowdrift.coop/market-research/payment-services>

### Grant writer role? (wolftune)
- wolftune: establishing there's a potential role here would be good, as getting funding to get launched would help
- NEXT STEP (wolftune): Open issue for adding this role
- Salt: a couple year sback, robert was talking about grants in germany
- wolftune: I have a huge backlog of potential grants for someone who knows what they're doing to work through


### NOTE: bigbluebutton -> alternative to mumble/jitsi/etherpad? https://bigbluebutton.org/ (mray)
- mray: Looked promising, but I don't have experience with it. Might have some extra benefits as an alternative
- Wanted to encourage people to check it out
- wolftune: last I checked (several years ago), it was also promising but needed to be self-hosted and that wasn't trivial.
- NEXT STEP (wolftune, mray): Find a time and test out <https://test.bigbluebutton.org/>
- We all tested via https://demo2.bigbluebutton.org/html5client/join?sessionToken=yfus2vcrgcuqu0rd


### Adding IRC/Matrix stats to weekly metrics, both channels (Adroit)
- adroit: investigated <https://matrix.org/docs/projects/bot/matrix-stats> but next step proposed would be figure out how to actually implement
- adroit: whether it's the best is a question: it can doc how many chats have been in a room, how many users, how many hours, how many days
- NEXT STEP (salt): Figure out how to deploy this

### h30 outreach (smichel)
- smichel: just documenting here that I'm going to follow up with reaching out to h30
- NEXT STEP (smichel): Open private issue in outreach gitlab
- wolftune: you can say the nice thing that "we have other people working on this and it would be great to get your assistance"

### Mailing list (smichel)
- smichel: may have been discussed enough on irc. Was looking through the forum and there are reasons people would turned off email list. Don't have another way of emailing besides them through the forum listing but there are other people who may want to be contacted regularly - seems like CIVI would be the way to manage this
- wolftune: yes, civi is the way to handle that; forum has a *separate* weekly digest
- stuff that goes in the "announce" category could work for this 
- smichel: as far as i'm aware you can't separate the setting for this bw announcements and normal emails via forum
- Salt: Is there a way for non-logged in people to get email/rss from announce category?
- wofltune: no?
- NEXT STEP (smichel) : Check about discourse features: non-logged in emails/rss
- NEXT STEP (Salt): Get civi mailing list functioning with new host

### Meeting time (Salt)
- salt: I am busy till 1:30, going to nudge people to update availabilities and propose finding a new meeting time
Would be better to move meeting than move time. Maybe 13:00 on wednesday?
- NEXT STEP [DUE: Friday] (everyone): Update availability so we can decide on a new time for 
- smichel: the date last updated is on the poll 
- How should we distribute the poll link to non-team members? (1) Who outside of @team and (2) What does the process look like? https://community.snowdrift.coop/t/team-availability-and-weekly-meeting-details/
- alignwaivers: looks like the poll got reset?
- NEXT STEP (wolftune, athan): Figure out what's up with poll
- NEXT STEP (wolftune): invite Adroit to add availability

### Facilitation by topic leader (Salt)
- I liked how Adroit was leading the discussion/facilitating his discussion
- Adroit: I was able to do that because of the etherpad chat notifications
- facilitator *assists* topic leader
- NEXT STEP (alignwaivers): Add this to the etherpad template somehow

<!-- Open discussion? ~5min. if time -->


---
 
## meeting evaluation / feedback / suggestions / appreciations round

- smichel: was unsettled at beginning, not what I had anticipated on my topic but made good progress on a number of topics
- wolftune: could maybe *write* out more in advance around topics, planned and people could review in advance
- salt: covered a lot of topics
- msiep: good meeting great to see you all here
- mray: +1, especially thanks to everyone for taking notes :D
- athan: good meeting :)
- Adroit: Thanks, happy to be part of the team
- smichel: Agenda for next meeting: Add task capture to open discussion section to address week-lag in capture


<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->
