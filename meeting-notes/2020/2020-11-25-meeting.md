<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — November 27, 2020
============================
<!--

    Personal check-in round

    Assign facilitator

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

    Assign note taker

    Assign someone to clean and save notes and update pad for next time

-->

1. Metrics
------------

- Attendees (last week): smichel, wolftune, davidak, Salt, mray, alignwaivers, MSiep, adroit, Salt, tannerd
- Attendees (current): wolftune, Salt, stephen, alignwaivers, MSiep, davidak, mray, tannerd

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 1
- New Topics: 3 -> 11
- Posts: 16 -> 67
- DAU/MAU:  49% -> 55%

Snowdrift patrons: 131 <!-- from https://snowdrift.coop/p/snowdrift -->

2. Review previous meeting feedback
----------------------------------------------

- wolftune: Feel like we need a way to break things up as we have more people, don't need to do rounds with everybody. 
   - breakout rooms seem to have an open discussion format rather than being focused
- adroit: happy with the meeting: propose name scheme for a/b/c/d for simulataneous
- smichel: snow-shoveling standup round covers topics that are basically equivalent to the whole meeting. It's not useful right now because it's asking people to say too much in one round. I think it should be broken up into multiple rounds.
    - re: wolftune — breakout rooms are a version of that

3. Last meeting next-steps review
------------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

- NEXT STEP (wolftune + alignwaivers + salt): cowork on org-roles [STARTED/CAPTURED]

### Some potential funding souces / good places to promote
- NEXT STEP (alignwaivers): draft write promotional blurb [STARTED/CAPTURED]
- NEXT STEP (davidak, Salt): review draft
- NEXT STEP (alignwaivers): Capture as a place to promote when we are live [CAPTURED]

### When is the right time to communicate publicly about Snowdrift?
- NEXT STEP (wolftune + alignwaivers + salt): update "how to help" so it's good enough that we can link people there directly.

###  Meeting format
- NEXT STEP (smichel): document / make arrangements on pad [DONE enough]

###  [Project Management] Next steps vs support materials 
- NEXT STEP (alignwaivers): add notes about actionable items vs support materials to gitlab flow documentation [DONE]


5. Stand-up
----------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    https://www.online-timers.com/timer-1-minute

    Does anyone need to leave early?

    ROUNDS: Questions with instant answers may be answered during the round. Otherwise add to agenda.

    If the asker needs to clarify for the answerer, add to agenda instead.

-->

### ROUND: What progress have you made in the last week?
<!-- Don't mention anything already discussed in last mtg next-steps review. -->

- meta note from smichel: modified snowshoveling round from previously have 4 questions that were overloading
- smichel: had an unexpected but very productive chat with Deb (lawyer) yesterday and good chat
- wolftune: had a board meeting Saturday, updated accounting, discussions on forum, replied to inquiries on fediverse, discussion of mechanism etc., meeting with OSI
- salt: coworked on promotional pitch blurb and co worked on roles
- align: ^^already mentioned :)
- mray: talking/thinking/forum discussion about mechanism
- davidak: worked on mechanisms and have much more clear understanding of proposals
- tannerd: been playing around with visualization packages

<!-- ### ROUND: Is there  -->

### ROUND: What is your top priority for next week? Is there any information you need?
- wolftune: mix of priorities, maybe role development
- msiep: keep up with forum discussions
- adroit: ^+
- davidak: read 2 new mechanism proposals and help make a decision
- tannerd: getting past two major mechanism proposals coded in an environment to play with
- smichel : gitlab organization
- mray: figuring out best mechanism proposal
- alignwaivers: gitlab organization, coworking roles, and funding organization
- Salt: mapping role space

### ROUND: Are there any decisions you'd like input on?
- smichel: brief discussion on this updated format (post meeting?)
- alignwaivers: gitlab organization if anyone has insights

### Open: What or who could help you make more progress? Are you blocked on anything?
<!-- This one's just a catch-all until we come up with better questions -->


### Open discussion <!-- Timebox: 5 minutes; skip if we have enough agenda -->

<!--

    Confirm agenda order and timeboxes (in 5 minute increments).

    Choose rooms for discussions that can happen in parallel

    Leave 5 minutes for meeting evaluation.

-->
6. Agenda
------------
<!--

### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->


### Getting team buy-in around alpha MVP mechanism (Salt) <!-- Timebox: 10 minutes -->
#### Summary
- We have been talking about the nuances of our "perfect" "launch" mechanism.
- Aaron made a proposal on the forum and I want to give it real-time discussion space.
- Proposal: Snowdrift.coop as the only project, $0.001 per patron pledge, 5000 patron cap before matching is turns off
- I am all for this as long as we add explicit messaging that this the heart of what we are doing but that flexibility will be added along with projects for the beta release
- [forum post]: https://community.snowdrift.coop/t/re-framing-mvp-alpha-toward-project-goal-direction/1657

#### What I Want
1. confirmation that everyone has seen this proposal
2. a round to collect thoughts
3. a vote to confirm that we can move forward with the design/development surrounding this mechanism
4. a list of next steps and someone to volunteer to add them to gitlab (read: each task doesn't need an owner yet)

#### Discussion
- adroit: if snowdrift is the only project and nothing else will be funded unless we choose to continue using, not much of a point. The idea behind matching is you can know that some projects wont get much money if not many fundraisers. Not sure this is the best way to test t
- mray: I don´t think it's perfect but good enough for MVP. Important to stay consistent with what the websites says, it's okay with me
- salt: think this is a good move - we've been in gridlock, stays with the heart of the mechanism while making some improvements that solve some issues. We have a goal that could actually change things, and actually makes sense. 
- davidak: think it's very good: important to me to remain consistent with what we want to have in the end
- msiep: just catching up with the proposal, seems like a good step generally. Thought it was a priority to get a first non-snowdrift project on board, so curious how that affects it. 
- alignwaivers: share concerns of adroit but think iterating an improvement is good
- smichel: sounds good
- wolftune: see some misunderstandings here but it provides a clear enough solutions to some major tensions. Very similar to extra caveats with 'shares' in the beginning. Was hard to explain the kicked out part of the limit. It's going to be more successful this way. reconsidering 6000 instead of 5000
- corepoint: the mechanism has never been announced: this is working toward a *formal* announcement of a mechanism. of 131 current donors, not necessarily all willing to give 6$. The whole idea of budget per project is new, wouldn't affect the current patrons. Looking forward to next steps
- alignwaviers: this is for current patrons?
- wolftune: yes, no change in the patrons, just framing change for the budget description

- call for round to confirm proposal: ACCEPTED 7 yes, 2 nonopinions
- NEXT STEP (): change video: edit to remove the kicked out part of the video
  - https://community.snowdrift.coop/t/update-video-on-website/1643
- NEXT STEP (): update how it works to reflect these updates
- NEXT STEP (): update project landing page p/snowdrift 
- NEXT STEP (): update dashboard for patrons looking at their own pledges
- NEXT STEP (): need to check backend code is working as described
- NEXT STEP (): create/add to alpha milestone
- NEXT STEP (): decide on actual number (5k or 6k) neutrally, regardless of framing for matching dollars vs patrons (at the point matching turns off). 
- NEXT STEP (): describe what we could do with that much money
- NEXT STEP (wolftune): write blog post about this decision
- NEXT STEP (smichel): capture these in gitlab

### OSI ending fiscal sponsorship (wolftune) <!-- Timebox: 3 minutes -->
#### Summary
- We agreed with Elana that OSI 501(c)(3) fiscal sponsorship will end this year 2020
- We do not need to "graduate" in 2020, they will work with us and celebrate graduation when we get to it
- wolftune: wanted to make sure people were clear on this and if anyone had any questions
- incubator projects was a bit in limbo, we need to spend the money this year
- salt; this is a bit of a kick. might want to make some fast decisions as the ability to accept donations as a 501c3, not understanding why we have to spend money before the end of the year
- woltune: the OSI doesn't want to deal with the extra financial stuff in filings etc
- wolftune: in order to get the money from OSI, they need to see a reimbursement request for 501c3 mission aligned
- salt: probably not the only option, but I think this means we need to prioritze becoming a 501c3 or using a fiscal umbrella

- NEXT STEP (): research alternative potential fiscal sponsor/umbrellas (such as SFC)


<!-- X. Report back -->




6. Meeting eval round: feedback, suggestions, appreciations <!-- Timebox: 5 minutes -->
--------------------------------------------------------------------------
- mray: good meeting, especially for keeping an eye on time
- tannerd: seems well put together, seeing the logistical stuff in meeting, understanding the gravity of the project
- wolftune: appreciate iteration on meeting structure, but room to keep improving. Like to be asked questions rather than being read since we all look at screens. But for people passing during rounds maybe that can be in the pad/chat *before* being called. Don't want to be too pedantic with structure, but I love when people have taken the time to structure/summarize the topic before hand, such as Salt's topic
    - I could see some rounds with people volunteering to speak next, and facilitator chooses to step in and/or use baton-passing in order to break the pattern if same people keep speaking first
- Salt: dont want to not have people change things, but tension around changes being made before I was aware of it. Appreciate everyone move this stuff forward. Suggestion: people can call the next person by writing down the next person's name, +1 to the idea of "passing" via writing it by their name. Possible facilitator roll: randomizing list of names for each round and pasting it.
- adroit: thanks to notetakers, thanks everyone for showing up
- smichel: distracted thinking about updates to pad structure and what didn't or did work. Timeboxed a bit to figure out structure (20 minutes) but didn't think it was too bad, as iteration revealed some stuff that might of wasted more time if I were just to think about it on my own. Also distracted since my headphones broke.
- davidak: good meeting, feels like we are finally making some progress, hard to know who to pass in round, would like if someone else would call or speak up
- alignwaivers: good meeting, thanks for everyones input and participation. Good to have diversity of thoughts/perspectives. Hooray for making alpha decision!


7. Informal Discussion <!-- Notes not needed here, only capture topics -->
---------------------------

### 1a. Meeting format discussion (smichel, requests all)
- smichel: MVP mechanism buy-in discussion is "Decisions you'd like input on" round
- smichel: OSI sponsorship is "Progress made this week", should rename to "Updates" or something
    - Salt's "Why does the OSI want sponsorship over?" should be a breakout room created by this update.
- Me giving an update on the updated pad structure should have been an "updates" topic
- GitLab Open Source Program should have been "Decisions you'd like input on"
- smichel: 
- Salt: Would be good to have a "socializing" room or "work" room for folks not implicated in an agenda item
- Salt: We need a report from each breakout during the meeting after each session
- wolftune: I want these breakouts to have formal structure and timers etc. or else they feel like post-meeting open-ended, but there's still a place for after-meeting open-ended, and I want a clear distinction

### 2a. GitLab Open Source Program (smichel, requests all)
- smichel: you apply, if youre accepted they send you an invoice fo $0. That's your contract, to be renewed yearly
  - they give us gold/ultimate tier. and we give them 
  - the ability to use our logo on their website... [LINK]
  - video case study recording
  - customer reference: we have to be ready to give them our user data if requested?
  - (and/or) go to market research materials as requested
- salt: features we don't have otherwise but could be nice: advanced board views, epics
- smichel: not totally clear, I should email them
- alignwaivers: can you also ask if we can keep organizing private repos inside the group?
- davidak: can we expect to renew everytime?
- NEXT STEP (smichel): email gitlab for confirmation on what the contract means, confirm that we keep our private repos

### 3a. OSI sponsorship discussion (wolftune, salt, smichel)
- to be a short meeting probably continued later
- wolftune: there's a lot of questions/limitations. We don't need a formal 501c3 sponsor necessarily
- smichel: OSI is shutting down entire incubator program
- wolftune: we know this is the case, we work on the spending of money going through OSI this year. Questions for Deb is 501c3 for ourselves vs others
- wolftune: Deb has enough experience with 501c3 that she understands better than platform cooperatives
- salt: seems like the move was to put remaining money OSI into a retainer for a lawyer by end of year
- wolftune: serendipitous meeting with Deb that coincided well
- smichel: you mentioned aaron williamson, is there a chance we'd want to use retainer for him?
- wolftune: he's probably one of the best fits except for 501c3 with coop. Overlap with him and Deb is distinct, but we should have an initial discussion at least with him

- NEXT STEPS (smichel and wolftune) : arrange for conversation with Aaron Williamson [DUE: Monday]
- wolftune: was on working group for OSI with Williamson, have interacted a lot with him. can operate as a coop with logistics without formally being a coop - knows the territory to some degree. Probably knows more now about donation/crowdfunding for open source since previously spoke with him.
- smichel: Work with both / put them in contact with one another?
- wolftune: makes sense to broach conversation with Williamson, the dream is he will work with us and can collaborate with Deb (a coop expert, and knows michigan law)

### 4a. How to capture alpha stuff in Gitlab (smichel, requests alignwaivers)
- smichel: all of the issues earlier from new mechanism should each be an issue connected with an epic. 
- smichel: Epic story project and to some extent milestone are all the same words. would make stories on the main repo
- smichel: once epics pages completed, think the roadmap page could be removed
- salt: I disagree, roadmap page could be for external links. Gitlab isn't easily for public consumption
- salt: is there harm in making it one issue?
- smichel: no, and issues can be moved into epics


<!--

    Meeting adjourned

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, settled during agenda confirmation
- at topic beginning, set timer
    - if using shared web timer, share link in chat
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
