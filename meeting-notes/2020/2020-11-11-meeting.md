<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — November 11, 2020
===========================

- Attendees from last week: MSiep, salt, adroit, alignwaivers, smichel, wolftune, mray
- Attendees (current): davidak, wolftune, smichel, alignwaivers, MSiep, adroit, Salt

<!--

    Personal check-in round

    Assign facilitator

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
----------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 2 -> 1
- Posts: 19 -> 14
- DAU/MAU:  41% -> 43%

Snowdrift patrons: 130 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
---------------------------------------------

- msiep: appreciate everyone's roles and participation
- salt: appreciate everyone being here. feedback (for myself), was trying to be looser but couldve been more strict on timing, learning that balance
- mray: good meeting, but sound/technical issues, couldn't hear a lot of what was said
- adroit: noticed the looser facilitation, didn't think was an issue (this time). I think about visual complements, considered building something with mumble integration, but is a big task
- wolftune: challenge with audio/visual (we don't have a great visual). it's sometimes hard to know what others are looking at while speaking. remembering to check the chat / pad more (at salts reminder) is helpful, potentially using vidchat, better cues for subtlety
- smichel: thought we had good discussion, not sure it was a good idea to bring up my topic to full meeting instead of breakout only


3 Last meeting next-steps review
----------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

- NEXT STEP (smichel + adroit): cowork on css [DONE]
- NEXT STEP (align): Nudge Salt about 'burning civi down' [DONE]

### Concerns about stalling out on mechanism decision 
- NEXT STEP (wolftune): propose a decision-making process, alignment with fallback etc
- NEXT STEP (salt): reach out to potential researchers

4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    https://www.online-timers.com/timer-1-minute

    Quick answers:

    What is your current focus?

    What or who could help you make more progress?

    Are you blocked on anything?

    Add  agenda or breakout room topics as needed

-->

- salt: worked with alignwaivers on nonprofit stuff, not anywhere with civi yet
- msiep: nothing in particular, just keeping up
- align: working w/ salt+david on nonprofit+grant writing, also looking at git documentation
- smichel: did some coworking w/ Adroit on css, and codebase: could use help from anyone with *some* css experience willing to put in time to do the work, or anyone with *more* experience to give advice
- adroit: ^
- davidak: focus is still to finish the mechanism, since it blocks communication with public
- wolftune: prepping seagl talk

5 New Agenda
------------------

<!--

    Does anyone need to leave early?

    Confirm agenda order and timeboxes (in 5 minute increments)


### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->

### Concerns about stalling out on mechanism decision  <!-- Timebox: 5 minutes -->
Two next steps, neither captured nor done:
- NEXT STEP (wolftune): propose a decision-making process, alignment with fallback etc
- NEXT STEP (salt): reach out to potential researchers
- salt: I think this was exceptional
- NEXT STEP (alignwaivers): make sure the above two items are captured by next week

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
----------------------
- wolftune: aaron williamson mentioned that producing FLO products is not acceptable for IRS for 501c3
- salt: spoke with professor about non-profit: there were questions he couldn't answer, it seemed like we would be a 501c3 for some reasons. If we were doing lobbying, it will be more  of a 501c4. 501c3 can lobby but not on specific election candidates / specific measures.
- the idea of facilitating money without touching it is a bit convoluted
- wolftune: if we have to focus on the end of the FLO goods, IRS wont accept. Our end is to free people of the tyranny of proprietary stuff



7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------

### Emailing deb
- NEXT STEP (smichel): email deb telling her about our 2020-09-21  board mtg that we'd like to have some progress before

### Moving attendees & open discussion (smichel)
- salt: I dont think open discussion should be at the top
- smichel: what about moving snow shoveling checkins to the end

    - example from today: I wanted to ask wolftune , is there any info needed 

- salt: I understood why you did it there, but kinda goes against use of breakout rooms
- salt: want to keep it at the end because it lends itself to creation of breakout rooms
- smichel: what if to put something on the agenda, you have to put it on in advance. if created during the meeting, could go into a breakout room
- wolftune: for the sake of iterating, we could try it. Those things could happen during snowshoveling round and next steps come up
- salt: researchs suggests you dont take agenda items before the fact
- NEXT STEP (smichel): will update pad will an iteration for this


### NonProfit (Salt, request alignwaivers)
- wolftune: its not enough to be open source to be considered charitable
- wolftune: fundraising is a category of 501c3
- salt: we aren't fundraising per se, we are brokering for donors. There's a term I wanted to research more
- One thing that came out of the conversation: what happens if a project we're sponsoring is a 501c3? There has to be a papertrail if the donations are to be deducatable.
- wolftune: Is it fine not to have a paper trail if the donor just doesn't care about the donation being tax deductible?
- Salt: It is illegal to do that if the money donation is over $250
- salt: if the projects are nonprofit and we're handling money transactions, need to be very careful because there are constraints. if deductable, its a big incentive to be able to donate / deduct more). Want to make sure this is taking account
- salt: this becomes a key question when we continue to get funded ourselves
- an example given was a public/ private partnership. the public was accounting the funds, and paying the private partnership - the government can rarely take an individuals contributions and use it to fund a public project.
- may be worth considering LLC or for-profit because more of a precedent for $ transactions
- smichel: what if we initially decide not to be non-profit, but make it a goal (when we get the resources to make that happen
- salt: a lot of benefits to be a 501c3. there are no owners. determination of business strucure might be done by mission statements
- wolftune: I agree with you completely, the issue is forming a mission statement in the form of 501c3 application. can evaluate from there, is that the mission we wanna do
- salt: agreed that mission statement will help clarify business structure. 501c3 application process has become a lot more streamlined
- smichel: we should figure out how many resources it will take
- salt: we've mostly focused on technical launch of platform for roadmap, but organizational launch is another consideration
- wolftune: past lawyer bowed out of tricky questions like "what if we made a inventory management system a business could use" - excluding such things that don't directly fit with mission statement is possible

- NEXT STEP (smichel): Ask former business law professor if he has any 501c3 specialty contacts

8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------

- msiep: good mtg, thanks for picking up role
- smichel: Super quick. Attendees under metrics? Open discussion first? Asking Aaron "When should we ping deb" should have been in showshoveling checkin.
- adroit: was listening to conversations, forgot about the 2nd timer
- davidak: short meeting, nice to meet everyone
- wolftune: pretty good meeting, awkward for me (because out and about not at computer). Glad everyone else is keeping up pace
- salt: nice to have short meeting, sorry for being late. Suggestion: to reset colors
- align: My bad on resetting colors. Glad we kept the meeting short

<!--

    Who will clean and save notes and then update pad?

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, settled during agenda confirmation
- at topic beginning, set timer
    - if using shared web timer, share link in chat
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->
