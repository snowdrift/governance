<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — October 28, 2020
===========================

- Attendees from last week: smichel, Salt, davidak,mray, alignwaivers, wolftune, Adroit, MSiep (late)
- Attendees (current): MSiep, smichel, Salt,mray

<!--

    Personal check-in round

    Assign facilitator

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
----------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 7 -> 3
- Posts: 131 -> 16
- DAU/MAU:  47% -> 40%

Snowdrift patrons: 128 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
---------------------------------------------

- alignwaivers: differentiate ascii art normal and handraise. 
  - postmeeting: how do we have parallel breakout rooms (text) chats, bbb?
  - also postmeeting: can we suggest pre-filling out shoveling round when possible? seems like an ideal workflow
- mray: overall good meeting, thanks for everyone to be here
- wolftune: happy that conversations and progress happening (regardless of my involvement)
- smichel: Felt I was not particularly prepared for leading the roadmap topic. Add check-in questions about: any information that would be helpful? Any team member who would be better suited to do a task you're currently doing (or just tasks you'd appreciate someone else taking)? 
- msiep: ^_^ thanks to everyone for roles and making it work
- davidak: good meeting, nice to see everyone and glad things are active
- adroit: nice and short, good to see all again
- salt: great to see everyone, thanks


3 Last meeting next-steps review
----------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

### Select new meeting time(s) 
- <https://community.snowdrift.coop/t/team-contact-info-availability-and-weekly-meeting-details/1345>
- NEXT STEP (alignwaivers): Update forum topic [done]
- NEXT STEP (smichel): Update shared calendar [done]

### Roadmap updates
- NEXT STEP (smichel): post on forum asking about css organization, tag Salt & adroit [done]



4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    https://www.online-timers.com/timer-1-minute

    Quick answers:

    What is your current focus?

    What or who could help you make more progress?

    Are you blocked on anything?

    Add  agenda or breakout room topics as needed

-->

- wolftune: talks coming up that need prep, bylaws
- smichel: bylaws, a little blocked on css updates but have a path forward (stop worrying about classes used on multiple pages)
- Salt: ready to give up on preserving civi reconfiguration, but no bandwidth to re-do it. Also thinking about bylaws.
- Adroit: catching up on forum discussions, ~halfway through
- mray: not much on my plate, "blocked" on mechanism discussion conclusion
- davidak: focused on mechanism, but think I've said everything I can. Not sure how to find consensus.
- msiep: focused on forum discussion.

5 New Agenda
------------------

<!--

    Does anyone need to leave early?

    Confirm agenda order and timeboxes (in 5 minute increments)


### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->

### Libreplanet 2021 CSF (Salt) <!-- Timebox: 1 minutes -->
- submissions due Nov 11th
- it's all virtual, might be a small in-person event
- Salt: I'm planning to submit
- NEXT STEP (wolftune): submit a LP proposal

### Concerns about stalling out (Salt) <!-- Timebox: 5 minutes -->
- Salt: forum was very active and passionate, but I think we are edging near a trouble area and want to bring that up before reaching it
- Salt: I think it's a combination of catching up on reading & people not knowing how to respond
- Salt: Risk of non-response going on too long
[some talk I missed about modeling]
- wolftune: I think it's important to have a fallback defined, so if we can't achieve alignment -- our goal -- then we don't get stuck making no progress
- NEXT STEP (wolftune): propose a decision-making process, alignment with fallback etc

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
----------------------





7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------

### By-laws suggestions (Salt, requesting at least wolftune)
- I heard there was pushback about required financial giving by the board, smichel suggested a breakout room would be valuable.

8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------

- msiep: good meeting
- mray: thanks for everyone doing what they're doing
- davidak: not sure if we have a solution, good to talk about it
- Adroit: enjoyed the meeting, keep an eye on the forum for my responses
- smichel: good mtg, but feel I wasn't a good note-taker, tired & distracted between note-taking and listening/engaging to the conversation
- wolftune: glad I was able to make it, happy that momentum has not stalled as much as in the past when I've been busy like this
- Salt: glad we had a bit more stalling-out discussion in open discussion. I think we can shorten the snow-shoveling check-in.

<!--

    Who will clean and save notes and then update pad?

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, settled during agenda confirmation
- at topic beginning, set timer
    - if using shared web timer, share link in chat
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->