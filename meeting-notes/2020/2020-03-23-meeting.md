<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below (select the whole line and drag to your bookmarks bar). You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
 
# Meeting — March 23, 2020
 
Attendees from last week: msiep, smichel, wolftune, iko, Salt
Attendees (current): smichel, Salt, wolftune, MSiep, Adroit, alignwaivers

<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 0
- New Topics: 1 -> 1
- Posts: 5 -> 7
- DAU/MAU:  31% -> 34%

Snowdrift patrons: 118 <!-- to be reviewed occasionally from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!
- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/
 
### Use chat in etherpad (and add your name)

### Conversation queuing
- three discussion mechanisms: hand symbols (below), call for a round (and vary the order), open discussion
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on

 
### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback

- smichel17: overall good meeting, coworking at the end felt unnecessary to be within formal meeting; Feel like goodbye round could be less moderated, more natural/freeform
- smichel: I like the one-minute and tension-bringing-up at the beginning, so much that I almost feel like I want the meeting to be *entirely* stuff like that and leave the "topical" type stuff we've been doing for forums (or for dedicated tactical meetings with appropriate people)
- wolftune: lots of thanks, all have good tools but don't need to use pedantically, good balance

## Last meeting next-steps review

### Wiki link in footer?
- NEXT STEPS (msiep): look into adding links to wiki and gitlab in the footer

### OSI incubation and Board (wolftune)
- wolftune: have some emails, but goals should be better captured by the end of the day

### Libreplanet recap/reflection
- NEXT STEP (wolftune): prepare blog post summarizing how it went from our view and what got discussed about Snowdrift.coop

### Volunteer form
- NEXT STEP (Salt): ping OSU, push this live [DONE]
- NEXT STEP (Salt, msiep): Post any bugs to gitlab issue


<!-- One minute silence, check with ourselves mentally and personal notes/tasks/emails to surface any tensions, add agenda if appropriate -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->


## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->


<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->

### Coworking (wolftune)
- wolftune: Trouble figuring out my own schedule, would like to discuss explicit plans for coworking rather than just spontaneous
- wolftune: hard for me to commit to any specific time, but have to start somewhere…
- smichel: I have a calendar appointment, this week haven't felt up
- Salt: school spring break here, not quite ready to do more snowdrift work, but getting tasks more organized
- wolftune: Would be nice to get back to where we were making some progress every day, would like to figure out exactly what the blockers are.
- NEXT STEP (smichel): Check in regardless of interest in coworking that particular day

### Libreplanet recap (wolftune)
- wolftune: Doubting whether a libreplanet follow-up blog is important.
- Salt: My understanding was that you would make a framework that smichel & I would fill out
- wolftune: Still wondering if it's a priority.
- Salt: I think so.
- wolftune: What about a short forum post instead? Even just a comment on the existing one.
- Salt: Seems reasonable, I'd still like a few questions to answer.
- NEXT STEP (smichel): Post a short reply on the libreplanet event post on  the forum <https://community.snowdrift.coop/t/libreplanet-online-march-14-15-2020/1466/8>

### CiviCRM (salt)
- https://contacts.snowdrift.coop/volunteer is up
- CiviCRM is up too
- wolftune: I have a backlog of emails, etc, that can go in
- wolftune, smichel, Salt: We have business cards to enter
- Salt: Need to make sure we enter them the right way, make sure we've checked in with them so we're not holding any info of people who don't want their info in db
- wolftune: generic/customized form?
- msiep: Looking at wireframes for this… originally I had a different form for new volunteers vs editing own profile.
- msiep: is the main form all that's set up so far?
- Salt: The 3-stage form is implementing and basically working, but not connected to civi.
- Salt: What's missing is the ability to query civi to make sure we're editing the right user. Civi not fully set up yet.
- msiep: is there a way for me to see the other forms yet?
- Salt: not yet, since they're not world-accessible (without the piping from civi they're basically blank forms)
- msiep: I was trying to get to the question of the log-in page — making sure people creating a profile don't think they have to log in.
- NEXT STEP (Adroit): Test a submission of the volunteer form <https://contacts.snowdrift.coop/volunteer> Done!
- NEXT STEP (salt): Test import of old data

### Nextcloud (Salt)
- Salt: We'd talked about having a shared calendar, on Nextcloud
- smichel: I have sysadmin stuff for my instance to do before I could add accounts
- need a list of people who want accounts
- msiep: is this about shared calendar to subscribe to?
- NEXT STEP (smichel): make forum thread, ping @team, ask who wants an account

### OSUOSL community@ forwarding (smichel)
- smichel: Some emails to community@ were not getting forwarded to smichel. He has asked OSU about this. Wondering where to capture this - ops repo?
- salt: What are you capturing?
- smichel: The fact that I have to send them an email. This is related to what we discussed last week about personal vs snowdrift to do lists.
- salt: Correct place is the ops repo. If it's quick enough (2-3 min) to not require capturing (per GTD principles) then ....?
- NEXT STEP (smichel): open ops issue

<!-- Open discussion? ~5min. if time -->




---
 
## meeting evaluation / feedback / suggestions / appreciations round

- could just read verbatim points when facilitator not focused well on a step in the meeting


<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->