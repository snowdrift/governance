<!--

    Previous weeks' meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/

    Meeting location: Mumble server: snowdrift.sylphs.net port 8008 (half-duplex mode on mobile: avoids echo)

    Alternates for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift

    See end of this pad for meeting-practices notes and reference

-->
 
Meeting — October 7th, 2020
===========================

- Attendees from last week:  smichel, Salt, MSiep, wolftune, mray, davidak, alignwaivers
- Attendees (current): Salt, mray, wolftune, smichel, MSiep

<!--

    Personal check-in round

    Assign facilitator

    Assign note taker

    Assign time keeper, use https://online-timers.com, start for each item, paste each url in the chat

-->

1 Metrics
----------

Discourse (over past week): <!-- from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 1 -> 1
- New Topics: 4 -> 9
- Posts: 26 -> 70
- DAU/MAU:  44% -> 52%

Snowdrift patrons: 127 <!-- from https://snowdrift.coop/p/snowdrift -->

2 Review previous meeting feedback
---------------------------------------------


- smichel: Lots of attendees = long rounds (check-in and agenda). Not sure what to do about it, maybe just people try to speak in shorter chunks
- wolftune: thanks for facilitation Salt: facilitation feedback is coming to mind (perhaps to explicitly call for feedback). Having things clearer would be helpful
    - smichel: Instead of asking for permission, "Not sure if I should do this, stop me…"
- Salt: think it went pretty well, thanks to everyone for being here. Feedback from myself: had 10-15 people in a meeting, with that number I have to be more forceful with facilitation in order to move forward with the queue etc.
    - smichel: mumble has "priority speaker" (everyone else gets quieter when they talk), could use that in these situations
- mray: thanks for facilitation. Was looking forward to talking with msiep, pity the time is up.
- MSiep: thought  it went well, appreciate notetaking and timekeeping, glad we got a somewhat extensive discussion on this
- davidak: glad we got consensus and are moving in the right direction



3 Last meeting next-steps review
----------------------------------------
<!-- Mark NEXT STEPs as [DONE] or [CAPTURED] <LINK> by the next meeting -->

### Changing Meeting Times (Salt)
- NEXT STEP (): decide on new time by Oct 14
- NEXT STEP (everyone): update availability by Oct 14
- NEXT STEP (smichel): Remind people ^


### New approach to crowdmatching: Next steps (MSiep)
- NEXT STEP (wolftune): post on the forum (that the team has agreed) on this and more discussion [DONE] <https://community.snowdrift.coop/t/options-for-next-step-for-new-approach-to-crowdmatching/1608/>



4 Snow-shoveling check-in round
-----------------------------------------
<!--

    One minute silence — check thoughts and notes/tasks/emails to surface tensions

    Quick answers:

    What is your current focus?

    Are you blocked on anything?

    What help could you use?

    Add to agenda or breakout room topics as needed

-->

- wolftune: engaging on forum, finishing bylaws; not blocked; no help needed
- Salt: feeling unfocused, between seaGL & civi state; need to respond to lance on GitHub; could use coworking time
- msiep: partially caught up on forum, not much activity
- smichel: focused bylaws when time permits (this week); not blocked except on finishing paid gig
- mray: thinking about mechanism evolution & possible brand changes/evolution; don't need help

5 New Agenda
------------------

<!--

    Does anyone need to leave early?

    Confirm agenda order and timeboxes (in 5 minute increments)


### TOPIC TEMPLATE (LEADER) <!-- Timebox: # minutes -->
<!-- -->

### Q4 Roadmap and pivot  (wolftune) <!-- Timebox: 10 minutes -->
Goal: decide on next steps for finishing the Q4 roadmap (assign to people, consensus on goals for it)

- Roadmap needs to be simpler, adjusted to our capacity, GitLab for real work
- https://community.snowdrift.coop/t/roadmap-format-reflection/1621
- wolftune: tension in forum post: roadmap for high-level & publicity, not day-to-day work tracking
    - tension even around *assigning* things on the roadmap, seems like that's for gitlab
    - roadmap is getting long/bulky; didn't get value out of it to match effort put in
- smichel: tension: big process each phrase when nothing has changed — shouldn't re-do it each quarter if not much has changed
- wolftune: how much are you suggesting format changes to roadmap?
- smichel: re-evaluate when reaching goals, not phases
- wofltune: get rid of phase timelines? Just phases without dates?
- mray: progress itself prompts more progress, so hard to predict/map given all the interactions
- wolftune: purpose of roadmap is to communicate the map: what will it take to get launched?
- wolftune: We should probably remove things not achieving that goal
- mray: any other goals seem dwarfed by goal of getting active contributors
- wolftune: proposed roadmap goal: communicate what's next
- wolftune: did we spend too long trying to make the roadmap useful for defining work?
- Salt: hard to answer that esp. given we were creating the process
- Salt: Any benefit to visual roadmap? Like mentioned on <https://community.snowdrift.coop/t/roadmap-format-reflection/1621/2>
- mray: what area I work on seems dependent on where I'm *able* to make progress (based on project progress overall)
- wolftune: propose coworking on this, make a proposal for next week
- Salt: I do think quarters are a valuable grouping
- smichel: I think we can continue on the forum
- NEXT STEP (wolftune+smichel): schedule cowork on updated format proposal and post to forum as appropripate

---

<!-- If anyone arrived late, their snowshoveling checkins here -->

6 Open discussion <!-- if time, timebox: 5 minutes -->
----------------------
- msiep: with new crowdmatching approach, for getting started, one option instead of building a crowd from scratch is to discuss with specific projects about the possibility of inviting an existing crowd to convert to crowdmatching


7 Breakout rooms <!-- Decide on topics, list, and invite specific participants; actual discussion later -->
-----------------------




8 Meeting eval round: feedback, suggestions, appreciations <!-- Reserves last 5 minutes -->
-------------------------------------------------------------------------

- msiep: d
- Salt: d
- mray: thanks for notes, facilitation
- smichel: note taking was awkward, hard to have conversation between two people taking their own notes
- wolftune: lower attendance then recently. Might it be useful to have more structured feedback round prompt? eg, "one thing that was good, that we should keep, and one thing that could be improved", also maybe the option to defer in rounds like "come back to me" where someone's thoughts may be sparked hearing others.

<!--

    Who will clean and save notes and then update pad?

    Standup portion of meeting adjourned!

    People are free to go if not staying for breakout sessions

    Clean up meeting notes, then add to wiki

    Prepare this pad for next meeting:

    Update next meetings date

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader) with the NEXT STEPs to review

    unbold the topic headers

    Open discussion notes

    Clear authorship colors

    Breakout room discussion

    Use multiple mumble channels for parallel discussions, if necessary

    Don't worry about notes, but do capture next steps, if needed.

    Update notes in wiki if needed and clean up breakout room section

-->


<!--
Meeting best-practices and tools

Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Agenda topics
- Each topic facilitated by topic lead with main facilitator help
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting

NEXT STEPS
- each topic should capture NEXT STEPS (or clarify that there aren't any)
- should be clear and actionable
- assignee agrees to capture or complete by next week

Timeboxing
- timebox each topic, settled during agenda confirmation
- at topic beginning, set timer
    - if using shared web timer, share link in chat
- at timebox end, facilitator may choose to extend by a specific amount
    - informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Discussion options
- open discussion
- call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- aim for shorthand / summary / key points (not transcript)
- don't type on the same line as another notetaker ( ok to do helpful edits after but not interfering with current typing)
-->