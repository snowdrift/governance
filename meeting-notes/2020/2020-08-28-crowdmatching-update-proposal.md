# Solidifying/iterating crowdmatching

## Presentation (meta)

- smichel: I think when we present this, it might make sense to give a *short* (1-2 sentence) overview of the proposal first, as an anchor, then drivers, then details of the proposal.
- I think we should get (fully) to alpha before we *decide* on this change (and maybe even before we pitch it to the wider team)

"matching factor" (we aren't consistent on terminology here, but that's good example)


## Drivers

1. **Patrons have different relation to money**
    - Richer and poorer people (and other aspects like higher or lower expenses, frugal etc)
    - We don't want people excluded if the pledge is too high
2. **Different relation to project**
    - e.g. someone who uses a software professionally depend more on it than occasional user
3. **Patrons need control over their budget limits**
    - Want to avoid having to kick people out (due to pledge level exceeding their limit)
4. We want as many people to be able to participate as possible
    - ?: Are there people we lose because they don't like our limited IN/OUT choice and want more options?
5. We want an egalitarian culture
6. **We want to *influence* people to care about growing the crowd** and to envision greater possibility than we see with existing fundraising
7. **We don't want people to counter the matching**
8. **Simple explanations and UX**
    - Crowdmatching and the choice to pledge should seem familiar, similar enough to existing pledge experiences
    - People should understand the situation with a small crowd (focus on crowd-building) and not think that it's wrong or bad for the initial funding to be low
    - The UX should be simple enough to not cause decision-paralysis etc.
    - Clear about how to change pledges over time and that projects show transparent progress, accountability
9. Projects can do goal-setting, having an idea of what counts as "success"


## Proposal

1. **Project sets an (ambitious?) goal**
2. **Patrons set a budget they're willing to make available toward that goal**
3. **Patrons give the same percent of their budget as the crowd's budget-total is a percentage of the project goal**

![](/assets/mechanism/2020-08-Crowdmatching-proposal.png)

- When the project reaches/exceeds their goal (several options):
    - Patrons stay giving their maximum (further donations are simple, non-matched, but if it dips below 100%, crowdmatching resumes)
    - Or crowdmatching continues (but is opt-in, so some patrons are matching farther, some stagnant)
    - Or project sets an additional stretch goal (probably patrons then opt-in to participating in that, like a separate crowd and patrons set a different budget for the stretch goal)
    - Or funding stays at 100% and the burden spreads with patrons going below their budget

- Any patron could change their budget at any time
    - We could limit this mechanism by fixing in place few or no choices

- Can projects change their goals? When?
    - if the project changes automatically scaled the patrons' budgets (and only downward), it couldn't be gamed to get the project more money. They could always *ask* patrons to increase their now-scaled-down budgets.
    - if the project wanted to scale *up* their goal, they'd lose money because we wouldn't automatically scale-up the patron budgets, but they could invite patrons to change.

Extra discussion:

Mray: extra option to consider: if we get to 100% goal for the crowd *now* anyone could give extra at a higher level than allowed before

MSiep: we don't need to make it public how much people give, just how many people are in the crowd, and the total pledged. We could also choose to report and emphasize the median pledge instead of the mean, so that a minority of patrons with very high pledges wouldn't affect what we presented as middle-of-the-range, as part of a high-level characterization of the crowd.

MSiep: We could also limit pledge within a range (not saying we should, just that we could)

David: pledge levels can be somewhat arbitrary

smichel: We could highlight median instead of mean, Could even only match up to the median: Had an idea of a slider of a dollar amount per patron, and always going to give X times the avg/median donation

Wolftune: the very **simplest version of this, as close as possible to current live alpha** would be: Instead of patrons having a system-wide budget (currently fixed at $10), we'd set a goal for the snowdrift project (say $25,000/mo) and fix patron budgets for the project at $5 each (we could pick a different goal if we simply match the budgets so it still works out to $1 per 1,000 patrons for now). That way, the existing pledges would be unchanged. That works as the absolute tiniest MVP. The fact that this new proposal is so *compatible* with the existing crowdmatching is nice. **The next simplest case for multiple projects is allowing each project to set a fixed goal and a fixed per-patron budget where patrons are simply in or out** (each project doesn't need to use the same matching-factor then). The next step would be to allow patrons to set different budgets. Finally, we could later think through the options for multiple goal-levels (stretch goals, separate crowds etc) and the complexities around projects changing their goals over time. So, we don't need to open up all the complexity right away.

closing round:

- wolftune: Overall feel good, especially compatibility with current system, and *especially* with limit interaction — nobody kicked out and no issue of having the pretense of matching undermined when someone joins but that kicks out a different person. Goal-setting, seems like it still works as long as the goal is *ambitious enough*. Removes marketing need to focus on the budget so much.

- mray:  like this proposal in general, new ways to approach explanation, framing and ux. Concerned about the  "flexibility can" but curious what is going to come out of it in form of a finalized version of a new mechanism.

 - davidak: looks good, would like to see an example, would like to have some flexibility (select $5, $10, or "other").

- Salt: glad this is discussed; excited, seems like good changes; appreciate David's thoughts/insights & new faces are nice to see. The issue of sub-projects as an option as well as maybe reduced frequency etc. from limits page still applies here

- alignwaivers: +1 to direction & new faces. Especially since getting kicked out is problematic for fluxuations in donations as well. Thanks wolftune for facilitation.

- msiep: +1 thanks for David to join, pleased at how many people joined.

- smichel: +1 what others said, thanks

Addendum notes:

- alignwaivers: curious how the growth rates will differ based on estimating high or low? seems like high estimates will mean less money sooner?
- wolftune: if project has high goals with high expectations, patrons might budget more in what they are willing to do as part of reaching that level of success, thus resulting in similar growth rate… but this needs real-world testing

## Extra notes on options

In the unusual case of projects that really cannot or will not make good use of funding less than some threshold, they could still set a goal well beyond that threshold. The pledges would be in relation to the higher goal, but lower threshold would be the point at which actual donations become active. Below that, pledges would not actually get processed each month.
