<!-- See end of this pad for location, meeting-practices notes and reference -->

Meeting — December 30, 2020
=======================

Personal Check-In Round
------------------------------
<!-- What's on your mind (besides Snowdrift)?
<!-- Add your name to attendees / leaving early / roles
<!-->

Meta
------

- Attendees: wolftune, Salt, davidak, smichel, alignwaivers
- Leaving early: 

- Facilitator: Salt
- Note Taker: alignwaivers
    - Note-saver/pad-updater (if different):

### Metrics

Attendees (last week):  Salt, MSiep, wolftune, alignwaivers, Tannerd

<!-- from https://community.snowdrift.coop/admin?period=weekly -->
#### Discourse (over past week):
- Signups: 0 -> 0
- New Topics: 2 -> 1
- Posts: 5 -> 2
- DAU/MAU:  2% -> 23%

<!-- from https://snowdrift.coop/p/snowdrift -->
#### Snowdrift patrons: 132

### Previous Meeting Feedback

- I'm still unclear what to take notes on, or not. Good to see everyone, went smoothly.
- Original concept of coworking seems to be lost; mistake to run mondays like meetings & use the pad.  
- Felt too much like a meeting, timeboxes weren't value - too much meta talk but relatively reasonable. Task switching overhead was significant.
- has always felt like 2 meetings per week, not much different. Made some progress. Care about fsf submission, would be great not to submit at the last minute.
-  agree it felt like a meeting, and felt disappointed to not get in touch with people on a discussion basis which I was hoping for (as forum doesn't work great for that either). thanks to everyone for showing up.
-   Making rooms and whatnot is a lot of overhead. I do not like using mondays for discussion or major updates. Happier to use mondays to put together quick whole-team presentations on Wednesday, but trying to do a whole-team presentation ad hoc is a waste of time.


Silent Centering
-------------------
<!-- Timebox: https://www.online-timers.com/timer-2-minutes
<!-- Prepare for Snow-Shoveling Discussion — gather things you'll want to talk about. Check:

    GitLab: https://gitlab.com/groups/snowdrift/-/issues

    Forum: https://community.snowdrift.coop/

    NEXT STEPs — marked as [DONE|CAPTURED] <LINK>

    Personal notes/tasks/emails

<!-->

Snow-Shoveling Discussion
--------------------------------
<!-- Timebox: https://www.online-timers.com/timer-8-minutes
<!-- Share progress updates, tensions, blockers; ask questions, etc. Add breakout rooms if needed.
<!-->

### Meeting/pad updates (smichel)
- Cleaned up comments even further
- Breakout room template should be filled out during *this* round (Snow-Shoveling Discussion)
    - Any template that's *not* filled out by the end automatically goes last in breakout room order

### 501 and legal structures
- NEXT STEP (Salt + wolftune): organizing planning what *are* the next steps
- NEXT STEP (wolftune): update Board about legal incorporation ideas (consider asynch audio)
- NEXT STEP (Salt): setup co-working time to work on mission statement with wolftune, smichel, alignwaivers, mray, and anyone else interested
- NEXT STEP (Salt): write discourse post about scheduling an all-day retreat in Team section

### capturing decisions reference in GitLab, issue use
- NEXT STEPS (smichel): open epics, update roadmap to link to epics, tour people on how to use well
- NEXT STEP (Salt): Explain what adroit did to wolftune+smichel [DONE]
- NEXT STEP (Salt, wolftune, smichel?): Chat about spreadsheet [DONE]
- NEXT STEP (wolftune): Enter contacts into spreadsheet
- NEXT STEP (alignwaivers): finish integrating gitlab (issue) roles info [DONE]
- NEXT STEP (smichel): email Salt+Lance to set up a conversation

### geek beacon festival
- NEXT STEP (salt): respond to gbfest email [captured by salt but where?]

### FSF HPP (High Priority Projects)
- NEXT STEP (wolftune): work on initial draft [due: 2021-01-03]

    - https://snowdrift.sylphs.net/pad/p/FSF_HPP


### Anything else
- smichel:
    - Some discourse to catch up on, trying to get through work so I can get back to snowdrift
    - I know Salt availability is higher, until when?
- wolftune: new public festival, annual report due to OSI (ideal if on time but okay if in 2021)

### Round (anyone who hasn't spoken yet)


### Choose order and rooms


Breakout Rooms
---------------------
<!--
### TOPIC (LEADER) <# minutes> Room 1A
- Goal: 
- Participants: 

#### Discussion (notes)
- 
<!-->

### Bylaws/org coordination (smichel) <5 minutes>
- Goal: We all have different availability. Figuring out how to coordinate / who does what, so we can continue making progress on bylaws.
- Participants: Salt, wolftune, alignwaivers

#### Discussion (notes)
- smichel: tension around what to focus on and when ??
- wolftune: we discussed async audio (voice messages) - anything thats different than read text online
- salt: need 15 - 20 to discuss inverse-of-open-collective idea
- wolftune: I think that's a prereq to the mission statement - structure is prereq to mission 
- next step (salt, smichel, wolftune): have meeting to discuss this in next week

### Upcoming Events (Salt) <5 minutes> 
- Goal: Discussing New_ Public Festival and (replying) geekbeacon fest. Follow up with salts reply to geekbeacon fest, and figure out how much energy to put into participating in such events 
- Participants: wolftune, alignwaivers

#### Discussion (notes)
- wolftune: I have the impression NPF https://newpublic.org/festival will yield great connections
- davidak: i think we should focus on getting launched
- smichel: if the people participating aren't preoccupied with other tasks, perhaps they could represent snowdrift
- NEXT STEPS (salt, wolftune, mray): discuss new public festival

### TODOs under Capturing NEXT STEPS in gitlab issues (Salt) <5 minutes> 
- Goal: This is a mess, what do we do
- Participants: wolftune, alignwaivers

#### Discussion (notes)
- salt: this is an issue, next steps aren't getting captured or carried over.  we need to discuss this 
- wolftune: discussions should have decisions, think it's the wrong word here. I think the purpose of the meeting is accountability but also decision making
- salt: was a decision made bc of civi stallout to keep todos here
- wolftune: would like to discuss this at some point and renegotiate the agreement
- wolftune: i propose we get meetings done as fast as possible and people are individaull
- NEXT STEP (alignwaivers): come up with tag / label / 3rd state - this next step being 'nudged' 
- smichel: have some pushback on this, think this is all bc gitlab isn't up to par yet
- wolftune: this is duct tape for now

### Mechanism (mray) <# minutes> 
- Goal: How do we plan to make progress?
- Participants: alignwaivers, wolftune, davidak, Salt

#### Discussion (notes)
- salt: everyone has stopped talking about a lot of these things, not knowing where to continue
- salt: which was why i was suggesting a 'retreat'
- wolftune: do think it needs dedicated time and structure to move forward
- wolftune: with FSF proposal there's framing: each patron donates *proportionally* to the end goal (increasing with more patrons).
- wolftune: if we had a map first we could chunk parts of it (and not do in one block)
- wolftune: I had a game design idea that could be visually represented
- salt: we are either meta or super in the weeds like about game, don't want this to be what we work on - the idea is to spend time figuring this out directly which can't be done easily asynchronous
- alignwaivers: i support this idea
- mray: whatever it takes
- NEXT STEP (salt): schedule a collective workshop/s
- NEXT STEP (salt, alignwaivers): prep structure/outline for workshops
- smichel: we could just do this in a meeting
- salt: we've tried that but it didn't work

Wrap-Up
-----------
<!-- Timebox: https://www.online-timers.com/timer-10-minutes -->

### Report back: Summarize conversations, add notes if needed

### Meeting eval round: feedback, suggestions, appreciations
- alignwaivers: disconnected from the pad a bunch, too much iterating not enough space, notetaker needs a heads up on changes
- mray: appreciate efforts, but too much meta talk - i'm feeling more and more disconnected, get the sense its for people who organize the meeting but not valuable for participants
- davidak: in general these meetings feel very formal. In other meetings where we have two or 3 headings in the notes there is time to casually talk about things. There's not a single second for this, every moment is calculated (there is 22 headings, this is blown out). Wish it would be more human instead of looking at the pad mainly
- wolftune: we all want to shovel snow, we dont all have full time to dedicated - one hour on one day we all congregate at snowshovel, and there's so much work to be done and silent centering but wait you have to do a bunch of stuff done while centering and it's a symptom of too much work to be done in an hour. setting up the entire structure of the meeting and discussing at the same time isn't realistic. I dont want everything little thing I say to be a breakout room. ends up being only 15 minutes or work in that hour.
  - erring on the side of not doing too much is ideal: quality over quantity
- mray: agree, breakout rooms take out too much vertical space on the pad
- smichel: agree with what people have said - we all have the same goal. Want to create a pad that mirrors what we are doing as in an human. the big problem revolves around breakout rooms - spent 15 minutes on meta before. perhaps a default is all discussions happen in main room, breakouts only necessitated as they arise. 
    - Breakout room creation should happen *during snow-shoveling discussion.* Would like note-taker to create breakout rooms based on discussion.
    - We need to figure out how to do parallel rooms WAY faster. 15 minutes of meta discussion in the middle of the meeting.
    - @Salt want to push back on "X and Y talk outside the meeting" — that's what the meeting is for!
- salt: think the pad has gone in a better direction in many ways. maybe if I didn't explain it would be better but I have a tendency to overexplain so sorry about that. One of the things we do at the meeting *is* refine the meeting.  Been some setbacks (today was one) but think we have a postive trajectory. Valuable feedback that it's taking up too much vertical space on the topics.  This meeting has reinforced my initial pushback on breakouts. thought we had a great snowshoveling discussion. Even with higher tensions it went smoothly. I am pro 'coming back from breakouts' but the time in between and sort the breakouts is too much: don't see a way of solving *unless* we were really using gitlab we could have a meeting board and sorted on a kanban and could be looked at beforehand. an issue that we don't know what we are talking about until the meeting. The open discussion / standup round can have items that 'need to be gotten into' but don't need to be topics. as a working meeting it's terrible. as a meeting meeting it's good. hoping gitlab will fix a lot of this


<!--
Meeting adjourned / Goodbyes
--------------------------------------

    Clean up meeting notes

    Add to wiki: https://wiki.snowdrift.coop/resources/meetings/2020/

    Prepare this pad for next meeting:

    Move new attendee list and metrics to previous, leave new blank

    Replace previous meeting eval notes with new, and filter it down to essential points

    Clear the "last meeting next-steps review" section

    Move new NEXT STEPs to that review section and then clear out other notes areas:

    The snowshoveling check-in notes

    Agenda items

    keeping the topic title (but not topic leader/time) with the NEXT STEPs to review

    unbold the topic headers and add one # mark

    Open discussion notes

    Clear authorship colors

    Update next meetings date


Meeting best-practices and tools
----------------------------------------

Location

    Mumble server: snowdrift.sylphs.net port 8008 (mobile: half-duplex mode avoids echo)

    and/or for video/screenshare: https://bigblue.cccfr.de/b/mra-qy2-x6n or https://meet.jit.si/snowdrift


Etherpad use
- Use chat in etherpad (and add your name)
- Option: audio notifications on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/

Snow-Shoveling Discussion
- Rule of thumb: If anyone needs to speak twice (ie, if there is any back-and-forth), use a breakout room.
- Make sure NEXT STEPs are done or captured, marked with [DONE|CAPTURED] <LINK>

Agenda topics / Breakout Rooms
- As needed, ping !folks on matrix to read over anything advance, ideally before the day of the meeting
- Each topic facilitated by topic lead (?)
- Timeboxes: prefer 5 minute increments. Leave time for Wrap-Up.
- Rooms: First topic is 1A. Letter = room; parallel discussions get the same number.
    - Any template that's not filled out all the way — that topic goes last


NEXT STEPS
- Each topic should capture NEXT STEPS (or clarify that there aren't any)
- Should be clear and actionable
- Assignee agrees to capture or complete by next week

Timeboxing
- Timebox each topic, settled during agenda confirmation
- At timebox end, facilitator may choose to extend by a specific amount
    - Informal "thumb polls" inform the decision for extra timebox
        - "^" approve, extend the timebox
        - "v" disagree, move onto the next topic
        - "." neutral

Timekeeper
- Use https://online-timers.com, start for each item, paste each url in the chat

Discussion options
- Open discussion
- Call for a round ("pass the mic" style, facilitator makes sure no one is skipped)
- Hand symbols
    - "o/" or "/" means you have something to say and puts you in the queue
    - "c/" or "?" means you have a clarifying question and jumps you to the top of the queue
    - "d" means thumbs up, encouragement, agreement, etc.
    - ">" means you understand someone's point, please move on
    - "d>" indicates feeling complete on an agenda item, ready to move on to

Notetaking
- "???" in notes means something missed, please help capturing what was said
- Aim for shorthand / summary / key points (not transcript)
- Don't type on the same line as another notetaker (ok to do helpful edits after but not interfering with current typing)
    - The "suggest changes" feature can help make edits without interfering
<!---->
