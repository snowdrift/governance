<!-- Previous week's meeting notes: https://wiki.snowdrift.coop/resources/meetings/2020/ -->
<!-- Meeting location: Mumble server: snowdrift.sylphs.net port 8008; backup option for video and screenshare: https://meet.jit.si/snowdrift -->
<!-- Group chat usage on bottom right of this page, please update username and choose color in the top right of this page -->
<!-- Bookmarklet to make the chat bar wider. Create a new bookmark with the below (select the whole line and drag to your bookmarks bar). You can adjust the width by changing "280" to whatever you want, in pixels.
javascript:(function () { const width='280'; const box = document.querySelector('div#chatbox'); if (box) { box.style.cssText=box.style.cssText+' width: '+width+'px !important;'; } const pad = document.querySelector('iframe').contentWindow.document.querySelector('iframe').contentWindow.document.querySelector('body#innerdocbody.innerdocbody'); if (pad) { pad.style.width=(document.body.clientWidth-width-50)+"px"; } })();
-->
 
# Meeting — March 30, 2020
 
Attendees from last week: smichel, Salt, wolftune, MSiep, Adroit, alignwaivers, mray
Attendees (current): Adroit, mray, msiep, smichel, wolftune

<!-- Check-in round -->
 
<!-- Assign note taker -->
 
## Metrics
 
Discourse (over past week): <!-- stats from https://community.snowdrift.coop/admin?period=weekly -->
 
- Signups: 0 -> 1
- New Topics: 1 -> 2
- Posts: 7 -> 10
- DAU/MAU:  34% -> 38%

Snowdrift patrons: 118 <!-- from https://snowdrift.coop/p/snowdrift -->
 
## Reminders on best-practice meeting habits
- Review previous meeting notes especially when absent!
- Audio notifications for etherpad chat available on firefox via https://addons.mozilla.org/en-US/firefox/addon/notification-sound/
 
### Use chat in etherpad (and add your name)

### Conversation queuing
- three discussion mechanisms: hand symbols (below), call for a round (and vary the order), open discussion
- "o/" or "/" means that you have something to say and want to be put in the queue
- "c/" or "?" means that you have a clarifying question and want to jump to the top of the queue
- "d" means thumbs up, encouragement, agreement, etc.
-  ">" as an indicator of understanding someone and the point can be concluded, please move on

 
### Notetaking
- "???" on the etherpad means the notetaker missed something and needs assistance capturing what was said
- aim for shorthand / summary / key points (not transcript)
 
## Review previous meeting feedback



## Last meeting next-steps review

### Coworking
- NEXT STEP (smichel): Check in regardless of interest in coworking that particular day

### Libreplanet recap
- NEXT STEP (smichel): Post a short reply on the libreplanet event post on the forum <https://community.snowdrift.coop/t/libreplanet-online-march-14-15-2020/1466/8>

### CiviCRM
- NEXT STEP (Adroit): Test a submission of the volunteer form <https://contacts.snowdrift.coop/volunteer> Done!
- NEXT STEP (salt): Test import of old data

### Nextcloud
- NEXT STEP (smichel): make forum thread, ping @team, ask who wants an account

### OSUOSL community@ forwarding
- NEXT STEP (smichel): open ops issue

<!-- One minute silence, check with ourselves mentally and personal notes/tasks/emails to surface any tensions, add agenda if appropriate -->
<!-- Confirm agenda order, inform if leaving early so as to not interrupt -->


## Current Agenda

<!-- New Agenda Down Here  (Added 48 hours before the meeting or earlier)-->

### Adding IRC/Matrix stats to weekly metrics, both channels (wolftune)

- salt: love the idea, is it practical?
- may look at the "Custom widget" integration through Riot.im?
- NEXT STEP (Adroit): Investigate if this is possible/practical <https://matrix.org/docs/projects/bot/matrix-stats>

<!-- Late New Agenda Down Here (Added within 48 hours of the meeting)-->

### Clearing notifications in etherpad / clearing the chat / ping without desktop notifications? (wolftune)
- wolftune: When I open the etherpad, I get a flood of notifications, for the backlog
- Salt: I don't get that
- smichel: I get just one notification at the start
- NEXT STEP (wolftune): Test this to try and find the cause

### Responding to threads on Discourse (Salt)
- Salt: There were cases where wolftune was asking others to respond, nobody else got around to it
- wolftune: I don't want the forum to be where people come to post for *me* to reply to them; it should be a place for *community* discussions.
- wolftune: This is a situation where, the more diversity of posts from more people, the more engaging the forum is.
- [further discussion missing notes, got into what types of topics should be on the forum, which need an "official" reply, disclaimers about personal-opinion vs representing the platform, use of different forum tools]
- wolftune: emphasized the work we've done on good CoC and values etc., everyone should revisit if any concerns about controversial topics

### IRC bot feature request process (Salt)
- Salt: Unsure of current status, but there's at least a few features we'd like
  - Command that says how long until next meeting
  - Maybe this is a good way to get some metrics
- NEXT STEP (Salt): Ask fr33domlover

### Status of implementation workflow (mray)
- mray: Feeling a little disconnected, iko and I have been working on what feels like a standalone project that *should* some day be connected to the actual code base
- mray: Feel like we're lacking an actual workflow. Has there been any progress on frontend workflow / getting someone working on that?
- Salt: Relates to project manager, that role can see if our workflow is working, make adjustments if necessary
- Salt: We do have the workflow of design work that is then handed over to implementation by developers
- mray: Design-wise, we have a good workflow for creating specs for all the pages, but currently they don't get implemented.
- wolftune: Coworking is helpful to get up momentum. All the stuff that cleans up noise is also helpful. Main blocker is still lack of coders though.
- mray: We have a history of processes where I feel my input ends up going "into the void", and that has an impact on my motivation. Right now I feel discouraged, but I'd like to feel encouraged!
- mray: I want to be part of finding more people, and doing whatever we need to get the wheels moving again, not sit by quietly and let nothing keep happening.
- wolftune: I *think* we only need someone familiar enough with html and css, who is willing to look at the designs and put together the html and css and to work with, learn from, or hand off to someone who knows how to integrate it with the hamlet and Haskell. I'm not a real programmer and I can hack it together if necessary.
- mray: Based on that assumption, iko set up a static site generator that is completely separate from our infrastructure. html+css is within reach of what I can do, although I'm not super comfortable. So I have some doubt that this is the actual problem.
- wolftune: I have not seen that. It might be as simple as saying, "X is on the static site generator, needs to get up on the site." We might already have people who can make that happen.
- mray: I think the [updated] About page is an example.
- mray: Here's the static site generator: <https://sdproto.gitlab.io/about/#status>
- wolftune: Is this ready to be put on the site?
- mray: yes, it's ready to go in the yesod framework; I think this is where the disconnect happens, not sure how to do that.
- Salt: This is the type of task that is very accomplishable by many people (volunteers, not just core contributors). I think our main problem is they're not tagged and we don't have a way to draw attention there.
- wolftune: I think it would be better to have different tags/issues for different steps (writing html/css, tying in to backend). I know there are volunteers who don't want to do html/css or haskell respectively.
- Adroit: Maybe we could deploy directly on to testing infrastructure instead of a static site generator?
- mray: The snowdrift site is not really flexible enough to be quickly iterated on from a design perspective. Yesod framework doesn't have good dev tooling for css iteration. Having to commit changes and make a MR, wait for build, is not good for iterating on changes collaboratively with msiep.
- wolftune: I want to assure you that I don't think the current duct-tape workflow is good, but it's a way forward for now.
- mray: That's why I'm bringing this up — we've had the duct tape workflow for several years now.
- wolftune: but if it were all happening, we wouldn't be having this tension. Part of it is that the implementation isn't going all the way through, even though that's not all of the tensions.
- Salt: I don't think using a static site generator to test things is duct taping; I think it's a totally reasonable way to do things — having a separate implementation in a separate space for design workflow.
- mray: Concern of iko's has been getting the static site generator so that things can be migrated to main site without too much difficulty. She's burnt out on this, it's a real problem.
- NEXT STEP (wolftune): add this to next week's agenda for further discussion (earlier in meeting)
- NEXT STEP (smichel): Inquire with bryan re: incrementally transitioning to a different frontend framework (than yesod)

<!-- Open discussion? ~5min. if time -->




---
 
## meeting evaluation / feedback / suggestions / appreciations round

- Adroit: effective meeting, relative to others
- mray: good meeting, sorry it took so long :)
- msiep: really glad, but going over was problematic tension, we need reliable process to acknowledge end of hour. Didn't feel like I really had the time but didn't want to drop.
- Salt: really happy about the whole meeting, don't think we spent too much time on mray's topic, agree about time mindfulness, thanks wolftune for facilitation, cue yourself is good etc, could spend more REAL time carefully reviewing order
- smichel: we should have prioritized better, put the important topic higher maybe
- wolftune: It was hard for me to take notes, facilitate, and participate. Minute of silence does seem really useful. Whatever tensions you're not sure about bringing up, seem to usually be worth bringing up more often than not.

<!-- Goodbye round -->
<!-- Capture NEXT STEPS -->
<!-- Clean up meeting notes, then add to wiki -->
<!-- Prepare this pad for next meeting: (A) replace previous meeting eval notes with new (B) Clear discussion notes, moving NEXT STEPs to "review last meeting's action steps" (C) Update next meetings date, clear attendee list  (D) Update old metrics, update date, leave new blank -->