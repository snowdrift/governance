## Meeting 2

**Saturday, March 9, 2013. 5PM Eastern**  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and via IRC text at [http://webchat.freenode.net/?channels=#snowdrift](http://webchat.freenode.net/?channels=#snowdrift)  
also etherpad: http://etherpad.wmflabs.org/pad/p/Snowdrift20130309*

### Agenda

* review of minutes from [meeting1](meeting1)
* report from Aaron about the status of things and progress over the month
* confirm proposal that the committee's decision-making by done by consensus
    * issue: what sorts of decisions should require committee approval?
    * how do we deal with absent members at meetings? what constitutes a quorum at this stage?
    * what sort of engagement is expected/required of the committee (just in terms of being knowledgeable about what is going on with the site and understanding how it actually works and all the ideas and plans)?
        * *Aaron: although we have greatly improved the clarity and simplicity of the presentation, there may be some confusion about how things work. I think it is reasonable to ask everyone to be sure they understand the basic system. We cannot have people making decisions about it, saying it is too complex or anything, if they don't fully understand what the idea is. So I hope everyone will read as much of the details as necessary for understanding, ask questions and identify when they are confused about anything, and be prepared to make informed decisions as committee members.*
* discuss process of accepting new members to committee
    * also issue of recruiting
* Explanation about Brandon: update on his status as legal council, stepping down from committee
* discuss initial proposal for filing for legal status in Michigan
    * e-mail sent in advance includes details
    * need to finalize statement of purpose
    * clarify details about Articles (Aaron will read through and confirm with everyone)
    * potentially consent to go ahead with the details as we agree outside of meeting, i.e. if details aren't finalized by this meeting, we can agree to allow the filing to go ahead pending no objections following a presentation to the committee by e-mail, thus finishing the filing before the April meeting
* discuss next steps, plans, any proposals / business, open discussion
    * esp. priority: plans to develop bylaws and other details
* adjournment

---

## Minutes

call participants:

* David Thomas
* Aaron Wolf
* Mark Holmquist
* Mike Linksvayer
* Greg Austic

**Agenda and notes**

Aaron: Review/discussion of meeting 1

* Talked about email list, but not yet set up.
* Independent organization is a good idea - unique focus (vs
* 501 (c) (3) is aim
* HTTPS + login system also still in works
    * *post-meeting follow-up: Decided to transfer from Encirca to Gandi.net for domain registrar, Gandi includes SSL on transfer and inexpensive after that, many additional services, supports FLOSS organizations; we'll do the transfer in conjunction with plans to update the server to have a single down-time; sometime soon, then we'll have SSL and be able to implement site-wide HTTPS*

Review of past month, many decisions, discussions

* plan is: make ourselves a project. Thus offer the tools we use for ourselves as resources for all projects.
*Mark is concerned that this is outside of the project scope; Mission creep
* David suggests the idea is to keep people engaged, emphasize more about the projects than just money
* Mark asks where the line is...
* Ticketing system in the works (use tagging) see [tickets](tickets) but for the foreseeable future avoid such things as code hosting or e-mail hosting
* David on status: Not a big change to add wikis for other projects
* Aaron: see [terminology](terminology) for overview of the scope of things via keywords

Discussion about role of committee

* Currently, Aaron writes proposals, David writes backend, etc.
* Want to make sure we get consensus from committee about major decisions
* What is "major"?
* Mike says it's an individual project; developers can do what they want and committee can suggest things
* Formality of having to submit decisions for approval may hold back the project
* Committee isn't a board
* Aaron: but still may be inappropriate to file for legal status without committee's approval
* Mark and Greg both figure at least conversations about broad ideas should happen

Discuss process of accepting new members to committee

* Aaron: What's the best way to recruit? Definitely want to recruit more people.
* Greg: Maybe list of orgs that are similar or have similar interests, go from there
* Aaron: Presentations maybe a good way to reach out
* Mike: Bad idea to send out an invitation without talking about it first (until launch, when people can find out about it)
* Aaron: Want people who are philosophically aligned
* Want more engaged criticism and brainstorming and active contributors:  Programmers or developers, or people who want to work on proposals and the like.

Time frame:

* Aaron OpenHelp - Cincinnatti - June (accepting submissions for presentation, maybe submit and then this is deadline for being more public?) *no decision reached on this*

* Greg: Have more visual representations of project's mission and background
* Aaron: see any project listing at "see projects" for the best current simple visual sense of pledging

Explanation about Brandon: update on his status as legal council, stepping down from committee

* Aaron: describes Brandon's [Creative Rights](http://creative-rights.org/) organization
    *Helps with free culture
    * Provides legal services to artists
* Have agreed to take snowdrift as a client - pro-bono!

Discuss initial proposal for filing for legal status in Michigan

* Aaron would be primary contact
* Bylaws happen later
* This week, Aaron will submit articles of incorporation
* Open credit union account, etc.

Articles of incorporation details

* Need to finalize purpose statement for articles
    * **plan: within next week, finalize by e-mail a purpose statement for articles, then confer with Brandon and submit incorporation filing in Michigan**
* David: Use Code for America as a reference?

* Mike: You can look at some existing FLOSS nonprofit 1023s at https://gitorious.org/floss-foundations/npo-public-filings/trees/master/Form1023s

* Mike on etherpad: I have a topic not on agenda that i should've brought up in context of wiki etc as a web development topic. That is status of payment processing capability, which is absolutely on shortest path to launch. :)
    * this was discussed then, with the idea that this is priority following transition to project-within-project; first step is functioning with fake money
    * David: External payment handling we've not dealt with yet.  We've got some code for moving "money" inside the system.  From what I hear integrating with balancedpayments or stripe or the like is not hard.
