# Meeting 5

**Friday 13 July, 1PM EDT, 11AM MDT, 10AM PDT**  
*phone chat on freeconferencecall.com (605) 475-4000 pin 564511#  
and [etherpad](http://etherpad.wmflabs.org/pad/p/Snowdrift20130309)*

## Background readings / updates / tasks

* Please review [the Mission page discussion](mission/discuss) and the [new committee responsibilities](joincommittee) (if you have not already) ahead of the meeting.

* [Starting draft of Terms of Use](https://www.dropbox.com/s/73v72cfaab9il1x/Snowdrift%20Terms%20of%20Use%20starting%20draft.odt), [Starting draft of Privacy Policy](https://www.dropbox.com/s/nsfii6ax49x68l3/Snowdrift%20Privacy%20Policy%20starting%20draft.odt), [Starting draft of Trademark Policy](https://www.dropbox.com/s/x4denjjxwgygurh/Snowdrift%20Trademark%20Policy%20starting%20draft.odt)
Provided for information purposes for now. I will summarize their contents at the meeting and ask for feedback, but we will not be voting on them yet.

## Agenda

* Kim's introduction as Facilitator/Recorder (5 minutes)
    * Check-in: tell us briefly what you hope this meeting will accomplish

* Review of minutes from [Meeting 4](meeting4) (10 minutes)

* Brief technical report from David (5 minutes)

* Committee participation/restructuring (10 minutes)
    * New responsibilities definition on w/joincommittee
    * Determination of new roles
    * Endorsing role?

* Recruitment (10 minutes)
    * How and how much
    * Website readiness

* Progress report by Kim on documents required to open site (20 minutes) 
    * Articles of Incorporation
    * Terms of Use
    * Privacy Policy
    * Bylaws
    * Q&A
    * How to proceed with editing

* Legal counsel options review and pro/con (20 minutes)
    * Report by Aaron on results of lawyer search, meeting with Deb Olson
    * Pro bono vs. expert in the field
    * Committee recommendations

* Open forum on broadening mission to include economic goals (10 minutes)

* Review of next steps / evaluations (5 minutes)

## Next steps assigned

* Greg: Look for developer contacts, work with Aaron and Kim on recruitment ideas    
* David: Project-in-Project and Snowbot
* Aaron: Get a first draft of a revised mission statement, take good notes at conference to share with Kim and Greg    
* James: Kim will work with him on bylaws
* Mike: Work with David on resources to recruit other developers, talk with Chris Sakkas https://snowdrift.coop/application/13     
* Kim: Email to FLOSS Foundation, call New Center, work with James on bylaws, work with Greg and Aaron

## Summary / minutes

* 1112AM start, everyone present except Mike
* Round-robin: who wants what out of meeting?
    * get everyone on same page
    * get next steps
    * bring everyone up to speed
    * figure out how to push things forward

* Minutes from Meeting 4
    * Everyone's seen them
    * Main points: new site homepage, developing site, legal status
    * No proposed changes

* Agenda review: approved tentatively

* Technical review: David
    * Projects-in-projects transition: slower than expected, may see within next couple of days IF no unexpected snags; migration to Yesod 1.2
    * IRC Bot is down right now, channel automatically mutes it when it tries to send many messages, David wants to send messages over the Internet
    * Ongoing work on tickets like permissions.
    * Finishing these both next steps

* Committee participation/restructuring: determination of new roles, discussion of potential recruitment methods.
    * Make sure everyone knows where to get information and information is available
    * Suggestion to make committee roles more specific, subject area oriented, progress can be made
    * Clear job description on the right track, minimum participation
    * Brainstorming on potential roles:
        * Legal issues expert - James, will get in contact via email after meeting
        * Recruiting and public relations 
        * Coordinating tickets/project manager
        * FLO Culture
        * Partnerships - Greg, work with Aaron on contacts, links to other organizations, defined contact points
        * Crowdfunding
        * Financial and corporate compliance
        * Diversity/International issues/Gender
        * Education
        * Licensing issues - Mike
    * Recruitment: 
        * priority is recruiting developers, partnerships can potentially cover other needs, Haskell is unsual need
        * need Javascript and web developers, not just Haskell
        * tickets on GitHub are quick things, should be clear enough to move forward with
        * screencast of development?
        * Aaron can recruit or get ideas at conference
        * Send list of web development contacts to Greg
        * Improve homepage and more illustrations on the site - Kim will take point
        * (James had to leave at this point.)

* Update on documents required to open site:
    * Used copyleft templates and tried for brevity and openness wherever possible.
    * Terms of Use closest to being ready—based on Wikimedia Terms.
        * Projects are independent entities 
        * Users are responsible for consequences of their own actions.
        * Snowdrift is not a bank, user accepts credit risks and ToS of chosen payment processor, transaction fees are subject to chargeback.
        * List of general violations, like harassment, fraud, illegal activities.
        * Any text submitted is CC-BY-SA, other media must be under approved, compatible license.
        * We comply with DCMA but encourage fair use to defend itself.
        * We reserve the right to terminate user and project accounts.
        * Jurisdiction is Michigan
        * Disclaimers on liability, risk, infringement, merchantability, etc.
        * Changes without  notice until stable site, then comment period.
        * Separate written agreement controls.
        * Severability.
    * Articles of Incorporation
         * Undergoing editing to reflect fact that we are no longer attempting 501(c)(3) status.
        * Including Michigan cooperative requirements such as specifying law we are incorporating under, membership based on fee instead of capital, method for altering the bylaws (how do we want to change the bylaws?).
        * Privacy Policy—ready for review, still need some questions answered; based on Mozilla Marketplace.
        * Private information is on need-to-know basis, periodically destroyed, we destroy as much as we can when someone leaves
        * Temporary cookies only.
        * In-house processing only.
        * Do we want to participate in Open Web data release?
        * Does data move to a successor?
        * Move to the wiki for more specific questions
    * Trademark Policy:
        * Philosophically based on Debian trademark policy, has at this point been rewritten so it can be licensed under CC-BY-SA only.
        * Allows honest, non-confusing use, including commercial merchandise.
        * Misleading use/use in company or domain names forbidden.
    * Bylaws: still in very rough draft. Want to make sure we address issues with majority rule.

* Legal counsel options review and pro/con.
    * Aaron's report on options:
        * Deb Olson: knowledgable on nonprofits and coops but would charge hourly
        * Perlman and Perlman: would charge flat rate but only do Articles and nonprofit status
    * Other options to investigate:
        * http://www.new.org/index.php
        * FLOSS Foundations email list?
        * Kim will get report together on grant possibilities
    * Will make an email decision between commitee meetings on legal counsel, with consensus

* Open forum on broadening mission to include economic goals
    * link: https://snowdrift.coop/w/mission/comment/394
    * Possible wider appeal to Free Culture and co-op community
    * Possible objection to idea that creators being paid is equal to economic justice, should emphasis that this is a larger economic framework
    * Everyone deserves to be able to make a living doing something meaningful
    * Concerns: Big tent - don't want to alienate people by over-emphasis on economic goals, don't want to focus on economic justice to detriment of goal of enabling people to connect
    * Aaron will create first draft for next step

* NEXT STEPS:
    * Greg: Look for developer contacts, work with Aaron and Kim on recruitment ideas    
    * David: Project-in-Project and Snowbot
    * Aaron: Get a first draft of a revised mission statement, take good notes at conference to share with Kim and Greg    
    * James: Kim will work with him on bylaws
    * Mike: Work with David on resources to recruit other developers, talk with Chris Sakkas https://snowdrift.coop/application/13     
    * Kim: Email to FLOSS Foundation, call New Center, work with James on bylaws, work with Greg and Aaron

* Evaluations
