Legend:

- **Bold ⇒ This is a complex issue; we have hard-to-summarize thoughts. Let's discuss.**

- *Italics ⇒ We defer to your judgment, especially on details/specifics. Probably no need to discuss.*


Snowdrift.coop Bylaws rough outline
===================================


Mission and Values
------------------

Not yet summarized; to discuss later.


Co-op Membership
----------------

- Not required to use the platform.
- Only benefit is participation in governance (the standard stuff)
- **Open to (not required of) active patrons of the Snowdrift project**
    - Humans only (not institution, corporation, AI…)
        - Note: Currently applies to all patrons (included in website ToS)
    - Residing anywhere in the world
        - *within legal restrictions*
    - **Concern: excluding poorer people.**
        - *Give the Board power to address this?*
- Members may voluntarily leave any time.
    - No refunds.
- Q: Where are membership agreement terms defined?
    - Q: Who gets to change them?
    - Involuntary termination of membership: when/how?
- **Concern: people (members) undermining the co-op**
    - **Solution: ability to kick out bad actors**
        - **Concern: don't want to create a tool for discrimination.**
        - Believing things that others don't like is not bad acting.
        - Shouldn't be too easy to move the line for what counts as bad acting.


### Meetings

- **Remote**
    - Wording to consider from Fairshares:  
       *Q: The second sentence, too?*  
       > General Meetings can take place through an online collaborative decision-making platform using technology agreed by members
       >
       > General Meetings may be held in public, broadcast live, recorded, and the records of such meetings can be made available to the public.
- **Asynchronous**
    - For different time zones, availability, and accessibility needs
    - Allow text-based forums or chat for engagement, voting, etc.
- **Q: How to prevent inadequate notice?** (common anti-pattern in co-ops)
    - Post prominently on website and forum
    - Send directly to members via their preferred communications method(s)
    - *21-90 days notice*
        - *Regular & special meetings, called by Board or Membership*
        - Whether or not the date is specified in bylaws
- **Q: What quorums for attendance & voting?**

### Voting Methods

- **We want to lock in some criteria, rather than one specific system.**
- Probably will use STAR voting initially


Board of Representatives
------------------------

- 7 Representatives
    - *Age 16+ (as law allows)*
    - Quorum for meetings is 5 Representatives
    - Any two reps can call a special meeting
- Remote meetings
- **Mostly asynchronous**
    - Via agreed-upon discussion/voting tool
    - Minimum one synchronous Board meeting per year
- **Include in Bylaws or standing rules?**
    - Facilitated consensus-based decision-making with a fallback procedure
    - *After* synchronous Board meeting, chance for async comments & votes from absent reps
    - Q: Public comment period for certain decisions? How long?
- *Standard responsibilities for General Manager, membership meetings, committees, policies, etc*
- *No financial compensation*
    - *Okay with reimbursement of expenses*
- Q: How does the Board deal with emergencies?
    - Ex: A decision needs to made sooner than the normal notice/async comment period.  
       Should bylaws specify a process, or just break the bylaws in this case?

### Elections

- Candidates elected each year in staggered cycles:
    - 3 year terms
    - Max two consecutive terms
    - Co-op members only
    - *No limitations on people filling either or both representative or officer roles*
    - Any co-op member can nominate a candidate (self-included)
    - Candidates must accept their nomination and submit some statement of why they are running
    - Candidates must disclose COIs, with vetting by Board for privacy reasons
- **When a representative vacates:**
    - Membership elects a replacement at the next regular election.
        - In addition to any other seats being voted on
        - For the remaining term only.
            - *Counts towards term limits if >=50% of the original term.*
    - Board may appoint interim member until then
    - Members (or Board?) may propose a special election to fill the seat earlier
        - Lower bar than normal ballot measures
- Q: How to handle recalls?
    - Q: Reps may be recalled by ballot measure, with or without cause?
    - *Q: Process and threshold for recall vote?*


Surplus Allocations
-------------------

- No refunds; all funds shall serve the mission
    - *Basically* only *450.3135(3)(b) — the "common benefit of the patrons" is the mission*
    - Specific inclusion: contributions to upstream projects


Amendment and Reorganization
----------------------------

- Should be easy enough to make Bylaws etc changes that have no opposition
- *Amendment votes should have both supermajority and higher quorum*
- **Indication of a strongly opposed minority (how defined?) counts as a veto**
    - Required reconciliation/discussion process
    - Override vote might not need higher threshold

Dissolution
-----------

Not yet discussed.

If you have a recommended approach or questions we ought to answer, we'd appreciate discussion prompts.

Liability
---------

- Q: What liability protection do we need?
