# Snowdrift.coop Bylaws questions and notes

These are things that don't belong in the short bullet-point summary we will send to Deb (our lawyer), but that we also don't want to forget about.

## Mission and Values

- *TODO:* More on **accessibility** in our values
    - ? principle on minimizing exclusionary impact of specific technology choices?

- *OPEN QUESTION:* values/mission enough lock-in of:
    - public-goods requirement
    - digital public goods must be FLO?

## Co-op Membership

- *OPEN QUESTIONS:*
    - How to safeguard governance against brigading / hostile takeover?
    - Maybe a waiting period for new members?

- *OPEN QUESTIONS:* Specify a formal member agreement? Who would have power to change the agreement?

- *OPEN QUESTION: How do we safeguard members against attempts by the Board (such as standing-rules changes) designed to exclude them?*

- Benefits of participation in governance:
    - *Elect and recall Representatives*
    - *Propose amendments to Bylaws and Articles*
    - *Petition for special meetings or elections*
    - *Serve on committees*
    - *Obtain information about co-op*
        - *except where subject to privacy or confidentiality concerns*

- *termination for violation of policies etc.*
    - OPEN QUESTION: *what's the process for this?*

## Meetings and votes

- Q: Requirement that board meetings are announced to general membership?
- *Annual meeting*
- *Q: How do we facilitate asynchronous meeting participation?*

### Voting systems

*Discussion of voting system considerations: <https://community.snowdrift.coop/t/election-voting-systems-star-proportional-etc-bylaws/1554>*

- **Use a voting method that allows (but does not require) voters to express preferences across *all* candidates**

- *OPEN QUESTIONS*
    - *?* method must consider all marked preferences in determining the outcome
    - *?* proportional representation for elections with multiple open seats with the same designation
    - what requirements for voting tools?
        - will they work as we require (esp PR complexity)?
        - practical fallback?
        - what criteria must tools meet (auditability, FLO, etc)?

- For diversity objectives, Board may label designated seats
    - labels do not limit candidate eligibility
    - candidates specify if they wish to run for such seats
    - include the candidates for such seats in the pool for at-large seats

- *Idea*: Voting method for yes/no measures
    - a sliding scale (weakly/strongly/very-strongly approve/disapprove)
    - require both a mean and a median level of support
- *Q: specify open voting time-frame in Bylaws?*

## Board of Representatives

- *Q: recall process?*

### Board Decision Making

- *OPEN QUESTION* Specify in standing rules the details of facilitated consensus-based decision-making with fallback

- *OPEN QUESTION: How to best allow later asynchronous participation/votes after a synchronous Board meeting? We would like to do this, in order to ensure that absent reps won't have views excluded by a quorum.*

- *OPEN QUESTION: Public comment period (how long?) for certain decisions?*

---

## Standing rules stuff for later (not for Bylaws)

- Fair treatment and compensation of workers - rules and procedures for management
    - Bylaws connection is the co-op principles probably

### Fallback voting etc

- Decisions by facilitated consensus
    - Prefer that Board appoints/hires outside neutral facilitator, not a rep?
- Failure of absent director to register opinion within 21 days authorizes remaining members to finalize decisio
- Facilitator may call for fallback vote
- Fallback required if absent Director registers block within 3 days
- Representative may block vote given concerns about violation of co-op principles, bylaws, or laws.
- Blocking rep must form reconciliation committee and attend at least one meeting (details about committee?)
- If reconciliation committee reaches consensus, proposal returns to board. Amendments may be proposed but can be rejected by any member of the committee. If proposal called fallback vote, it cannot be blocked.
- If reconciliation committee cannot reach consensus proposal is provisionally blocked. For block to become final, rep must post public written reason for their blocking within 3 days of final committee meeting. If not, original proposal returns to board and may not be blocked again.
- Fallback vote on approval scale (6-point) like ballot measures, high mean / median required (e.g. 4)

----

## Some key governance questions

- how are decisions made and communicated?
- how is conflict resolved?
- how are conflicts of interest handled?
- who are our leaders, and how are they selected, evaluated, and held accountable?
- how is community membership decided?
- how are tasks tracked and delegated?
- what is our mission, vision and how are we planning to achieve that?
- what are our values and are we living up to them?
- what is our reputation within our own community and outside of it?
- do ensure we have the resources we need to carry out our mission both in the short-term and the long-term?
- how are we developing our community members and leaders?
- are we serving our mission or just ourselves?
- are we in compliance with with local laws? are we up to date on our taxes?
- are we doing what our community needs us to do?
- are we hearing all the feedback we need to be? from whom we need to hear it?

